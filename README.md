# IT5

## Description
"The Relict" (IT5) is a stealth/action game written in Python using pyglet.
It is the 5th entry of the Intruder's Thunder series, and a (very) loose prequel to "The Endling's Artifice" (IT4).

## Starting the Game
Currently, there is no bundled executable to be run. I plan on changing this in the future.
This game requires Python 3.7 to run. After installing Python, run ```pip install -r requirements.txt```.
Once the dependencies have been downloaded, if you're using Windows, run the ```run.bat``` script to start.
If you aren't on Windows, just execute the command ```python -O -m it5``` in a terminal from the project's directory to begin playing.

## Controls
```
[W] or [UP] Move up, or if paused, cycle forwards in item inventory
[A] or [LEFT] Move left, or if paused, cycle backwards in weapon inventory
[S] or [DOWN] Move down, or if paused, cycle backwards in item inventory
[D] or [RIGHT] Move right, or if paused, cycle forwards in weapon inventory
[Enter] Toggle pause
[Tab] Bring up Mission Viewer
[Space] Fire weapon, or if paused, consume medkit if equipped
[Ctrl] Melee, or if paused, consume medkit if equipped
[Shift] Toggle Prone
[E] Talk to a person
[F5] Save the game
[Esc] Bring up exit menu
```

## Story
It will take place in the 1945 and the 1973 in two separate locations.
The tutorial mission will take place in the 1945, at the end of WW2.
The main game will take place in 1973.

## Map Editing
Maps are created in tiled and exported as .json files.
Maps are chained together in levels, also stored as .json files.
Level files look something like this:
```
{
    "name": "Test Level",
    "start": {
        "mapIndex": 0,
        "x": 1,
        "y": 1
    },
    "maps": [
        "vr1.json"
    ],
    "dialogs": "test",
    "quests": {
        "main": "Main Mission"
    }
}
```
The "start" block has some noteworthy stuff in it.
The "mapIndex" specifies the index of the map to load under the "maps" list, which is specified by "maps". The "maps" key can also point to a dictionary, where all map indices are given names (e.g. `maps: {"0": "foo.json", "my awesome map": "bar.json"}`). When "maps" refers to a dictionary, the convention for the first map in the level is still to give it an index name of "0".
The "x" and "y" fields refer to a tile coordinate in which the player will start at on the specified map.

Levelsets are simple, and only consist of a list of levels.
```
{
    "levels": [
        "prologue.json",
        "chapter1.json",
        "chapter2.json"
    ]
}
```

### NPCs
Adding NPCs is a 2-step process. Step one is to add a "waypoints" object group to the map.
Each waypoint object should have the custom properties "direction" and "behavior".
A waypoint's direction can be "up", "down", "left", or "right".
Valid waypoint behaviors can be found in the WaypointBehavior enum class.
Each waypoint needs a name; this is needed so the NPC can reference it.
Waypoints that aren't part of an NPC's path will be discarded.

Step two is to add an "npcs" object group to the map.
NPCs are given an x and y coordinate, which may or may not be used, depending on if the NPC references valid waypoints.
NPCs also have a type. Certain NPCs are deemed "friendly" and will not attack the player. Others, such as guards, are not friendly
and will attack the player upon detection.
The most important part of an NPC is its "waypoints" csv string. This is part of the object's custom properties.
The waypoints string must be a CSV of the names of the waypoints that will make up its path.
The final waypoint must link the NPC back to the starting waypoint in a straight line using the proper direction.

### Items
Items must be added to the "items" object group.

#### Weapons
Weapons are items given a type of "weapon".
Custom properties include "type" and "rank". For example "type" = "pistol" and "rank" = 0.
The type and rank will tell the application which weapon this item corresponds to.

##### Supported Weapons
types: pistol, smg, carbine, grenade, mine

#### Other Items
Other items include medkits, keys, ammo, etc.
Ammo has custom properties "count" and "type", where "count" = number of bullets and "type" = compatible weapon.
For example, "count":"8", "type":"pistol".

Keys/Cardkeys have a "level" and a type of "key" or "cardkey". Levels start at 1; level 0 doors require no key.

##### Supported Item Types
medkit
key
cardkey
ammo
alt_mine
nvg
flashlight
gasmask
bronze
silver
gold
bronze_egg
silver_egg
gold_egg

### Doors
Doors must be added to the "doors" object group in the map.
Doors are placed in the bottom left-most tile that they will appear.
Doors have custom properties "direction", "objective", "security", and "width".
Currently supported directions are "up" and "down".
The "objective" property is an objective level lock on the door. Use this for special cases. This takes in a json string as its value.
The "security" property ensures the door cannot open unless the player has a key >= the specified level equipped.
The "width" property can be "1" or "2". Skinny doors have a width of 1, wide doors have a width of 2.
The "locked" property can be set to True to permanently lock a door without having to give it a fake quest/objective.

### Warps
Warps must be added to the "warps" object group in the map.
A Warp links two maps together, and also links levels together.
Required properties:
"map": The index/name of map in the level for the player to warp to
"x": The x coordinate of the tile for the player to warp to
"y": The y coordinate of the tile for the player to warp to

Optional properties:
"level": The index of the level in the levelset for the player to warp to.
This should be specified only when you wish to change levels.

### Dialog
Dialogs are formatted just like IT4 dialogs, where a [INSERT_NAME_HERE] tag will indicate that another character is speaking.
Dialogs will be referenced by filename in the maps.
A [script] tag in a dialog may trigger a script to start in the future, but this isn't currently implemented.
Dialogs can be triggered when a map loads, or when the player walks over a DialogTrigger.
A speaker named `[Player]` will substitute "Player" for the name the player entered before starting the game.
Pieces of dialog that read `{Player}` will be replaced with the name the player entered before starting the game.
Remember, `[Player]` for speaker, `{Player}` for text.

### Security Cameras
Two types of cameras are supported: camera and gun.
Gun cameras shoot at the player but don't trigger an alert. Regular cameras do the opposite.

### Quests (Missions)
All quests available in a level must be defined in the level json as a "quests" dictionary.
For example:
```
"quests": {
    "main": "Main Mission",
    "side": "Some Side Mission"
}
```
This will create two quests, "main" and "side", whose titles (what gets shown to the player) are
"Main Mission" and "Some Side Mission", respectively.
By default, the main quest will be available to the player at the start of the level.
Side quests must be triggered by QuestTriggers. You must have a "triggers" object layer in your map to add QuestTriggers.
A QuestTrigger must have a type of "quest", and a custom property "quest" which references the name (not the title) of
the quest to start.

#### Objectives (Quest Stages)
Adding mission objectives is simple. You must have a "triggers" object layer in your map.
Your objective must have a type of "objective".
Custom properties:
```
name: name of the objective
quest: name of the quest this objective belongs to
order: integer order, increment by 10 initially, to make room for objectives in-between
title: title of this objective that is visible to the player
dialog: if present, spawns a dialog after the objective has been completed
```

### Scripts
You must have a "triggers" object layer in your map to add script triggers.
Your script trigger must have a type of "script", and a custom property "script" which
references the class of the Script that will be launched.
Script implementations must inherit from OneTimeScript or PersistentScript in order to be recognized by the game.
Each Script has an on_load(self) method that is called once when the Script starts, and an update(self, dt) method that is called once per frame.
A OneTimeScript can contain logic in update(self, dt), but that method will only get called once.
