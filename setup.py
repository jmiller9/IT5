from setuptools import setup
import re

VERSIONFILE="it5/_version.py"
verstrline = open(VERSIONFILE, "rt").read()
VSRE = r"^__version__ = ['\"]([^'\"]*)['\"]"
mo = re.search(VSRE, verstrline, re.M)
if mo:
    _version = mo.group(1)
else:
    raise RuntimeError("Unable to find version string in %s." % (VERSIONFILE,))

setup(name='IT5',
      version=_version,
      description='IT5',
      url='https://gitlab.com/jmiller9/IT5',
      author='jmiller9',
      author_email='',
      license='MIT',
      packages=['it5'],
      install_requires=[
          'pyglet<2',
          'pyglet-ffmpeg<1'
      ],
      zip_safe=False)