import pyglet
from it5._version import __version__
from it5.common import CommonManager
from it5.gui.manager import UIManager
from it5.gui.widget import Widget
from it5.controls import Controls, ControlAction
from it5.persistence import PersistenceManager
from it5.audio import SoundManager


about_str = """The Relict © 2018-2022 by jmiller9

Programmed by:
jmiller9

Story by:
jmiller9

Art by:
Pipoya
Viktor Hahn
Brett Steele
OwlishMedia
Kauzz
Buch (http://blog-buch.rhcloud.com)
Bonsaiheldin | http://bonsaiheld.org
Daniel Eddeland: https://opengameart.org/node/11117
jh2assets: https://jimhatama.itch.io/
jmiller9

Soundtrack by:
Matthew Pablo
yd
brandon75689
Zander Noriega
İlker Yalçıner (ilkeryalciner.com)
Alexandr Zhelanov https://soundcloud.com/alexandr-zhelanov
"Enemy Infiltration" by Eric Matyas soundimage.org
Dark Ambience Loop by Iwan Gabovitch qubodup.net
Gobusto https://sites.google.com/site/gobusto/music
Seamless Rain Loop http://freesound.org/people/qubodup/sounds/238911 by Iwan 'qubodup' Gabovitch http://freesound.org/people/qubodup
Roald Strauss

Additional SFX by:
Mike Koenig http://soundbible.com/1008-Decapitation.html
thecheeseman http://soundbible.com/1458-Monster-Roar.html"""

class MainMenuAction:
    RESUME_GAME = "Return to Game"
    NEW_GAME = "New Game"
    CONTINUE = "Continue From Save"
    CONTROLS = "Controls"
    ABOUT = "About"
    TOGGLE_FULLSCREEN = "Toggle Fullscreen"
    QUIT = "Quit"

class ControlTopMenuAction:
    VIEW = "View"
    SET = "Set"
    QUIT = "Quit"

class MainMenu(Widget):
    def __init__(self, batch, batch_groups, window, on_new_game, on_continue, on_toggle_fullscreen, in_game=False, last_soundtrack=None):
        self.on_new_game = on_new_game
        self.on_continue = on_continue
        self.on_toggle_fullscreen = on_toggle_fullscreen
        background_color = (64,0,0,255)
        self.dialog_color = (63,63,63,255)
        menu_background_pattern = pyglet.image.SolidColorImagePattern(background_color)
        menu_background_image = menu_background_pattern.create_image(window.width,window.height)
        self.window = window
        title = "The Relict"
        self.in_game = in_game
        self.last_soundtrack = last_soundtrack
        options = []
        if self.in_game:
            options.append((MainMenuAction.RESUME_GAME, self._resume_game))
        options += [
            (MainMenuAction.CONTINUE, self._continue),
            (MainMenuAction.NEW_GAME, self._new_game),
            (MainMenuAction.TOGGLE_FULLSCREEN, self._toggle_fullscreen),
            (MainMenuAction.CONTROLS, self._controls),
            (MainMenuAction.ABOUT, self._about),
            (MainMenuAction.QUIT, self._quit),
        ]
        self.menu_background_sprite = pyglet.sprite.Sprite(
            img=menu_background_image,
            batch=batch,
            group=batch_groups['menuBackground']
        )
        self.version_label = pyglet.text.Label(__version__,
            font_size=12,
            x=0, y=0,
            anchor_x='left', anchor_y='bottom', batch=batch,
            group=batch_groups['menuForeground']
        )
        width = int(self.window.width / 4)
        height = int(self.window.height / 2)
        x = self.window.width / 2
        y = self.window.height
        self.controls_state_id = None
        UIManager.attach(self)
        UIManager.show_menu(title, options, width=width, height=height, x=x, y=y,
                            option_font_size=18, title_font_size=48, title_background_color=background_color, exit_option=None,
                            anchor_x="center", anchor_y="top")

    def _entered_name_callback(self, name):
        if name is not None:
            if len(name) > 0:
                print("Name is " + name)
                CommonManager.set_player_name(name)
                UIManager.hide()
                self.on_new_game()
            else:
                width = int(self.window.width / 2)
                height = int(self.window.height / 4)
                x = self.window.width / 2
                y = self.window.height / 2
                UIManager.show_popup("Player Name Required", "You must enter a name.", width=width, height=height, x=x, y=y,
                                     background_color=self.dialog_color, anchor_x="center", anchor_y="center")

    def _new_game(self, *args):
        width = int(self.window.width / 2)
        height = int(self.window.height / 4)
        x = self.window.width / 2
        y = self.window.height / 2
        UIManager.show_text_input("Enter Your Name", self._entered_name_callback, self.window,
                                  width=width, height=height, x=x, y=y, background_color=self.dialog_color,
                                  anchor_x="center", anchor_y="center")

    def _continue(self, *args):
        UIManager.hide()
        self.on_continue()

    def _resume_game(self, *args):
        if self.last_soundtrack is not None:
            SoundManager.play_music(self.last_soundtrack)
        UIManager.hide()

    def _toggle_fullscreen(self, *args):
        UIManager.hide()
        self.on_toggle_fullscreen(self.in_game, self.last_soundtrack)

    def _controls_view(self, *args):
        width = int(self.window.width / 2)
        height = int(self.window.height / 2)
        x = self.window.width / 2
        y = self.window.height / 2
        UIManager.show_popup("Controls", Controls.get_controls_str(), width=width, height=height, x=x, y=y,
                             background_color=self.dialog_color, anchor_x="center", anchor_y="center")

    def _controls_open_key_action_binding_menu(self, key_name):
        width = int(self.window.width / 2)
        height = int(self.window.height / 4)
        x = self.window.width / 2
        y = self.window.height
        UIManager.show_key_mapper(key_name, width=width, height=height, x=x, y=y,
                             background_color=self.dialog_color, anchor_x="left", anchor_y="top")

    def _controls_reset_default(self, *args):
        # Not passing in a key_action_map will set all keys to default bindings
        Controls.initialize()
        width = int(self.window.width / 2)
        height = int(self.window.height / 4)
        x = self.window.width / 2
        y = self.window.height
        UIManager.show_popup("Info", "Controls have been reset to default settings.", width=width, height=height, x=x, y=y,
                             background_color=self.dialog_color, anchor_x="left", anchor_y="top")

    def _controls_set_menu_close(self, *args):
        UIManager.hide_active()
        if Controls.state_id != self.controls_state_id:
            # Controls have been changed
            print("Saving updated controls settings")
            PersistenceManager.save_controls(Controls.get_key_action_map())

    def _controls_set_select_action(self, *args):
        width = int(self.window.width / 4)
        height = int(self.window.height)
        x = self.window.width / 4
        y = self.window.height
        # Don't let the user break the UI by unbinding important keys
        ignore_actions = set([ControlAction.NOOP.name, ControlAction.EXIT.name, ControlAction.TOGGLE_PAUSE.name])
        controls_set_select_options = []
        for name, action in ControlAction.__members__.items():
            if name not in ignore_actions:
                controls_set_select_options.append((name, self._controls_open_key_action_binding_menu))
        controls_set_select_options.append(("Reset to Defaults", self._controls_reset_default))
        # Keep track of Controls.state_id. If it changes, save the updated controls to disk
        self.controls_state_id = Controls.state_id
        UIManager.show_foreground_menu("Key Bindings", controls_set_select_options, width=width, height=height, x=x, y=y,
                            background_color=self.dialog_color,
                            anchor_x="left", anchor_y="top", exit_callback=self._controls_set_menu_close)

    def _controls(self, *args):
        width = int(self.window.width / 4)
        height = int(self.window.height / 4)
        x = 0
        y = self.window.height
        controls_options = [
            (ControlTopMenuAction.VIEW, self._controls_view),
            (ControlTopMenuAction.SET, self._controls_set_select_action),
        ]
        UIManager.show_foreground_menu("Controls", controls_options, width=width, height=height, x=x, y=y,
                            option_font_size=12, title_font_size=18,
                            background_color=self.dialog_color,
                            anchor_x="left", anchor_y="top")

    def _about(self, *args):
        width = int(self.window.width / 2)
        height = int(self.window.height / 2)
        x = self.window.width / 2
        y = self.window.height / 2
        UIManager.show_popup("About", about_str, width=width, height=height, x=x, y=y,
                             background_color=self.dialog_color, anchor_x="center", anchor_y="center")

    def end_game(self):
        width = int(self.window.width / 2)
        height = int(self.window.height / 4)
        x = self.window.width / 2
        y = self.window.height / 2
        UIManager.show_popup("The End!", "You have reached the end of the game!", width=width, height=height, x=x, y=y,
                             background_color=self.dialog_color, anchor_x="center", anchor_y="center")

    def _quit(self, *args):
        # Closing the window exits the game
        self.window.on_close()

    def delete(self):
        self.version_label.delete()
        self.menu_background_sprite.delete()