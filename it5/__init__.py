from multiprocessing import freeze_support
from it5.util import PathfindingProcessManager

def main():
    freeze_support()
    PathfindingProcessManager.initialize()
    from it5 import game
    game.run()