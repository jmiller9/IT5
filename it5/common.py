from it5 import spriteloader
import pyglet
from it5.projectile import ParticleManager, BulletManager, EventManagerProxy
from it5.hud import HUD
from it5.lighting import LightingManager

common_tileset = spriteloader.getSprites("common.png", 8, 8)

common_spritemap = {
    "item": (0,7),
    "ammo": (0,6),
    "booster": (0,5),
    "medkit": (0,4),
    "weapon_special": (0,3),
    "weapon": (0,2),
    "camera_right": (1,7),
    "camera_left": (1,6),
    "camera_down": (1,5),
    "camera_up": (1,4),
    "cardkey": (1,3),
    "landmine": (1,2),
    "gun_camera_right": (2,7),
    "gun_camera_left": (2,6),
    "gun_camera_down": (2,5),
    "gun_camera_up": (2,4),
    "laser_vertical": (2,3),
    "laser_horizontal": (2,2),
    "shadow": (2,1),
    "objective": (2,0),
    "bar": (5, 7),
    "up": (4, 6),
    "down": (3, 6),
    "left": (4, 7),
    "right": (3, 7),
    "question_mark": (5,6),
    "dialog_icon": (5,5),
    "explosion": (5,4),
    "alt_mine": (5,3),
    "smalldoor0": (3, 0),
    "smalldoor1": (4, 0),
    "door0": (3, 1),
    "door1": (4, 1),
    "door2": (3, 2),
    "door3": (4, 2),
    "keypadbottom": (3, 3),
    "keypadtop": (4, 3),
    "keypadtopgreen": (4, 4),
    "keypadtopred": (4, 5),
    "smalldooralt": (5, 0),
    "dooralt0": (5, 1),
    "dooralt1": (5, 2),
    "bronze": (6,7),
    "silver": (6,6),
    "gold": (6,5),
    "bronze_egg": (6,4),
    "silver_egg": (6,3),
    "gold_egg": (7,7),
    "c4": (7,6),
    "sleep": (7,5),
    "lowres_circle": (7,4),
    "radio_icon": (7,3),
    "green_static": (7,2),
    "smoke": (6,1),
}

def __get_invisible_cursor():
    grid = pyglet.image.ImageGrid(common_tileset[(0,0)], 4, 4)
    return pyglet.window.ImageMouseCursor(grid[(0, 0)])

hidden_mouse_cursor = __get_invisible_cursor()

lighting = LightingManager()

class ShadedOrderedGroup(pyglet.graphics.OrderedGroup):
    def __init__(self, order, name, color=(255,255,255), parent=None):
        '''Create an ordered group.

        :Parameters:
            `order` : int
                Order of this group.
            `name` : str
                Name of this group.
            `color` : tuple
                Color of the objects in this group.
            `parent` : `~pyglet.graphics.Group`
                Parent of this group.

        '''
        self.name = name
        self.color = color
        super(ShadedOrderedGroup, self).__init__(order, parent)

    def set_state(self):
        lighting.bind_secondary(self.name, color=self.color)

    def unset_state(self):
        lighting.unbind_secondary(self.name)

radarBackground = pyglet.graphics.OrderedGroup(0)
radarForeground = pyglet.graphics.OrderedGroup(1)

backgroundGroup = ShadedOrderedGroup(0, "background")
floorGroup = ShadedOrderedGroup(1, "floor")
doorsGroup = ShadedOrderedGroup(2, "doors")
wallsGroup = ShadedOrderedGroup(3, "walls")
decalsGroup = ShadedOrderedGroup(4, "decals")
itemsGroup = ShadedOrderedGroup(5, "items")
feetGroup = ShadedOrderedGroup(6, "feet")
pantsGroup = ShadedOrderedGroup(7, "pants")
feetPlayerGroup = ShadedOrderedGroup(8, "feet_player")
grassGroup = ShadedOrderedGroup(9, "grass")
waterGroup = ShadedOrderedGroup(10, "water")
headsGroup = ShadedOrderedGroup(11, "heads")
shirtsGroup = ShadedOrderedGroup(12, "shirts")
hairGroup = ShadedOrderedGroup(13, "hair")
headsPlayerGroup = ShadedOrderedGroup(14, "heads_player")
explosionsGroup = ShadedOrderedGroup(15, "explosions")
subAirborneGroup = ShadedOrderedGroup(16, "subairborne")
airborneGroup = ShadedOrderedGroup(17, "airborne")
markersGroup = ShadedOrderedGroup(18, "markers")
weatherGroup = ShadedOrderedGroup(19, "weather")

menuBackgroundGroup = pyglet.graphics.OrderedGroup(0)
menuButtonGroup = pyglet.graphics.OrderedGroup(1)
menuForegroundGroup = pyglet.graphics.OrderedGroup(2)
dialogBackgroundGroup = pyglet.graphics.OrderedGroup(3)
dialogButtonGroup = pyglet.graphics.OrderedGroup(4)
dialogForegroundGroup = pyglet.graphics.OrderedGroup(5)

batchGroups = {
    "radarBackground": radarBackground,
    "radarForeground": radarForeground,
    "background": backgroundGroup,
    "floor": floorGroup,
    "doors": doorsGroup,
    "walls": wallsGroup,
    "decals": decalsGroup,
    "items": itemsGroup,
    "feet": feetGroup,
    "pants": pantsGroup,
    "feet_player": feetPlayerGroup,
    "grass": grassGroup,
    "water": waterGroup,
    "shirts": shirtsGroup,
    "heads": headsGroup,
    "hair": hairGroup,
    "heads_player": headsPlayerGroup,
    "explosions": explosionsGroup,
    "subairborne": subAirborneGroup,
    "airborne": airborneGroup,
    "markers": markersGroup,
    "weather": weatherGroup,
    "menuBackground": menuBackgroundGroup,
    "menuButtons": menuButtonGroup,
    "menuForeground": menuForegroundGroup,
    "dialogBackground": dialogBackgroundGroup,
    "dialogButtons": dialogButtonGroup,
    "dialogForeground": dialogForegroundGroup,
}

mainBatch = pyglet.graphics.Batch()
hudBatch = pyglet.graphics.Batch()
radarBatch = pyglet.graphics.Batch()
dialogBatch = pyglet.graphics.Batch()
menuBatch = pyglet.graphics.Batch()

batches = {
    "main": mainBatch,
    "hud": hudBatch,
    "radar": radarBatch,
    "dialog": dialogBatch,
    "menu": menuBatch,
}

class CommonManager:
    weather = None
    bullets = None
    hud = None
    player_name = None

    @classmethod
    def init_weather(cls, particle_count, weather_type, batch, batch_group):
        cls.weather = ParticleManager(particle_count, weather_type, batch, batch_group)

    @classmethod
    def init_bullets(cls, max_bullets, batch, batch_group, alt_batch_group):
        cls.bullets = BulletManager(max_bullets, batch, batch_group, alt_batch_group)

    @classmethod
    def init_hud(cls, window, batch, anchor_x):
        cls.hud = HUD(window, hudBatch, anchor_x)

    @classmethod
    def get_weather_manager(cls):
        return cls.weather

    @classmethod
    def get_bullet_manager(cls):
        return cls.bullets

    @classmethod
    def get_hud(cls):
        return cls.hud

    @classmethod
    def set_player_name(cls, name):
        cls.player_name = name

    @classmethod
    def get_player_name(cls):
        if cls.player_name is None:
            return "Player"
        return cls.player_name

class EventManager:
    eventDispatcher = None
    current_dialog_name = None

    @classmethod
    def initialize(cls):
        cls.eventDispatcher = pyglet.event.EventDispatcher()
        cls.eventDispatcher.register_event_type("set_notification")
        cls.eventDispatcher.register_event_type("dialog_finished")
        cls.eventDispatcher.register_event_type("gunshot")
        cls.eventDispatcher.register_event_type("alert")
        cls.eventDispatcher.register_event_type("player_change_clothes")
        cls.eventDispatcher.register_event_type("cancel_alert")
        cls.eventDispatcher.register_event_type("explosion")
        cls.eventDispatcher.register_event_type("smoke_bomb")
        cls.eventDispatcher.register_event_type("show_image_popout")
        cls.eventDispatcher.register_event_type("reset_camera")


    @classmethod
    def get_event_dispatcher(cls):
        return cls.eventDispatcher

    @classmethod
    def set_notification(cls, notification):
        cls.eventDispatcher.dispatch_event("set_notification", notification)

    @classmethod
    def dialog_finished(cls):
        cls.eventDispatcher.dispatch_event("dialog_finished", cls.current_dialog_name)

    @classmethod
    def gunshot(cls, radius):
        cls.eventDispatcher.dispatch_event("gunshot", radius)

    @classmethod
    def alert(cls, npc_id):
        cls.eventDispatcher.dispatch_event("alert", npc_id)

    @classmethod
    def cancel_alert(cls):
        cls.eventDispatcher.dispatch_event("cancel_alert")

    @classmethod
    def explosion(cls, x, y, damage=100):
        cls.eventDispatcher.dispatch_event("explosion", x, y, damage)

    @classmethod
    def smoke_bomb(cls, x, y):
        cls.eventDispatcher.dispatch_event("smoke_bomb", x, y)

    @classmethod
    def dialog_started(cls, dialog_name):
        cls.current_dialog_name = dialog_name

    @classmethod
    def show_image_popout(cls, image, img_width, img_height):
        cls.eventDispatcher.dispatch_event("show_image_popout", image, img_width, img_height)

    @classmethod
    def reset_camera(cls):
        cls.eventDispatcher.dispatch_event("reset_camera")

    @classmethod
    def player_change_clothes(cls, into_suit):
        cls.eventDispatcher.dispatch_event("player_change_clothes", into_suit)

    @classmethod
    def set_handler(cls, name, handler):
        cls.eventDispatcher.set_handler(name, handler)

    @classmethod
    def remove_handler(cls, name, handler):
        cls.eventDispatcher.remove_handler(name, handler)

EventManagerProxy.initialize()