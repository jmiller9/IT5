import pyglet
import math
from it5 import spriteloader
import random
import uuid
from it5.item import WeaponType

projectileImageGrid = spriteloader.getSprites('common.png', 8, 8)
projectileSprites = {
    "bullet": projectileImageGrid[1,0],
    "grenade": projectileImageGrid[1,1],
    "mine": projectileImageGrid[1,2],
    "rain": projectileImageGrid[0,0],
    "snow": projectileImageGrid[0,1],
    "rocket": projectileImageGrid[6,2], 
}

class ProjectileManager(object):
    def __init__(self, size, projectileType, batch, batchGroup):
        self.buffer = []
        self.maxSize = size
        self.batch = batch
        self.batchGroup = batchGroup
        for i in range(0, size):
            projectile = None
            if projectileType == 'bullet' or projectileType == 'grenade' or projectileType == 'rocket':
                projectile = Bullet(projectileSprites[projectileType], batch, batchGroup)
            else:
                projectile = Projectile(-1024, -768, 0, 0, 0, projectileSprites[projectileType], batch, batchGroup)
            self.buffer.append(projectile)

    def add(self, x, y, dx, dy, pRange):
        for projectile in self.buffer:
                if projectile.active == False:
                    projectile.setProperties(x,y,dx,dy,pRange,True)
                    return projectile
        return None

    def move(self, dt):
        for projectile in self.buffer:
            if projectile.active == True:
                projectile.move(dt)

    def stop(self):
        for projectile in self.buffer:
            projectile.active = False

class ParticleManager(ProjectileManager):
    def __init__(self, size, projectileType, batch, batchGroup, chunkSize=192):
        super(ParticleManager, self).__init__(size, projectileType, batch, batchGroup)
        self.started = False
        if projectileType == "snow":
            self.snow = True
        else:
            self.snow = False
        self.active = False
        self.batch = batch
        self.map_width = 0
        self.map_height = 0
        self.chunk_size = chunkSize
        self.curr_chunk = 0
        self.num_chunks = math.ceil(float(size) / float(chunkSize))

    def move(self, dt):
        if self.active:
            if self.curr_chunk >= self.num_chunks:
                self.curr_chunk = 0
            start = self.curr_chunk * self.chunk_size
            end = min((self.curr_chunk + 1) * self.chunk_size, self.maxSize)
            y = self.map_height + 32
            for i in range(start, end):
                particle = self.buffer[i]
                if particle.active:
                    particle.move(dt * self.num_chunks)
                else:
                    x = particle.getOriginalX()
                    dx = 0
                    dy = random.uniform(-600, -400)
                    if self.snow:
                        dx = random.uniform(-50, 50)
                        dy = random.uniform(-75, -50)
                    particle.setProperties(x,y,dx,dy,y+32,True)
            self.curr_chunk += 1

    def activate(self, map_width, map_height):
        self.map_width = map_width
        self.map_height = map_height
        self.active = True
        for p in self.buffer:
            p.show()

    def deactivate(self):
        self.active = False
        for p in self.buffer:
            p.hide()

    def changeToSnow(self):
        if self.snow == False or self.active == False:
            for p in self.buffer:
                p.active = False
                p.changeSprite(projectileSprites['snow'], self.batch, self.batchGroup)
            self.snow = True
            self.started = False

    def changeToRain(self):
        if self.snow == True or self.active == False:
            for p in self.buffer:
                p.active = False
                p.changeSprite(projectileSprites['rain'], self.batch, self.batchGroup)
            self.snow = False
            self.started = False

    def changeType(self):
        if self.snow == True:
            self.changeToRain()
        else:
            self.changeToSnow()

    def generateParticles(self):
        if self.active:
            if self.started == False:
                x = 0
                xstep = self.map_width / self.maxSize
                for i in range(0, self.maxSize):
                    y = random.randrange(0, self.map_height + 32)
                    dx = 0
                    dy = random.uniform(-600, -400)
                    if self.snow:
                        dx = random.uniform(-50, 50)
                        dy = random.uniform(-75, -50)
                    projectile = self.add(x, y, dx, dy, y+32)
                    if projectile != None:
                        projectile.setOriginalX(x)
                    x += xstep
        self.started = True

class Projectile(object):
    def __init__(self, x, y, dx, dy, pRange, image, batch, batchGroup):
        self.sprite = pyglet.sprite.Sprite(
            img=image,
            batch=batch,
            group=batchGroup
        )
        self.setProperties(x,y,dx,dy,pRange,False)
        self.originalX = x
        self.id = uuid.uuid4()

    def setProperties(self, x, y, dx, dy, pRange, active):
        self.x = x
        self.y = y
        self.originX = self.x
        self.originY = self.y
        self.dx = dx
        self.dy = dy
        self.range = pRange
        self.distance = 0
        self.active = active
        self.sprite.position = (self.x, self.y)

    def move(self, dt):
        if self.distance < self.range:
            delta_x = math.copysign(min(abs(self.dx * dt), 32.0), self.dx)
            delta_y = math.copysign(min(abs(self.dy * dt), 32.0), self.dy)
            self.x += delta_x
            self.y += delta_y
            self.sprite.position = (self.x, self.y)
            self.distance = math.sqrt(((self.x - self.originX) ** 2) + ((self.y - self.originY) ** 2))
        else:
            self.active = False
        if self.active == False:
            if self.x >= 0 or self.y >= 0:
                self.x = -1024
                self.y = -768
                self.sprite.position = (self.x, self.y)

    def show(self):
        self.sprite.visible = True

    def hide(self):
        self.sprite.visible = False

    def changeSprite(self, image, batch, batchGroup):
        self.sprite.delete()
        self.sprite = pyglet.sprite.Sprite(
            img=image,
            batch=batch,
            group=batchGroup
        )
        self.sprite.position = (self.x, self.y)

    def setOriginalX(self, x):
        self.originalX = x

    def getOriginalX(self):
        return self.originalX

class BulletManager(ProjectileManager):
    def __init__(self, size, batch, batchGroup, alt_batch_group):
        super(BulletManager, self).__init__(size, 'bullet', batch, batchGroup)
        self.alt_batch_group = alt_batch_group

    def add(self, x, y, dx, dy, pRange, damage, weapon_type, from_hostile):
        bullet = super(BulletManager, self).add(x, y, dx, dy, pRange)
        if bullet != None:
            bullet.damage = damage
            if weapon_type == WeaponType.GRENADE and bullet.type != WeaponType.GRENADE:
                bullet.changeSprite(projectileSprites['grenade'], self.batch, self.batchGroup)
            elif weapon_type == WeaponType.MINE and bullet.type != WeaponType.MINE:
                bullet.changeSprite(projectileSprites['mine'], self.batch, self.alt_batch_group)
            elif weapon_type == WeaponType.ROCKET_LAUNCHER and bullet.type != WeaponType.ROCKET_LAUNCHER:
                bullet.changeSprite(projectileSprites['rocket'], self.batch, self.batchGroup)
            elif bullet.type == WeaponType.GRENADE or bullet.type == WeaponType.MINE or bullet.type == WeaponType.ROCKET_LAUNCHER:
                if weapon_type != WeaponType.GRENADE and weapon_type != WeaponType.MINE and weapon_type != WeaponType.ROCKET_LAUNCHER:
                    bullet.changeSprite(projectileSprites['bullet'], self.batch, self.batchGroup)
            bullet.type = weapon_type
            bullet.from_hostile = from_hostile

    def checkCollisions(self, obstacleMatrix, player, npcs, doors):
        for bullet in self.buffer:
            bullet.checkCollision(obstacleMatrix, player, npcs, doors)

    def stop(self):
        super(BulletManager, self).stop()
        for bullet in self.buffer:
            bullet.x = -1024
            bullet.y = -768
            bullet.sprite.position = (bullet.x, bullet.y)

class EventManagerProxy:
    event_manager = None
    @classmethod
    def initialize(cls):
        from it5.common import EventManager
        cls.event_manager = EventManager

class Bullet(Projectile):
    def __init__(self, image, batch, batchGroup):
        super(Bullet, self).__init__(-1024, -768, 0, 0, 1024, image, batch, batchGroup)
        self.damage = 0
        self.type = WeaponType.PISTOL
        self.from_hostile = False

    def getTileX(self):
        return int((self.x+15) / 32)

    def getTileY(self):
        return int((self.y-31) / 32)

    def move(self, dt):
        active = self.active
        x = self.x
        y = self.y
        super(Bullet, self).move(dt)
        if active and not self.active:
            if self.type == WeaponType.GRENADE or self.type == WeaponType.ROCKET_LAUNCHER:
                EventManagerProxy.event_manager.explosion(x, y)

    def checkCollision(self, obstacleMatrix, player, npcs, doors):
        if self.active:
            tileX = int((self.x+15) / 32)
            tileY = int((self.y-31) / 32)
            if tileY >= 0 and tileY < len(obstacleMatrix) and tileX >= 0 and tileX < len(obstacleMatrix[0]):
                if obstacleMatrix[tileY][tileX] == 0:
                    self.active = False
                else:
                    # Check collisions with doors
                    for door in doors:
                        if door.isClosed():
                            if (tileX == door.tileX or tileX == door.tileX + (door.width-1)) and tileY == door.tileY:
                                self.active = False
                    # If still active, check collisions with humanoids
                    if self.active:
                        if self.from_hostile:
                            if tileX == player.getTileX() and (tileY == player.getTileY() or tileY == player.getTileY()+1):
                                player.receiveDamage(self.damage)
                                self.active = False
                            else:
                                for npc in npcs:
                                    if npc.friendly:
                                        if tileX == npc.getTileX() and (tileY == npc.getTileY() or tileY == npc.getTileY()+1):
                                            npc.receiveDamage(self.damage)
                                            self.active = False
                        else:
                            for npc in npcs:
                                if not npc.friendly:
                                    if tileX == npc.getTileX() and (tileY == npc.getTileY() or tileY == npc.getTileY()+1):
                                        npc.receiveDamage(self.damage, player)
                                        self.active = False
            else:
                self.active = False
            if not self.active:
                if self.type == WeaponType.GRENADE or self.type == WeaponType.MINE or self.type == WeaponType.ROCKET_LAUNCHER:
                    EventManagerProxy.event_manager.explosion(self.x, self.y)
                self.x = -1024
                self.y = -768
                self.sprite.position = (self.x, self.y)