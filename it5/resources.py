import pyglet
import json


class ResourceManager:

    @classmethod
    def set_resource_path(cls):
        pyglet.resource.path = ["@it5.sprites", "@it5.audio_files", "@it5.dialogs", "@it5.maps"]
        pyglet.resource.reindex()

    @classmethod
    def _get_file_obj(cls, location):
        return pyglet.resource.file(location, "r")

    @classmethod
    def load_json(cls, location):
        f = cls._get_file_obj(location)
        data = json.loads(f.read())
        f.close()
        return data