from it5.quest import QuestManager

class DefaultSpawnConditional:
    def should_spawn(self):
        return True

class IsabelHomeSpawnConditional(DefaultSpawnConditional):
    def should_spawn(self):
        quest = QuestManager.getQuest("main")
        if quest is not None:
            if quest.isStageComplete("find_isabel") and not quest.isStageComplete("exit_shed"):
                return True
        return False

class IsabelHomePostPartySpawnConditional(DefaultSpawnConditional):
    def should_spawn(self):
        quest = QuestManager.getQuest("main")
        if quest is not None:
            if quest.isStageComplete("retire_to_guest_house"):
                return True
        return False

class MarcusColoniaSpawnConditional(DefaultSpawnConditional):
    def should_spawn(self):
        quest = QuestManager.getQuest("main")
        if quest is not None:
            if quest.isStageComplete("find_mercedes") and not quest.isStageComplete("find_marcus"):
                return True
        return False

class MarcusAtIsabelHomeSpawnConditional(DefaultSpawnConditional):
    def should_spawn(self):
        quest = QuestManager.getQuest("main")
        if quest is not None:
            if quest.isStageComplete("escape_relict") and not quest.isStageComplete("enter_lab"):
                return True
        return False

class ShedSpawnConditional(DefaultSpawnConditional):
    def should_spawn(self):
        quest = QuestManager.getQuest("main")
        if quest is not None:
            if quest.isStageComplete("catch_dr_rees") and not quest.isStageComplete("interrogate_rees"):
                return True
        return False

class TailorSpawnConditional(DefaultSpawnConditional):
    def should_spawn(self):
        quest = QuestManager.getQuest("main")
        if quest is not None:
            if not quest.isStageComplete("exit_shed"):
                return True
            elif quest.isStageComplete("change_clothes"):
                return True
        return False

class IsabelInalcoHouseSpawnConditional(DefaultSpawnConditional):
    def should_spawn(self):
        quest = QuestManager.getQuest("main")
        if quest is not None:
            if quest.isStageComplete("enter_inalco_house") and not quest.isStageComplete("talk_to_lady_inalco"):
                return True
        return False

class DrinkingGuestStage1InalcoHouseSpawnConditional(DefaultSpawnConditional):
    def should_spawn(self):
        quest = QuestManager.getQuest("main")
        if quest is not None:
            if not quest.isStageComplete("talk_to_lady_inalco"):
                return True
        return False

class DrinkingGuestStage2InalcoHouseSpawnConditional(DefaultSpawnConditional):
    def should_spawn(self):
        quest = QuestManager.getQuest("main")
        if quest is not None:
            if quest.isStageComplete("talk_to_lady_inalco") and not quest.isStageComplete("retire_to_guest_house"):
                return True
        return False

class DVBadManSpawnConditional(DefaultSpawnConditional):
    def should_spawn(self):
        quest = QuestManager.getQuest("domestic_violence")
        if quest is not None:
            if quest.isStageComplete("confront_barkeeps_bf"):
                return False
        return True

class DVBarkeepMidQuestConditional(DefaultSpawnConditional):
    def should_spawn(self):
        quest = QuestManager.getQuest("domestic_violence")
        if quest is not None:
            if quest.active:
                if not quest.isStageComplete("rough_up_barkeeps_bf"):
                    return True
        return False

class DVBarkeepConditional(DefaultSpawnConditional):
    def should_spawn(self):
        quest = QuestManager.getQuest("domestic_violence")
        if quest is not None:
            if quest.active:
                if not quest.isStageComplete("rough_up_barkeeps_bf"):
                    return False
        return True

class OFVicenteFirstConditional(DefaultSpawnConditional):
    def should_spawn(self):
        quest = QuestManager.getQuest("old_flame")
        if quest is not None:
            if quest.active:
                return False
        return True

class OFVicenteSecondConditional(DefaultSpawnConditional):
    def should_spawn(self):
        quest = QuestManager.getQuest("old_flame")
        if quest is not None:
            if quest.active:
                if not quest.isStageComplete("deliver_message"):
                    return True
        return False

class OFVicenteThirdConditional(DefaultSpawnConditional):
    def should_spawn(self):
        quest = QuestManager.getQuest("old_flame")
        if quest is not None:
            if quest.active:
                if quest.isStageComplete("deliver_message") and not quest.isStageComplete("return_to_vicente"):
                    return True
        return False

class OFVicenteFourthConditional(DefaultSpawnConditional):
    def should_spawn(self):
        quest = QuestManager.getQuest("old_flame")
        if quest is not None:
            if quest.active:
                if quest.isStageComplete("return_to_vicente"):
                    return True
        return False

class OFMatildaConditional(DefaultSpawnConditional):
    def should_spawn(self):
        quest = QuestManager.getQuest("main")
        if quest is not None:
            if quest.isStageComplete("retire_to_guest_house"):
                return False
        quest = QuestManager.getQuest("old_flame")
        if quest is not None:
            if quest.active:
                if not quest.isStageComplete("deliver_message"):
                    return True
                else:
                    return False
        return True

class OFMatilda2ndConditional(DefaultSpawnConditional):
    def should_spawn(self):
        quest = QuestManager.getQuest("main")
        if quest is not None:
            if quest.isStageComplete("retire_to_guest_house"):
                return False
        quest = QuestManager.getQuest("old_flame")
        if quest is not None:
            if quest.active:
                if quest.isStageComplete("deliver_message"):
                    return True
        return False

class LadyInalcoConditional(DefaultSpawnConditional):
    def should_spawn(self):
        quest = QuestManager.getQuest("main")
        if quest is not None:
            if quest.active:
                if quest.isStageComplete("return_to_town_post_party"):
                    return False
        return True

class LadyInalco2ndConditional(DefaultSpawnConditional):
    def should_spawn(self):
        quest = QuestManager.getQuest("main")
        if quest is not None:
            if quest.active:
                if quest.isStageComplete("return_to_town_post_party"):
                    quest = QuestManager.getQuest("old_flame_2")
                    if quest is not None:
                        if not quest.active:
                            return True
        return False

class LadyInalco3rdConditional(DefaultSpawnConditional):
    def should_spawn(self):
        quest = QuestManager.getQuest("old_flame_2")
        if quest is not None:
            if quest.active:
                if not quest.isStageComplete("deliver_message"):
                    return True
        return False

class LadyInalco4thConditional(DefaultSpawnConditional):
    def should_spawn(self):
        quest = QuestManager.getQuest("old_flame_2")
        if quest is not None:
            if quest.active:
                if quest.isStageComplete("deliver_message") and not quest.isStageComplete("return_to_lady_inalco"):
                    return True
        return False

class LadyInalco5thConditional(DefaultSpawnConditional):
    def should_spawn(self):
        quest = QuestManager.getQuest("old_flame_2")
        if quest is not None:
            if quest.active:
                if quest.isStageComplete("return_to_lady_inalco"):
                    return True
        return False

class PetraConditional(DefaultSpawnConditional):
    def should_spawn(self):
        quest = QuestManager.getQuest("old_flame_2")
        if quest is not None:
            if quest.active:
                return False
        return True

class Petra2ndConditional(DefaultSpawnConditional):
    def should_spawn(self):
        quest = QuestManager.getQuest("old_flame_2")
        if quest is not None:
            if quest.active:
                if not quest.isStageComplete("deliver_message"):
                    return True
        return False

class Petra3rdConditional(DefaultSpawnConditional):
    def should_spawn(self):
        quest = QuestManager.getQuest("old_flame_2")
        if quest is not None:
            if quest.active:
                if quest.isStageComplete("deliver_message"):
                    return True
        return False

class MrNunezConditional(DefaultSpawnConditional):
    def should_spawn(self):
        quest = QuestManager.getQuest("concerned_parents")
        if quest is not None:
            if quest.active:
                return False
        return True

class MrNunez2ndConditional(DefaultSpawnConditional):
    def should_spawn(self):
        quest = QuestManager.getQuest("concerned_parents")
        if quest is not None:
            if quest.active:
                if not quest.isStageComplete("kill_guards"):
                    return True
        return False

class MrNunez3rdConditional(DefaultSpawnConditional):
    def should_spawn(self):
        quest = QuestManager.getQuest("concerned_parents")
        if quest is not None:
            if quest.active:
                if quest.isStageComplete("kill_guards") and not quest.isStageComplete("return_to_mr_nunez"):
                    return True
        return False

class MrNunez4thConditional(DefaultSpawnConditional):
    def should_spawn(self):
        quest = QuestManager.getQuest("concerned_parents")
        if quest is not None:
            if quest.active:
                if quest.isStageComplete("return_to_mr_nunez"):
                    return True
        return False

class CristianCaveConditional(DefaultSpawnConditional):
    def should_spawn(self):
        quest = QuestManager.getQuest("concerned_parents")
        if quest is not None:
            if quest.active:
                if quest.isStageComplete("find_violeta") and not quest.isStageComplete("kill_guards"):
                    return True
        return False

class CristianConditional(DefaultSpawnConditional):
    def should_spawn(self):
        quest = QuestManager.getQuest("concerned_parents")
        if quest is not None:
            if quest.active:
                if quest.isStageComplete("kill_guards"):
                    quest = QuestManager.getQuest("main")
                    if quest.active:
                        if not quest.isStageComplete("escape_relict"):
                            return True
        return False

class Cristian2ndConditional(DefaultSpawnConditional):
    def should_spawn(self):
        quest = QuestManager.getQuest("concerned_parents")
        if quest is not None:
            if quest.active:
                if quest.isStageComplete("kill_guards"):
                    quest = QuestManager.getQuest("main")
                    if quest.active:
                        if quest.isStageComplete("escape_relict"):
                            quest = QuestManager.getQuest("cave_monster")
                            if not quest.active:
                                return True
        return False

class Cristian3rdConditional(DefaultSpawnConditional):
    def should_spawn(self):
        quest = QuestManager.getQuest("concerned_parents")
        if quest is not None:
            if quest.active:
                if quest.isStageComplete("kill_guards"):
                    quest = QuestManager.getQuest("main")
                    if quest.active:
                        if quest.isStageComplete("escape_relict"):
                            quest = QuestManager.getQuest("cave_monster")
                            if quest.active:
                                if not quest.isStageComplete("find_evidence"):
                                    return True
        return False

class Cristian4thConditional(DefaultSpawnConditional):
    def should_spawn(self):
        quest = QuestManager.getQuest("concerned_parents")
        if quest is not None:
            if quest.active:
                if quest.isStageComplete("kill_guards"):
                    quest = QuestManager.getQuest("main")
                    if quest.active:
                        if quest.isStageComplete("escape_relict"):
                            quest = QuestManager.getQuest("cave_monster")
                            if quest.active:
                                if quest.isStageComplete("find_evidence") and not quest.isStageComplete("return_to_cristian"):
                                    return True
        return False

class Cristian5thConditional(DefaultSpawnConditional):
    def should_spawn(self):
        quest = QuestManager.getQuest("concerned_parents")
        if quest is not None:
            if quest.active:
                if quest.isStageComplete("kill_guards"):
                    quest = QuestManager.getQuest("main")
                    if quest.active:
                        if quest.isStageComplete("escape_relict"):
                            quest = QuestManager.getQuest("cave_monster")
                            if quest.active:
                                if quest.isStageComplete("return_to_cristian"):
                                    return True
        return False

class VioletaConditional(DefaultSpawnConditional):
    def should_spawn(self):
        quest = QuestManager.getQuest("concerned_parents")
        if quest is not None:
            if quest.active:
                if not quest.isStageComplete("find_violeta"):
                    return True
        return False

class Violeta2ndConditional(DefaultSpawnConditional):
    def should_spawn(self):
        quest = QuestManager.getQuest("concerned_parents")
        if quest is not None:
            if quest.active:
                if quest.isStageComplete("find_violeta"):
                    return True
        else:
            return True
        return False

class KarlConditional(DefaultSpawnConditional):
    def should_spawn(self):
        quest = QuestManager.getQuest("treasure_hunter")
        if quest is not None:
            if not quest.active:
                return True
        return False

class Karl2ndConditional(DefaultSpawnConditional):
    def should_spawn(self):
        quest = QuestManager.getQuest("treasure_hunter")
        if quest is not None:
            if quest.active:
                if not quest.isStageComplete("find_eggs"):
                    return True
        return False

class Karl3rdConditional(DefaultSpawnConditional):
    def should_spawn(self):
        quest = QuestManager.getQuest("treasure_hunter")
        if quest is not None:
            if quest.active:
                if quest.isStageComplete("find_eggs"):
                    return True
        return False

class InalcoGuestConditional(DefaultSpawnConditional):
    def should_spawn(self):
        quest = QuestManager.getQuest("main")
        if quest is not None:
            if not quest.isStageComplete("retire_to_guest_house"):
                return True
        return False

class SpawnConditionalManager:
    available_conditionals = {}
    __default_spawn_conditional = DefaultSpawnConditional()

    @classmethod
    def initialize(cls):
        subclasses = {_cls.__name__: _cls for _cls in DefaultSpawnConditional.__subclasses__()}
        for subclass in subclasses:
            cls.available_conditionals[subclass] = subclasses[subclass]()

    @classmethod
    def should_spawn(cls, conditional_script_name):
        return cls.available_conditionals.get(conditional_script_name, cls.__default_spawn_conditional).should_spawn()