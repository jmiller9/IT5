import pyglet

class StaticObject(object):
    def __init__(self, x, y, image, batch, group=None):
        self.x = x * 32
        self.y = y * 32
        self.originalX = x
        self.originalY = y
        self.sprite = None
        self.image = image
        self.batch = batch
        self.group = group
    
    def move(self, dx, dy, dt):
        if self.sprite != None:
            self.x += dx * dt
            self.y += dy * dt
            self.sprite.position = (self.x, self.y)
    
    def setPosition(self, x, y):
        if self.sprite != None:
            self.x = x
            self.y = y
            self.sprite.position = (self.x, self.y)
    
    def hide(self):
        if self.sprite != None:
            self.sprite.delete()
            self.sprite = None

    def show(self):
        if self.image != None and self.batch != None:
            self.sprite = pyglet.sprite.Sprite(
                img=self.image,
                batch=self.batch,
                group=self.group
            )
            self.sprite.position = (self.x, self.y)

    def setColor(self, color):
        if self.sprite != None:
            self.sprite.color = color