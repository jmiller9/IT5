from it5.staticobject import StaticObject
from it5 import spriteloader
from it5.common import common_spritemap, common_tileset, mainBatch, airborneGroup, radarBatch, radarForeground
from it5.quest import QuestManager
from it5.script import ScriptManager

objective_marker = common_tileset[common_spritemap['objective']]
dialog_marker = common_tileset[common_spritemap['dialog_icon']]
quest_marker = common_tileset[common_spritemap['question_mark']]
lowres_circle = common_tileset[common_spritemap['lowres_circle']]

class Trigger(StaticObject):
    def __init__(self, x, y, image, trigger_name, repeatable=False):
        super(Trigger, self).__init__(x, y, image, mainBatch, airborneGroup)
        self.active = True
        self.repeatable = repeatable
        self.last_check = False
        self.trigger_name = trigger_name
        self.tileX = self.originalX
        self.tileY = self.originalY - 1
        self.saveState()
    
    def _check(self, playerX, playerY):
        if playerX == self.tileX and playerY == self.tileY:
            if self.last_check == False:
                self.last_check = True
                if self.active:
                    if self.repeatable == False:
                        self.active = False
                        self.hide()
                    return True
        else:
            self.last_check = False
        return False
    
    def check(self, playerX, playerY):
        if self._check(playerX, playerY):
            print("Trigger " + str(self.trigger_name) + " has been triggered")

    def update(self, active):
        self.setActive(active)
        self.last_check = False
    
    def setActive(self, active):
        self.active = active
        if active == True:
            self.show()
        else:
            self.hide()

    def reset(self):
        self.update(self._active)

    def saveState(self):
        self._active = self.active

    def to_dict(self):
        trigger_dict = {
            "trigger_name": self.trigger_name,
            "active": self._active,
            "tileX": self.tileX,
            "tileY": self.tileY
        }
        return trigger_dict

class Objective(Trigger):
    def __init__(self, x, y, name, quest, order, trigger_name, dialog=None, script_name=None, visible=True):
        super(Objective, self).__init__(x, y, objective_marker if visible else None, trigger_name)
        self.name = name
        self.quest = quest
        self.order = order
        self.dialog = dialog
        self.script_name = script_name
        self.visible = visible
        self.radar_unit = StaticObject(28, 20, lowres_circle, radarBatch, radarForeground)

    def show(self):
        super(Objective, self).show()
        if self.visible:
            self.radar_unit.show()
            self.radar_unit.sprite.scale_x = 0.125
            self.radar_unit.sprite.scale_y = 0.125
            self.radar_unit.sprite.color = (255,255,0)
            self.radar_unit.setPosition((self.x / 4) + 2, (self.y / 4)+4)

    def hide(self):
        super(Objective, self).hide()
        if self.visible:
            self.radar_unit.hide()

    def check(self, playerX, playerY):
        if self._check(playerX, playerY):
            print("Objective '" + str(self.name) + "' has been triggered")
            quest = QuestManager.getQuest(self.quest)
            quest_active = False
            if quest is not None:
                if quest.active:
                    if quest.completeStage(self.name):
                        quest_active = True
                        #print(str(QuestManager.getActiveQuestsJSON()))
                    else:
                        self.setActive(True)
                else:
                    self.active = True
            else:
                print('WARNING: quest "' + str(self.quest) + '" is None')
            if quest_active:
                if self.script_name is not None:
                    ScriptManager.load(self.script_name)
                if self.dialog is not None:
                    print("Should be spawning dialog " + self.dialog)
                    return {"type": "dialog", "data": self.dialog}

class QuestTrigger(Trigger):
    def __init__(self, x, y, quest, trigger_name, dialog=None):
        super(QuestTrigger, self).__init__(x, y, quest_marker, trigger_name)
        self.quest = quest
        self.dialog = dialog
    
    def check(self, playerX, playerY):
        if self._check(playerX, playerY):
            print("Quest Trigger '" + str(self.trigger_name) + "' has been triggered")
            quest = QuestManager.getQuest(self.quest)
            if quest is not None:
                quest.setActive(True)
                print("Activating quest " + self.quest)
                print(str(QuestManager.getActiveQuestsJSON()))
            else:
                print('WARNING: quest "' + str(self.quest) + '" is None')
            if self.dialog is not None:
                print("Should be spawning dialog " + self.dialog)
                return {"type": "dialog", "data": self.dialog}

class DialogTrigger(Trigger):
    def __init__(self, x, y, trigger_name, dialog, repeatable):
        super(DialogTrigger, self).__init__(x, y+1, dialog_marker, trigger_name, repeatable)
        self.dialog = dialog
        self.tileY -= 1
    
    def check(self, playerX, playerY):
        if self._check(playerX, playerY):
            print("Dialog '" + str(self.dialog) + "' has been triggered")
            return {"type": "dialog", "data": self.dialog}

class ScriptTrigger(Trigger):
    def __init__(self, x, y, trigger_name, script_name, visible, repeatable):
        super(ScriptTrigger, self).__init__(x, y, quest_marker if visible else None, trigger_name, repeatable)
        self.script_name = script_name

    def check(self, playerX, playerY):
        if self._check(playerX, playerY):
            ScriptManager.load(self.script_name)