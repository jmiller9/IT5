from it5.humanoid import Humanoid
from it5 import spriteloader
from it5.waypoint import Path, WaypointBehavior
from it5.util import searchForPlayer, getHalfVisionConeHeight
from it5.direction import Direction
import math
from it5.staticobject import StaticObject
from it5.dynamicobject import DynamicObject
from it5.common import common_spritemap, common_tileset, EventManager, lighting
from it5.audio import SoundManager
from it5.item import WeaponType
import pyglet

guard_sprites = spriteloader.getSprites('guard.png', 8, 16)
brother_1945_sprites = spriteloader.getSprites('brother_1945.png', 8, 8)
f_scientist_sprites = spriteloader.getSprites('f_scientist.png', 8, 8)
scientist_sprites = spriteloader.getSprites('scientist.png', 8, 8)
wolfgang_sprites = spriteloader.getSprites('wolfgang.png', 8, 8)
wolfgang73_sprites = spriteloader.getSprites('wolfgang73.png', 8, 8)
tortured_ss_sprites = spriteloader.getSprites('tortured_ss.png', 2, 8)
rees_interrogation_sprites = spriteloader.getSprites('rees_interrogation.png', 2, 4)
relict_sprites = spriteloader.getSprites('relict.png', 8, 16)
isabel_sprites = spriteloader.getSprites('isabel.png', 8, 8)
isabel_dress_sprites = spriteloader.getSprites('isabel_dress.png', 8, 8)
commando_sprites = spriteloader.getSprites('commando.png', 8, 16)
inalco_sprites = spriteloader.getSprites('mr_inalco.png', 8, 8)
bad_man_sprites = spriteloader.getSprites('bad_man.png', 8, 8)
tortured_bad_man_sprites = spriteloader.getSprites('tortured_bad_man.png', 2, 6)

# For dynamic characters
man_sprites = spriteloader.getSprites('base_man.png', 8, 8)
woman_sprites = spriteloader.getSprites('base_woman.png', 8, 8)
man_pants = spriteloader.getSprites('m_generic_pants.png', 8, 8)
man_shirt = spriteloader.getSprites('m_generic_shirt.png', 8, 8)
woman_dress = spriteloader.getSprites('f_generic_dress.png', 8, 8)
# Includes man and woman hair. Alternates woman, man
hair = spriteloader.getSprites('hair.png', 8, 8)

class NPC(Humanoid):
    def __init__(self, x, y, sprites, friendly, waypoints, dialog, damage, batches, batchGroups, color=(255,255,255), no_movement=False, script=None):
        super(NPC, self).__init__(x, y, sprites, batches['main'], batchGroups, color=color, no_movement=no_movement)
        self.stamina = 100
        self.friendly = friendly
        self.initialize_path_from_waypoints(waypoints)
        self.originalPath = self.path.copy()
        self.dialog = dialog
        self.script = script
        self.script_triggered = False
        self.waypoint = self.path.getNextWaypoint()
        self.waitTimer = 0
        self.moveSpeed = 60
        self.viewDistance = 384
        self.stunned = False
        self.tempStunned = False
        self.suspicious = False
        self.canSeePlayer = False
        self.alert = False
        self.searching = False
        self.searching_time = 0
        self.awake = True
        self.wait_for_return_path = False
        self.returning_to_patrol_path = False
        self.wait_for_new_path = False
        self.awaiting_path = None
        self.damage = damage
        self.shotCooldown = 1
        self.shotTimer = 0
        self.stunTimer = 0
        self.tempStunTimer = 0
        self.suspiciousTimer = 0
        self.color = color
        self.batches = batches
        self.batchGroups = batchGroups
        self.no_movement = no_movement
        self.nextDirection = None
        self.radarUnit = StaticObject(28, 20, common_tileset[common_spritemap['bar']], self.batches['radar'], self.batchGroups['radarForeground'])
        self.radarUnit.show()
        self.radarUnit.sprite.scale_x = 0.125
        self.radarUnit.sprite.scale_y = 0.125
        self.createVisionCone()
        if friendly:
            self.radarUnit.sprite.color = (0,127,255)
        else:
            self.radarUnit.sprite.color = (255,0,0)
        self.radarViewDistance = self.viewDistance/4
        self.halfVisionConeHeight = getHalfVisionConeHeight(self.radarViewDistance)
        self.lastX = self.x
        self.lastY = self.y
        self.lastDirection = []
        self.createDialogMarker()
        self.sleepMarker = None
    
    def cleanUp(self):
        self.radarUnit.hide()
        self.destroyVisionCone()
        self.hideDialogMarker()
        self.hideSleepMarker()
        self.destroy()

    def copy(self):
        return NPC(self.getTileX(), self.getTileY(), self.bottom.imagegrid, self.friendly, self.path.waypoints,
                   self.dialog, self.damage, self.batches, self.batchGroups, color=self.color, no_movement=self.no_movement)

    def initialize_path_from_waypoints(self, waypoints):
        self.path = Path(waypoints)

    def get_maximum_delta_x(self):
        return abs(self.x - self.waypoint.x)
    
    def get_maximum_delta_y(self):
        return abs(self.y - self.waypoint.y)

    def cacheLastDirection(self):
        self.lastDirection.append((self.direction, self.moving))

    def resumePostEvent(self):
        if not self.stunned and not self.tempStunned and not self.suspicious:
            while len(self.lastDirection) > 0:
                direction, moving = self.lastDirection.pop()
                self.changeDirection(direction)
                self.moving = moving

    def createDialogMarker(self):
        if self.friendly and (self.dialog is not None or self.script is not None):
            self.dialogMarker = StaticObject(self.x, self.y+2, common_tileset[common_spritemap['dialog_icon']], self.batches['main'], self.batchGroups['markers'])
            self.dialogMarker.show()
        else:
            self.dialogMarker = None

    def updateDialogMarker(self):
        if self.dialogMarker is not None:
            self.dialogMarker.setPosition(self.x, self.y+64)

    def hideDialogMarker(self):
        if self.dialogMarker is not None:
            self.dialogMarker.hide()

    def setAwake(self, is_awake):
        self.awake = is_awake
        self.hideSleepMarker()
        self.createSleepMarker()

    def createSleepMarker(self):
        if not self.awake:
            self.sleepMarker = StaticObject(self.x, self.y+2, common_tileset[common_spritemap['sleep']], self.batches['main'], self.batchGroups['markers'])
            self.sleepMarker.show()
        else:
            self.sleepMarker = None

    def updateSleepMarker(self):
        if self.sleepMarker is not None:
            self.sleepMarker.setPosition(self.x, self.y+64)

    def hideSleepMarker(self):
        if self.sleepMarker is not None:
            self.sleepMarker.hide()

    def updateVisionCone(self):
        if not self.friendly:
            if self.direction == Direction.UP:
                x1 = (self.x/4) + 3
                y1 = (self.y/4) + 6
                x2 = x1 + self.halfVisionConeHeight
                y2 = y1 + self.radarViewDistance
                x3 = x1 - self.halfVisionConeHeight
                y3 = y1 + self.radarViewDistance
            elif self.direction == Direction.DOWN:
                x1 = (self.x/4) + 3
                y1 = (self.y/4) + 4
                x2 = x1 + self.halfVisionConeHeight
                y2 = y1 - self.radarViewDistance
                x3 = x1 - self.halfVisionConeHeight
                y3 = y1 - self.radarViewDistance
            elif self.direction == Direction.LEFT:
                x1 = (self.x/4)+1
                y1 = (self.y/4) + 5
                x2 = x1 - self.radarViewDistance
                y2 = y1 + self.halfVisionConeHeight
                x3 = x1 - self.radarViewDistance
                y3 = y1 - self.halfVisionConeHeight
            elif self.direction == Direction.RIGHT:
                x1 = (self.x/4) + 4
                y1 = (self.y/4) + 5
                x2 = x1 + self.radarViewDistance
                y2 = y1 - self.halfVisionConeHeight
                x3 = x1 + self.radarViewDistance
                y3 = y1 + self.halfVisionConeHeight
            self.visionCone.vertices[0] = x1
            self.visionCone.vertices[1] = y1
            self.visionCone.vertices[2] = x2
            self.visionCone.vertices[3] = y2
            self.visionCone.vertices[4] = x3
            self.visionCone.vertices[5] = y3

    def hear_gunshot(self, player, currLevelMap, radius_squared):
        distance = ((self.x - player.x) * (self.x - player.x) + (self.y - player.y) * (self.y - player.y))
        if distance <= radius_squared:
            return not self.stunned
        return False

    def move(self, dx, dy, dt, obstacleMatrix, doors):
        super(NPC, self).move(dx, dy, dt, obstacleMatrix, doors)
        self.updateVisionCone()

    def changeDirection(self, moveDirection):
        super(NPC, self).changeDirection(moveDirection)
        self.updateVisionCone()

    def createVisionCone(self):
        if not self.friendly:
            self.visionCone = self.batches['radar'].add(3, pyglet.gl.GL_TRIANGLES, self.batchGroups['radarForeground'],
                ('v2f/stream', (0, 0, 0, 0, 0, 0)),
                ('c4B/static', (0, 255, 223, 127, 0, 255, 223, 127, 0, 255, 223, 127))
            )
        else:
            self.visionCone = None

    def destroyVisionCone(self):
        if self.visionCone is not None:
            self.visionCone.delete()

    def hideVisionCone(self):
        if not self.friendly:
            self.visionCone.vertices[0] = self.x/4
            self.visionCone.vertices[1] = (self.y/4)+4
            self.visionCone.vertices[2] = self.x/4
            self.visionCone.vertices[3] = (self.y/4)+4
            self.visionCone.vertices[4] = self.x/4
            self.visionCone.vertices[5] = (self.y/4)+4

    def getPunched(self, x1, x2, y1, y2, player=None):
        if not self.friendly and not self.stunned:
            tx = self.getTileX()
            ty = self.getTileY()
            if tx >= x1 and tx <= x2 and ty >= y1 and ty <= y2:
                self.stamina -= 35
                print("NPC has been punched! " + str(self.stamina))
                if self.stamina <= 0:
                    self.stunned = True
                    self.hideVisionCone()
                    self.stunTimer = 0
                    if not self.alert:
                        self.cacheLastDirection()
                    self.stopMoving()
                    self.top.updateSprite(7,6)
                    self.bottom.updateSprite(6,6)
                else:
                    self.receiveDamage(0, player)
                SoundManager.play_sfx('punch')

    def scatter(self):
        self.setAwake(True)
        path = self.path.copy(reverse=True)
        if len(path.waypoints) > 1:
            path.waypoints[0].behavior = WaypointBehavior.CONTINUE
            path.waypoints[-1].direction = self.path.waypoints[0].direction
            self.searching = True
        waypoints = []
        first_direction = path.waypoints[0].direction
        for waypoint in path.waypoints:
            if waypoint.direction == first_direction:
                waypoints.append(waypoint)
            else:
                break
        if first_direction == Direction.UP:
            waypoints[-1].direction = Direction.DOWN
        elif first_direction == Direction.DOWN:
            waypoints[-1].direction = Direction.UP
        elif first_direction == Direction.LEFT:
            waypoints[-1].direction = Direction.RIGHT
        elif first_direction == Direction.RIGHT:
            waypoints[-1].direction = Direction.LEFT
        self.path = Path(waypoints)

    def reset_path(self):
        self.returning_to_patrol_path = False
        self.path = self.originalPath.copy()
        self.moveSpeed = 60

    def reachedWaypoint(self, currLevelMap, player):
        if self.wait_for_new_path:
            self.wait_for_new_path = False
            nx = self.getTileX()
            ny = self.getTileY()
            self.awaiting_path = currLevelMap.findPath(nx, ny, player.getTileX(), player.getTileY())
            self.stopMoving()
        elif self.wait_for_return_path:
            self.wait_for_return_path = False
            nx = self.getTileX()
            ny = self.getTileY()
            start = self.originalPath.getStartingWaypoint()
            start_x = start.getTileX()
            start_y = start.getTileY()
            self.awaiting_path = currLevelMap.findPath(nx, ny, start_x, start_y)
            self.stopMoving()
            self.returning_to_patrol_path = True
        if self.awaiting_path is not None:
            if self.awaiting_path.done():
                self.path = self.awaiting_path.result()
                self.waypoint = self.path.getNextWaypoint()
                # Indicate that we are no longer awaiting a path
                self.awaiting_path = None
            else:
                # Waiting for our path
                return
        if self.waitTimer > 0:
            # NPC is waiting, don't move on to the next waypoint yet
            return
        if not self.suspicious or self.moving:
            self.changeDirection(self.waypoint.direction)
        if self.waypoint.behavior == WaypointBehavior.CONTINUE:
            self.startMoving(self.waypoint.direction)
        elif self.waypoint.behavior == WaypointBehavior.STOP:
            if not self.friendly and self.alert and not self.returning_to_patrol_path:
                self.scatter()
            elif not self.friendly and self.returning_to_patrol_path:
                self.reset_path()
            else:
                self.stopMoving()
        elif self.waypoint.behavior == WaypointBehavior.SLEEP:
            self.setAwake(False)
            self.hideVisionCone()
            self.stopMoving()
        elif self.waypoint.behavior == WaypointBehavior.DIE:
            self.alive = False
            self.stopMoving()
        elif self.waypoint.behavior == WaypointBehavior.WARP:
            self.alive = False
            self.stopMoving()
        elif self.waypoint.behavior == WaypointBehavior.EXPLODE:
            EventManager.explosion(self.x, self.y, damage=0)
            self.alive = False
            self.stopMoving()
        elif self.waypoint.behavior == WaypointBehavior.WAIT:
            self.stopMoving()
            self.waitTimer = 3
        elif self.waypoint.behavior == WaypointBehavior.WAIT_LONGER:
            self.stopMoving()
            self.waitTimer = 6
        elif self.waypoint.behavior == WaypointBehavior.WAIT_LONGEST:
            self.stopMoving()
            self.waitTimer = 12
        self.nextDirection = self.waypoint.direction
        self.waypoint = self.path.getNextWaypoint()

    def check_reached_waypoint(self, currLevelMap, player):
        if (self.x + 14 >= self.waypoint.x + 14 and self.x + 17 <= self.waypoint.x + 17) and (self.y - 14 <= self.waypoint.y - 14 and self.y - 17 >= self.waypoint.y - 17):
            self.reachedWaypoint(currLevelMap, player)
            return True
        return False

    def lookAtPlayer(self, player):
        dx = player.x - self.x
        dy = player.y - self.y
        mx = dx * dx
        my = dy * dy
        if mx > my:
            if dx > 0:
                self.changeDirection(Direction.RIGHT)
            else:
                self.changeDirection(Direction.LEFT)
        else:
            if dy > 0:
                self.changeDirection(Direction.UP)
            else:
                self.changeDirection(Direction.DOWN)

    def attack(self, player, bulletManager):
        #self.moving = False
        dx = 0
        dy = 0
        sx = self.x
        sy = self.y
        velocity = 1200
        if self.direction == Direction.UP:
            #self.bottom.updateSprite(6,5)
            #self.top.updateSprite(7,5)
            sy += 27
        elif self.direction == Direction.DOWN:
            #self.bottom.updateSprite(4,5)
            #self.top.updateSprite(5,5)
            sy += 16
        elif self.direction == Direction.LEFT:
            #self.bottom.updateSprite(0,8)
            #self.top.updateSprite(1,8)
            sy += 16
        elif self.direction == Direction.RIGHT:
            #self.bottom.updateSprite(2,8)
            #self.top.updateSprite(3,8)
            sy += 16
        x = player.x - self.x
        y = player.y - self.y

        normal = math.sqrt(float(x * x) + float(y * y))
        if normal > 0:
            nX = x/normal
            nY = y/normal
            dx = velocity * nX
            dy = velocity * nY
            SoundManager.play_sfx('rifle_gunshot')
            bulletManager.add(sx, sy, dx, dy, 1024, self.damage, WeaponType.CARBINE, not self.friendly)

    def enter_alert_phase(self):
        if not self.alert:
            SoundManager.play_music('alert')
            SoundManager.play_sfx('alarm')
        self.setAwake(True)
        self.alert = True
        self.moveSpeed = 90
        EventManager.set_notification('DANGER! You have been spotted!')
        #print("Player spotted from " + str((self.getTileX(), self.getTileY())))
        if not self.moving:
            if self.waitTimer > 0:
                self.waitTimer = 0.01
        self.wait_for_new_path = True
        self.returning_to_patrol_path = False
        self.wait_for_return_path = False
        self.searching = False
        self.searching_time = 0

    def on_alert(self):
        if not self.friendly:
            self.enter_alert_phase()
        else:
            self.alert = True

    def cancel_alert(self):
        self.alert = False
        if not self.friendly:
            self.searching = False
            self.searching_time = 0
            self.wait_for_return_path = True

    def updateStunState(self):
        self.tempStunned = False
        if self.stunTimer < 9.4 and self.stunTimer >= 0.4:
            self.top.updateSprite(7,7)
            self.bottom.updateSprite(6,7)
        elif self.stunTimer < 10 and self.stunTimer >= 9.4:
            self.top.updateSprite(7,6)
            self.bottom.updateSprite(6,6)
        elif self.stunTimer >= 10:
            self.stunned = False
            self.stamina = 100
            self.updateVisionCone()
            self.stopMoving()
            self.resumePostEvent()

    def update(self, dt, currLevelMap, player, bulletManager):
        super(NPC, self).update(dt, currLevelMap.obstacleMatrix, currLevelMap.doors)
        if self.alive:
            if not self.stunned and not self.tempStunned:
                if self.shotTimer > 0:
                    self.shotTimer -= dt
                if self.waitTimer > 0:
                    self.waitTimer -= dt
                    if self.waitTimer <= 0:
                        self.startMoving(self.nextDirection)
                self.check_reached_waypoint(currLevelMap, player)

            self.radarUnit.setPosition((self.x / 4) + 2, (self.y / 4)+4)
            self.updateDialogMarker()
            self.updateSleepMarker()

            if not self.friendly:
                if self.moving == False and self.alert == True and self.stunned == False and self.tempStunned == False and self.awake:
                    self.lookAtPlayer(player)
                if not self.stunned and not self.tempStunned and self.awake:
                    # Search for player
                    found = searchForPlayer(self.x, self.y, self.direction, self.viewDistance, currLevelMap, player)
                    if found:
                        if self.shotTimer <= 0:
                            self.attack(player, bulletManager)
                            self.shotTimer = self.shotCooldown
                        if self.canSeePlayer == False:
                            if self.suspicious:
                                self.suspicious = False
                                self.stopMoving()
                                self.resumePostEvent()
                            EventManager.alert(id(self))
                        self.canSeePlayer = True
                    else:
                        self.canSeePlayer = False
                    if not self.canSeePlayer and self.searching:
                        self.searching_time += dt
                        if self.searching_time >= 10:
                            EventManager.cancel_alert()
                    if self.suspicious:
                        if self.suspiciousTimer >= 1.5:
                            self.suspicious = False
                            self.stopMoving()
                            self.resumePostEvent()
                        self.suspiciousTimer += dt
                else:
                    if not self.awake:
                        if self.getTileX() == player.getTileX() and self.getTileY() == player.getTileY():
                            # Player has walked into sleeping enemy NPC
                            EventManager.alert(id(self))
                    if self.stunned:
                        self.updateStunState()
                        self.stunTimer += dt
                    elif self.tempStunned:
                        if self.tempStunTimer >= 0.35:
                            self.tempStunned = False
                            self.updateVisionCone()
                            self.suspiciousTimer = 0
                            self.suspicious = True
                            self.lookAtPlayer(player)
                        self.tempStunTimer += dt
            self.lastX = self.x
            self.lastY = self.y

'''
Colors that look good:
(191,127,63) or (127,96,63) for tan guard
(127,127,63) for green guard
(127,96,127) or (159,127,159) for purple guard
(31,96,127) or (63,96,127) for blue guard
(255,255,0) for yellow guard
(127,127,127) for grey guard
'''
class Guard(NPC):
    def __init__(self, x, y, waypoints, damage, batches, batchGroups, color=(255,255,255), light_color=None):
        super(Guard, self).__init__(x, y, guard_sprites, False, waypoints, None, damage, batches, batchGroups, color)
        self.light_color = light_color
        self.light = None
        if self.light_color is not None:
            self.create_light()

    def create_light(self):
        self.light = lighting.create_light(x=self.x+16, y=self.y+32, radius=128, color=self.light_color)

    def destroy_light(self):
        lighting.destroy_light(self.light)
        self.light = None

    def copy(self):
        return Guard(self.getTileX(), self.getTileY(), self.path.waypoints, self.damage, self.batches, self.batchGroups, self.color, self.light_color)

    def hear_gunshot(self, player, currLevelMap, radius_squared):
        audible = super(Guard, self).hear_gunshot(player, currLevelMap, radius_squared)
        if audible:
            EventManager.alert(id(self))

    def receiveDamage(self, damage, player=None):
        super(Guard, self).receiveDamage(damage)
        if not self.awake:
            if self.waypoint.behavior == WaypointBehavior.SLEEP:
                self.waypoint.behavior = WaypointBehavior.STOP
        self.setAwake(True)
        if player is not None:
            if not self.stunned:
                self.tempStunTimer = 0
                if not self.tempStunned and not self.suspicious:
                    self.tempStunned = True
                    self.hideVisionCone()
                    self.cacheLastDirection()
                    self.stopMoving()

    def update(self, dt, currLevelMap, player, bulletManager):
        super(Guard, self).update(dt, currLevelMap, player, bulletManager)
        if self.light is not None:
            self.light.set_position(self.x+16, self.y+32)

    def cleanUp(self):
        super(Guard, self).cleanUp()
        if self.light is not None:
            self.destroy_light()

class Commando(Guard):
    def __init__(self, x, y, waypoints, damage, batches, batchGroups, light_color=None):
        super(Guard, self).__init__(x, y, commando_sprites, False, waypoints, None, damage, batches, batchGroups)
        self.light_color = light_color
        self.light = None
        if self.light_color is not None:
            self.create_light()
        self.health = 150
        self.maxHealth = 150

    def copy(self):
        return Commando(self.getTileX(), self.getTileY(), self.path.waypoints, self.damage, self.batches, self.batchGroups, self.light_color)

class FriendlyCommando(NPC):
    def __init__(self, x, y, waypoints, batches, batchGroups, dialog=None):
        super(FriendlyCommando, self).__init__(x, y, commando_sprites, True, waypoints, dialog, 100, batches, batchGroups)
        self.health = 500
        self.maxHealth = 500

    def copy(self):
        return FriendlyCommando(self.getTileX(), self.getTileY(), self.path.waypoints, self.batches, self.batchGroups, self.dialog)

class Brother_1945(NPC):
    def __init__(self, x, y, waypoints, batches, batchGroups, dialog=None):
        super(Brother_1945, self).__init__(x, y, brother_1945_sprites, True, waypoints, dialog, 100, batches, batchGroups)
        self.health = 500
        self.maxHealth = 500

    def copy(self):
        return Brother_1945(self.getTileX(), self.getTileY(), self.path.waypoints, self.batches, self.batchGroups, self.dialog)

class FemaleScientist(NPC):
    def __init__(self, x, y, waypoints, batches, batchGroups, dialog=None):
        super(FemaleScientist, self).__init__(x, y, f_scientist_sprites, True, waypoints, dialog, 0, batches, batchGroups)

    def copy(self):
        return FemaleScientist(self.getTileX(), self.getTileY(), self.path.waypoints, self.batches, self.batchGroups, self.dialog)

class Scientist(NPC):
    def __init__(self, x, y, waypoints, batches, batchGroups, dialog=None):
        super(Scientist, self).__init__(x, y, scientist_sprites, True, waypoints, dialog, 0, batches, batchGroups)
    def copy(self):
        return Scientist(self.getTileX(), self.getTileY(), self.path.waypoints, self.batches, self.batchGroups, self.dialog)

class Wolfgang_1945(NPC):
    def __init__(self, x, y, waypoints, batches, batchGroups):
        super(Wolfgang_1945, self).__init__(x, y, wolfgang_sprites, False, waypoints, None, 200, batches, batchGroups)
        self.health = 10000
        self.maxHealth = 10000
        self.viewDistance = 768
        self.radarViewDistance = self.viewDistance/4
        self.halfVisionConeHeight = getHalfVisionConeHeight(self.radarViewDistance)

    def copy(self):
        return Wolfgang_1945(self.getTileX(), self.getTileY(), self.path.waypoints, self.batches, self.batchGroups)

    def getPunched(self, x1, x2, y1, y2, player=None):
        tx = self.getTileX()
        ty = self.getTileY()
        if tx >= x1 and tx <= x2 and ty >= y1 and ty <= y2:
            print("Wolfgang has been punched! It had no effect...")
            self.receiveDamage(0)

class Wolfgang_1973(NPC):
    def __init__(self, x, y, waypoints, batches, batchGroups, dialog=None):
        super(Wolfgang_1973, self).__init__(x, y, wolfgang73_sprites, True, waypoints, dialog, 0, batches, batchGroups)

    def copy(self):
        return Wolfgang_1973(self.getTileX(), self.getTileY(), self.path.waypoints, self.batches, self.batchGroups, self.dialog)

class BadMan(NPC):
    def __init__(self, x, y, waypoints, batches, batchGroups, dialog=None):
        super(BadMan, self).__init__(x, y, bad_man_sprites, True, waypoints, dialog, 100, batches, batchGroups)

    def copy(self):
        return BadMan(self.getTileX(), self.getTileY(), self.path.waypoints, self.batches, self.batchGroups, self.dialog)

class Tortured_BadMan(NPC):
    def __init__(self, x, y, batches, batchGroups):
        super(Tortured_BadMan, self).__init__(x, y, tortured_bad_man_sprites, False, [], None, 0, batches, batchGroups, no_movement=True)
        self.viewDistance = 0
        self.direction = Direction.DOWN
        self.stamina = 50

    def copy(self):
        return Tortured_BadMan(self.getTileX(), self.getTileY(), self.batches, self.batchGroups)

    def _receive_stamina_damage(self, damage):
        if self.stamina > 0:
            self.receiveDamage(0)
            self.damageTimer = 0
            self.top.setAlpha(255)
            self.bottom.setAlpha(255)
            self.stamina -= damage
        if self.stamina == 40:
            self.bottom.updateSprite(0,1)
            self.top.updateSprite(1,1)
        elif self.stamina == 30:
            self.bottom.updateSprite(0,2)
            self.top.updateSprite(1,2)
        elif self.stamina == 20:
            self.bottom.updateSprite(0,3)
            self.top.updateSprite(1,3)
        elif self.stamina == 10:
            self.bottom.updateSprite(0,4)
            self.top.updateSprite(1,4)
        elif self.stamina <= 0:
            self.bottom.updateSprite(0,5)
            self.top.updateSprite(1,5)

    def getPunched(self, x1, x2, y1, y2, player=None):
        tx = self.getTileX()
        ty = self.getTileY()
        if tx >= x1 and tx <= x2 and ty >= y1 and ty <= y2:
            print("Tortured NPC was punched!")
            self._receive_stamina_damage(10)

    def receiveDamage(self, damage, player=None):
        if damage > 0:
            damage = self.maxHealth - 1
        super(Tortured_BadMan, self).receiveDamage(damage)
        if damage > 0:
            self._receive_stamina_damage(40)

    def move(self, dx, dy, dt, obstacleMatrix, doors):
        pass

    def startMoving(self, moveDirection):
        pass
    
    def changeDirection(self, moveDirection):
        pass

    def stopMoving(self):
        pass

    def update(self, dt, currLevelMap, player, bulletManager):
        self.radarUnit.setPosition((self.x / 4) + 2, (self.y / 4)+4)
        if self.damageTimer > 0:
            self.damageTimer -= 1
            if self.damageTimer == 0:
                self.top.setAlpha(255)
                self.bottom.setAlpha(255)
            else:
                self.top.setAlpha(128)
                self.bottom.setAlpha(128)

class Tortured_SS(NPC):
    def __init__(self, x, y, batches, batchGroups):
        super(Tortured_SS, self).__init__(x, y, tortured_ss_sprites, False, [], None, 0, batches, batchGroups, no_movement=True)
        self.viewDistance = 0
        self.direction = Direction.DOWN
        self.stamina = 100

    def copy(self):
        return Tortured_SS(self.getTileX(), self.getTileY(), self.batches, self.batchGroups)

    def getPunched(self, x1, x2, y1, y2, player=None):
        tx = self.getTileX()
        ty = self.getTileY()
        if tx >= x1 and tx <= x2 and ty >= y1 and ty <= y2:
            print("Tortured NPC was punched!")
            if self.stamina > 0:
                self.receiveDamage(0)
                self.damageTimer = 0
                self.top.setAlpha(255)
                self.bottom.setAlpha(255)
                self.stamina -= 10
            if self.stamina == 90:
                self.bottom.updateSprite(0,1)
                self.top.updateSprite(1,1)
            elif self.stamina == 80:
                self.bottom.updateSprite(0,2)
                self.top.updateSprite(1,2)
            elif self.stamina == 70:
                self.bottom.updateSprite(0,3)
                self.top.updateSprite(1,3)
            elif self.stamina == 50:
                self.bottom.updateSprite(0,4)
                self.top.updateSprite(1,4)
            elif self.stamina == 20:
                self.bottom.updateSprite(0,5)
                self.top.updateSprite(1,5)
            elif self.stamina == 10:
                self.bottom.updateSprite(0,6)
                self.top.updateSprite(1,6)
            elif self.stamina == 0:
                self.bottom.updateSprite(0,7)
                self.top.updateSprite(1,7)
    
    def move(self, dx, dy, dt, obstacleMatrix, doors):
        pass

    def startMoving(self, moveDirection):
        pass
    
    def changeDirection(self, moveDirection):
        pass

    def stopMoving(self):
        pass

    def update(self, dt, currLevelMap, player, bulletManager):
        self.radarUnit.setPosition((self.x / 4) + 2, (self.y / 4)+4)
        if self.damageTimer > 0:
            self.damageTimer -= 1
            if self.damageTimer == 0:
                self.top.setAlpha(255)
                self.bottom.setAlpha(255)
            else:
                self.top.setAlpha(128)
                self.bottom.setAlpha(128)
    
class Relict_Idle(NPC):
    def __init__(self, x, y, batches, batchGroups):
        super(Relict_Idle, self).__init__(x, y, relict_sprites, False, [], None, 0, batches, batchGroups)
        self.viewDistance = 0

    def copy(self):
        return Relict_Idle(self.getTileX(), self.getTileY(), self.batches, self.batchGroups)

    def update(self, dt, currLevelMap, player, bulletManager):
        self.radarUnit.setPosition((self.x / 4) + 2, (self.y / 4)+4)
        self.lookAtPlayer(player)

    def updateVisionCone(self):
        pass

class Guard_Blind(NPC):
    def __init__(self, x, y, waypoints, batches, batchGroups, dialog=None):
        super(Guard_Blind, self).__init__(x, y, guard_sprites, False, waypoints, dialog, 100, batches, batchGroups)
        self.viewDistance = 0

    def copy(self):
        return Guard_Blind(self.getTileX(), self.getTileY(), self.path.waypoints, self.batches, self.batchGroups, self.dialog)

# These NPCs have adjustable hair and clothes colors
class Man(NPC):
    def __init__(self, x, y, waypoints, batches, batchGroups, hair_color=(255,255,255), shirt_color=(255,255,255), pants_color=(255,255,255), dialog=None, script=None):
        super(Man, self).__init__(x, y, man_sprites, True, waypoints, dialog, 0, batches, batchGroups, script=script)
        self.hair_color = hair_color
        self.shirt_color = shirt_color
        self.pants_color = pants_color
        hair_opacity = 255
        shirt_opacity = 255
        pants_opacity = 255
        if hair_color is None:
            hair_opacity = 0
            hair_color = (255,255,255)
        if shirt_color is None:
            shirt_opacity = 0
            shirt_color = (255,255,255)
        if pants_color is None:
            pants_opacity = 0
            pants_color = (255,255,255)
        self.clothes_bottom = DynamicObject(x, y, man_shirt, 4, 2, batches['main'], batchGroups['pants'], shirt_color, opacity=shirt_opacity)
        self.clothes_top = DynamicObject(x, y+1, man_shirt, 5, 2, batches['main'], batchGroups['shirts'], shirt_color, opacity=shirt_opacity)
        self.pants = DynamicObject(x, y, man_pants, 4, 2, batches['main'], batchGroups['pants'], pants_color, opacity=pants_opacity)
        self.hair = DynamicObject(x, y+1, hair, 4, 2, batches['main'], batchGroups['hair'], hair_color, opacity=hair_opacity)
        # Keep track of dynamic objects
        self.all_dynamic_objects.append(self.clothes_bottom)
        self.all_dynamic_objects.append(self.clothes_top)
        self.all_dynamic_objects.append(self.pants)
        self.all_dynamic_objects.append(self.hair)

    def copy(self):
        return Man(self.getTileX(), self.getTileY(), self.path.waypoints, self.batches, self.batchGroups, self.hair_color, self.shirt_color, self.pants_color, self.dialog, self.script)

    def initialize(self):
        super(NPC, self).initialize()
        self.clothes_top.updateSprite(self.clothes_top.imagerow, self.clothes_top.imagecol)
        self.clothes_bottom.updateSprite(self.clothes_bottom.imagerow, self.clothes_bottom.imagecol)
        self.pants.updateSprite(self.pants.imagerow, self.pants.imagecol)
        self.hair.updateSprite(self.hair.imagerow, self.hair.imagecol)

    def startMoving(self, moveDirection):
        if self.direction != moveDirection:
            # Change sprite
            self.currFrameX = 0
            self.maxFrameX = len(self.animationIndex[self.prone][moveDirection])
            self.animationStep = (self.animationCycles)/self.maxFrameX
            if moveDirection == Direction.UP:
                self.bottom.updateSprite(6,2)
                self.top.updateSprite(7,2)
                self.hair.updateSprite(6,2)
                self.clothes_top.updateSprite(7,2)
                self.clothes_bottom.updateSprite(6,2)
                self.pants.updateSprite(6,2)
            elif moveDirection == Direction.DOWN:
                self.bottom.updateSprite(4,2)
                self.top.updateSprite(5,2)
                self.hair.updateSprite(4,2)
                self.clothes_top.updateSprite(5,2)
                self.clothes_bottom.updateSprite(4,2)
                self.pants.updateSprite(4,2)
            elif moveDirection == Direction.LEFT:
                self.bottom.updateSprite(0,0)
                self.top.updateSprite(1,0)
                self.hair.updateSprite(0,0)
                self.clothes_top.updateSprite(1,0)
                self.clothes_bottom.updateSprite(0,0)
                self.pants.updateSprite(0,0)
            elif moveDirection == Direction.RIGHT:
                self.bottom.updateSprite(2,0)
                self.top.updateSprite(3,0)
                self.hair.updateSprite(2,0)
                self.clothes_top.updateSprite(3,0)
                self.clothes_bottom.updateSprite(2,0)
                self.pants.updateSprite(2,0)
            self.frame = 0
        self.direction = moveDirection
        if self.moving == False:
            if self.top_left is not None:
                self.top_left.destroy()
            if self.top_right is not None:
                self.top_right.destroy()
        self.moving = True

    def stopMoving(self):
        # Change sprite
        self.currFrameX = 0
        self.maxFrameX = len(self.animationIndex[self.prone][self.direction])
        self.animationStep = (self.animationCycles)/self.maxFrameX
        if self.direction == Direction.UP:
            self.bottom.updateSprite(6,2)
            self.top.updateSprite(7,2)
            self.hair.updateSprite(6,2)
            self.clothes_top.updateSprite(7,2)
            self.clothes_bottom.updateSprite(6,2)
            self.pants.updateSprite(6,2)
        elif self.direction == Direction.DOWN:
            self.bottom.updateSprite(4,2)
            self.top.updateSprite(5,2)
            self.hair.updateSprite(4,2)
            self.clothes_top.updateSprite(5,2)
            self.clothes_bottom.updateSprite(4,2)
            self.pants.updateSprite(4,2)
        elif self.direction == Direction.LEFT:
            self.bottom.updateSprite(0,0)
            self.top.updateSprite(1,0)
            self.hair.updateSprite(0,0)
            self.clothes_top.updateSprite(1,0)
            self.clothes_bottom.updateSprite(0,0)
            self.pants.updateSprite(0,0)
        elif self.direction == Direction.RIGHT:
            self.bottom.updateSprite(2,0)
            self.top.updateSprite(3,0)
            self.hair.updateSprite(2,0)
            self.clothes_top.updateSprite(3,0)
            self.clothes_bottom.updateSprite(2,0)
            self.pants.updateSprite(2,0)
        if self.top_left is not None:
            self.top_left.destroy()
        if self.top_right is not None:
            self.top_right.destroy()
        self.frame = 0
        self.moving = False

    def update(self, dt, currLevelMap, player, bulletManager):
        self._update(dt, currLevelMap.obstacleMatrix, currLevelMap.doors)
        if self.alive:
            if not self.stunned:
                if self.shotTimer > 0:
                    self.shotTimer -= dt
                if self.waitTimer > 0:
                    self.waitTimer -= dt
                    if self.waitTimer <= 0:
                        self.startMoving(self.nextDirection)
                self.check_reached_waypoint(currLevelMap, player)

            self.radarUnit.setPosition((self.x / 4) + 2, (self.y / 4)+4)
            self.updateDialogMarker()
            self.updateSleepMarker()

    def _update(self, dt, obstacleMatrix, doors):
        if self.moving == True:
            if self.direction == Direction.UP:
                self.move(0, self.moveSpeed, dt, obstacleMatrix, doors)
            elif self.direction == Direction.DOWN:
                self.move(0, -1 * self.moveSpeed, dt, obstacleMatrix, doors)
            elif self.direction == Direction.LEFT:
                self.move(-1 * self.moveSpeed, 0, dt, obstacleMatrix, doors)
            elif self.direction == Direction.RIGHT:
                self.move(self.moveSpeed, 0, dt, obstacleMatrix, doors)
            self.frame += 1
            if self.frame >= self.animationCycles:
                self.frame = 0
            nextFrameX = int(self.frame / self.animationStep)
            if nextFrameX != self.currFrameX:
                self.currFrameX = nextFrameX
                if self.currFrameX < len(self.animationIndex[self.prone][self.direction]):
                    frameColumn = self.animationIndex[self.prone][self.direction][self.currFrameX]
                    self.bottom.updateSprite(self.bottom.imagerow, frameColumn)
                    self.top.updateSprite(self.top.imagerow, frameColumn)
                    self.hair.updateSprite(self.hair.imagerow, frameColumn)
                    self.clothes_top.updateSprite(self.clothes_top.imagerow, frameColumn)
                    self.clothes_bottom.updateSprite(self.clothes_bottom.imagerow, frameColumn)
                    self.pants.updateSprite(self.pants.imagerow, frameColumn)
        if self.damageTimer > 0:
            self.damageTimer -= 1
            if self.damageTimer == 0:
                self.top.setAlpha(255)
                self.bottom.setAlpha(255)
                self.hair.setAlpha(255)
                self.clothes_top.setAlpha(255)
                self.clothes_bottom.setAlpha(255)
                self.pants.setAlpha(255)
            else:
                self.top.setAlpha(128)
                self.bottom.setAlpha(128)
                self.hair.setAlpha(128)
                self.clothes_top.setAlpha(128)
                self.clothes_bottom.setAlpha(128)
                self.pants.setAlpha(128)

    def jump(self, x, y):
        super(Man, self).jump(x, y)
        self.hair.jump(x, y+1)
        self.clothes_top.jump(x, y+1)
        self.clothes_bottom.jump(x, y)
        self.pants.jump(x, y)

class Woman(NPC):
    def __init__(self, x, y, waypoints, batches, batchGroups, hair_color=(255,255,255), dress_color=(255,255,255), dialog=None, script=None):
        super(Woman, self).__init__(x, y, woman_sprites, True, waypoints, dialog, 0, batches, batchGroups, script=script)
        self.hair_color = hair_color
        self.dress_color = dress_color
        hair_opacity = 255
        dress_opacity = 255
        if hair_color is None:
            hair_opacity = 0
            hair_color = (255,255,255)
        if dress_color is None:
            dress_opacity = 0
            dress_color = (255,255,255)
        self.clothes_bottom = DynamicObject(x, y, woman_dress, 4, 2, batches['main'], batchGroups['pants'], dress_color, opacity=dress_opacity)
        self.clothes_top = DynamicObject(x, y+1, woman_dress, 5, 2, batches['main'], batchGroups['shirts'], dress_color, opacity=dress_opacity)
        self.hair = DynamicObject(x, y+1, hair, 5, 2, batches['main'], batchGroups['hair'], hair_color, opacity=hair_opacity)
        # Keep track of dynamic objects
        self.all_dynamic_objects.append(self.clothes_bottom)
        self.all_dynamic_objects.append(self.clothes_top)
        self.all_dynamic_objects.append(self.hair)

    def copy(self):
        return Woman(self.getTileX(), self.getTileY(), self.path.waypoints, self.batches, self.batchGroups, self.hair_color, self.dress_color, self.dialog, self.script)

    def initialize(self):
        super(NPC, self).initialize()
        self.clothes_top.updateSprite(self.clothes_top.imagerow, self.clothes_top.imagecol)
        self.clothes_bottom.updateSprite(self.clothes_bottom.imagerow, self.clothes_bottom.imagecol)
        self.hair.updateSprite(self.hair.imagerow, self.hair.imagecol)

    def startMoving(self, moveDirection):
        if self.direction != moveDirection:
            # Change sprite
            self.currFrameX = 0
            self.maxFrameX = len(self.animationIndex[self.prone][moveDirection])
            self.animationStep = (self.animationCycles)/self.maxFrameX
            if moveDirection == Direction.UP:
                self.bottom.updateSprite(6,2)
                self.top.updateSprite(7,2)
                self.hair.updateSprite(7,2)
                self.clothes_top.updateSprite(7,2)
                self.clothes_bottom.updateSprite(6,2)
            elif moveDirection == Direction.DOWN:
                self.bottom.updateSprite(4,2)
                self.top.updateSprite(5,2)
                self.hair.updateSprite(5,2)
                self.clothes_top.updateSprite(5,2)
                self.clothes_bottom.updateSprite(4,2)
            elif moveDirection == Direction.LEFT:
                self.bottom.updateSprite(0,0)
                self.top.updateSprite(1,0)
                self.hair.updateSprite(1,0)
                self.clothes_top.updateSprite(1,0)
                self.clothes_bottom.updateSprite(0,0)
            elif moveDirection == Direction.RIGHT:
                self.bottom.updateSprite(2,0)
                self.top.updateSprite(3,0)
                self.hair.updateSprite(3,0)
                self.clothes_top.updateSprite(3,0)
                self.clothes_bottom.updateSprite(2,0)
            self.frame = 0
        self.direction = moveDirection
        if self.moving == False:
            if self.top_left is not None:
                self.top_left.destroy()
            if self.top_right is not None:
                self.top_right.destroy()
        self.moving = True

    def stopMoving(self):
        # Change sprite
        self.currFrameX = 0
        self.maxFrameX = len(self.animationIndex[self.prone][self.direction])
        self.animationStep = (self.animationCycles)/self.maxFrameX
        if self.direction == Direction.UP:
            self.bottom.updateSprite(6,2)
            self.top.updateSprite(7,2)
            self.hair.updateSprite(7,2)
            self.clothes_top.updateSprite(7,2)
            self.clothes_bottom.updateSprite(6,2)
        elif self.direction == Direction.DOWN:
            self.bottom.updateSprite(4,2)
            self.top.updateSprite(5,2)
            self.hair.updateSprite(5,2)
            self.clothes_top.updateSprite(5,2)
            self.clothes_bottom.updateSprite(4,2)
        elif self.direction == Direction.LEFT:
            self.bottom.updateSprite(0,0)
            self.top.updateSprite(1,0)
            self.hair.updateSprite(1,0)
            self.clothes_top.updateSprite(1,0)
            self.clothes_bottom.updateSprite(0,0)
        elif self.direction == Direction.RIGHT:
            self.bottom.updateSprite(2,0)
            self.top.updateSprite(3,0)
            self.hair.updateSprite(3,0)
            self.clothes_top.updateSprite(3,0)
            self.clothes_bottom.updateSprite(2,0)
        if self.top_left is not None:
            self.top_left.destroy()
        if self.top_right is not None:
            self.top_right.destroy()
        self.frame = 0
        self.moving = False

    def update(self, dt, currLevelMap, player, bulletManager):
        self._update(dt, currLevelMap.obstacleMatrix, currLevelMap.doors)
        if self.alive:
            if not self.stunned:
                if self.shotTimer > 0:
                    self.shotTimer -= dt
                if self.waitTimer > 0:
                    self.waitTimer -= dt
                    if self.waitTimer <= 0:
                        self.startMoving(self.nextDirection)
                self.check_reached_waypoint(currLevelMap, player)

            self.radarUnit.setPosition((self.x / 4) + 2, (self.y / 4)+4)
            self.updateDialogMarker()
            self.updateSleepMarker()

    def _update(self, dt, obstacleMatrix, doors):
        if self.moving == True:
            if self.direction == Direction.UP:
                self.move(0, self.moveSpeed, dt, obstacleMatrix, doors)
            elif self.direction == Direction.DOWN:
                self.move(0, -1 * self.moveSpeed, dt, obstacleMatrix, doors)
            elif self.direction == Direction.LEFT:
                self.move(-1 * self.moveSpeed, 0, dt, obstacleMatrix, doors)
            elif self.direction == Direction.RIGHT:
                self.move(self.moveSpeed, 0, dt, obstacleMatrix, doors)
            self.frame += 1
            if self.frame >= self.animationCycles:
                self.frame = 0
            nextFrameX = int(self.frame / self.animationStep)
            if nextFrameX != self.currFrameX:
                self.currFrameX = nextFrameX
                if self.currFrameX < len(self.animationIndex[self.prone][self.direction]):
                    frameColumn = self.animationIndex[self.prone][self.direction][self.currFrameX]
                    self.bottom.updateSprite(self.bottom.imagerow, frameColumn)
                    self.top.updateSprite(self.top.imagerow, frameColumn)
                    self.hair.updateSprite(self.hair.imagerow, frameColumn)
                    self.clothes_top.updateSprite(self.clothes_top.imagerow, frameColumn)
                    self.clothes_bottom.updateSprite(self.clothes_bottom.imagerow, frameColumn)
        if self.damageTimer > 0:
            self.damageTimer -= 1
            if self.damageTimer == 0:
                self.top.setAlpha(255)
                self.bottom.setAlpha(255)
                self.hair.setAlpha(255)
                self.clothes_top.setAlpha(255)
                self.clothes_bottom.setAlpha(255)
            else:
                self.top.setAlpha(128)
                self.bottom.setAlpha(128)
                self.hair.setAlpha(128)
                self.clothes_top.setAlpha(128)
                self.clothes_bottom.setAlpha(128)

    def jump(self, x, y):
        super(Woman, self).jump(x, y)
        self.hair.jump(x, y+1)
        self.clothes_top.jump(x, y+1)
        self.clothes_bottom.jump(x, y)

class Isabel(NPC):
    def __init__(self, x, y, waypoints, batches, batchGroups, dialog=None):
        super(Isabel, self).__init__(x, y, isabel_sprites, True, waypoints, dialog, 100, batches, batchGroups)
        self.health = 500
        self.maxHealth = 500

    def copy(self):
        return Isabel(self.getTileX(), self.getTileY(), self.path.waypoints, self.batches, self.batchGroups, self.dialog)

class Isabel_Dress(NPC):
    def __init__(self, x, y, waypoints, batches, batchGroups, dialog=None):
        super(Isabel_Dress, self).__init__(x, y, isabel_dress_sprites, True, waypoints, dialog, 100, batches, batchGroups)
        self.health = 500
        self.maxHealth = 500

    def copy(self):
        return Isabel_Dress(self.getTileX(), self.getTileY(), self.path.waypoints, self.batches, self.batchGroups, self.dialog)


class Rees_Interrogation(NPC):
    def __init__(self, x, y, batches, batchGroups):
        super(Rees_Interrogation, self).__init__(x, y, rees_interrogation_sprites, False, [], None, 0, batches, batchGroups, no_movement=True)
        self.viewDistance = 0
        self.direction = Direction.DOWN
        self.stamina = 30

    def copy(self):
        return Rees_Interrogation(self.getTileX(), self.getTileY(), self.batches, self.batchGroups)

    def getPunched(self, x1, x2, y1, y2, player=None):
        tx = self.getTileX()
        ty = self.getTileY()
        if tx >= x1 and tx <= x2 and ty >= y1 and ty <= y2:
            print("Tortured NPC was punched! %d" % self.stamina)
            if self.stamina > 0:
                self.receiveDamage(0)
                self.damageTimer = 0
                self.top.setAlpha(255)
                self.bottom.setAlpha(255)
                self.stamina -= 10
            if self.stamina == 20:
                self.bottom.updateSprite(0,1)
                self.top.updateSprite(1,1)
            elif self.stamina == 10:
                self.bottom.updateSprite(0,2)
                self.top.updateSprite(1,2)
            elif self.stamina == 0:
                self.bottom.updateSprite(0,3)
                self.top.updateSprite(1,3)
    
    def move(self, dx, dy, dt, obstacleMatrix, doors):
        pass

    def startMoving(self, moveDirection):
        pass
    
    def changeDirection(self, moveDirection):
        pass

    def stopMoving(self):
        pass

    def update(self, dt, currLevelMap, player, bulletManager):
        self.radarUnit.setPosition((self.x / 4) + 2, (self.y / 4)+4)
        if self.damageTimer > 0:
            self.damageTimer -= 1
            if self.damageTimer == 0:
                self.top.setAlpha(255)
                self.bottom.setAlpha(255)
            else:
                self.top.setAlpha(128)
                self.bottom.setAlpha(128)

class Inalco(NPC):
    def __init__(self, x, y, waypoints, batches, batchGroups, dialog=None):
        super(Inalco, self).__init__(x, y, inalco_sprites, True, waypoints, dialog, 100, batches, batchGroups)
        self.health = 1000
        self.maxHealth = 1000

    def copy(self):
        return Inalco(self.getTileX(), self.getTileY(), self.path.waypoints, self.batches, self.batchGroups, self.dialog)
