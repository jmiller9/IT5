import json
import pyglet
from it5.staticobject import StaticObject
from it5.dynamicobject import DynamicObject
from it5.item import Item
from it5.door import Door, DuctVent
from it5.direction import Direction
from it5.util import PathfindingProcessManager
from it5.waypoint import pathCreator, Waypoint
from it5.npc import Guard, Commando, Brother_1945, FemaleScientist, Isabel_Dress, Wolfgang_1945, Tortured_SS, Relict_Idle, Woman, Man, Isabel, Scientist, Rees_Interrogation, Inalco, FriendlyCommando, BadMan, Tortured_BadMan
from it5.surveillance_camera import SurveillanceCameraFactory
from it5.warp import Warp
from it5.dialog import DialogManager
from it5.tileset_manager import TilesetManager
from it5.quest import QuestManager, Stage
from it5.common import common_spritemap, common_tileset
from it5.trigger import Objective, QuestTrigger, DialogTrigger, ScriptTrigger
from it5.resources import ResourceManager
from it5.conditionals.spawn_conditionals import SpawnConditionalManager


'''
A LevelSet is defined as a collection of Levels
'''
class LevelSet(object):
    def __init__(self, location, batches, batchGroups, window, anchor_x):
        self.levels = []
        self.location = location
        self.batches = batches
        self.batchGroups = batchGroups
        self.window = window
        self.currentLevel = None
        self.index = -1
        self.anchor_x = anchor_x
        lsData = ResourceManager.load_json(location)
        for level in lsData['levels']:
            self.levels.append(str(level))

    def loadLevel(self, index):
        if index < len(self.levels):
            return Level(self.levels[index], self.batches, self.batchGroups, self.window, index, self.anchor_x)
        else:
            return None

    def to_dict(self):
        ls_dict = {
            'location': self.location
        }
        return ls_dict

'''
A Level is defined as a collection of LevelMaps
'''
class Level(object):
    def __init__(self, location, batches, batchGroups, window, index, anchor_x):
        self.maps = {}
        self.index = index
        self.currentMap = None
        self.dialogManager = DialogManager(batches['dialog'], batchGroups, window, anchor_x)
        self.name = ''
        self.startLocation = {
            "mapIndex": 0,
            "x": 1,
            "y": 1
        }
        self.load(location, batches, batchGroups)
    
    def getCurrentMap(self):
        return self.currentMap
    
    def changeMap(self, index):
        self.currentMap = self.maps[str(index)]
    
    def load(self, filename, batches, batchGroups):
        if filename != None:
            levelData = ResourceManager.load_json(filename)
            self.name = levelData['name']
            self.startLocation = levelData['start']
            if 'dialogs' in levelData:
                dialog_path = levelData['dialogs']
                self.dialogManager.loadDialogs(dialog_path)
            if 'quests' in levelData:
                QuestManager.initialize(levelData['quests'])
            else:
                QuestManager.initialize()
            maps = levelData['maps']
            if isinstance(maps, list):
                _maps = {}
                i = 0
                for lmap in maps:
                    _maps[str(i)] = lmap
                    i += 1
                maps = _maps
            for k, m in maps.items():
                levelMap = LevelMap(k)
                levelMap.load(m, batches, batchGroups)
                self.maps[k] = levelMap
            self.changeMap(self.startLocation['mapIndex'])

    def to_dict(self, recently_acquired_items, curr_map_index):
        maps = {}
        for key, level_map in self.maps.items():
            if level_map.index == curr_map_index:
                maps[key] = level_map.to_dict(recently_acquired_items)
            else:
                maps[key] = level_map.to_dict(None)
        dialogs = self.dialogManager.to_dict()
        level_dict = {
            "name": self.name,
            "index": self.index,
            "maps": maps,
            "dialogs": dialogs
        }
        return level_dict

    def from_dict(self, level_dict):
        for key, map_dict in level_dict['maps'].items():
            level_map = self.maps.get(key, None)
            if level_map is not None:
                level_map.from_dict(map_dict)
        self.dialogManager.from_dict(level_dict['dialogs'])

class LevelMap(object):
    def __init__(self, index):
        self.tileLayers = {}
        self.items = set()
        self.npcs = set()
        self.waypointMap = None
        self.originalNPCs = set()
        self.original_cameras = set()
        self.cameras = set()
        self.doors = []
        self.warps = []
        self.dialog = None
        self.weather = None
        self.soundtrack = None
        self.obstacleMatrix = []
        self.width = 3
        self.height = 3
        self.width_px = self.width * 32
        self.height_px = self.height * 32
        self.triggers = []
        self.objective_triggers = {}

        self.firstgid_map = None

        self.batches = {}
        self.radarObstacles = []
        self.index = index
        self.lights = []
        self.cover_regions = []
        self.ambience = (1.0, 1.0, 1.0)
        self.force_prone = False
        self.gas = False
        self.radar_jammed = False
        self.animation_objects = []
        self.metadata = {}
        self.lasers = []
    
    def initialize(self, active_quest):
        PathfindingProcessManager.change_map(self.obstacleMatrix)
        self.npcs.clear()
        for onpc, spawn_conditional in self.originalNPCs:
            if SpawnConditionalManager.should_spawn(spawn_conditional):
                npc = onpc.copy()
                self.npcs.add(npc)
                npc.initialize()
        for item in self.items:
            item.show()
        self.cameras.clear()
        for ocamera in self.original_cameras:
            camera = ocamera.copy()
            self.cameras.add(camera)
            camera.show()
        for door in self.doors:
            door.show()
        for layer in self.tileLayers:
            for tileList in self.tileLayers[layer]:
                for tile in tileList:
                    if tile != None:
                        tile.show()
        for trigger in self.triggers:
            if trigger.active:
                trigger.show()
        self.refreshObjectives(active_quest)
        self.create_radar_obstacles()
        for radarObstacle in self.radarObstacles:
            radarObstacle.show()
            radarObstacle.sprite.scale_x = 0.25
            radarObstacle.sprite.scale_y = 0.25
            radarObstacle.sprite.color = (0,255,63)
            radarObstacle.setPosition(radarObstacle.originalX*8, (radarObstacle.originalY*8)+12)
    
    def tearDown(self):
        for item in self.items:
            item.hide()
        for camera in self.cameras:
            camera.hide()
        for door in self.doors:
            door.forceClose()
            door.hide()
        for layer in self.tileLayers:
            for tileList in self.tileLayers[layer]:
                for tile in tileList:
                    if tile != None:
                        tile.hide()
        for npc in self.npcs:
            npc.cleanUp()
        for trigger in self.triggers:
            trigger.hide()
        for radarObstacle in self.radarObstacles:
            radarObstacle.hide()
        self.radarObstacles.clear()

    def refreshObjectives(self, active_quest):
        for quest_name in self.objective_triggers:
            triggers = self.objective_triggers[quest_name]
            if active_quest == quest_name:
                for trigger in triggers:
                    if trigger.active:
                        trigger.show()
            else:
                for trigger in triggers:
                    trigger.hide()

    def convert_props(self, original_props):
        '''
        Tiled decided to change how it stored custom properties, so support both formats
        '''
        if isinstance(original_props, list):
            props = {}
            for prop_struct in original_props:
                props[prop_struct['name']] = prop_struct['value']
            return props
        else:
            return original_props

    def extract_direction_from_props(self, props, default_direction=None):
        if default_direction is None:
            return Direction[props["direction"].upper()]
        else:
            return Direction[props.get("direction", default_direction.name).upper()]

    def _findPath(self, startX, startY, endX, endY):
        if self.pathfinder != None:
            print('Finding path from ' + str((startX, startY)) + ' to ' + str((endX, endY)))
            nodes = self.pathfinder.solve((startX, startY), (endX, endY))
            if nodes != None:
                return pathCreator(nodes)
        return pathCreator([(startX, startY)])

    def findPath(self, startX, startY, endX, endY):
        return PathfindingProcessManager.find_path((startX, startY), (endX, endY))

    def createNPCs(self, waypointMap, npcList):
        for npcBuilder in npcList:
            waypoints = []
            for waypointKey in npcBuilder['waypoints']:
                if waypointKey in waypointMap:
                    waypoints.append(waypointMap[waypointKey].copy())
            #TODO: Support more npc types
            npcType = npcBuilder['type']
            npc = None
            x = npcBuilder['x']
            y = npcBuilder['y']
            if len(waypoints) > 0:
                x = waypoints[0].x // 32
                y = (waypoints[0].y // 32) + 1
            color = (255,255,255)
            if 'color' in npcBuilder:
                color = npcBuilder['color']
            dialog = None
            if 'dialog' in npcBuilder:
                dialog = npcBuilder['dialog']
            script = None
            if 'script' in npcBuilder:
                script = npcBuilder['script']
            light_color = None
            if 'light_color' in npcBuilder:
                light_color = npcBuilder['light_color']
            spawn_conditional = npcBuilder.get('spawn_conditional', None)
            if npcType == 'GUARD':
                npc = Guard(x, y, waypoints, 20, self.batches, self.batchGroups, color, light_color)
            elif npcType == 'COMMANDO':
                npc = Commando(x, y, waypoints, 35, self.batches, self.batchGroups, light_color)
            elif npcType == 'BROTHER_1945':
                npc = Brother_1945(x, y, waypoints, self.batches, self.batchGroups, dialog)
            elif npcType == 'FEMALE_SCIENTIST':
                npc = FemaleScientist(x, y, waypoints, self.batches, self.batchGroups, dialog)
            elif npcType == 'WOLFGANG_1945':
                npc = Wolfgang_1945(x, y, waypoints, self.batches, self.batchGroups)
            elif npcType == 'TORTURED_SS':
                npc = Tortured_SS(x, y, self.batches, self.batchGroups)
            elif npcType == 'REES_INTERROGATION':
                npc = Rees_Interrogation(x, y, self.batches, self.batchGroups)
            elif npcType == 'RELICT_IDLE':
                npc = Relict_Idle(x, y, self.batches, self.batchGroups)
            elif npcType == 'WOMAN':
                options = {}
                if dialog is not None:
                    options['dialog'] = dialog
                if script is not None:
                    options['script'] = script
                options['hair_color'] = npcBuilder.get('hair_color', None)
                options['dress_color'] = npcBuilder.get('dress_color', None)
                npc = Woman(x, y, waypoints, self.batches, self.batchGroups, **options)
            elif npcType == 'MAN':
                options = {}
                if dialog is not None:
                    options['dialog'] = dialog
                if script is not None:
                    options['script'] = script
                options['hair_color'] = npcBuilder.get('hair_color', None)
                options['shirt_color'] = npcBuilder.get('shirt_color', None)
                options['pants_color'] = npcBuilder.get('pants_color', None)
                npc = Man(x, y, waypoints, self.batches, self.batchGroups, **options)
            elif npcType == 'ISABEL':
                npc = Isabel(x, y, waypoints, self.batches, self.batchGroups, dialog)
            elif npcType == 'ISABEL_DRESS':
                npc = Isabel_Dress(x, y, waypoints, self.batches, self.batchGroups, dialog)
            elif npcType == 'SCIENTIST':
                npc = Scientist(x, y, waypoints, self.batches, self.batchGroups, dialog)
            elif npcType == 'INALCO':
                npc = Inalco(x, y, waypoints, self.batches, self.batchGroups, dialog)
            elif npcType == 'FRIENDLY_COMMANDO':
                npc = FriendlyCommando(x, y, waypoints, self.batches, self.batchGroups, dialog)
            elif npcType == 'BAD_MAN':
                npc = BadMan(x, y, waypoints, self.batches, self.batchGroups, dialog)
            elif npcType == 'TORTURED_BAD_MAN':
                npc = Tortured_BadMan(x, y, self.batches, self.batchGroups)
            if npc != None:
                self.originalNPCs.add((npc, spawn_conditional))

    def parseObjective(self, props, x, y, trigger_name):
        name = props['name']
        title = props['title']
        order = int(props['order'])
        quest_name = props['quest']
        dialog = None
        script = None
        if 'dialog' in props:
            dialog = props['dialog']
        if 'script' in props:
            script = props['script']
        quest = QuestManager.getQuest(quest_name)
        if quest is None:
            print('Could not find quest "' + str(quest_name) + '"; make sure it is specified in the level .json')
        quest.addStage(Stage(name, title, order))
        _visible = props.get("visible", "True")
        is_visible = _visible.strip().title() == "True"
        objective = Objective(x, y, name, quest_name, order, trigger_name, dialog, script, visible=is_visible)
        self.triggers.append(objective)
        if quest_name not in self.objective_triggers:
            self.objective_triggers[quest_name] = []
        self.objective_triggers[quest_name].append(objective)

    def parseQuestTrigger(self, props, x, y, trigger_name):
        quest_name = props['quest']
        dialog = None
        if 'dialog' in props:
            dialog = props['dialog']
        questTrigger = QuestTrigger(x, y, quest_name, trigger_name, dialog)
        self.triggers.append(questTrigger)
    
    def parseDialogTrigger(self, props, x, y, trigger_name):
        dialog = props['dialog']
        repeatable = False
        if 'repeatable' in props:
            repeatable = bool(props['repeatable'])
        dialogTrigger = DialogTrigger(x, y, trigger_name, dialog, repeatable)
        self.triggers.append(dialogTrigger)

    def parseScriptTrigger(self, props, x, y, trigger_name):
        script_name = props['script']
        _visible = props.get("visible", "True")
        visible = _visible.strip().title() == "True"
        _repeatable = props.get("repeatable", "False")
        repeatable = _repeatable.strip().title() == "True"
        scriptTrigger = ScriptTrigger(x, y, trigger_name, script_name, visible, repeatable)
        self.triggers.append(scriptTrigger)

    def parseTrigger(self, trigger):
        tileX = (trigger['x'] + 15) // 32
        tileY = self.height - ((trigger['y'] - 15) // 32)
        props = self.convert_props(trigger.get("properties", {}))
        trigger_name = trigger['name']
        if trigger['type'] == 'objective':
            self.parseObjective(props, tileX, tileY, trigger_name)
        elif trigger['type'] == 'quest':
            self.parseQuestTrigger(props, tileX, tileY, trigger_name)
        elif trigger['type'] == 'dialog':
            self.parseDialogTrigger(props, tileX, tileY, trigger_name)
        elif trigger['type'] == 'script':
            self.parseScriptTrigger(props, tileX, tileY, trigger_name)

    def parseLight(self, light):
        visible = light.get("visible", True)
        if not visible:
            return
        props = self.convert_props(light.get("properties", {}))
        x = light["x"] + (light["width"] / 2)
        y = ((self.height + 1) * 32) - (light["y"] + (light["height"] / 2))
        radius = min(light["width"], light["height"])/2
        color_str = props.get("color", "255,255,255")
        color_strs = str(color_str).split(',')
        if len(color_strs) < 3:
            color_strs = ["255","255","255"]
        color = (int(color_strs[0]), int(color_strs[1]), int(color_strs[2]))
        exponent = props.get("exponent", 2)
        steps = props.get("steps", 8)
        light_type = light.get("type", "").lower().strip()
        light_dict = {
            "x": x,
            "y": y,
            "radius": radius,
            "color": color,
            "exponent": exponent,
            "steps": steps,
            "light_type": light_type,
        }
        self.lights.append(light_dict)

    def parseSurveillanceCamera(self, camera):
        visible = camera.get("visible", True)
        if not visible:
            return
        tileX = (camera["x"] + 15) // 32
        tileY = self.height - ((camera["y"] - 15) // 32)
        has_gun = str(camera.get("type")).lower() == "gun"
        props = self.convert_props(camera.get("properties", {}))
        direction = self.extract_direction_from_props(props, Direction.DOWN)
        move_delta = int(props.get("delta", 0))
        surveillance_camera = SurveillanceCameraFactory.create_camera(tileX, tileY, direction, move_delta, has_gun, self.batches, self.batchGroups)
        self.original_cameras.add(surveillance_camera)

    def parseCoverRegion(self, cover_region):
        tileX = (cover_region['x'] + 15) // 32
        tileY = (cover_region['y'] - 15) // 32
        height = cover_region['height'] // 32
        width = cover_region['width'] // 32
        cover_type = cover_region['type'].lower()
        normalized_region = {
            "x": tileX,
            "y": tileY + 1,
            "width": width,
            "height": height,
            "type": cover_type,
        }
        self.cover_regions.append(normalized_region)

    def parseLaser(self, laser):
        tileX = (laser['x'] + 15) // 32
        tileY = self.height - ((laser['y'] - 15) // 32)
        orientation = laser['type']
        normalized_laser = {
            "x": tileX,
            "y": tileY,
            "orientation": orientation,
        }
        self.lasers.append(normalized_laser)

    def apply_cover_regions_to_obstacle_matrix(self):
        weight_map = {
            "grass": 2,
            "water": 3,
        }
        for cover_region in self.cover_regions:
            weight = weight_map.get(cover_region["type"], 1)
            y = cover_region["y"]
            x = cover_region["x"]
            width = cover_region["width"]
            height = cover_region["height"]
            start_x = x
            start_y = y
            end_x = start_x + width
            end_y = start_y + height
            while y < end_y:
                x = start_x
                while x < end_x:
                    if self.obstacleMatrix[y][x] != 0:
                        self.obstacleMatrix[y][x] = weight
                    x += 1
                y += 1

    def parseAmbience(self, ambience):
        r = 1.0
        g = 1.0
        b = 1.0
        alist = ambience.split(',')
        if len(alist) >= 3:
            r = float(alist[0])
            g = float(alist[1])
            b = float(alist[2])
        self.ambience = (r, g, b)

    def load(self, location, batches, batchGroups):
        self.batches = batches
        self.batchGroups = batchGroups
        f = pyglet.resource.file(location, 'r')
        mapdata = json.loads(f.read())
        f.close()
        self.width = mapdata['width']
        self.height = mapdata['height']
        self.width_px = self.width * 32
        self.height_px = self.height * 32

        # Use this for weather, soundtrack, lighting, etc.
        if 'properties' in mapdata:
            mapProps = self.convert_props(mapdata.get("properties", {}))
            if 'weather' in mapProps:
                self.weather = mapProps['weather']
            if 'dialog' in mapProps:
                self.dialog = mapProps['dialog']
            if 'soundtrack' in mapProps:
                self.soundtrack = mapProps['soundtrack']
            if 'ambience' in mapProps:
                self.parseAmbience(mapProps['ambience'])
            if 'force_prone' in mapProps:
                fp_str = str(mapProps['force_prone']).strip().lower()
                if fp_str == "true":
                    self.force_prone = True
            if 'gas' in mapProps:
                gas = str(mapProps['gas']).strip().lower()
                if gas == "true":
                    self.gas = True
            if 'radar_jammed' in mapProps:
                radar_jammed = str(mapProps['radar_jammed']).strip().lower()
                if radar_jammed == "true":
                    self.radar_jammed = True
        
        tilesets = mapdata['tilesets']

        # Cache tilesets
        self.firstgid_map = TilesetManager.cacheTilesets(tilesets)

        self.waypointMap = {}
        npcList = []
        layers = mapdata['layers']
        for layer in layers:
            if layer['type'] == 'tilelayer':
                layerName = layer['name']
                self.tileLayers[layerName] = [[]]
                layerData = layer['data']
                lastY = 0
                for i in range(0, len(layerData)):
                    x = i % self.width
                    y = int(i / self.width)
                    if y > lastY:
                        self.tileLayers[layerName].append([])
                    lastY = y
                    tindex = layerData[i]
                    if tindex > 0:
                        actualY = self.height - y
                        image = TilesetManager.getTile(tindex, self.firstgid_map)
                        tile = StaticObject(x, actualY, image, batches['main'], batchGroups[layerName])
                        self.tileLayers[layerName][-1].append(tile)
                    else:
                        self.tileLayers[layerName][-1].append(None)
            elif layer['type'] == 'objectgroup':
                if layer['name'] == 'items':
                    # Parse items
                    objects = layer['objects']
                    for obj in objects:
                        tileX = (obj['x'] + 15) // 32
                        tileY = self.height - ((obj['y'] - 15) // 32)
                        # Custom object properties
                        objProps = self.convert_props(obj.get("properties", {}))
                        objType = obj['type']
                        objName = obj['name']
                        objVisible = obj['visible']
                        image = None
                        if objVisible:
                            image = common_tileset[common_spritemap[objType]]
                        item = Item(tileX, tileY, image, batches["main"], batchGroups["items"], objType, objProps)
                        self.items.add(item)
                elif layer['name'] == 'doors':
                    doors = layer['objects']
                    for door in doors:
                        tileX = (door['x'] + 15) // 32
                        tileY = self.height - ((door['y'] - 15) // 32)
                        doorProps = self.convert_props(door.get("properties", {}))
                        direction = self.extract_direction_from_props(doorProps, Direction.UP)
                        doorWidth = 1
                        if 'width' in doorProps:
                            doorWidth = int(doorProps['width'])
                        security = 0
                        if 'security' in doorProps:
                            security = int(doorProps['security'])
                        objective = None
                        if 'objective' in doorProps:
                            objective = json.loads(doorProps['objective'])
                        _locked = doorProps.get("locked", "False")
                        locked = _locked.strip().title() == "True"
                        door_id = doorProps.get("id", None)
                        door = Door(tileX, tileY, batches['main'], batchGroups, direction, doorWidth, security, objective, locked=locked, door_id=door_id)
                        self.doors.append(door)
                elif layer['name'] == 'ducts':
                    ducts = layer['objects']
                    for duct in ducts:
                        tileX = (duct['x'] + 15) // 32
                        tileY = self.height - ((duct['y'] - 15) // 32)
                        duct_entrance = DuctVent(tileX, tileY)
                        # DuctVents should be treated like Doors
                        self.doors.append(duct_entrance)
                elif layer['name'] == 'animation':
                    anim_objects = layer['objects']
                    for anim_obj in anim_objects:
                        tileX = (anim_obj['x'] + 15) // 32
                        tileY = self.height - ((anim_obj['y'] - 15) // 32)
                        tindex = anim_obj.get("gid")
                        if tindex is not None:
                            imagegrid, imagerow, imagecol = TilesetManager.getSpriteSheetAndCoords(tindex, self.firstgid_map)
                            anim_obj = DynamicObject(tileX, tileY, imagegrid, imagerow, imagecol, batches['main'], batchGroups['background'])
                            self.animation_objects.append(anim_obj)
                elif layer['name'] == 'waypoints':
                    waypoints = layer['objects']
                    for waypoint in waypoints:
                        tileX = (waypoint['x'] + 15) // 32
                        tileY = self.height - ((waypoint['y'] - 15) // 32)
                        waypointProps = self.convert_props(waypoint.get("properties", {}))
                        direction = self.extract_direction_from_props(waypointProps)
                        behavior = str(waypointProps['behavior']).upper()
                        name = str(waypoint['name'])
                        actualWaypoint = Waypoint(tileX, tileY, direction, behavior)
                        self.waypointMap[name] = actualWaypoint
                elif layer['name'] == 'npcs':
                    npcs = layer['objects']
                    for npc in npcs:
                        tileX = (npc['x'] + 15) // 32
                        tileY = self.height - ((npc['y'] - 15) // 32)
                        npcProps = self.convert_props(npc.get("properties", {}))
                        npcType = str(npc['type']).upper()
                        waypointsStr = ''
                        if 'waypoints' in npcProps:
                            waypointsStr = str(npcProps['waypoints'])
                        waypoints = waypointsStr.split(',')
                        color = None
                        if 'color' in npcProps:
                            colorStrs = str(npcProps['color']).split(',')
                            color = (int(colorStrs[0]), int(colorStrs[1]), int(colorStrs[2]))
                        hair_color = None
                        if 'hair_color' in npcProps:
                            colorStrs = str(npcProps['hair_color']).split(',')
                            hair_color = (int(colorStrs[0]), int(colorStrs[1]), int(colorStrs[2]))
                        dress_color = None
                        if 'dress_color' in npcProps:
                            colorStrs = str(npcProps['dress_color']).split(',')
                            dress_color = (int(colorStrs[0]), int(colorStrs[1]), int(colorStrs[2]))
                        shirt_color = None
                        if 'shirt_color' in npcProps:
                            colorStrs = str(npcProps['shirt_color']).split(',')
                            shirt_color = (int(colorStrs[0]), int(colorStrs[1]), int(colorStrs[2]))
                        pants_color = None
                        if 'pants_color' in npcProps:
                            colorStrs = str(npcProps['pants_color']).split(',')
                            pants_color = (int(colorStrs[0]), int(colorStrs[1]), int(colorStrs[2]))
                        dialog = None
                        if 'dialog' in npcProps:
                            dialog = str(npcProps['dialog'])
                        script = None
                        if 'script' in npcProps:
                            script = str(npcProps['script'])
                        light_color = None
                        if 'light_color' in npcProps:
                            light_color_strs = str(npcProps['light_color']).split(',')
                            light_color = (int(light_color_strs[0]), int(light_color_strs[1]), int(light_color_strs[2]))
                        npcBuilder = {'x': tileX, 'y': tileY+1, 'waypoints': waypoints, 'type': npcType}
                        if color != None:
                            npcBuilder['color'] = color
                        if hair_color != None:
                            npcBuilder['hair_color'] = hair_color
                        if dress_color != None:
                            npcBuilder['dress_color'] = dress_color
                        if shirt_color != None:
                            npcBuilder['shirt_color'] = shirt_color
                        if pants_color != None:
                            npcBuilder['pants_color'] = pants_color
                        if dialog != None:
                            npcBuilder['dialog'] = dialog
                        if script != None:
                            npcBuilder['script'] = script
                        if light_color != None:
                            npcBuilder['light_color'] = light_color
                        npcBuilder['spawn_conditional'] = npcProps.get("spawn_conditional")
                        npcList.append(npcBuilder)
                elif layer['name'] == 'warps':
                    warps = layer['objects']
                    for warp in warps:
                        tileX = (warp['x'] + 15) // 32
                        tileY = self.height - ((warp['y'] - 15) // 32)
                        warpProps = self.convert_props(warp.get("properties", {}))
                        warpX = int(warpProps['x'])
                        warpY = int(warpProps['y'])
                        warpMap = warpProps['map']
                        warpLevel = None
                        if 'level' in warpProps:
                            warpLevel = int(warpProps['level'])
                        elevator_ref = warpProps.get("elevator")
                        actualWarp = Warp(tileX, tileY - 1, warpX, warpY, warpMap, warpLevel, elevatorRef=elevator_ref)
                        self.warps.append(actualWarp)
                elif layer['name'] == 'triggers':
                    triggers = layer['objects']
                    for trigger in triggers:
                        self.parseTrigger(trigger)
                elif layer['name'] == 'lights':
                    lights = layer['objects']
                    for light in lights:
                        self.parseLight(light)
                elif layer['name'] == 'cover':
                    cover_regions = layer['objects']
                    for cover_region in cover_regions:
                        self.parseCoverRegion(cover_region)
                elif layer['name'] == 'cameras':
                    cameras = layer['objects']
                    for camera in cameras:
                        self.parseSurveillanceCamera(camera)
                elif layer['name'] == 'lasers':
                    lasers = layer['objects']
                    for laser in lasers:
                        self.parseLaser(laser)
        wallsLayer = self.tileLayers['walls']
        for i in range(0, self.height):
            obstacleRow = []
            for j in range(0, self.width):
                obstacleRow.append(1)
            self.obstacleMatrix.append(obstacleRow)
        for i in range(0, len(wallsLayer)):
            for j in range(0, len(wallsLayer[i])):
                if wallsLayer[i][j] != None:
                    self.obstacleMatrix[i][j] = 0
            #print(str(self.obstacleMatrix[i]))
        self.apply_cover_regions_to_obstacle_matrix()
        self.obstacleMatrix.reverse()

        self.createNPCs(self.waypointMap, npcList)

        #print(str(QuestManager.getActiveQuestsJSON()))

    def create_radar_obstacles(self):
        if self.radar_jammed:
            # Don't bother setting these up if radar is jammed
            return
        image = common_tileset[common_spritemap['bar']]
        for i in range(0, len(self.obstacleMatrix)):
            for j in range(0, len(self.obstacleMatrix[0])):
                if self.obstacleMatrix[i][j] == 0:
                    radarObstacle = StaticObject(j, i, image, self.batches['radar'], self.batchGroups['radarBackground'])
                    self.radarObstacles.append(radarObstacle)

    def to_dict(self, recently_acquired_items):
        items = []
        triggers = []
        for item in self.items:
            items.append(item.to_dict())
        if recently_acquired_items is not None:
            for item in recently_acquired_items:
                items.append(item.to_dict())
        for trigger in self.triggers:
            triggers.append(trigger.to_dict())
        map_dict = {
            "items": items,
            "triggers": triggers,
            "index": self.index
        }
        return map_dict

    def from_dict(self, map_dict):
        remove_items = []
        for item in self.items:
            if item.to_dict() not in map_dict["items"]:
                remove_items.append(item)
        for item in remove_items:
            item.hide()
            self.items.remove(item)
        for trigger in self.triggers:
            for _trigger in map_dict["triggers"]:
                if trigger.trigger_name == _trigger["trigger_name"]:
                    trigger.active = _trigger["active"]
                    if not trigger.active:
                        trigger.setActive(False)
                    trigger.saveState()