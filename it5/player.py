from it5 import spriteloader
from it5.humanoid import Humanoid
from it5.item import createWeapon, Weapon, WeaponType
from it5.direction import Direction
import copy
from it5.common import EventManager, CommonManager, lighting
from it5.audio import SoundManager
from it5.controls import Controls, ControlAction

player_sprites = spriteloader.getSprites("player.png", 8, 16)
player_suit_sprites = spriteloader.getSprites("player_suit.png", 8, 8)

WEAPON_SFX_MAP = {
    WeaponType.PISTOL: "pistol_gunshot",
    WeaponType.SMG: "pistol_gunshot",
    WeaponType.CARBINE: "rifle_gunshot",
    WeaponType.ROCKET_LAUNCHER: "rifle_gunshot",
}

class Inventory(object):
    def __init__(self):
        self.items = {'None':None}
        self.weapons = {'None':None}
        self.max_medkits = 2
        self.currWeapon = 'None'
        self.currItem = 'None'
        self.key_level = 0
    
    def copy(self):
        _inventory = Inventory()
        _inventory.max_medkits = self.max_medkits
        _inventory.key_level = self.key_level
        _inventory.currWeapon = self.currWeapon
        _inventory.currItem = self.currItem
        _inventory.items = copy.deepcopy(self.items)
        _inventory.weapons = copy.deepcopy(self.weapons)
        return _inventory

    def to_dict(self):
        weapons = {}
        for weapon in self.weapons:
            _weapon = self.weapons[weapon]
            if _weapon is not None:
                weapons[weapon] = _weapon.to_dict()
            else:
                weapons[weapon] = None
        inventory_dict = {
            "max_medkits": self.max_medkits,
            "key_level": self.key_level,
            "currWeapon": self.currWeapon,
            "currItem": self.currItem,
            "items": self.items,
            "weapons": weapons
        }
        return inventory_dict

    def from_dict(self, inventory_dict):
        weapons = {}
        for weapon_key in inventory_dict["weapons"]:
            weapon_dict = inventory_dict["weapons"][weapon_key]
            if weapon_dict is not None:
                weapons[weapon_key] = Weapon.from_dict(weapon_dict)
            else:
                weapons[weapon_key] = None
        self.max_medkits = inventory_dict["max_medkits"]
        self.key_level = inventory_dict["key_level"]
        self.currWeapon = inventory_dict["currWeapon"]
        self.currItem = inventory_dict["currItem"]
        self.items = inventory_dict["items"]
        self.weapons = weapons

    def getCurrWeapon(self):
        if self.currWeapon in self.weapons:
            return self.weapons[self.currWeapon]
        return None
    
    def getCurrItemValue(self):
        if self.currItem in self.items:
            return self.items[self.currItem]
        return None
    
    def setCurrItemValue(self, val):
        if self.currItem in self.items:
            self.items[self.currItem] = val
    
    def getCurrItem(self):
        return self.currItem
    
    def changeWeapon(self, delta):
        wkeys = list(self.weapons.keys())
        currIndex = 0
        nextIndex = 0
        try:
            currIndex = wkeys.index(self.currWeapon)
        except:
            pass
        if currIndex + delta < len(wkeys):
            nextIndex = currIndex + delta
        self.currWeapon = wkeys[nextIndex]

    def changeItem(self, delta):
        ikeys = list(self.items.keys())
        currIndex = 0
        nextIndex = 0
        try:
            currIndex = ikeys.index(self.currItem)
        except:
            pass
        if currIndex + delta < len(ikeys):
            nextIndex = currIndex + delta
        self.currItem = ikeys[nextIndex]
        if delta != 0:
            if self.currItem == 'medkit' or self.currItem == 'booster':
                EventManager.set_notification("Press %s to Consume Item" % Controls.get_key_names_str(ControlAction.FIRE))
            else:
                EventManager.set_notification("")
        return self.currItem

    def __str__(self):
        weaponsstr = ''
        for key in self.weapons:
            weaponsstr += str(key) + ': ' + str(self.weapons[key]) + '\n'
        return str(self.items) + '\n' + weaponsstr + str(self.max_medkits)
    
    def addTo(self, item):
        itemType = item.getType()
        itemProps = item.getProps()
        itemName = itemType.title()
        success = True
        if itemType == 'weapon':
            weaponType = itemProps['type']
            itemName = weaponType.title()
            weapon = createWeapon(weaponType, itemProps)
            if weaponType not in self.weapons:
                self.weapons[weaponType] = weapon
            else:
                existingWeapon = self.weapons[weaponType]
                if weapon.rank > existingWeapon.rank:
                    if existingWeapon.suppressed:
                        weapon.attachSuppressor()
                    weapon.addAmmo(existingWeapon.ammo)
                    self.weapons[weaponType] = weapon
                else:
                    success = existingWeapon.addAmmo(weapon.ammo)
        elif itemType == 'item':
            if 'type' in itemProps:
                iType = itemProps['type']
                itemName = iType.title()
                if iType == 'suppressor' or iType == 'silencer':
                    weaponType = itemProps.get('weapon_type', 'pistol')
                    if weaponType in self.weapons:
                        weapon = self.weapons[weaponType]
                        success = weapon.attachSuppressor()
                        if not success:
                            EventManager.set_notification(weaponType.title() + " already has a suppressor")
                    else:
                        EventManager.set_notification("No use, find %s first." % weaponType)
                        success = False
                else:
                    if iType in self.items:
                        if iType == 'cardkey' or iType == 'key':
                            ccLevel = itemProps['level']
                            itemName += ' Lv. ' + str(ccLevel)
                            if ccLevel > self.items[iType]:
                                self.items[iType] = ccLevel
                                self.key_level = ccLevel
                            else:
                                success = False
                    else:
                        if iType == 'cardkey' or iType == 'key':
                            ccLevel = itemProps['level']
                            itemName += ' Lv. ' + str(ccLevel)
                            self.items[iType] = ccLevel
                            self.key_level = ccLevel
                        else:
                            self.items[iType] = 1
        elif itemType == 'ammo':
            if 'type' in itemProps and 'count' in itemProps:
                weaponType = itemProps['type']
                ammoCount = itemProps['count']
                itemName = weaponType.title() + ' Ammo x' + str(ammoCount)
                if weaponType in self.weapons:
                    weapon = self.weapons[weaponType]
                    success = weapon.addAmmo(ammoCount)
                    if not success:
                        EventManager.set_notification(weaponType.title() + ' ammo full')
                else:
                    EventManager.set_notification("No use, find %s first." % weaponType)
                    success = False
        elif itemType == 'medkit':
            if 'medkit' not in self.items:
                self.items['medkit'] = 0
            if self.items['medkit'] < self.max_medkits:
                self.items['medkit'] += 1
            else:
                EventManager.set_notification('Medkits full')
                success = False
        elif itemType == "booster":
            if "booster" not in self.items:
                self.items["booster"] = 0
            self.items["booster"] += 1
        elif itemType == "bronze":
            if "treasure" not in self.items:
                self.items["treasure"] = 0
            self.items["treasure"] += 100
            itemName = "Treasure x100"
        elif itemType == "silver":
            if "treasure" not in self.items:
                self.items["treasure"] = 0
            self.items["treasure"] += 250
            itemName = "Treasure x250"
        elif itemType == "gold":
            if "treasure" not in self.items:
                self.items["treasure"] = 0
            self.items["treasure"] += 1000
            itemName = "Treasure x1000"
        elif itemType == "bronze_egg":
            if "bronze egg" not in self.items:
                self.items["bronze egg"] = 0
            self.items["bronze egg"] += 1
        elif itemType == "silver_egg":
            if "silver egg" not in self.items:
                self.items["silver egg"] = 0
            self.items["silver egg"] += 1
        elif itemType == "gold_egg":
            if "gold egg" not in self.items:
                self.items["gold egg"] = 0
            self.items["gold egg"] += 1
        elif itemType == "treasure":
            if "treasure" not in self.items:
                self.items["treasure"] = 0
            treasure_count = itemProps.get("count", 0)
            self.items["treasure"] += treasure_count
            itemName += " x" + str(treasure_count)
        if success:
            EventManager.set_notification('Found ' + itemName)
        return success

class Player(Humanoid):
    def __init__(self, x, y, eventDispatcher, batch, batchGroups, spritesheet=player_sprites, alt_clothing=False):
        super(Player, self).__init__(x, y, spritesheet, batch, batchGroups)
        self.protected = True
        self.moveStack = []
        self.inventory = Inventory()
        self.shadow_inventory = Inventory()
        self.weapon = None
        self.firing_full_auto = False
        self.shot_timer = 0
        self.eventDispatcher = eventDispatcher
        self.light = None
        self.eventDispatcher.register_event_type('item_change')
        self.eventDispatcher.register_event_type('weapon_change')
        self.eventDispatcher.register_event_type('health_change')
        self.eventDispatcher.register_event_type('max_health_change')
        self.normal_speed = self.moveSpeed
        self.crawl_speed = self.normal_speed / 2
        self.oxygen = 100
        self.in_gas = False
        self.gasmask_equipped = False
        self.alt_clothing = alt_clothing
        self.cant_shoot = False
        self.last_warp_time = 0
        self.saveStatus()

    def set_last_warp_time(self, warp_time):
        self.last_warp_time = warp_time

    def get_last_warp_time(self):
        return self.last_warp_time

    def respawn(self):
        self.inventory = self._inventory.copy()
        self.shadow_inventory = self._shadow_inventory.copy()
        self.health = self._health
        self.oxygen = self._oxygen
        self.maxHealth = self._maxHealth
        if self.health <= 0:
            self.health = 1
        if self.maxHealth <= 0:
            self.maxHealth = 100
        self.alive = True
        self.stop_shooting()
        self.jump(self._x, self._y)
        self.changeDirection(self._direction)
        self.moveStack = []
        self.changeWeapon(0)
        self.changeItem(0)
        self.top.setColor(self.top.color)
        self.bottom.setColor(self.bottom.color)
        self.top.setAlpha(255)
        self.bottom.setAlpha(255)
        self.top.setVisible(True)
        self.bottom.setVisible(True)
        self.damageTimer = 0
        self.reset_prone_state()

    def reset_prone_state(self):
        # We toggle prone to set the appropriate move speed without having to save additional info
        self.prone = not self._prone
        self.toggle_prone()

    def saveStatus(self):
        self._inventory = self.inventory.copy()
        self._shadow_inventory = self.shadow_inventory.copy()
        self._health = self.health
        self._maxHealth = self.maxHealth
        self._oxygen = self.oxygen
        self._prone = self.prone
        self._alt_clothing = self.alt_clothing
        self._x = self.getTileX()
        self._y = self.getTileY()
        self._direction = self.direction

    def to_dict(self):
        player_dict = {
            "x": self._x,
            "y": self._y,
            "health": self._health,
            "maxHealth": self._maxHealth,
            "oxygen": self._oxygen,
            "prone": self._prone,
            "gasmask_equipped": self.gasmask_equipped,
            "direction": self._direction.name,
            "inventory": self._inventory.to_dict(),
            "shadow_inventory": self._shadow_inventory.to_dict(),
            "alt_clothing": self._alt_clothing,
        }
        return player_dict

    def from_dict(self, player_dict):
        self.jump(player_dict["x"], player_dict["y"])
        self.health = player_dict["health"]
        self.maxHealth = player_dict["maxHealth"]
        self.oxygen = player_dict.get("oxygen", 100)
        self.changeDirection(Direction[player_dict["direction"]])
        self.inventory.from_dict(player_dict["inventory"])
        if "shadow_inventory" in player_dict:
            self.shadow_inventory.from_dict(player_dict["shadow_inventory"])
        # This will need to be handled in higher level code if alt_clothing=True
        self.alt_clothing = player_dict.get("alt_clothing", False)
        # Toggle prone to set the appropriate move speed without having to save additional info
        # self.stopMoving(self.direction) is called in self.toggle_prone(). If taken out of that method,
        # call it here:
        #self.stopMoving(self.direction)
        self.prone = not player_dict.get("prone", False)
        self.toggle_prone()
        #self.gasmask_equipped = player_dict.get("gasmask_equipped", False)
        self.saveStatus()
        self.changeItem(0)
        self.changeWeapon(0)

    def toggle_prone(self, force_prone=False):
        if force_prone:
            self.prone = True
        else:
            self.prone = not self.prone
        if self.prone:
            self.moveSpeed = self.crawl_speed
        else:
            self.moveSpeed = self.normal_speed
            self.concealed = False
        self.stopMoving(self.direction)

    def receiveDamage(self, damage):
        super(Player, self).receiveDamage(damage)
        self.eventDispatcher.dispatch_event('health_change', self.health)

    def getItem(self):
        return self.inventory.getCurrItem()
    
    def getItemValue(self):
        return self.inventory.getCurrItemValue()
    
    def setItemValue(self, val):
        self.inventory.setCurrItemValue(val)

    def getKeyLevel(self):
        return self.inventory.key_level

    def attempt_to_heal(self):
        medkits = self.inventory.items.get("medkit", 0)
        booster_kits = self.inventory.items.get("booster", 0)
        if self.health < self.maxHealth:
            if medkits > 0:
                target_item = "medkit"
            elif booster_kits > 0:
                target_item = "booster"
            else:
                # No healing items in inventory
                return
        else:
            # Player is at full health; only attempt to use booster kit
            if booster_kits > 0:
                target_item = "booster"
            else:
                # No booster kits in inventory
                return
        first_item = self.getItem()
        if first_item == "medkit" or first_item == "booster":
            # Assume the player wants to heal with the currently selected item
            self.useItem()
            return
        self.changeItem(1)
        item_name = self.getItem()
        item_used = False
        # Loop through inventory, use a healing item, all while preserving the player's selected item
        while item_name != first_item:
            if item_name == target_item and not item_used:
                self.useItem()
                item_used = True
            self.changeItem(1)
            item_name = self.getItem()

    def changeWeapon(self, delta):
        self.inventory.changeWeapon(delta)
        self.weapon = self.inventory.getCurrWeapon()
        #print(str(self.weapon))
        ammo = None
        maxAmmo = None
        if self.weapon != None:
            ammo = self.weapon.getAmmo()
            maxAmmo = self.weapon.getMaxAmmo()
        self.eventDispatcher.dispatch_event('weapon_change', str(self.weapon), ammo, maxAmmo)
    
    def changeItem(self, delta):
        last_item = self.getItem()
        item = self.inventory.changeItem(delta)
        #print(str(self.getItem()) + ' ' + str(self.getItemValue()))
        self.eventDispatcher.dispatch_event('item_change', str(item), self.getItemValue())
        if item == "flashlight":
            self.create_light()
        elif last_item == "flashlight":
            self.destroy_light()
        if item == "gas mask" or item == "gasmask":
            self.gasmask_equipped = True
        else:
            self.gasmask_equipped = False

    def useItem(self):
        if self.getItem() == 'medkit':
            if self.getItemValue() > 0:
                if self.health < self.maxHealth:
                    newhealth = self.health + 50
                    if newhealth > self.maxHealth:
                        newhealth = self.maxHealth
                    self.setHealth(newhealth)
                    self.setItemValue(self.getItemValue() - 1)
                    self.eventDispatcher.dispatch_event('item_change', str(self.getItem()), self.getItemValue())
            else:
                EventManager.set_notification('Out of Medkits')
        elif self.getItem() == 'booster':
            if self.getItemValue() > 0:
                self.setMaxHealth(self.maxHealth + 10)
                self.setHealth(self.maxHealth)
                self.setItemValue(self.getItemValue() - 1)
                self.eventDispatcher.dispatch_event('item_change', str(self.getItem()), self.getItemValue())
            else:
                EventManager.set_notification('Out of Booster kits')

    def setHealth(self, health):
        self.health = health
        self.eventDispatcher.dispatch_event('health_change', self.health)

    def setMaxHealth(self, max_health):
        self.maxHealth = max_health
        self.eventDispatcher.dispatch_event('max_health_change', self.maxHealth)

    def startMoving(self, moveDirection):
        super(Player, self).startMoving(moveDirection)
        self.moveStack.append(moveDirection)
    
    def clearMoveStack(self):
        self.moveStack.clear()
    
    def removeFromMoveStack(self, moveDirection):
        for direction in self.moveStack:
            if direction == moveDirection:
                self.moveStack.remove(direction)
                break

    def stopMoving(self, moveDirection):
        super(Player, self).stopMoving()
        self.removeFromMoveStack(moveDirection)
        if len(self.moveStack) > 0:
            newdir = self.moveStack.pop()
            self.startMoving(newdir)

    def toggle_light(self):
        if self.light is None:
            self.create_light()
        else:
            self.destroy_light()

    def create_light(self):
        if self.light is None:
            self.light = lighting.create_light(x=self.x+16, y=self.y+32, radius=128, color=(255,255,127))

    def destroy_light(self):
        if self.light is not None:
            lighting.destroy_light(self.light)
            self.light = None

    def final_check_door_collides(self, door, collision):
        # Prevents the player from going through doors they aren't supposed to but have been opened by an NPC
        if collision:
            if not door.is_objective_satisfied():
                return True
            if self.getKeyLevel() < door.securityLevel:
                return True
            return not door.isOpen()
        return collision

    def update(self, dt, obstacleMatrix, items, doors):
        super(Player, self).update(dt, obstacleMatrix, doors)
        if self.light is not None:
            self.light.set_position(self.x+16, self.y+32)
        if self.firing_full_auto:
            if self.shot_timer <= 0:
                shot_fired = self.shoot(CommonManager.get_bullet_manager())
                if shot_fired:
                    CommonManager.get_hud().updateAmmoDisplay(self.weapon.getAmmo())
            else:
                self.shot_timer -= dt
        if self.submerged:
            self.oxygen -= (7 * dt)
            self.eventDispatcher.dispatch_event('o2_change', self.oxygen)
            if self.oxygen <= 0:
                self.oxygen = 0
                self.health -= (14 * dt)
                self.eventDispatcher.dispatch_event('health_change', self.health)
                if self.health <= 0:
                    self.alive = False
        elif self.in_gas:
            o2_rate = 7
            if self.gasmask_equipped:
                o2_rate = 1
            self.oxygen -= (o2_rate * dt)
            self.eventDispatcher.dispatch_event('o2_change', self.oxygen)
            if self.oxygen <= 0:
                self.oxygen = 0
                self.health -= (14 * dt)
                self.eventDispatcher.dispatch_event('health_change', self.health)
                if self.health <= 0:
                    self.alive = False
        else:
            if self.oxygen < 100:
                self.oxygen += (30 * dt)
                if self.oxygen > 100:
                    self.oxygen = 100
                self.eventDispatcher.dispatch_event('o2_change', self.oxygen)
        itemRemovals = False
        for item in items:
            if item.collidesWith(self.getTileX(), self.getTileY()):
                if item.getType() == 'landmine' or item.getType() == 'alt_mine':
                    success = True
                    EventManager.explosion(item.x, item.y)
                else:
                    success = self.inventory.addTo(item)
                if success:
                    item.flagForDeletion()
                    itemRemovals = True
        return itemRemovals
    
    def punch(self, npcs):
        if self.prone:
            return
        self.moving = False
        minX = self.getTileX()
        minY = self.getTileY()
        maxX = self.getTileX()
        maxY = self.getTileY() + 1
        if self.direction == Direction.UP:
            self.bottom.updateSprite(6,6)
            self.top.updateSprite(7,6)
            maxY += 1
        elif self.direction == Direction.DOWN:
            self.bottom.updateSprite(4,6)
            self.top.updateSprite(5,6)
            minY -= 1
        elif self.direction == Direction.LEFT:
            self.bottom.updateSprite(0,10)
            self.top.updateSprite(1,10)
            minX -= 1
        elif self.direction == Direction.RIGHT:
            self.bottom.updateSprite(2,10)
            self.top.updateSprite(3,10)
            maxX += 1

        # Apply stun damage to NPCs
        for npc in npcs:
            npc.getPunched(minX, maxX, minY, maxY, self)

    def shoot(self, bulletManager):
        if self.weapon == None or self.prone:
            return False
        if self.cant_shoot:
            EventManager.set_notification("Weapons have been disabled")
            return False
        self.moving = False
        dx = 0
        dy = 0
        sx = self.x
        sy = self.y
        velocity = 1200
        if self.direction == Direction.UP:
            if self.weapon.type == WeaponType.PISTOL or self.weapon.type == WeaponType.SMG or self.weapon.type == WeaponType.CARBINE or self.weapon.type == WeaponType.ROCKET_LAUNCHER:
                self.bottom.updateSprite(6,5)
                self.top.updateSprite(7,5)
            else:
                pass
            dy = velocity
            sy += 27
        elif self.direction == Direction.DOWN:
            if self.weapon.type == WeaponType.PISTOL:
                self.bottom.updateSprite(4,5)
                self.top.updateSprite(5,5)
            elif self.weapon.type == WeaponType.SMG or self.weapon.type == WeaponType.CARBINE or self.weapon.type == WeaponType.ROCKET_LAUNCHER:
                self.bottom.updateSprite(4,7)
                self.top.updateSprite(5,7)
            else:
                pass
            dy = -velocity
            sy += 16
        elif self.direction == Direction.LEFT:
            if self.weapon.type == WeaponType.PISTOL:
                self.bottom.updateSprite(0,8)
                self.top.updateSprite(1,8)
            elif self.weapon.type == WeaponType.SMG or self.weapon.type == WeaponType.CARBINE or self.weapon.type == WeaponType.ROCKET_LAUNCHER:
                self.bottom.updateSprite(0,12)
                self.top.updateSprite(1,12)
                self.top_left.updateSprite(1, 11)
            else:
                pass
            dx = -velocity
            sy += 16
        elif self.direction == Direction.RIGHT:
            if self.weapon.type == WeaponType.PISTOL:
                self.bottom.updateSprite(2,8)
                self.top.updateSprite(3,8)
            elif self.weapon.type == WeaponType.SMG or self.weapon.type == WeaponType.CARBINE or self.weapon.type == WeaponType.ROCKET_LAUNCHER:
                self.bottom.updateSprite(2,11)
                self.top.updateSprite(3,11)
                self.top_right.updateSprite(3, 12)
            else:
                pass
            dx = velocity
            sy += 16

        pRange = self.weapon.range
        damage = self.weapon.damage
        is_thrown = self.weapon.type == WeaponType.MINE or self.weapon.type == WeaponType.GRENADE
        if self.weapon.ammo > 0:
            if self.weapon.fire_rate > 0:
                self.firing_full_auto = True
                self.shot_timer = self.weapon.fire_rate_60_hz
            if not is_thrown:
                if not self.weapon.suppressed:
                    sfx = WEAPON_SFX_MAP.get(self.weapon.type, None)
                    if sfx is not None:
                        SoundManager.play_sfx(sfx)
                        EventManager.gunshot(416)
                else:
                    SoundManager.play_sfx('silenced_gunshot')
            self.weapon.ammo -= 1
            if self.weapon.type == WeaponType.GRENADE:
                dx = dx / 4
                dy = dy / 4
            elif self.weapon.type == WeaponType.ROCKET_LAUNCHER:
                dx = dx / 1.5
                dy = dy / 1.5
            elif self.weapon.type == WeaponType.MINE:
                dx = 0
                dy = 0
                sx = self.x
                sy = self.y
            bulletManager.add(sx, sy, dx, dy, pRange, damage, self.weapon.type, False)
            return True
        else:
            self.firing_full_auto = False
            if not is_thrown:
                SoundManager.play_sfx('empty_gunshot')
        return False

    def stop_shooting(self):
        self.firing_full_auto = False
        self.top_left.destroy()
        self.top_right.destroy()

    def clear_inventory(self):
        self.inventory = Inventory()
        self.changeWeapon(0)
        self.changeItem(0)

    def get_feet_group(self):
        # Overridden to reduce flickering
        return "feet_player"

    def get_heads_group(self):
        # Overridden to reduce flickering
        return "heads_player"

    def from_player(self, player, loading_game=False):
        self.setMaxHealth(player.maxHealth)
        self.setHealth(player.maxHealth)
        if loading_game:
            self.inventory = player.inventory
        else:
            self.inventory = player.shadow_inventory

class PlayerInSuit(Player):
    def __init__(self, x, y, eventDispatcher, batch, batchGroups):
        super(PlayerInSuit, self).__init__(x, y, eventDispatcher, batch, batchGroups, spritesheet=player_suit_sprites, alt_clothing=True)

    def from_player(self, player, loading_game=False):
        self.setMaxHealth(player.maxHealth)
        self.setHealth(player.maxHealth)
        if loading_game:
            self.shadow_inventory = player.shadow_inventory
        else:
            self.shadow_inventory = player.inventory

    def toggle_prone(self, force_prone=False):
        # Behavior is disabled
        pass

    def punch(self, npcs):
        # Behavior is disabled
        pass

    def shoot(self, bulletManager):
        # Behavior is disabled
        pass

    def reset_prone_state(self):
        # Overridden to prevent prone flag from messing up sprites on respawn
        pass
