import math
from it5.common import lighting

class Animation(object):
    # keyframes are a list of ImageGrid coordinate tuples
    def __init__(self, dynamic_object, keyframes, frame_time_delta, lifetime_seconds=None, looping=False, dx=0, dy=0):
        self.dynamic_object = dynamic_object
        self.keyframes = keyframes
        self.frame_time_delta = frame_time_delta
        self.lifetime_seconds = lifetime_seconds
        self.looping = looping
        self.elapsed_time = 0
        self.time_in_frame = 0
        self.kf_index = 0
        self.active = True
        self.dx = dx
        self.dy = dy
        row, column = self.keyframes[self.kf_index]
        self.dynamic_object.updateSprite(row, column)

    def animate(self, dt):
        if self.active:
            delta_x = math.copysign(min(abs(self.dx * dt), 32), self.dx)
            delta_y = math.copysign(min(abs(self.dy * dt), 32), self.dy)
            self.dynamic_object.move(delta_x, delta_y)
            if self.time_in_frame >= self.frame_time_delta:
                self.time_in_frame = 0
                if self.kf_index + 1 < len(self.keyframes):
                    self.kf_index += 1
                elif self.looping:
                    self.kf_index = 0
                # Switch frames
                row, column = self.keyframes[self.kf_index]
                self.dynamic_object.updateSprite(row, column)
            else:
                self.time_in_frame += dt
            if self.lifetime_seconds is not None:
                if self.elapsed_time >= self.lifetime_seconds:
                    self.active = False
                    self.dynamic_object.destroy()
            self.elapsed_time += dt
        return self.active

class AnimationGroup(object):
    def __init__(self, *animations):
        self.animations = animations
        self.light = None
        self.on_destroy = None

    def set_light(self, light):
        self.light = light

    def set_on_destroy(self, on_destroy):
        self.on_destroy = on_destroy

    def animate(self, dt):
        active = False
        for animation in self.animations:
            active |= animation.animate(dt)
        return active

    def destroy(self):
        for animation in self.animations:
            animation.dynamic_object.destroy()
        if self.light is not None:
            lighting.destroy_light(self.light)
        if self.on_destroy is not None:
            self.on_destroy()

class AnimationGroupManager:
    animation_groups = []
    expired_groups = []

    @classmethod
    def add_animation_group(cls, animation_group):
        cls.animation_groups.append(animation_group)

    @classmethod
    def animate(cls, dt):
        for group in cls.animation_groups:
            if not group.animate(dt):
                cls.expired_groups.append(group)
        while len(cls.expired_groups) > 0:
            group = cls.expired_groups.pop()
            cls.animation_groups.remove(group)
            group.destroy()

    @classmethod
    def clear(cls):
        for group in cls.animation_groups:
            for animation in group.animations:
                animation.dynamic_object.destroy()
        cls.animation_groups.clear()