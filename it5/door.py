from it5.direction import Direction
from it5.staticobject import StaticObject
import uuid
from it5.quest import QuestManager
from it5.common import EventManager, common_spritemap as door_spritemap, common_tileset as common_sprites
from it5.audio import SoundManager

class Door(object):
    def __init__(self, x, y, batch, batchGroups, direction, width, securityLevel, objectiveLevel, locked=False, door_id=None):
        self.id = door_id or uuid.uuid4()
        self.tileX = int(x)
        self.tileY = int(y)
        self.securityLevel = securityLevel
        self.objectiveLevel = objectiveLevel
        self.locked = locked
        self.permanent_locked = locked
        self.width = width
        self.height = 1
        self.bottom = None
        self.top = None
        self.bottomRight = None
        self.topRight = None
        self.keypadTop = None
        self.keypadBottom = None
        self.keypadState = None
        self.inactive = False
        self.openX = (self.tileX + width)
        self.openPos = 0
        self.opening = False
        self.closing = False
        self.batch = batch
        self.batchGroups = batchGroups
        if direction == Direction.UP:
            self.height = 2
            if width > 1:
                self.bottom = StaticObject(self.tileX, self.tileY, common_sprites[door_spritemap['door0']], batch, batchGroups['doors'])
                self.top = StaticObject(self.tileX, self.tileY+1, common_sprites[door_spritemap['door1']], batch, batchGroups['doors'])
                self.bottomRight = StaticObject(self.tileX+1, self.tileY, common_sprites[door_spritemap['door2']], batch, batchGroups['doors'])
                self.topRight = StaticObject(self.tileX+1, self.tileY+1, common_sprites[door_spritemap['door3']], batch, batchGroups['doors'])
            else:
                self.bottom = StaticObject(self.tileX, self.tileY, common_sprites[door_spritemap['smalldoor0']], batch, batchGroups['doors'])
                self.top = StaticObject(self.tileX, self.tileY+1, common_sprites[door_spritemap['smalldoor1']], batch, batchGroups['doors'])
            if securityLevel > 0:
                self.keypadBottom = StaticObject(self.tileX - 1, self.tileY, common_sprites[door_spritemap['keypadbottom']], batch, batchGroups['decals'])
                self.keypadTop = StaticObject(self.tileX - 1, self.tileY + 1, common_sprites[door_spritemap['keypadtop']], batch, batchGroups['decals'])
        elif direction == Direction.DOWN:
            if width > 1:
                self.bottom = StaticObject(self.tileX, self.tileY, common_sprites[door_spritemap['dooralt0']], batch, batchGroups['doors'])
                self.bottomRight = StaticObject(self.tileX+1, self.tileY, common_sprites[door_spritemap['dooralt1']], batch, batchGroups['doors'])
            else:
                self.bottom = StaticObject(self.tileX, self.tileY, common_sprites[door_spritemap['smalldooralt']], batch, batchGroups['doors'])
            if securityLevel > 0:
                self.keypadTop = StaticObject(self.tileX - 1, self.tileY, common_sprites[door_spritemap['keypadtop']], batch, batchGroups['decals'])
            self.tileY -= 1
        
    def show(self):
        self.bottom.show()
        if self.height > 1:
            self.top.show()
        if self.width > 1:
            if self.height > 1:
                self.topRight.show()
            self.bottomRight.show()
        if self.securityLevel > 0:
            if self.height > 1:
                self.keypadBottom.show()
            self.keypadTop.show()
        self.inactive = False
        self.locked = self.permanent_locked
    
    def hide(self):
        if self.height > 1:
            self.top.hide()
        self.bottom.hide()
        if self.width > 1:
            if self.height > 1:
                self.topRight.hide()
            self.bottomRight.hide()
        if self.securityLevel > 0:
            if self.height > 1:
                self.keypadBottom.hide()
            self.keypadTop.hide()
        self.inactive = True
        self.locked = self.permanent_locked

    def setColor(self, color):
        self.bottom.setColor(color)
        if self.height > 1:
            self.top.setColor(color)
        if self.width > 1:
            if self.height > 1:
                self.topRight.setColor(color)
            self.bottomRight.setColor(color)
        if self.securityLevel > 0:
            if self.height > 1:
                self.keypadBottom.setColor(color)
            self.keypadTop.setColor(color)

    def open(self, dx, dt):
        if self.openPos < (self.width * 32):
            self.openPos += (dx*dt)
            if self.height > 1:
                self.top.move(dx, 0, dt)
            self.bottom.move(dx, 0, dt)
            if self.width > 1:
                if self.height > 1:
                    self.topRight.move(dx, 0, dt)
                self.bottomRight.move(dx, 0, dt)
            if self.openPos > (self.width * 32):
                self.openPos = (self.width * 32)
                if self.height > 1:
                    self.top.x = (self.tileX * 32) + self.openPos
                    self.top.move(0, 0, 0)
                self.bottom.x = (self.tileX * 32) + self.openPos
                self.bottom.move(0, 0, 0)
                if self.width > 1:
                    if self.height > 1:
                        self.topRight.x = (self.tileX * 32) + self.openPos + 32
                        self.topRight.move(0, 0, 0)
                    self.bottomRight.x = (self.tileX * 32) + self.openPos + 32
                    self.bottomRight.move(0, 0, 0)
        else:
            self.opening = False
    
    def close(self, dx, dt):
        if self.openPos > 0:
            self.openPos += (dx*dt)
            if self.height > 1:
                self.top.move(dx, 0, dt)
            self.bottom.move(dx, 0, dt)
            if self.width > 1:
                if self.height > 1:
                    self.topRight.move(dx, 0, dt)
                self.bottomRight.move(dx, 0, dt)
            if self.openPos < 0:
                self.openPos = 0
                if self.height > 1:
                    self.top.x = (self.tileX * 32) + self.openPos
                    self.top.move(0, 0, 0)
                self.bottom.x = (self.tileX * 32) + self.openPos
                self.bottom.move(0, 0, 0)
                if self.width > 1:
                    if self.height > 1:
                        self.topRight.x = (self.tileX * 32) + self.openPos + 32
                        self.topRight.move(0, 0, 0)
                    self.bottomRight.x = (self.tileX * 32) + self.openPos + 32
                    self.bottomRight.move(0, 0, 0)
        else:
            self.closing = False

    def isOpen(self):
        return self.openPos >= ((self.width * 32) - 1)
    
    def isClosed(self):
        return self.openPos < 1
    
    def forceClose(self):
        self.opening = False
        while self.openPos > 0:
            self.close(-1, 1)

    def shouldOpen(self, humanoid):
        if self.inactive or self.locked:
            return False
        for i in range(0, self.width):
            if (self.tileX + i == humanoid.getTileX()) and ((self.tileY + 1) >= humanoid.getTileY()) and ((self.tileY - 1) <= humanoid.getTileY()+1):
                return True
        return False
    
    def collidesWith(self, px, py):
        for i in range(0, self.width):
            if (self.tileX + i == px) and ((py >= self.tileY - (self.height - 1)) and (py <= self.tileY)):
                return True
        return False

    def updateKeypadState(self, newstate):
        if not self.inactive:
            if self.securityLevel > 0:
                if self.keypadState != newstate:
                    self.keypadTop.sprite.delete()
                    keypad_key = 'keypadtop'
                    if newstate == True:
                        keypad_key = 'keypadtopgreen'
                    elif newstate == False:
                        keypad_key = 'keypadtopred'
                    self.keypadTop = StaticObject(self.tileX - 1, self.tileY + 1, common_sprites[door_spritemap[keypad_key]], self.batch, self.batchGroups['decals'])
                    self.keypadTop.show()
                    self.keypadState = newstate

    def is_objective_satisfied(self):
        if self.objectiveLevel is not None:
            return QuestManager.checkObjectiveSatisfaction(self.objectiveLevel)
        return True

    def update(self, npcs, player, dt, curr_time):
        shouldOpen = False
        for npc in npcs:
            shouldOpen |= self.shouldOpen(npc)
        shouldOpenPlayer = self.shouldOpen(player)
        if not shouldOpen and not shouldOpenPlayer:
            self.updateKeypadState(None)
        if shouldOpenPlayer:
            shouldOpenPlayer = self.is_objective_satisfied()
            if not shouldOpenPlayer:
                EventManager.set_notification("Access Denied: Door is locked")
            if self.securityLevel > 0 and shouldOpenPlayer == True:
                if player.getKeyLevel() >= self.securityLevel:
                    self.updateKeypadState(True)
                else:
                    EventManager.set_notification("Access Denied: Need Lv. " + str(self.securityLevel) + " key")
                    self.updateKeypadState(False)
                    shouldOpenPlayer = False
        shouldOpen |= shouldOpenPlayer
        if shouldOpen:
            if not self.isOpen():
                if not self.opening:
                    if curr_time - player.get_last_warp_time() >= 0.5:
                        # We don't want to play this sound twice when warping auto-opens a door
                        SoundManager.play_sfx("door")
                self.opening = True
                self.closing = False
        else:
            if not self.isClosed():
                self.opening = False
                self.closing = True
        if self.opening:
            self.open(100, dt)
        elif self.closing:
            self.close(-100, dt)

class DuctVent:
    def __init__(self, x, y):
        self.id = uuid.uuid4()
        self.tileX = int(x)
        self.tileY = int(y)-1
        self.open = False
        self.width = 1
        self.securityLevel = 0
        
    def show(self):
        pass

    def hide(self):
        pass

    def isOpen(self):
        return self.open
    
    def isClosed(self):
        return not self.open

    def forceClose(self):
        self.open = False

    def is_objective_satisfied(self):
        # DuctVents can't have objectives
        return True

    def collidesWith(self, px, py):
        if self.tileX == px and py == self.tileY:
            return not self.open
        return False

    def shouldOpen(self, humanoid):
        if (self.tileX == humanoid.getTileX()) and ((self.tileY + 1) >= humanoid.getTileY()) and ((self.tileY - 1) <= humanoid.getTileY()+1):
            return humanoid.prone
        return False

    def update(self, npcs, player, dt, curr_time):
        if self.shouldOpen(player):
            self.open = True
        else:
            self.open = False