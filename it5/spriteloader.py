import pyglet
import os

if "@it5.sprites" not in pyglet.resource.path:
    pyglet.resource.path.append("@it5.sprites")
    pyglet.resource.reindex()

working_path = os.path.realpath(pyglet.resource.location("common.png").path)

def getSprites(filename, rows, cols):
    #print("pyglet.resource.path is %s" % str(pyglet.resource.path))
    spritesheet = pyglet.resource.image(filename)
    pyglet.gl.glTexParameteri(spritesheet.target, pyglet.gl.GL_TEXTURE_MAG_FILTER, pyglet.gl.GL_NEAREST)
    pyglet.gl.glTexParameteri(spritesheet.target, pyglet.gl.GL_TEXTURE_MIN_FILTER, pyglet.gl.GL_NEAREST)
    sprites = pyglet.image.ImageGrid(spritesheet, rows, cols)
    return sprites

def getImage(filename):
    # If we aren't grabbing sprites, don't cache them to pyglet's TextureBin
    image = pyglet.image.load(os.path.join(working_path, filename))
    return image