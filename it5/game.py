import pyglet
# Grants us a nice performance speedup if python wasn't started with -O flag:
pyglet.options["debug_gl"] = False
from it5.player import Player, PlayerInSuit
from it5.direction import Direction
from it5.level import LevelSet
from it5.mainmenu import MainMenu
from it5.dialog import DialogAction
from it5.common import batches, batchGroups, EventManager, hidden_mouse_cursor, CommonManager, common_tileset, common_spritemap, lighting
from it5.quest import QuestManager, QuestUI, QuestUIAction
import math
from it5.script import ScriptManager
from it5.conditionals.spawn_conditionals import SpawnConditionalManager
from it5.audio import SoundManager
from it5.persistence import PersistenceManager
from it5.animation import AnimationGroupManager, Animation, AnimationGroup
from it5.dynamicobject import DynamicObject
from it5.camera import Camera
from it5.viewport import FixedResolutionViewport
from it5.gui.manager import UIManager
from it5.window import GameWindow
from it5.util import PathfindingProcessManager
from it5.resources import ResourceManager
from it5.warp import Warp
from it5.item import Item
from it5.controls import Controls, ControlAction
import json

ResourceManager.set_resource_path()
Controls.initialize(PersistenceManager.load_controls())

#FULLSCREEN = False
#window = GameWindow(width=1024, height=768, fullscreen=FULLSCREEN)
#fps_display = pyglet.window.FPSDisplay(window)
FULLSCREEN = True
window = GameWindow(fullscreen=FULLSCREEN)

window.set_exclusive_mouse(FULLSCREEN)
window.set_caption("The Relict")

pyglet.gl.glEnable(pyglet.gl.GL_BLEND)
pyglet.gl.glBlendFunc(pyglet.gl.GL_SRC_ALPHA, pyglet.gl.GL_ONE_MINUS_SRC_ALPHA)

def get_anchor_x(viewport):
    aspect_width = viewport.window.width / float(viewport.width)
    scale_width = aspect_width * viewport.width
    visible_scale_width = aspect_width * (viewport.width - 256)
    return viewport.window.width - (scale_width - visible_scale_width)

def getSidePanel(viewport):
    aspect_width = viewport.window.width / float(viewport.width)
    aspect_height = viewport.window.height / float(viewport.height)
    scale_width = aspect_width * viewport.width
    visible_scale_width = aspect_width * (viewport.width - 256)
    scale_height = aspect_height * viewport.height

    radarBGPattern = pyglet.image.SolidColorImagePattern((0,0,0,255))
    radarImage = radarBGPattern.create_image(256,256)

    hudBGPattern = pyglet.image.SolidColorImagePattern((64,64,64,255))
    anchor_x = viewport.window.width - (scale_width - visible_scale_width)
    hudImage = hudBGPattern.create_image(int(scale_width - visible_scale_width), window.height)

    hPanel = pyglet.sprite.Sprite(img=hudImage)
    hPanel.position = (anchor_x, 0)

    rPanel = pyglet.sprite.Sprite(img=radarImage)
    rPanel_x = (anchor_x + (window.width - 256)) / 2
    rPanel.position = (rPanel_x, window.height - 256)

    rOverlay = pyglet.sprite.Sprite(img=common_tileset[common_spritemap["green_static"]])
    rOverlay.position = rPanel.position
    rOverlay.scale_x = 8.0
    rOverlay.scale_y = 8.0
    rOverlay.visible = False

    return hPanel, rPanel, rOverlay

playerEventDispatcher = pyglet.event.EventDispatcher()
playerEventDispatcher.register_event_type("o2_change")
EventManager.initialize()
commonEventDispatcher = EventManager.get_event_dispatcher()

mainBatch = batches["main"]
hudBatch = batches["hud"]
radarBatch = batches["radar"]
dialogBatch = batches["dialog"]
menuBatch = batches["menu"]

CommonManager.init_weather(192, "rain", mainBatch, batchGroups["weather"])
weather = CommonManager.get_weather_manager()

CommonManager.init_bullets(64, mainBatch, batchGroups["airborne"], batchGroups["items"])
bullets = CommonManager.get_bullet_manager()

SpawnConditionalManager.initialize()

player = None

viewport = FixedResolutionViewport(window, 
    1024, 768, filtered=True)

camera = Camera(viewport)
sidePanel, radarPanel, radarOverlay = getSidePanel(viewport)

CommonManager.init_hud(window, hudBatch, get_anchor_x(viewport))
hud = CommonManager.get_hud()
currLevelSet = None
currLevel = None
currLevelMap = None
recently_acquired_items = set()
recent_dialog = None
paused = True
deadNPCs = []

def on_new_game():
    global paused
    SoundManager.stop_music()
    if currLevelMap is not None:
        currLevelMap.tearDown()
    initialize()
    paused = False
    SoundManager.play_music(currLevelMap.soundtrack)

def on_continue():
    global paused
    global player
    SoundManager.stop_music()
    if currLevelMap is not None:
        currLevelMap.tearDown()
    # Load game
    if PersistenceManager.load(initialize, changeMap):
        if player.alt_clothing:
            player_change_clothes(True, loading_game=True)
        paused = False
        SoundManager.play_music(currLevelMap.soundtrack)
    hud.setMaxPlayerHealth(player.maxHealth)
    hud.setPlayerHealth(player.health)

def on_toggle_fullscreen(in_game=False, last_soundtrack=None):
    global FULLSCREEN
    global sidePanel
    global radarPanel
    global radarOverlay
    global mainMenu
    FULLSCREEN = not FULLSCREEN
    SoundManager.stop_music()
    cursor = None
    if FULLSCREEN:
        window.set_fullscreen(True)
        cursor = hidden_mouse_cursor
    else:
        window.set_fullscreen(False, width=1024, height=768)
    window.set_mouse_cursor(cursor)
    window.set_exclusive_mouse(FULLSCREEN)
    sidePanel, radarPanel, radarOverlay = getSidePanel(viewport)
    anchor_x = get_anchor_x(viewport)
    hud.resize(anchor_x)
    SoundManager.play_music('title')
    ScriptManager.set_anchor_x(anchor_x)
    QuestUI.resize(anchor_x)
    if currLevel is not None:
        currLevel.dialogManager.resize(anchor_x)
    if in_game and player is not None:
        player.changeItem(0)
        player.changeWeapon(0)
    # Create a new main menu instead of trying to resize things
    mainMenu = MainMenu(menuBatch, batchGroups, window, on_new_game, on_continue, on_toggle_fullscreen, in_game=in_game, last_soundtrack=last_soundtrack)

mainMenu = MainMenu(menuBatch, batchGroups, window, on_new_game, on_continue, on_toggle_fullscreen)
SoundManager.initialize()

def initialize(levelset_file="master_levelset.json"):
    global player
    global currLevel
    global currLevelSet
    global currLevelMap
    global recent_dialog
    global paused

    player = Player(0, 0, playerEventDispatcher, mainBatch, batchGroups)
    player.initialize()
    hud.setMaxPlayerHealth(player.maxHealth)
    hud.setPlayerHealth(player.health)

    anchor_x = get_anchor_x(viewport)
    currLevelSet = LevelSet(levelset_file, batches, batchGroups, window, anchor_x)
    currLevel = currLevelSet.loadLevel(0)
    currLevelMap = currLevel.getCurrentMap()
    player.jump(currLevel.startLocation['x'], currLevel.startLocation['y'])
    lighting.destroy_lights()
    currLevelMap.initialize(QuestManager.active_quest)
    for light in currLevelMap.lights:
        lighting.create_light(**light)
    lighting.set_ambience(currLevelMap.ambience)
    hud.setLevel(currLevel.name)
    weather.deactivate()
    if currLevelMap.weather is not None:
        if currLevelMap.weather == 'rain':
            weather.changeToRain()
        elif currLevelMap.weather == 'snow':
            weather.changeToSnow()
        weather.activate(currLevelMap.width_px, currLevelMap.height_px)
    if currLevelMap.dialog != None:
        currLevel.dialogManager.openDialog(currLevelMap.dialog)
        EventManager.dialog_started(currLevelMap.dialog)

    for trigger in currLevelMap.triggers:
        trigger.saveState()

    recently_acquired_items.clear()
    recent_dialog = None
    player.saveStatus()
    QuestManager.saveCopy()
    QuestUI.initialize(menuBatch, batchGroups, window, anchor_x)
    paused = True

    ScriptManager.initialize(player, bullets, window, anchor_x)
    ScriptManager.change_map(currLevelMap)

    return (currLevel, player)

def openDialog(dialog, rewind=False):
    global paused
    if rewind:
        currLevel.dialogManager.rewind(dialog)
    if currLevel.dialogManager.openDialog(dialog):
        EventManager.dialog_started(dialog)
        paused = True

def changeMap(mapIndex, x, y, levelIndex=None, respawning=False, load_dict=None):
    global currLevel
    global currLevelMap
    global paused
    global recent_dialog
    global mainMenu
    currLevelMap.tearDown()
    AnimationGroupManager.clear()
    lighting.destroy_lights()
    bullets.stop()
    finishedGame = False
    if levelIndex != None:
        nextLevel = currLevelSet.loadLevel(levelIndex)
        if nextLevel != None:
            currLevel = nextLevel
        else:
            hud.setLevel("The End!")
            finishedGame = True
            ScriptManager.clear()
            mainMenu = MainMenu(menuBatch, batchGroups, window, on_new_game, on_continue, on_toggle_fullscreen)
            mainMenu.end_game()
            SoundManager.play_music("title")
    if not finishedGame:
        if load_dict is not None:
            # We have to load our quests here, since LevelSet.loadLevel() clears this
            QuestManager.from_dict(load_dict['quests'], load_dict['active_quest'])
        elif respawning:
            QuestManager.loadCopy()
        player.set_last_warp_time(camera.time)
        currLevel.changeMap(mapIndex)
        currLevelMap = currLevel.getCurrentMap()
        lighting.destroy_lights()
        currLevelMap.initialize(QuestManager.active_quest)
        player_has_light_equipped = player.getItem() == "flashlight"
        if player.light is not None:
            player.destroy_light()
        for light in currLevelMap.lights:
            lighting.create_light(**light)
        lighting.set_ambience(currLevelMap.ambience)
        warpY = (currLevelMap.height - y) - 1
        if respawning:
            warpY = y
        player.jump(x, warpY)
        camera.reset(player, currLevelMap)
        hud.setLevel(currLevel.name)
        ScriptManager.change_map(currLevelMap)
        player.saveStatus()
        # Handle weather
        weather.deactivate()
        if currLevelMap.weather is not None:
            if currLevelMap.weather == "rain":
                weather.changeToRain()
            elif currLevelMap.weather == "snow":
                weather.changeToSnow()
            weather.activate(currLevelMap.width_px, currLevelMap.height_px)
        # Handle dialog
        if currLevelMap.dialog != None:
            if currLevel.dialogManager.openDialog(currLevelMap.dialog):
                paused = True
                recent_dialog = currLevelMap.dialog
                EventManager.dialog_started(recent_dialog)
        # Handle soundtrack
        SoundManager.play_music(currLevelMap.soundtrack)
        # Re-add the player's light
        if player_has_light_equipped:
            player.create_light()
        # Set gas status of player
        player.in_gas = currLevelMap.gas
        if currLevelMap.gas:
            set_notification("Danger! Poison gas detected!")
        if currLevelMap.radar_jammed:
            radarOverlay.visible = True
        else:
            radarOverlay.visible = False
    return (currLevel, currLevelMap)

def checkNPCDialog():
    x = player.x + 15
    y = player.y - 15
    candidates = []
    max_distance = 64
    for npc in currLevelMap.npcs:
        if npc.friendly and (npc.dialog is not None or (npc.script is not None and not npc.script_triggered)):
            tx = npc.x + 15
            ty = npc.y - 15
            dx = x - tx
            dy = y - ty
            distance = math.sqrt((dx * dx) + (dy * dy))
            if distance < max_distance:
                facing = False
                if player.direction == Direction.UP:
                    if dy <= 0:
                        facing = True
                elif player.direction == Direction.DOWN:
                    if dy >= 0:
                        facing = True
                elif player.direction == Direction.LEFT:
                    if dx >= 0:
                        facing = True
                elif player.direction == Direction.RIGHT:
                    if dx <= 0:
                        facing = True
                if facing or distance < 16:
                    if abs(dx) > abs(dy):
                        # Left/Right
                        if dx < 0:
                            direction = Direction.LEFT
                        else:
                            direction = Direction.RIGHT
                    else:
                        # Up/Down
                        if dy > 0:
                            direction = Direction.UP
                        else:
                            direction = Direction.DOWN
                    candidates.append((npc, distance, direction))
    top_candidate = None
    for candidate in candidates:
        if top_candidate is None:
            top_candidate = candidate
        else:
            # Choose candidate w/ shortest distance to player
            if candidate[1] < top_candidate[1]:
                top_candidate = candidate

    if top_candidate is not None:
        npc, distance, direction = top_candidate
        npc.cacheLastDirection()
        npc.changeDirection(direction)
        # Hack: this prevents the NPC from reverting its direction before a script can trigger a dialog
        npc.waitTimer += 0.1
        if npc.script is not None:
            if not npc.script_triggered:
                npc.script_triggered = True
                if npc.dialog is None:
                    # Hide the dialog marker if NPC doesn't have dialog
                    npc.hideDialogMarker()
                ScriptManager.load(npc.script)
            elif npc.dialog is not None:
                openDialog(npc.dialog, True)
        else:
            openDialog(npc.dialog, True)

@playerEventDispatcher.event
def item_change(item, itemValue):
    #print(item + " " + str(itemValue))
    title=True
    if item.lower() == "nvg":
        item = "NVG"
        itemValue = ""
        title=False
        lighting.set_shader("night_vision")
    else:
        if lighting.selected_shader is "night_vision":
            lighting.set_shader("ambient")
        if item.lower() == "flashlight":
            itemValue = ""
    hud.setItem(item, itemValue, title=title)

@playerEventDispatcher.event
def weapon_change(weapon, ammo, maxAmmo):
    #print(weapon)
    hud.setWeapon(weapon, ammo, maxAmmo)

@playerEventDispatcher.event
def health_change(health):
    hud.setPlayerHealth(health)

@playerEventDispatcher.event
def o2_change(o2):
    hud.setPlayerOxygen(o2)

@playerEventDispatcher.event
def max_health_change(health):
    hud.setMaxPlayerHealth(health)

@commonEventDispatcher.event
def set_notification(notification):
    hud.updateNotificationMessage(notification)

@commonEventDispatcher.event
def gunshot(radius):
    r_squared = radius * radius
    for npc in currLevelMap.npcs:
        npc.hear_gunshot(player, currLevelMap, r_squared)

@commonEventDispatcher.event
def alert(npc_id):
    npcs = []
    for npc in currLevelMap.npcs:
        if id(npc) == npc_id:
            npcs = [npc] + npcs
        else:
            npcs.append(npc)
    for npc in npcs:
        npc.on_alert()

@commonEventDispatcher.event
def cancel_alert():
    for npc in currLevelMap.npcs:
        npc.cancel_alert()
    for s_camera in currLevelMap.cameras:
        s_camera.cancel_alert()
    SoundManager.play_music(currLevelMap.soundtrack)

@commonEventDispatcher.event
def reset_camera():
    camera.reset(player, currLevelMap)

@commonEventDispatcher.event
def show_image_popout(image, img_width, img_height):
    UIManager.show_image_popout(image, width=img_width, height=img_height, window_width=window.width, window_height=window.height)

@commonEventDispatcher.event
def player_change_clothes(into_suit, loading_game=False):
    global player
    if player.alt_clothing != into_suit or loading_game:
        tx = player.getTileX()
        ty = player.getTileY()
        if into_suit:
            new_player = PlayerInSuit(player.getTileX(), player.getTileY(), playerEventDispatcher, mainBatch, batchGroups)
        else:
            new_player = Player(player.getTileX(), player.getTileY(), playerEventDispatcher, mainBatch, batchGroups)
        new_player.from_player(player, loading_game=loading_game)
        # Remove Research Materials from player's inventory... He's supposed to lose them here.
        new_player.shadow_inventory.items.pop("Research Materials", None)
        new_player.initialize()
        player.destroy()
        player = new_player
        if player.alt_clothing:
            player.clear_inventory()
        player.jump(tx, ty)
        ScriptManager.change_player(player)

@commonEventDispatcher.event
def explosion(x, y, damage=100):
    tileX = int((x+15)/32)
    tileY = int((y-31)/32)+1
    # Damage application
    radius = 48
    radius_squared = radius * radius
    for npc in currLevelMap.npcs:
        distance = ((npc.x - x) * (npc.x - x) + (npc.y - y) * (npc.y - y))
        distance2 = ((npc.x - x) * (npc.x - x) + (npc.y + 32 - y) * (npc.y + 32 - y))
        if distance <= radius_squared or distance2 <= radius_squared:
            npc.receiveDamage(damage, player)
    for s_camera in currLevelMap.cameras:
        distance = ((s_camera.x - x) * (s_camera.x - x) + (s_camera.y - y) * (s_camera.y - y))
        if distance <= radius_squared:
            s_camera.receive_damage(damage)
    # Check if Player in range of explosion
    distance = ((player.x - x) * (player.x - x) + (player.y - y) * (player.y - y))
    distance2 = ((player.x - x) * (player.x - x) + (player.y + 32 - y) * (player.y + 32 - y))
    if distance <= radius_squared or distance2 <= radius_squared:
        player.receiveDamage(damage)
    # Create the animation
    batch = currLevelMap.batches["main"]
    batchGroups = currLevelMap.batchGroups
    center_obj = DynamicObject(tileX, tileY, common_tileset, common_spritemap['explosion'][0], common_spritemap['explosion'][1], batch, batchGroups['explosions'])
    xdelta = x - center_obj.x
    ydelta = y - center_obj.y
    center_obj.move(xdelta, ydelta)
    n_obj = DynamicObject(tileX, tileY, common_tileset, common_spritemap['explosion'][0], common_spritemap['explosion'][1], batch, batchGroups['explosions'])
    n_obj.move(xdelta, ydelta)
    s_obj = DynamicObject(tileX, tileY, common_tileset, common_spritemap['explosion'][0], common_spritemap['explosion'][1], batch, batchGroups['explosions'])
    s_obj.move(xdelta, ydelta)
    e_obj = DynamicObject(tileX, tileY, common_tileset, common_spritemap['explosion'][0], common_spritemap['explosion'][1], batch, batchGroups['explosions'])
    e_obj.move(xdelta, ydelta)
    w_obj = DynamicObject(tileX, tileY, common_tileset, common_spritemap['explosion'][0], common_spritemap['explosion'][1], batch, batchGroups['explosions'])
    w_obj.move(xdelta, ydelta)
    ne_obj = DynamicObject(tileX, tileY, common_tileset, common_spritemap['explosion'][0], common_spritemap['explosion'][1], batch, batchGroups['explosions'])
    ne_obj.move(xdelta, ydelta)
    nw_obj = DynamicObject(tileX, tileY, common_tileset, common_spritemap['explosion'][0], common_spritemap['explosion'][1], batch, batchGroups['explosions'])
    nw_obj.move(xdelta, ydelta)
    se_obj = DynamicObject(tileX, tileY, common_tileset, common_spritemap['explosion'][0], common_spritemap['explosion'][1], batch, batchGroups['explosions'])
    se_obj.move(xdelta, ydelta)
    sw_obj = DynamicObject(tileX, tileY, common_tileset, common_spritemap['explosion'][0], common_spritemap['explosion'][1], batch, batchGroups['explosions'])
    sw_obj.move(xdelta, ydelta)
    frames = [common_spritemap['explosion']]
    center_anim = Animation(center_obj, frames, 1, lifetime_seconds=0.1)
    velocity = 225
    edge_velocity = 175
    n_anim = Animation(n_obj, frames, 1, lifetime_seconds=0.1, dx=0, dy=velocity)
    s_anim = Animation(s_obj, frames, 1, lifetime_seconds=0.1, dx=0, dy=-velocity)
    e_anim = Animation(e_obj, frames, 1, lifetime_seconds=0.1, dx=velocity, dy=0)
    w_anim = Animation(w_obj, frames, 1, lifetime_seconds=0.1, dx=-velocity, dy=0)
    ne_anim = Animation(ne_obj, frames, 1, lifetime_seconds=0.1, dx=edge_velocity, dy=edge_velocity)
    nw_anim = Animation(nw_obj, frames, 1, lifetime_seconds=0.1, dx=-edge_velocity, dy=edge_velocity)
    se_anim = Animation(se_obj, frames, 1, lifetime_seconds=0.1, dx=edge_velocity, dy=-edge_velocity)
    sw_anim = Animation(sw_obj, frames, 1, lifetime_seconds=0.1, dx=-edge_velocity, dy=-edge_velocity)
    explosion_anim_group = AnimationGroup(center_anim, n_anim, s_anim, e_anim, w_anim, ne_anim, nw_anim, se_anim, sw_anim)
    explosion_anim_group.set_light(lighting.create_light(x=x+16, y=y+16, radius=radius*2, color=(255,255,255)))
    AnimationGroupManager.add_animation_group(explosion_anim_group)
    SoundManager.play_sfx('explosion')
    # This is ok; there should never be any explosions that don't alert the guards to the player
    gunshot(768)

@commonEventDispatcher.event
def smoke_bomb(x, y):
    tileX = int((x+15)/32)
    tileY = int((y-31)/32)+1
    # Create the animation
    batch = currLevelMap.batches["main"]
    batch_group = currLevelMap.batchGroups["explosions"]
    smoke_sprite_uv = common_spritemap["smoke"]
    center_obj = DynamicObject(tileX, tileY, common_tileset, smoke_sprite_uv[0], smoke_sprite_uv[1], batch, batch_group)
    xdelta = x - center_obj.x
    ydelta = y - center_obj.y
    center_obj.move(xdelta, ydelta)
    n_obj = DynamicObject(tileX, tileY, common_tileset, smoke_sprite_uv[0], smoke_sprite_uv[1], batch, batch_group)
    n_obj.move(xdelta, ydelta)
    e_obj = DynamicObject(tileX, tileY, common_tileset, smoke_sprite_uv[0], smoke_sprite_uv[1], batch, batch_group)
    e_obj.move(xdelta, ydelta)
    w_obj = DynamicObject(tileX, tileY, common_tileset, smoke_sprite_uv[0], smoke_sprite_uv[1], batch, batch_group)
    w_obj.move(xdelta, ydelta)
    ne_obj = DynamicObject(tileX, tileY, common_tileset, smoke_sprite_uv[0], smoke_sprite_uv[1], batch, batch_group)
    ne_obj.move(xdelta, ydelta)
    nw_obj = DynamicObject(tileX, tileY, common_tileset, smoke_sprite_uv[0], smoke_sprite_uv[1], batch, batch_group)
    nw_obj.move(xdelta, ydelta)
    frames = [smoke_sprite_uv]
    center_anim = Animation(center_obj, frames, 1, lifetime_seconds=0.1)
    velocity = 225
    edge_velocity = 175
    n_anim = Animation(n_obj, frames, 1, lifetime_seconds=0.1, dx=0, dy=velocity)
    e_anim = Animation(e_obj, frames, 1, lifetime_seconds=0.1, dx=velocity, dy=0)
    w_anim = Animation(w_obj, frames, 1, lifetime_seconds=0.1, dx=-velocity, dy=0)
    ne_anim = Animation(ne_obj, frames, 1, lifetime_seconds=0.1, dx=edge_velocity, dy=edge_velocity)
    nw_anim = Animation(nw_obj, frames, 1, lifetime_seconds=0.1, dx=-edge_velocity, dy=edge_velocity)
    smoke_anim_group = AnimationGroup(center_anim, n_anim, e_anim, w_anim, ne_anim, nw_anim)
    smoke_anim_group.set_light(lighting.create_light(x=x+16, y=y+16, radius=384, steps=8, light_type="shimmering"))
    AnimationGroupManager.add_animation_group(smoke_anim_group)

def quit_game(*args):
    # Close the window, exit the game
    window.on_close()

def save_game(*args):
    if "elevator" not in currLevelMap.metadata:
        PersistenceManager.save(currLevelSet, currLevel, currLevelMap, player, recently_acquired_items)
        EventManager.set_notification("Game Saved")
    else:
        EventManager.set_notification("Cannot save game; please exit elevator before saving")
    UIManager.hide()

def return_to_main_menu(*args):
    global mainMenu
    global paused
    UIManager.hide()
    mainMenu = MainMenu(menuBatch, batchGroups, window, on_new_game, on_continue, on_toggle_fullscreen, in_game=True, last_soundtrack=SoundManager.current_soundtrack)
    paused = True
    SoundManager.play_music("title")

def _console_input_callback(console_input):
    global paused
    try:
        if console_input is not None:
            # Tilde chars will always be stripped out
            cinput = console_input.strip().replace("`", "")
            cinput_ignorecase = cinput.lower()
            csplit = cinput.split(" ")
            if cinput_ignorecase.startswith("warp"):
                if len(csplit) > 3:
                    dest_x = int(csplit[1])
                    dest_y = int(csplit[2])
                    dest_map = csplit[3]
                    level_index = None
                    if len(csplit) > 4:
                        level_index = int(csplit[4])
                    warp = Warp(0, 0, dest_x, dest_y, dest_map, levelIndex=level_index)
                    playerWarp(warp)
                else:
                    print("Not enough args supplied to warp command")
            elif cinput_ignorecase.startswith("jump"):
                if len(csplit) > 2:
                    x = int(csplit[1])
                    y = int(csplit[2])
                    if player is not None:
                        player.jump(x, y)
                else:
                    print("Not enough args supplied to jump command")
            elif cinput_ignorecase.startswith("set_health"):
                if len(csplit) > 1:
                    health = int(csplit[1])
                    if player is not None:
                        player.setHealth(health)
                else:
                    print("Not enough args supplied for set_health command")
            elif cinput_ignorecase.startswith("set_max_health"):
                if len(csplit) > 1:
                    health = int(csplit[1])
                    if player is not None:
                        player.setMaxHealth(health)
                else:
                    print("Not enough args supplied for set_max_health command")
            elif cinput_ignorecase.startswith("give_item"):
                if len(csplit) > 2:
                    item_type = csplit[1]
                    item_props = cinput[cinput.find(csplit[2]):]
                    print("item_props are: %s" % item_props)
                    item = Item(0, 0, None, None, None, item_type, json.loads(item_props))
                    if player is not None:
                        player.inventory.addTo(item)
                else:
                    print("Not enough args supplied for give_item command")
            elif cinput_ignorecase.startswith("key"):
                if len(csplit) > 1:
                    cardkey_level = int(csplit[1])
                    if player is not None:
                        item = Item(0, 0, None, None, None, "item", {"type": "key", "level": cardkey_level})
                        player.inventory.addTo(item)
                else:
                    print("Not enough args supplied for key command")
            elif cinput_ignorecase.startswith("objective_complete"):
                if len(csplit) > 2:
                    quest_name = csplit[1]
                    stage_name = csplit[2]
                    quest = QuestManager.getQuest(quest_name)
                    if quest is not None:
                        quest.forceCompleteStage(stage_name)
                else:
                    print("Not enough args supplied for objective_complete command")
            elif cinput_ignorecase.startswith("quests"):
                print(json.dumps(QuestManager.getActiveQuestsJSON(), indent=2))
            elif cinput_ignorecase.startswith("help"):
                print("command list:")
                print("warp <x> <y> <map> <level_index (optional)>")
                print("jump <x> <y>")
                print("set_health <health>")
                print("set_max_health <max_health>")
                print("give_item <type> <props -- this is in json format>")
                print("objective_complete <quest> <stage_name>")
                print("key <cardkey_level>")
                print("quests")
                print("help")
    except Exception as e:
        print("An error occurred when parsing console input")
        print(str(e))
    paused = False

@window.event
def on_draw():
    pyglet.gl.glPushMatrix()
    pyglet.gl.glTranslatef(camera.translateX, camera.translateY, 0)
    window.clear()
    viewport.begin()
    lighting.set_time(camera.time)
    mainBatch.draw()
    #lighting.bind()
    viewport.end(lighting)
    #lighting.unbind()
    pyglet.gl.glPopMatrix()

    sidePanel.draw()
    radarPanel.draw()
    hudBatch.draw()
    
    pyglet.gl.glPushMatrix()
    anchor_x = (get_anchor_x(viewport) + (window.width - 256)) / 2
    translateX = ((anchor_x + 128) - int(player.x/4)) - 2
    translateY = ((window.height - 164) - int(player.y/4)) + 31
    pyglet.gl.glTranslatef(translateX, translateY, 0)
    pyglet.gl.glEnable(pyglet.gl.GL_SCISSOR_TEST)
    pyglet.gl.glScissor(int(anchor_x), window.height-256, 256, 256)
    radarBatch.draw()
    pyglet.gl.glDisable(pyglet.gl.GL_SCISSOR_TEST)
    pyglet.gl.glPopMatrix()
    radarOverlay.draw()
    dialogBatch.draw()
    menuBatch.draw()
    #fps_display.draw()

@window.event
def on_key_press(symbol, modifiers):
    global paused

    action = Controls.get_keypress_action(symbol)
    if currLevel.dialogManager.isOpen():
        _action = currLevel.dialogManager.on_key_press(action, modifiers)
        if _action == DialogAction.CLOSE:
            for npc in currLevelMap.npcs:
                npc.resumePostEvent()
            EventManager.dialog_finished()
            paused = False
    elif QuestUI.isOpen():
        _action = QuestUI.on_key_press(action, modifiers)
        if _action == QuestUIAction.CLOSE:
            paused = False
        elif _action == QuestUIAction.SELECT:
            currLevelMap.refreshObjectives(QuestManager.active_quest)
    elif UIManager.is_open():
        paused = UIManager.on_key_press(action, symbol, modifiers)
    else:
        if not paused:
            if player.alive:
                if action == ControlAction.UP:
                    player.startMoving(Direction.UP)
                elif action == ControlAction.DOWN:
                    player.startMoving(Direction.DOWN)
                elif action == ControlAction.LEFT:
                    player.startMoving(Direction.LEFT)
                elif action == ControlAction.RIGHT:
                    player.startMoving(Direction.RIGHT)
                elif action == ControlAction.FIRE:
                    shot_fired = player.shoot(bullets)
                    if shot_fired:
                        hud.updateAmmoDisplay(player.weapon.getAmmo())
                elif action == ControlAction.MELEE:
                    player.punch(currLevelMap.npcs)
                elif action == ControlAction.TOGGLE_PRONE:
                    player.toggle_prone(currLevelMap.force_prone)
                elif action == ControlAction.TALK:
                    checkNPCDialog()
                elif action == ControlAction.USE_MEDKIT:
                    player.attempt_to_heal()
                elif action == ControlAction.TOGGLE_CONSOLE:
                    paused = True
                    anchor_x = get_anchor_x(viewport)
                    UIManager.show_text_input("Console", _console_input_callback, window,
                                  width=int(window.width/2), height=128, x=0, y=window.height,
                                  anchor_x="left", anchor_y="top")
                elif action == ControlAction.SAVE:
                    save_game()
                elif action == ControlAction.EXIT:
                    paused = True
                    anchor_x = get_anchor_x(viewport)
                    UIManager.show_menu("Quit Game?", [("Quit Without Saving", quit_game), ("Save Game", save_game), ("Return to Main Menu", return_to_main_menu)],
                    width=250, height=200, x=anchor_x/2, y=window.height/2, exit_option="Cancel", anchor_x="center", anchor_y="center")
        else:
            if player.alive:
                if action == ControlAction.UP:
                    player.changeItem(1)
                elif action == ControlAction.DOWN:
                    player.changeItem(-1)
                elif action == ControlAction.LEFT:
                    player.changeWeapon(-1)
                elif action == ControlAction.RIGHT:
                    player.changeWeapon(1)
                elif action == ControlAction.FIRE or action == ControlAction.MELEE:
                    player.useItem()

        if action == ControlAction.TOGGLE_PAUSE:
            if not QuestUI.isOpen():
                paused = not paused
                if paused:
                    hud.pause()
                    if player.moving:
                        player.stopMoving(None)
                    #print((player.getTileX(), player.getTileY()))
                    #print(player.inventory)
                else:
                    hud.resume()
                    if player.alive == False:
                        player.respawn()
                        max_health_change(player.maxHealth)
                        health_change(player.health)
                        o2_change(player.oxygen)
                        changeMap(currLevelMap.index, player.getTileX(), player.getTileY(), respawning=True)
                        # Re-add items that were recently acquired, respawn recent dialogs, objectives, script triggers
                        while len(recently_acquired_items) > 0:
                            respawn_item = recently_acquired_items.pop()
                            respawn_item.remove = False
                            currLevelMap.items.add(respawn_item)
                            respawn_item.show()
                        if recent_dialog is not None:
                            openDialog(recent_dialog, rewind=True)
                        for trigger in currLevelMap.triggers:
                            trigger.reset()
                        currLevelMap.refreshObjectives(QuestManager.active_quest)
        elif action == ControlAction.TOGGLE_MISSION_VIEWER:
            if not paused:
                if not QuestUI.isOpen():
                    paused = True
                    QuestUI.open()

@window.event
def on_key_release(symbol, modifiers):
    action = Controls.get_keypress_action(symbol)
    if UIManager.is_open():
        UIManager.on_key_release(action, symbol, modifiers)
    elif player.alive:
        if action == ControlAction.UP:
            player.stopMoving(Direction.UP)
        elif action == ControlAction.DOWN:
            player.stopMoving(Direction.DOWN)
        elif action == ControlAction.LEFT:
            player.stopMoving(Direction.LEFT)
        elif action == ControlAction.RIGHT:
            player.stopMoving(Direction.RIGHT)
        elif action == ControlAction.FIRE:
            player.stopMoving(None)
            player.stop_shooting()
        elif action == ControlAction.MELEE:
            player.stopMoving(None)

def playerDied():
    global paused
    paused = True
    print('Player has died')
    player.stopMoving(None)
    player.top.setVisible(False)
    player.bottom.setVisible(False)
    hud.gameOver()

def playerWarp(warp):
    '''
    This is where things should be cached, not in changeMap().
    '''
    global recent_dialog
    # Clear cached game objects
    recently_acquired_items.clear()
    recent_dialog = None
    # Cache relevant state information
    QuestManager.saveCopy()
    for trigger in currLevelMap.triggers:
        trigger.saveState()
    # This is important so we don't overshoot triggers
    player.clearMoveStack()
    player.stopMoving(player.direction)
    changeMap(warp.mapIndex, warp.warpX, warp.warpY, warp.levelIndex)
    if warp.elevatorRef is not None:
        # This is how we reference which elevator warp context to use
        currLevelMap.metadata["elevator"] = warp.elevatorRef
    currLevelMap.refreshObjectives(QuestManager.active_quest)

def handleTriggerResult(result):
    if result['type'] == 'dialog':
        openDialog(result['data'], rewind=True)

def update(dt):
    if not paused:
        itemRemovals = player.update(dt, currLevelMap.obstacleMatrix, currLevelMap.items, currLevelMap.doors)
        camera.update(player, currLevelMap, dt)
        if itemRemovals:
            player.changeWeapon(0)
            player.changeItem(0)
            toRemove = []
            for item in currLevelMap.items:
                if item.shouldRemove():
                    toRemove.append(item)
            for r in toRemove:
                r.hide()
                currLevelMap.items.remove(r)
                if not r.temporary:
                    recently_acquired_items.add(r)
        weather.generateParticles()
        weather.move(dt)
        bullets.move(dt)
        bullets.checkCollisions(currLevelMap.obstacleMatrix, player, currLevelMap.npcs, currLevelMap.doors)
        for door in currLevelMap.doors:
            door.update(currLevelMap.npcs, player, dt, camera.time)
        for npc in currLevelMap.npcs:
            npc.update(dt, currLevelMap, player, bullets)
            if npc.alive == False:
                deadNPCs.append(npc)
        while len(deadNPCs) > 0:
            npc = deadNPCs.pop()
            npc.cleanUp()
            currLevelMap.npcs.remove(npc)
        for s_camera in currLevelMap.cameras:
            s_camera.update(dt, currLevelMap, player, bullets)
        warp = None
        playerTileX = player.getTileX()
        playerTileY = player.getTileY()
        for trigger in currLevelMap.triggers:
            trv = trigger.check(playerTileX, playerTileY)
            if trv is not None:
                handleTriggerResult(trv)
                break
        for _warp in currLevelMap.warps:
            if _warp.check(playerTileX, playerTileY):
                warp = _warp
                break
        if warp != None:
            playerWarp(warp)
        if player.alive == False:
            playerDied()
        ScriptManager.update(dt)
        AnimationGroupManager.animate(dt)
        hud.update(dt)
        UIManager.update(dt)
        PathfindingProcessManager.monitor_nowait()
    else:
        currLevel.dialogManager.update(dt)

pyglet.clock.schedule(update) # Update once per frame refresh

def run():
    SoundManager.play_music("title")
    initialize()
    pyglet.app.run()
