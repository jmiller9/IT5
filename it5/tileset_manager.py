from it5 import spriteloader

class TilesetManager:
    tileset_map = {}

    @classmethod
    def cacheTilesets(cls, tilesets):
        '''
        Caches tilesets, returns map of name:firstgid for each tileset,
        regardless of whether or not the image was added
        '''
        firstgid_mapping = {}
        for ts in tilesets:
            name = ts['name']
            # The "common" spritesheet is not considered a tileset
            if name != 'common':
                rawimageloc = ts['image']
                tilesetLocation = rawimageloc.split('/')[-1]
                tilesetHeight = ts['imageheight']
                tilesetWidth = ts['imagewidth']
                tileHeight = ts['tileheight']
                tileWidth = ts['tilewidth']
                firstgid = ts['firstgid']
                rowCount = int(tilesetHeight / tileHeight)
                colCount = int(tilesetWidth / tileWidth)

                if 'tilecount' in ts:
                    tilecount = ts['tilecount']
                else:
                    tilecount = rowCount * colCount
                
                firstgid_mapping[(firstgid, tilecount)] = name

                if name not in cls.tileset_map:
                    spritesheet_with_metadata = {
                        "rowCount": rowCount,
                        "colCount": colCount,
                        "tileset": spriteloader.getSprites(tilesetLocation, rowCount, colCount)
                    }
                    cls.tileset_map[name] = spritesheet_with_metadata
        return firstgid_mapping

    @classmethod
    def getTile(cls, gid, firstgid_mapping):
        for key in firstgid_mapping:
            firstgid, tilecount = key
            if gid >= firstgid and gid < firstgid + tilecount:
                spritesheet = cls.tileset_map[firstgid_mapping[key]]
                actualTileIndex = gid - firstgid
                actualTileX = int(actualTileIndex % spritesheet['colCount']) # col
                actualTileY = (spritesheet['rowCount'] - 1) - int(actualTileIndex / spritesheet['colCount']) # row
                return spritesheet['tileset'][(actualTileY, actualTileX)]

    @classmethod
    def getSpriteSheetAndCoords(cls, gid, firstgid_mapping):
        # Needed for creating Animations using DynamicObjects
        # DynamicObjects require an imagegrid and spritesheet coordinates
        for key in firstgid_mapping:
            firstgid, tilecount = key
            if gid >= firstgid and gid < firstgid + tilecount:
                spritesheet = cls.tileset_map[firstgid_mapping[key]]
                actualTileIndex = gid - firstgid
                actualTileX = int(actualTileIndex % spritesheet['colCount']) # col
                actualTileY = (spritesheet['rowCount'] - 1) - int(actualTileIndex / spritesheet['colCount']) # row
                return spritesheet['tileset'], actualTileY, actualTileX
        return None, None, None
