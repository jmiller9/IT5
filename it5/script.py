from it5.common import EventManager
from it5.quest import QuestManager
from it5.item import Item
from it5.warp import Warp

SCRIPTS_DIR = 'it5.scripts'

class ScriptType:
    ONETIME = 0
    PERSISTENT = 1

class CancelScriptLoadException(Exception):
    pass

class Script(object):
    def __init__(self, script_type):
        self.type = script_type
        self.started = False
        self.completed = False
        self.event_manager = EventManager
        self.triggers = []
        self.warps = []

    def _on_load(self, currLevelMap, player, bullet_manager, window, anchor_x, custom_options):
        self.currLevelMap = currLevelMap
        self.player = player
        self.bullet_manager = bullet_manager
        self.window = window
        self.anchor_x = anchor_x
        self.started = True
        self._options = custom_options
        self.on_load()

    def _update(self, dt):
        if not self.completed and self.started:
            self.update(dt)

    def _destroy(self):
        if self.currLevelMap is not None:
            for trigger in self.triggers:
                self.currLevelMap.triggers.remove(trigger)
            self.triggers.clear()
            for warp in self.warps:
                self.currLevelMap.warps.remove(warp)
            self.warps.clear()
        self.destroy()

    def _on_player_change(self, player):
        self.player = player
        self.on_player_change()

    def open_dialog(self, dialog):
        from it5.trigger import DialogTrigger
        if self.currLevelMap is not None and self.player is not None:
            # Player should stop moving when placing the DialogTrigger
            self.player.clearMoveStack()
            self.player.stopMoving(self.player.direction)
            dialog_trigger = DialogTrigger(self.player.getTileX(), self.player.getTileY() + 1, 'script_dialog_trigger_' + str(dialog), dialog, False)
            self.currLevelMap.triggers.append(dialog_trigger)
            self.triggers.append(dialog_trigger)

    def place_warp(self, tile_x, tile_y, warp_x, warp_y, map_index, level_index=None):
        warp = Warp(tile_x, tile_y, warp_x, warp_y, map_index, level_index)
        self.currLevelMap.warps.append(warp)
        self.warps.append(warp)

    def start_quest(self, quest_name, set_as_active=True):
        # Activate the quest without using a QuestTrigger:
        quest = QuestManager.getQuest(quest_name)
        is_new = not quest.active
        quest.active = True
        if is_new:
            self.event_manager.set_notification("New mission: " + quest.title)
        if set_as_active:
            QuestManager.active_quest = quest_name
            self.currLevelMap.refreshObjectives(quest_name)

    def get_quest_data(self, quest_name):
        return QuestManager.getQuest(quest_name).data

    def get_quest(self, quest_name):
        return QuestManager.getQuest(quest_name)

    def complete_objective(self, quest_name, objective_name):
        for objective in self.currLevelMap.objective_triggers[quest_name]:
            if objective.name == objective_name:
                objective.check(objective.tileX, objective.tileY)
                objective.hide()
                break

    def give_treasure(self, count):
        self.player.inventory.addTo(Item(0, 0, None, None, None, "treasure", {"count": count}))
        self.player.changeItem(0)

    def lock_doors(self):
        for door in self.currLevelMap.doors:
            door.locked = True

    def unlock_doors(self):
        for door in self.currLevelMap.doors:
            door.locked = False

    def start(self):
        if not self.completed:
            self.started = True

    def finish(self):
        self.completed = True

    def on_load(self):
        pass

    def update(self, dt):
        pass

    def destroy(self):
        pass

    def on_player_change(self):
        pass

class OneTimeScript(Script):
    def __init__(self):
        super().__init__(ScriptType.ONETIME)

    def _update(self, dt):
        super()._update(dt)
        if self.started:
            self.finish()

class PersistentScript(Script):
    def __init__(self):
        super().__init__(ScriptType.PERSISTENT)

class ScriptManager(object):
    scripts = []
    available_scripts = {}
    player = None
    bullet_manager = None
    currLevelMap = None
    window = None
    anchor_x = 768

    @classmethod
    def populate_scripts_dict(cls, script_class):
        subclasses = {_cls.__name__: _cls for _cls in script_class.__subclasses__()}
        for subclass_name, subclass in subclasses.items():
            cls.available_scripts[subclass_name] = subclass
            cls.populate_scripts_dict(subclass)

    @classmethod
    def initialize(cls, player, bullet_manager, window, anchor_x):
        cls.player = player
        cls.bullet_manager = bullet_manager
        cls.window = window
        cls.anchor_x = anchor_x
        from it5 import scripts
        cls.populate_scripts_dict(OneTimeScript)
        cls.populate_scripts_dict(PersistentScript)
        #print("Loaded Scripts")

    @classmethod
    def set_anchor_x(cls, anchor_x):
        cls.anchor_x = anchor_x

    @classmethod
    def change_map(cls, currLevelMap):
        cls.clear()
        cls.currLevelMap = currLevelMap

    @classmethod
    def load(cls, script_name, **custom_options):
        if script_name in cls.available_scripts:
            script = cls.available_scripts[script_name]()
            loaded = True
            try:
                script._on_load(cls.currLevelMap, cls.player, cls.bullet_manager, cls.window, cls.anchor_x, custom_options)
            except CancelScriptLoadException as csle:
                loaded = False
                print(csle)
            if loaded:
                cls.scripts.append(script)
        else:
            print("Warning: could not find script " + script_name)

    @classmethod
    def clear(cls):
        for script in cls.scripts:
            script._destroy()
        cls.scripts = []

    @classmethod
    def update(cls, dt):
        for script in cls.scripts:
            script._update(dt)

    @classmethod
    def change_player(cls, player):
        cls.player = player
        for script in cls.scripts:
            script._on_player_change(player)
