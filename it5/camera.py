class Camera:
    def __init__(self, viewport):
        self.cameraXOffset = float((viewport.width - 256) / 2)
        self.cameraYOffset = float(viewport.height / 2)
        self.translateX = 0
        self.translateY = 0
        self.lastTranslateXY = (0,0)
        self.viewport = viewport
        self.time = 0

    def lerp(self, v0: float, v1: float, t: float):
        return (1 - t) * v0 + t * v1

    def update(self, target, currLevelMap, dt):
        self.translateX = round(self.cameraXOffset - target.x, 0)
        self.translateY = round(self.cameraYOffset - target.y, 0)
        if self.viewport.height < (currLevelMap.width * 32):
            if target.x < self.cameraXOffset:
                self.translateX = 0
            elif target.x > (currLevelMap.width * 32) - self.cameraXOffset:
                self.translateX = self.viewport.width - (currLevelMap.width * 32) - 256
        else:
            self.translateX = (self.viewport.width - 256 - (currLevelMap.width * 32))/2
        if self.viewport.height < (currLevelMap.height * 32):
            if target.y < self.cameraYOffset + 32:
                self.translateY = -32
            elif target.y > (currLevelMap.height * 32) - self.cameraYOffset + 32:
                self.translateY = self.viewport.height - (currLevelMap.height * 32) - 32
        else:
            self.translateY = ((self.viewport.height - currLevelMap.height * 32)/2) - 32
        self.time += dt
        lastX, lastY = self.lastTranslateXY
        t = 0.25
        self.translateX = round(self.lerp(lastX, self.translateX, t), 0)
        self.translateY = round(self.lerp(lastY, self.translateY, t), 0)
        self.lastTranslateXY = (self.translateX, self.translateY)

    def reset(self, target, currLevelMap):
        translateX = round(self.cameraXOffset - target.x, 0)
        translateY = round(self.cameraYOffset - target.y, 0)
        if self.viewport.height < (currLevelMap.width * 32):
            if target.x < self.cameraXOffset:
                translateX = 0
            elif target.x > (currLevelMap.width * 32) - self.cameraXOffset:
                translateX = self.viewport.width - (currLevelMap.width * 32) - 256
        else:
            translateX = (self.viewport.width - 256 - (currLevelMap.width * 32))/2
        if self.viewport.height < (currLevelMap.height * 32):
            if target.y < self.cameraYOffset + 32:
                translateY = -32
            elif target.y > (currLevelMap.height * 32) - self.cameraYOffset + 32:
                translateY = self.viewport.height - (currLevelMap.height * 32) - 32
        else:
            translateY = ((self.viewport.height - currLevelMap.height * 32)/2) - 32
        self.lastTranslateXY = (translateX, translateY)