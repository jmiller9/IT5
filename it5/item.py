from it5.staticobject import StaticObject
import uuid
from enum import Enum
import copy

class WeaponType(Enum):
    PISTOL = 0
    SMG = 1
    CARBINE = 2
    GRENADE = 3
    ROCKET_LAUNCHER = 4
    MINE = 5

    @classmethod
    def get_type_from_str(cls, name):
        _name = name.upper().split(' ')[0]
        return getattr(cls, _name, None)

WeaponType.PISTOL.mag_size = 8
WeaponType.SMG.mag_size = 34
WeaponType.CARBINE.mag_size = 30
WeaponType.GRENADE.mag_size = 4
WeaponType.ROCKET_LAUNCHER.mag_size = 4
WeaponType.MINE.mag_size = 1

class Weapon(object):
    def __init__(self, name, damage, w_range, ammo, max_ammo, suppressed, fire_rate, rank):
        self.name = name
        self.damage = damage
        self.range = w_range
        self.ammo = ammo
        self.max_ammo = max_ammo
        self.suppressed = suppressed
        self.fire_rate = fire_rate
        self.fire_rate_60_hz = float(fire_rate) / 60.0
        self.rank = rank
        self.cycle = 0
        self.weaponStr = str(self.name)
        self.type = WeaponType.get_type_from_str(self.name)

    def to_dict(self):
        _dict = copy.copy(self.__dict__)
        _dict["type"] = self.type.value
        return _dict

    def __str__(self):
        return self.name

    def addAmmo(self, ammo):
        added = False
        if (self.ammo + ammo) <= self.max_ammo:
            self.ammo += ammo
            added = True
        elif self.ammo < self.max_ammo:
            self.ammo = self.max_ammo
            added = True
        return added

    def attachSuppressor(self):
        if self.suppressed == False:
            self.suppressed = True
            if self.type is not WeaponType.GRENADE and self.type is not WeaponType.MINE and self.type is not WeaponType.ROCKET_LAUNCHER:
                self.name += ' [S]'
            return True
        return False

    def getAmmo(self):
        return self.ammo

    def getMaxAmmo(self):
        return self.max_ammo

    @classmethod
    def from_dict(cls, weapon_dict):
        name = weapon_dict['name']
        damage = weapon_dict['damage']
        w_range = weapon_dict['range']
        ammo = weapon_dict['ammo']
        max_ammo = weapon_dict['max_ammo']
        suppressed = weapon_dict['suppressed']
        fire_rate = weapon_dict['fire_rate']
        rank = weapon_dict['rank']
        return Weapon(name, damage, w_range, ammo, max_ammo, suppressed, fire_rate, rank)

def createWeapon(weaponType, weaponProps):
    ammo = 0
    max_ammo = 1
    w_range = 32
    rank = weaponProps.get('rank', 0)
    name = weaponType.title()
    if rank > 0:
        name += ' R' + str(rank)
    damage = 0
    suppressed = False
    fire_rate = 0
    if weaponType == 'pistol':
        ammo = 8
        max_ammo = ammo * (rank + 2)
        w_range = 1024
        damage = int(35 + (rank * 5))
    elif weaponType == 'smg':
        ammo = 34
        max_ammo = ammo * (rank + 2)
        w_range = 1024
        damage = int(26 + (rank * 3))
        fire_rate = 7
        if rank == 1:
            fire_rate = 6
        elif rank == 2:
            fire_rate = 5
        elif rank > 2:
            fire_rate = 4
    elif weaponType == 'carbine':
        ammo = 30
        if rank > 0:
            fire_rate = 8
        max_ammo = ammo * (rank + 1)
        w_range = 2048
        damage = 50 + (rank * 9)
    elif weaponType == 'grenade':
        ammo = 2
        max_ammo = ammo * (rank + 4)
        w_range = 192
        damage = 0
    elif weaponType == 'rocket_launcher':
        ammo = 4
        max_ammo = ammo * 3
        w_range = 512
        damage = 0
    elif weaponType == 'mine':
        ammo = 1
        max_ammo = ammo * (rank + 4)
        w_range = 32
        damage = 100 + (rank * 10)
    return Weapon(name, damage, w_range, ammo, max_ammo, suppressed, fire_rate, rank)

class Item(StaticObject):
    def __init__(self, x, y, image, batch, batchGroup, objType, props, temporary=False):
        super(Item, self).__init__(x, y, image, batch, batchGroup)
        self.id = uuid.uuid4()
        self.tileX = int(x)
        self.tileY = int(y)
        self.type = objType
        self.props = {}
        self.color = (255,255,255)
        for prop in props:
            val = str(props[prop])
            try:
                val = int(props[prop])
            except:
                pass
            self.props[str(prop)] = val
        self.remove = False
        if self.type == 'alt_mine':
            mine_color_str = self.props.get('color', '63,95,63')
            mine_color = tuple([int(val) for val in mine_color_str.split(',')])
            self.setColor(mine_color)
        else:
            if 'color' in props:
                color_str = self.props.get('color', '255,255,255')
                color = tuple([int(val) for val in color_str.split(',')])
                self.setColor(color)
            else:
                self.setColor((255,255,255))
        self.temporary = temporary

    def getProps(self):
        return self.props

    def getType(self):
        return self.type

    def flagForDeletion(self):
        self.remove = True

    def shouldRemove(self):
        return self.remove

    def collidesWith(self, x, y):
        return self.tileX == x and self.tileY == y+1

    def to_dict(self):
        item_dict = {
            "tileX": self.tileX,
            "tileY": self.tileY,
            "type": self.type,
            "props": self.props
        }
        return item_dict

    def show(self):
        super(Item, self).show()
        if self.sprite != None:
            self.sprite.color = self.color

    def setColor(self, color):
        super(Item, self).setColor(color)
        self.color = color