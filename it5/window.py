import pyglet
from it5.util import PathfindingProcessManager

class GameWindow(pyglet.window.Window):
    def on_key_press(self, symbol, modifiers):
        if symbol == pyglet.window.key.ESCAPE:
            return pyglet.event.EVENT_HANDLED

    def on_close(self):
        super().on_close()
        PathfindingProcessManager.shutdown()