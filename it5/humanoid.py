import math
from it5.dynamicobject import DynamicObject
from it5.direction import Direction
from it5.audio import SoundManager
from it5.common import common_spritemap, common_tileset

class Humanoid(object):
    direction_deltas_x = {
        Direction.UP: 0,
        Direction.DOWN: 0,
        Direction.LEFT: -1,
        Direction.RIGHT: 1,
    }
    direction_deltas_y = {
        Direction.UP: 1,
        Direction.DOWN: -1,
        Direction.LEFT: 0,
        Direction.RIGHT: 0,
    }
    def __init__(self, x, y, sprites, batch, batchGroups, color=(255,255,255), no_movement=False):
        self.all_dynamic_objects = []

        # Init all dynamic objects associated with this Humanoid
        self.set_dynamic_objects(x, y, sprites, batch, batchGroups, color, no_movement)

        self.direction = Direction.DOWN
        self.prone = False # This should ONLY be set to True for the Player character
        self.submerged = False
        self.x = x * 32
        self.y = y * 32
        self.moving = False
        self.moveSpeed = 120 # 180 is a good run speed
        self._animationIndex = {
            "vertical": [2,3,4,2,0,1],
            "horizontal": [0,1,2,3,4,5,6,7],
            "prone_horizontal_right": [11, 13, 15, 13], # y=7, bottom_left is at x-1, top is invisible
            "prone_horizontal_left": [10, 12, 14, 12], # y=6, bottom_right is at x+1, top is invisible
            "prone_vertical_up": [7, 8, 9, 8], # y=7
            "prone_vertical_down": [8, 9, 10, 9], # y=6
        }
        self.animationIndex = {False: {}, True: {}}
        self.animationIndex[False][Direction.UP] = self._animationIndex["vertical"]
        self.animationIndex[False][Direction.DOWN] = self._animationIndex["vertical"]
        self.animationIndex[False][Direction.LEFT] = self._animationIndex["horizontal"]
        self.animationIndex[False][Direction.RIGHT] = self._animationIndex["horizontal"]

        self.animationIndex[True][Direction.UP] = self._animationIndex["prone_vertical_up"]
        self.animationIndex[True][Direction.DOWN] = self._animationIndex["prone_vertical_down"]
        self.animationIndex[True][Direction.LEFT] = self._animationIndex["prone_horizontal_left"]
        self.animationIndex[True][Direction.RIGHT] = self._animationIndex["prone_horizontal_right"]

        self.currFrameX = 0
        self.maxFrameX = len(self.animationIndex[self.prone][self.direction])
        self.animationCycles = 48
        self.halfAnimationCycles = self.animationCycles / 2
        self.lastIdleFrame = self.halfAnimationCycles
        self.animationStep = (self.animationCycles)/self.maxFrameX
        self.frame = 0
        self.color = color
        self.damageTimer = 0
        # Player and Bosses get protected
        self.protected = False
        self.protected_timer = 0
        self.health = 100
        self.maxHealth = self.health
        self.alive = True
        self.concealed = False

    def set_dynamic_objects(self, x, y, sprites, batch, batchGroups, color=(255,255,255), no_movement=False):
        feet = self.get_feet_group()
        heads = self.get_heads_group()
        if no_movement:
            self.bottom = DynamicObject(x, y, sprites, 0, 0, batch, batchGroups[feet], color)
            self.top = DynamicObject(x, y+1, sprites, 1, 0, batch, batchGroups[heads], color)
            self.cover_shadow = DynamicObject(x, y, common_tileset, common_spritemap["shadow"][0], common_spritemap["shadow"][1], batch, batchGroups[heads])
            self.bottom_left = None
            self.bottom_right = None
            self.top_left = None
            self.top_right = None
            self.prone_top = None
            self.prone_bottom = None
            self.all_dynamic_objects.append(self.bottom)
            self.all_dynamic_objects.append(self.top)
            self.all_dynamic_objects.append(self.cover_shadow)
        else:
            self.bottom = DynamicObject(x, y, sprites, 4, 2, batch, batchGroups[feet], color)
            self.top = DynamicObject(x, y+1, sprites, 5, 2, batch, batchGroups[heads], color)
            self.cover_shadow = DynamicObject(x, y, common_tileset, common_spritemap["shadow"][0], common_spritemap["shadow"][1], batch, batchGroups[heads])
            self.bottom_left = DynamicObject(x-1, y, sprites, 5, 7, batch, batchGroups[feet], color)
            self.bottom_right = DynamicObject(x+1, y, sprites, 5, 7, batch, batchGroups[feet], color)
            self.top_left = DynamicObject(x-1, y+1, sprites, 5, 7, batch, batchGroups[heads], color)
            self.top_right = DynamicObject(x+1, y+1, sprites, 5, 7, batch, batchGroups[heads], color)
            self.prone_top = DynamicObject(x, y+1, sprites, 5, 7, batch, batchGroups[feet], color)
            self.prone_bottom = DynamicObject(x, y-1, sprites, 5, 7, batch, batchGroups[feet], color)
            self.all_dynamic_objects.append(self.bottom)
            self.all_dynamic_objects.append(self.top)
            self.all_dynamic_objects.append(self.cover_shadow)
            self.all_dynamic_objects.append(self.bottom_left)
            self.all_dynamic_objects.append(self.top_left)
            self.all_dynamic_objects.append(self.bottom_right)
            self.all_dynamic_objects.append(self.top_right)
            self.all_dynamic_objects.append(self.prone_top)
            self.all_dynamic_objects.append(self.prone_bottom)

    def get_feet_group(self):
        return "feet"

    def get_heads_group(self):
        return "heads"

    def initialize(self):
        self.top.updateSprite(self.top.imagerow, self.top.imagecol)
        self.bottom.updateSprite(self.bottom.imagerow, self.bottom.imagecol)
        self.cover_shadow.updateSprite(*common_spritemap["shadow"])
        self.cover_shadow.setVisible(False)
        if self.top_left is not None:
            self.top_left.destroy()
        if self.top_right is not None:
            self.top_right.destroy()
        if self.bottom_left is not None:
            self.bottom_left.destroy()
        if self.bottom_right is not None:
            self.bottom_right.destroy()
        if self.prone_bottom is not None:
            self.prone_bottom.destroy()
        if self.prone_top is not None:
            self.prone_top.destroy()

    def destroy(self):
        for dynamic_obj in self.all_dynamic_objects:
            dynamic_obj.destroy()

    def final_check_door_collides(self, door, collision):
        # NPCs can walk through any door
        if collision:
            return not door.isOpen()
        return collision

    def check_doors(self, px0, px1, py0, py1, doors):
        doorCollision = False
        for door in doors:
            doorCollides = False
            doorCollides |= door.collidesWith(px0, py0)
            doorCollides |= door.collidesWith(px0, py1)
            doorCollides |= door.collidesWith(px1, py0)
            doorCollides |= door.collidesWith(px1, py1)
            doorCollision |= self.final_check_door_collides(door, doorCollides)
        return doorCollision

    def get_maximum_delta_x(self):
        return 32.0

    def get_maximum_delta_y(self):
        return 32.0

    def move(self, dx, dy, dt, obstacleMatrix, doors):
        delta_x = math.copysign(min(abs(dx * dt), self.get_maximum_delta_x()), dx)
        delta_y = math.copysign(min(abs(dy * dt), self.get_maximum_delta_y()), dy)

        px0 = int(((self.x + delta_x)+7)/32)
        px1 = int(((self.x + delta_x)+23)/32)
        py0 = int(((self.y + delta_y)-15)/32)
        py1 = int(((self.y + delta_y)-31)/32)
        
        if py0 >= 0 and py1 < len(obstacleMatrix) and px0 >= 0 and px1 < len(obstacleMatrix[0]):
            if obstacleMatrix[py0][px0] != 0 and obstacleMatrix[py0][px1] != 0 and obstacleMatrix[py1][px0] != 0 and obstacleMatrix[py1][px1] != 0:
                doorCollison = self.check_doors(px0, px1, py0, py1, doors)
                if not doorCollison:
                    self.x += delta_x
                    self.y += delta_y
                    for dynamic_obj in self.all_dynamic_objects:
                        dynamic_obj.move(delta_x, delta_y)
            else:
                if dx > 0:
                    # Right
                    py0a = int((self.y - 7) / 32)
                    py1a = int((self.y - 39) / 32)
                    if obstacleMatrix[py0a][px1] != 0:
                        doorCollison = self.check_doors(px0, px1, py0, py1, doors)
                        if not doorCollison:
                            self.y += delta_x
                            for dynamic_obj in self.all_dynamic_objects:
                                dynamic_obj.move(0, delta_x)
                    elif obstacleMatrix[py1a][px1] != 0:
                        doorCollison = self.check_doors(px0, px1, py0, py1, doors)
                        if not doorCollison:
                            delta_x *= -1
                            self.y += delta_x
                            for dynamic_obj in self.all_dynamic_objects:
                                dynamic_obj.move(0, delta_x)
                elif dx < 0:
                    # Left
                    py0a = int((self.y - 7) / 32)
                    py1a = int((self.y - 39) / 32)
                    if obstacleMatrix[py0a][px0] != 0:
                        doorCollison = self.check_doors(px0, px1, py0, py1, doors)
                        if not doorCollison:
                            delta_x *= -1
                            self.y += delta_x
                            for dynamic_obj in self.all_dynamic_objects:
                                dynamic_obj.move(0, delta_x)
                    elif obstacleMatrix[py1a][px0] != 0:
                        doorCollison = self.check_doors(px0, px1, py0, py1, doors)
                        if not doorCollison:
                            self.y += delta_x
                            for dynamic_obj in self.all_dynamic_objects:
                                dynamic_obj.move(0, delta_x)
                elif dy > 0:
                    # Up
                    px0a = int((self.x + 4) / 32)
                    px1a = int((self.x + 24) / 32)
                    if obstacleMatrix[py0][px0a] != 0:
                        doorCollison = self.check_doors(px0, px1, py0, py1, doors)
                        if not doorCollison:
                            delta_y *= -1
                            self.x += delta_y
                            for dynamic_obj in self.all_dynamic_objects:
                                dynamic_obj.move(delta_y, 0)
                    elif obstacleMatrix[py0][px1a] != 0:
                        doorCollison = self.check_doors(px0, px1, py0, py1, doors)
                        if not doorCollison:
                            self.x += delta_y
                            for dynamic_obj in self.all_dynamic_objects:
                                dynamic_obj.move(delta_y, 0)
                elif dy < 0:
                    # Down
                    px0a = int((self.x + 4) / 32)
                    px1a = int((self.x + 24) / 32)
                    if obstacleMatrix[py1][px0a] != 0:
                        doorCollison = self.check_doors(px0, px1, py0, py1, doors)
                        if not doorCollison:
                            self.x += delta_y
                            for dynamic_obj in self.all_dynamic_objects:
                                dynamic_obj.move(delta_y, 0)
                    elif obstacleMatrix[py1][px1a] != 0:
                        doorCollison = self.check_doors(px0, px1, py0, py1, doors)
                        if not doorCollison:
                            delta_y *= -1
                            self.x += delta_y
                            for dynamic_obj in self.all_dynamic_objects:
                                dynamic_obj.move(delta_y, 0)
        else:
            self.x += delta_x
            self.y += delta_y
            for dynamic_obj in self.all_dynamic_objects:
                dynamic_obj.move(delta_x, delta_y)
    
    def jump(self, x, y):
        self.x = x*32
        self.y = (y*32)+31
        self.bottom.jump(x, y)
        self.top.jump(x, y+1)
        self.cover_shadow.jump(x, y)
        if self.top_left is not None:
            self.top_left.jump(x-1, y+1)
        if self.top_right is not None:
            self.top_right.jump(x+1, y+1)
        if self.bottom_left is not None:
            self.bottom_left.jump(x-1, y)
        if self.bottom_right is not None:
            self.bottom_right.jump(x+1, y)
        if self.prone_bottom is not None:
            self.prone_bottom.jump(x, y-1)
        if self.prone_top is not None:
            self.prone_top.jump(x, y+1)
    
    def getTileX(self):
        return int((self.x+15) / 32)
    
    def getTileY(self):
        return int((self.y-31) / 32)

    def set_idle_sprites(self, moveDirection):
        if not self.prone:
            if self.prone_top is not None:
                self.prone_top.destroy()
            if self.prone_bottom is not None:
                self.prone_bottom.destroy()
            if self.bottom_left is not None:
                self.bottom_left.destroy()
            if self.bottom_right is not None:
                self.bottom_right.destroy()
            if moveDirection == Direction.UP:
                self.bottom.updateSprite(6,2)
                self.top.updateSprite(7,2)
            elif moveDirection == Direction.DOWN:
                self.bottom.updateSprite(4,2)
                self.top.updateSprite(5,2)
            elif moveDirection == Direction.LEFT:
                self.bottom.updateSprite(0,0)
                self.top.updateSprite(1,0)
            elif moveDirection == Direction.RIGHT:
                self.bottom.updateSprite(2,0)
                self.top.updateSprite(3,0)
        else:
            self.top.destroy()
            if moveDirection == Direction.UP:
                self.prone_bottom.updateSprite(6,8)
                self.bottom.updateSprite(7,8)
                if self.bottom_left is not None:
                    self.bottom_left.destroy()
                if self.bottom_right is not None:
                    self.bottom_right.destroy()
                if self.prone_top is not None:
                    self.prone_top.destroy()
            elif moveDirection == Direction.DOWN:
                self.bottom.updateSprite(4,10)
                self.prone_top.updateSprite(5,10)
                if self.bottom_left is not None:
                    self.bottom_left.destroy()
                if self.bottom_right is not None:
                    self.bottom_right.destroy()
                if self.prone_bottom is not None:
                    self.prone_bottom.destroy()
            elif moveDirection == Direction.LEFT:
                self.bottom.updateSprite(6,12)
                self.bottom_right.updateSprite(6,13)
                if self.bottom_left is not None:
                    self.bottom_left.destroy()
                if self.prone_top is not None:
                    self.prone_top.destroy()
                if self.prone_bottom is not None:
                    self.prone_bottom.destroy()
            elif moveDirection == Direction.RIGHT:
                self.bottom.updateSprite(7,13)
                self.bottom_left.updateSprite(7,12)
                if self.bottom_right is not None:
                    self.bottom_right.destroy()
                if self.prone_top is not None:
                    self.prone_top.destroy()
                if self.prone_bottom is not None:
                    self.prone_bottom.destroy()

    def startMoving(self, moveDirection):
        if self.direction != moveDirection:
            # Change sprite
            self.currFrameX = 0
            self.maxFrameX = len(self.animationIndex[self.prone][moveDirection])
            self.animationStep = (self.animationCycles)/self.maxFrameX
            self.set_idle_sprites(moveDirection)
            self.frame = 0
        self.direction = moveDirection
        if self.moving == False:
            if self.top_left is not None:
                self.top_left.destroy()
            if self.top_right is not None:
                self.top_right.destroy()
        self.moving = True
    
    def changeDirection(self, moveDirection):
        self.startMoving(moveDirection)
        self.moving = False

    def resetFrameOnStop(self):
        if self.lastIdleFrame == 0:
            self.frame = self.halfAnimationCycles - 1
        else:
            self.frame = 0
        self.lastIdleFrame = self.frame
        self.currFrameX = int(self.frame / self.animationStep)

    def stopMoving(self):
        # Change sprite
        self.maxFrameX = len(self.animationIndex[self.prone][self.direction])
        self.animationStep = (self.animationCycles)/self.maxFrameX
        self.set_idle_sprites(self.direction)
        if self.top_left is not None:
            self.top_left.destroy()
        if self.top_right is not None:
            self.top_right.destroy()
        self.resetFrameOnStop()
        self.moving = False

    def animate(self, dt):
        self.frame += dt * 60
        if self.frame >= self.animationCycles:
            self.frame = 0
        nextFrameX = int(self.frame / self.animationStep)
        if nextFrameX != self.currFrameX:
            self.currFrameX = nextFrameX
            if self.currFrameX < len(self.animationIndex[self.prone][self.direction]):
                frameColumn = self.animationIndex[self.prone][self.direction][self.currFrameX]
                self.bottom.updateSprite(self.bottom.imagerow, frameColumn)
                if not self.prone:
                    self.top.updateSprite(self.top.imagerow, frameColumn)
                else:
                    if self.direction == Direction.LEFT:
                        self.bottom_right.updateSprite(self.bottom_right.imagerow, frameColumn+1)
                    elif self.direction == Direction.RIGHT:
                        self.bottom_left.updateSprite(self.bottom_left.imagerow, frameColumn-1)
                    elif self.direction == Direction.UP:
                        self.prone_bottom.updateSprite(self.prone_bottom.imagerow, frameColumn)
                    elif self.direction == Direction.DOWN:
                        self.prone_top.updateSprite(self.prone_top.imagerow, frameColumn)

    def update_cover_shadow(self, obstacleMatrix):
        tx = int((self.x+15)/32)
        ty = int((self.y-15)/32)
        if ty >= 0 and ty < len(obstacleMatrix) and tx >= 0 and tx < len(obstacleMatrix[0]):
            if obstacleMatrix[ty][tx] > 1:
                if not self.cover_shadow.isVisible():
                    self.cover_shadow.setVisible(True)
                self.concealed = self.prone
                if obstacleMatrix[ty][tx] == 3:
                    if self.concealed:
                        self.submerged = True
                    else:
                        self.submerged = False
                else:
                    self.submerged = False
            else:
                if self.cover_shadow.isVisible():
                    self.cover_shadow.setVisible(False)
                self.concealed = False
                self.submerged = False
        else:
            if self.cover_shadow.isVisible():
                self.cover_shadow.setVisible(False)
            self.concealed = False
            self.submerged = False

    def update(self, dt, obstacleMatrix, doors):
        if self.moving == True:
            self.move(self.direction_deltas_x[self.direction] * self.moveSpeed, self.direction_deltas_y[self.direction] * self.moveSpeed, dt, obstacleMatrix, doors)
            self.animate(dt)
        if self.damageTimer > 0:
            self.damageTimer -= 1
            if self.damageTimer == 0:
                self.top.setAlpha(255)
                self.bottom.setAlpha(255)
            else:
                self.top.setAlpha(128)
                self.bottom.setAlpha(128)
        if self.protected_timer > 0:
            self.protected_timer -= dt
        self.update_cover_shadow(obstacleMatrix)

    def receiveDamage(self, damage, player=None):
        SoundManager.play_sfx("punch")
        if self.protected_timer <= 0:
            if self.protected:
                # This gives the protected humanoids a 2 second invincibility period until they can be damaged again
                self.protected_timer = 2.0
            self.health -= damage
            self.damageTimer = 12
            self.top.setAlpha(128)
            self.bottom.setAlpha(128)
            if self.health <= 0:
                self.alive = False

    def setColor(self, color):
        for dobj in self.all_dynamic_objects:
            if dobj is not None:
                dobj.setColor(color)