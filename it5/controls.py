from enum import Enum, auto
from pyglet.window import key
from uuid import uuid4

class ControlAction(Enum):
    NOOP = auto()
    UP = auto()
    DOWN = auto()
    LEFT = auto()
    RIGHT = auto()
    FIRE = auto()
    MELEE = auto()
    TOGGLE_PRONE = auto()
    TALK = auto()
    USE_MEDKIT = auto()
    TOGGLE_PAUSE = auto()
    TOGGLE_MISSION_VIEWER = auto()
    SAVE = auto()
    EXIT = auto()
    TOGGLE_CONSOLE = auto()

class Controls:
    @classmethod
    def initialize(cls, key_action_map=None):
        # Set unimportant keys to map to NOOP for performance reasons
        cls.all_keys_action_map = {k : ControlAction.NOOP for k in key._key_names.keys()}
        cls.controls_desc_dict = {
            ControlAction.UP: "Move up, or if paused, cycle forwards in item inventory",
            ControlAction.LEFT: "Move left, or if paused, cycle backwards in weapon inventory",
            ControlAction.DOWN: "Move down, or if paused, cycle backwards in item inventory",
            ControlAction.RIGHT: "Move right, or if paused, cycle forwards in weapon inventory",
            ControlAction.TOGGLE_PAUSE: "Toggle pause",
            ControlAction.TOGGLE_MISSION_VIEWER: "Bring up Mission Viewer",
            ControlAction.FIRE: "Fire weapon, or if paused, consume medkit if equipped",
            ControlAction.MELEE: "Melee, or if paused, consume medkit if equipped",
            ControlAction.TOGGLE_PRONE: "Toggle prone (crawl)",
            ControlAction.TALK: "Talk to a person",
            ControlAction.USE_MEDKIT: "Use a medkit to heal",
            ControlAction.SAVE: "Save the game",
            ControlAction.EXIT: "Exit the game",
        }
        cls.default_action_map = {
            key.W: ControlAction.UP,
            key.S: ControlAction.DOWN,
            key.A: ControlAction.LEFT,
            key.D: ControlAction.RIGHT,
            key.UP: ControlAction.UP,
            key.DOWN: ControlAction.DOWN,
            key.LEFT: ControlAction.LEFT,
            key.RIGHT: ControlAction.RIGHT,
            key.SPACE: ControlAction.FIRE,
            key.LCTRL: ControlAction.MELEE,
            key.RCTRL: ControlAction.MELEE,
            key.LSHIFT: ControlAction.TOGGLE_PRONE,
            key.RSHIFT: ControlAction.TOGGLE_PRONE,
            key.E: ControlAction.TALK,
            key.H: ControlAction.USE_MEDKIT,
            key.ENTER: ControlAction.TOGGLE_PAUSE,
            key.TAB: ControlAction.TOGGLE_MISSION_VIEWER,
            key.F5: ControlAction.SAVE,
            key.ESCAPE: ControlAction.EXIT,
        }
        if key_action_map is not None:
            # Expects a dict of str:str k/v pairs where key is str(int) and v is control_action.name
            cls.key_action_map = {int(k) : ControlAction[v] for k, v in key_action_map.items()}
        else:
            cls.key_action_map = cls.default_action_map
        cls.action_key_map = {}
        # Iterate through the important keys
        for k, v in cls.key_action_map.items():
            if v not in cls.action_key_map:
                cls.action_key_map[v] = []
            cls.action_key_map[v].append((key.symbol_string(k), k))
            # Map the ControlAction to the key in all_keys_action_map
            cls.all_keys_action_map[k] = v
        cls.refresh_controls_str()
        cls.state_id = uuid4()

    @classmethod
    def get_keypress_action(cls, keypress):
        return cls.all_keys_action_map[keypress]

    @classmethod
    def get_key_names_str(cls, action):
        return " or ".join(["[%s]" % k[0].lstrip("_") for k in cls.action_key_map.get(action, [("UNBOUND",)])])

    @classmethod
    def refresh_controls_str(cls):
        control_descriptions = []
        for action, desc in cls.controls_desc_dict.items():
            control_descriptions.append("%s: %s" % (cls.get_key_names_str(action), desc))
        cls.controls_str = "\n".join(control_descriptions)

    @classmethod
    def get_controls_str(cls):
        return cls.controls_str

    @classmethod
    def get_key_action_map(cls):
        return cls.key_action_map

    @classmethod
    def unbind_action(cls, action):
        kam_copy = {k : v.name for k, v in cls.key_action_map.items()}
        key_list = cls.action_key_map.get(action, [])
        for kt in key_list:
            symbol_string, symbol = kt
            kam_copy.pop(symbol, None)
        cls.initialize(kam_copy)

    @classmethod
    def bind_key(cls, keypress, action):
        kam_copy = {k : v.name for k, v in cls.key_action_map.items()}
        kam_copy[keypress] = action.name
        cls.initialize(kam_copy)
