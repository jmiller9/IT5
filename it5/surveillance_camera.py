import pyglet
import math
from it5.util import searchForPlayer, getHalfVisionConeHeight
from it5.common import common_spritemap, common_tileset, EventManager, lighting
from it5.audio import SoundManager
from it5.item import WeaponType
from it5.staticobject import StaticObject
from it5.direction import Direction

class SurveillanceCamera(StaticObject):
    def __init__(self, x, y, direction, move_delta, has_gun, image, batches, batch_groups):
        super(SurveillanceCamera, self).__init__(x, y, image, batches["main"], batch_groups["explosions"])
        self.direction = direction
        self.has_gun = has_gun
        self.view_distance = 384
        self.returning = False
        self.dx = 0
        self.dy = 0
        self.start_x = self.x
        self.start_y = self.y
        self.destination_x = self.x
        self.destination_y = self.y
        self.set_view_offsets()
        if direction == Direction.LEFT or direction == Direction.RIGHT:
            self.destination_y += (move_delta * 32)
            if move_delta > 0:
                self.dy = 1
            elif move_delta < 0:
                self.dy = -1
        else:
            self.destination_x += (move_delta * 32)
            if move_delta > 0:
                self.dx = 1
            elif move_delta < 0:
                self.dx = -1
        self.static = (move_delta == 0)
        self.moving = False
        self.wait_timer = 0
        self.time_to_wait = 2
        self.move_speed = 15
        self.move_delta = move_delta
        self.shot_timer = 0
        self.shoot_interval = 0.5
        self.batches = batches
        self.batch_groups = batch_groups
        self.vision_cone = None
        self.light = None
        self.radar_unit = StaticObject(28, 20, common_tileset[common_spritemap["bar"]], self.batches["radar"], self.batch_groups["radarForeground"])
        self.radar_unit.show()
        self.radar_unit.sprite.scale_x = 0.125
        self.radar_unit.sprite.scale_y = 0.125
        self.radar_unit.sprite.color = (255,0,0)
        self.radar_view_distance = self.view_distance/4
        self.half_vision_cone_height = getHalfVisionConeHeight(self.radar_view_distance)
        self.damage = 25
        self.health = 50
        self.functional = True
        self.alert = False
        self.can_see_player = False
        self.idle_color = (0,255,64)
        self.alert_color = (255,0,0)

    def set_view_offsets(self):
        self.view_offset_x = 0
        self.view_offset_y = 0
        self.light_offset_x = 0
        self.light_offset_y = 0
        if self.direction == Direction.UP:
            self.view_offset_y = 64
            self.light_offset_x = 16
            self.light_offset_y = 28
        elif self.direction == Direction.DOWN:
            self.view_offset_y = -72
            self.light_offset_x = 16
            self.light_offset_y = 4
        elif self.direction == Direction.LEFT:
            self.view_offset_x = -64
            self.light_offset_x = 4
            self.light_offset_y = 16
        elif self.direction == Direction.RIGHT:
            self.view_offset_x = 64
            self.light_offset_x = 28
            self.light_offset_y = 16

    def copy(self):
        return SurveillanceCamera(self.originalX, self.originalY, self.direction, self.move_delta, self.has_gun, self.image, self.batches, self.batch_groups)

    def create_light(self):
        self.light = lighting.create_light(x=self.x+self.light_offset_x, y=self.y+self.light_offset_y, radius=24, color=self.idle_color)

    def destroy_light(self):
        if self.light is not None:
            lighting.destroy_light(self.light)
        self.light = None

    def create_vision_cone(self):
        self.destroy_vision_cone()
        self.vision_cone = self.batches['radar'].add(3, pyglet.gl.GL_TRIANGLES, self.batch_groups['radarForeground'],
            ('v2f/stream', (0, 0, 0, 0, 0, 0)),
            ('c4B/static', (127, 127, 0, 127, 127, 127, 0, 127, 127, 127, 0, 127))
        )

    def destroy_vision_cone(self):
        if self.vision_cone is not None:
            self.vision_cone.delete()
            self.vision_cone = None

    def update_vision_cone(self):
        _x = (self.x + self.view_offset_x) / 4
        _y = (self.y + self.view_offset_y) / 4
        if self.direction == Direction.UP:
            x1 = _x + 2
            y1 = _y + 6
            x2 = x1 + self.half_vision_cone_height
            y2 = y1 + self.radar_view_distance
            x3 = x1 - self.half_vision_cone_height
            y3 = y1 + self.radar_view_distance
        elif self.direction == Direction.DOWN:
            x1 = _x + 2
            y1 = _y + 4
            x2 = x1 + self.half_vision_cone_height
            y2 = y1 - self.radar_view_distance
            x3 = x1 - self.half_vision_cone_height
            y3 = y1 - self.radar_view_distance
        elif self.direction == Direction.LEFT:
            x1 = _x
            y1 = _y + 5
            x2 = x1 - self.radar_view_distance
            y2 = y1 + self.half_vision_cone_height
            x3 = x1 - self.radar_view_distance
            y3 = y1 - self.half_vision_cone_height
        elif self.direction == Direction.RIGHT:
            x1 = _x + 2
            y1 = _y + 5
            x2 = x1 + self.radar_view_distance
            y2 = y1 - self.half_vision_cone_height
            x3 = x1 + self.radar_view_distance
            y3 = y1 + self.half_vision_cone_height
        self.vision_cone.vertices[0] = x1
        self.vision_cone.vertices[1] = y1
        self.vision_cone.vertices[2] = x2
        self.vision_cone.vertices[3] = y2
        self.vision_cone.vertices[4] = x3
        self.vision_cone.vertices[5] = y3

    def hide(self):
        super(SurveillanceCamera, self).hide()
        self.radar_unit.hide()
        self.destroy_vision_cone()
        self.destroy_light()

    def show(self):
        super(SurveillanceCamera, self).show()
        self.create_vision_cone()
        self.update_vision_cone()
        self.create_light()

    def move(self, dx, dy, dt):
        super(SurveillanceCamera, self).move(dx, dy, dt)
        self.update_vision_cone()
        if self.light is not None:
            self.light.set_position(self.x+self.light_offset_x, self.y+self.light_offset_y)

    def attack(self, player, bulletManager):
        dx = 0
        dy = 0
        sx = self.x + self.view_offset_x
        sy = self.y + self.view_offset_y
        velocity = 1200
        x = player.x - self.x
        y = player.y - self.y
        normal = math.sqrt(float(x * x) + float(y * y))
        if normal > 0:
            nX = x/normal
            nY = y/normal
            dx = velocity * nX
            dy = velocity * nY
            SoundManager.play_sfx('rifle_gunshot')
            bulletManager.add(sx, sy, dx, dy, 1024, self.damage, WeaponType.CARBINE, True)

    def cancel_alert(self):
        self.alert = False
        if self.light is not None:
            self.light.set_color(self.idle_color)

    def receive_damage(self, damage):
        if self.functional:
            self.health -= damage
            if self.health <= 0:
                self.functional = False
                self.hide()

    def update(self, dt, currLevelMap, player, bulletManager):
        if not self.functional:
            return
        self.radar_unit.setPosition(self.x / 4, (self.y / 4)+4)
        found = searchForPlayer(self.x + self.view_offset_x, self.y + self.view_offset_y, self.direction, self.view_distance, currLevelMap, player, is_camera=True)
        if self.shot_timer > 0:
            self.shot_timer += dt
            if self.shot_timer >= self.shoot_interval:
                self.shot_timer = 0
        last_saw_player = self.can_see_player
        if found:
            if self.has_gun:
                self.can_see_player = True
                # Shoot at player
                if self.shot_timer == 0:
                    self.attack(player, bulletManager)
                    self.shot_timer = dt
            else:
                # Trigger alert
                if not self.alert:
                    EventManager.alert(id(self))
                    self.alert = True
                    if self.light is not None:
                        self.light.set_color(self.alert_color)
        else:
            self.shot_timer = 0
            self.can_see_player = False
        if self.can_see_player and not last_saw_player:
            self.light.set_color(self.alert_color)
        elif not self.can_see_player and last_saw_player:
            self.light.set_color(self.idle_color)
        if not self.static:
            if self.moving:
                self.move(self.dx * self.move_speed, self.dy * self.move_speed, dt)
                if not self.returning:
                    target_x = self.destination_x
                    target_y = self.destination_y
                else:
                    target_x = self.start_x
                    target_y = self.start_y
                should_rest_and_go_back = False
                if self.dx > 0:
                    if self.x >= target_x:
                        should_rest_and_go_back = True
                elif self.dx < 0:
                    if self.x <= target_x:
                        should_rest_and_go_back = True
                elif self.dy > 0:
                    if self.y >= target_y:
                        should_rest_and_go_back = True
                elif self.dy < 0:
                    if self.y <= target_y:
                        should_rest_and_go_back = True
                if should_rest_and_go_back:
                    self.setPosition(target_x, target_y)
                    self.update_vision_cone()
                    self.returning = not self.returning
                    self.dx *= -1
                    self.dy *= -1
                    self.moving = False
                    self.wait_timer = 0
            else:
                # While not moving, wait
                self.wait_timer += dt
                if self.wait_timer >= self.time_to_wait:
                    self.moving = True


class SurveillanceCameraFactory:
    @classmethod
    def __get_image(cls, direction, has_gun):
        camera_sprite_name = "camera_down"
        if direction == Direction.LEFT:
            camera_sprite_name = "camera_left"
        elif direction == Direction.RIGHT:
            camera_sprite_name = "camera_right"
        elif direction == Direction.UP:
            camera_sprite_name = "camera_up"
        if has_gun:
            camera_sprite_name = "gun_" + camera_sprite_name
        return common_tileset[common_spritemap[camera_sprite_name]]

    @classmethod
    def create_camera(cls, x, y, direction, move_delta, has_gun, batches, batch_groups):
        return SurveillanceCamera(x, y, direction, move_delta, has_gun, cls.__get_image(direction, has_gun), batches, batch_groups)