from it5.script import OneTimeScript
from it5.script import PersistentScript
from it5.npc import Guard, FemaleScientist, Isabel_Dress, Rees_Interrogation, Wolfgang_1973, Isabel, Man, Inalco
from it5.bosses.boss1 import Boss1
from it5.bosses.hex import Hex
from it5.bosses.owl import Owl
from it5.bosses.wolfgang import WolfgangBoss
from it5.bosses.relict import Relict
from it5.bosses.asp import Asp
from it5.item import Item, WeaponType
from it5.common import EventManager, common_tileset, common_spritemap, batches, batchGroups
from it5.direction import Direction
from it5.waypoint import Path
from it5.audio import SoundManager

class ColoniaHouse4Spawn(OneTimeScript):
    def on_load(self):
        print("Loaded ColoniaHouse4Spawn!")
        actualX = self.currLevelMap.waypointMap['g1a'].getTileX()
        actualY = self.currLevelMap.waypointMap['g1a'].getTileY()+1
        waypoints = [
            self.currLevelMap.waypointMap['g1a'],
            self.currLevelMap.waypointMap['g1b'],
            self.currLevelMap.waypointMap['g1c'],
            self.currLevelMap.waypointMap['g1d'],
            self.currLevelMap.waypointMap['g1e'],
            self.currLevelMap.waypointMap['g1f'],
        ]
        guard = Guard(actualX, actualY, waypoints, 50, self.currLevelMap.batches, self.currLevelMap.batchGroups, color=(127,127,63))
        guard.initialize()
        self.currLevelMap.npcs.add(guard)

class FirstTimeInIsabelHouseScript(OneTimeScript):
    def on_load(self):
        print("Loaded FirstTimeInIsabelHouseScript!")
        self.player.changeDirection(Direction.UP)
        self.player.clearMoveStack()
        grandmother = list(self.currLevelMap.npcs)[0]
        wx = self.currLevelMap.waypointMap['woman2'].getTileX()
        wy = self.currLevelMap.waypointMap['woman2'].getTileY()
        grandmother.jump(wx, wy)
        self.open_dialog('isabel_grandmother_objective.dlg')

class Boss1Script(PersistentScript):
    def on_load(self):
        print("Loaded Boss1Script!")
        SoundManager.play_music("alert")
        self.event_manager.set_handler('dialog_finished', self.dialog_finished)
        # Add Isabel
        actualX = self.currLevelMap.waypointMap['i0'].getTileX()
        actualY = self.currLevelMap.waypointMap['i0'].getTileY()+1
        waypoints = [self.currLevelMap.waypointMap['i0']]
        self.isabel = Isabel(actualX, actualY, waypoints, self.currLevelMap.batches, self.currLevelMap.batchGroups)
        self.isabel.initialize()
        self.currLevelMap.npcs.add(self.isabel)
        # Add the boss
        actualX = self.currLevelMap.waypointMap['b1'].getTileX()
        actualY = self.currLevelMap.waypointMap['b1'].getTileY()+1
        waypoints = [self.currLevelMap.waypointMap['b1']]
        self.boss = Boss1(actualX, actualY, waypoints, self.currLevelMap.batches, self.currLevelMap.batchGroups)
        self.boss.initialize()
        self.currLevelMap.npcs.add(self.boss)
        self.door = self.currLevelMap.doors[0]
        self.door.locked = True
        self.ammo = None
        self.medkit = None
        self.spawn_medkit(25, 20)
        self.open_dialog('boss1.dlg')

    def update(self, dt):
        if self.ammo is not None:
            if self.ammo.shouldRemove():
                self.ammo = None
        if self.medkit is not None:
            if self.medkit.shouldRemove():
                self.medkit = None
        if self.boss is not None:
            if self.boss.health <= 0:
                self.boss = None
                self.door.locked = False
                # TODO: Fade out/in to this?
                waypoint = self.currLevelMap.waypointMap["i1"]
                self.isabel.jump(waypoint.getTileX(), waypoint.getTileY())
                player_waypoint = self.currLevelMap.waypointMap["p1"]
                self.player.changeDirection(Direction.UP)
                self.player.clearMoveStack()
                self.player.jump(player_waypoint.getTileX(), player_waypoint.getTileY())
                EventManager.reset_camera()
                SoundManager.play_music("creepy")
                self.open_dialog('isabel_post_boss1.dlg')
            else:
                if self.player.weapon is not None:
                    if self.player.weapon.type == WeaponType.PISTOL:
                        if self.player.weapon.ammo == 0:
                            self.spawn_ammo(15, 11, self.player.weapon.type)

    def spawn_ammo(self, x, y, weapon_type, count=1):
        if self.ammo is None:
            if isinstance(weapon_type, WeaponType):
                count = weapon_type.mag_size
                # This is a little hacky, but the weapon type is expected to be a string in the properties dict
                weapon_type = weapon_type.name.lower()
            image = common_tileset[common_spritemap["ammo"]]
            batch = batches["main"]
            batch_group = batchGroups["items"]
            self.ammo = Item(x, y, image, batch, batch_group, 'ammo', {'count':count, 'type':weapon_type}, temporary=True)
            self.ammo.show()
            self.currLevelMap.items.add(self.ammo)

    def spawn_medkit(self, x, y):
        if self.medkit is None:
            image = common_tileset[common_spritemap["medkit"]]
            batch = batches["main"]
            batch_group = batchGroups["items"]
            self.medkit = Item(x, y, image, batch, batch_group, 'medkit', {}, temporary=True)
            self.medkit.show()
            self.currLevelMap.items.add(self.medkit)

    def dialog_finished(self, dialog_name):
        print("Dialog " + dialog_name + " has finished")
        if dialog_name == 'isabel_post_boss1.dlg':
            # Isabel exits the warehouse
            self.isabel.alive = False
            actualX = self.currLevelMap.waypointMap['i2'].getTileX()
            actualY = self.currLevelMap.waypointMap['i2'].getTileY()+1
            waypoints = [self.currLevelMap.waypointMap["i2"],
                         self.currLevelMap.waypointMap["i3"],
                         self.currLevelMap.waypointMap["i4"],
                         self.currLevelMap.waypointMap["i5"],
                         self.currLevelMap.waypointMap["i6"]]
            self.isabel = Isabel(actualX, actualY, waypoints, self.currLevelMap.batches, self.currLevelMap.batchGroups)
            self.isabel.initialize()
            self.currLevelMap.npcs.add(self.isabel)

    def destroy(self):
        self.event_manager.remove_handler('dialog_finished', self.dialog_finished)
        if self.ammo is not None:
            self.currLevelMap.items.remove(self.ammo)
            self.ammo.hide()
        if self.medkit is not None:
            self.currLevelMap.items.remove(self.medkit)
            self.medkit.hide()

class IsabelPreColoniaScript(OneTimeScript):
    def on_load(self):
        print("Loaded IsabelPreColoniaScript!")
        self.event_manager.set_handler("dialog_finished", self.dialog_finished)
        self.player.changeDirection(Direction.UP)
        self.player.clearMoveStack()
        self.open_dialog("isabel_pre_colonia.dlg")

    def dialog_finished(self, dialog_name):
        print("Dialog " + dialog_name + " has finished")
        if dialog_name == "isabel_pre_colonia.dlg":
            # Resupply and heal the player after the first boss
            ammo = Item(0, 0, None, None, None, "ammo", {"count":16, "type":"pistol"})
            self.player.inventory.addTo(ammo)
            self.player.changeWeapon(0)
            self.player.setHealth(self.player.maxHealth)

    def destroy(self):
        self.event_manager.remove_handler("dialog_finished", self.dialog_finished)

class Boss2Script(PersistentScript):
    def on_load(self):
        print("Loaded Boss2Script!")
        SoundManager.stop_music()
        self.event_manager.set_handler('dialog_finished', self.dialog_finished)
        # Add the boss
        actualX = self.currLevelMap.waypointMap['hex1'].getTileX()
        actualY = self.currLevelMap.waypointMap['hex1'].getTileY()+1
        waypoints = [self.currLevelMap.waypointMap['hex1'], self.currLevelMap.waypointMap['hex2'], self.currLevelMap.waypointMap['hex3'], self.currLevelMap.waypointMap['hex4']]
        self.boss = Hex(actualX, actualY, waypoints, self.currLevelMap.batches, self.currLevelMap.batchGroups)
        self.boss.initialize()
        self.currLevelMap.npcs.add(self.boss)
        self.doors = self.currLevelMap.doors
        for door in self.currLevelMap.doors:
            door.locked = True
        self.ammo = None
        self.medkit = None
        self.spawn_medkit(3, 7)
        self.open_dialog("boss2.dlg")

    def update(self, dt):
        if self.ammo is not None:
            if self.ammo.shouldRemove():
                self.ammo = None
        if self.medkit is not None:
            if self.medkit.shouldRemove():
                self.medkit = None
        if self.boss is not None:
            if self.boss.health <= 0:
                self.boss = None
                for door in self.currLevelMap.doors:
                    door.locked = False
                SoundManager.play_music("zombies")
            else:
                if self.player.weapon is not None:
                    if self.player.weapon.type == WeaponType.PISTOL:
                        if self.player.weapon.ammo == 0:
                            self.spawn_ammo(17, 21, self.player.weapon.type)

    def spawn_ammo(self, x, y, weapon_type, count=1):
        if self.ammo is None:
            if isinstance(weapon_type, WeaponType):
                count = weapon_type.mag_size
                # This is a little hacky, but the weapon type is expected to be a string in the properties dict
                weapon_type = weapon_type.name.lower()
            image = common_tileset[common_spritemap["ammo"]]
            batch = batches["main"]
            batch_group = batchGroups["items"]
            self.ammo = Item(x, y, image, batch, batch_group, 'ammo', {'count':count, 'type':weapon_type}, temporary=True)
            self.ammo.show()
            self.currLevelMap.items.add(self.ammo)

    def spawn_medkit(self, x, y):
        if self.medkit is None:
            image = common_tileset[common_spritemap["medkit"]]
            batch = batches["main"]
            batch_group = batchGroups["items"]
            self.medkit = Item(x, y, image, batch, batch_group, 'medkit', {}, temporary=True)
            self.medkit.show()
            self.currLevelMap.items.add(self.medkit)

    def dialog_finished(self, dialog_name):
        print("Dialog " + dialog_name + " has finished")

    def destroy(self):
        self.event_manager.remove_handler('dialog_finished', self.dialog_finished)
        if self.ammo is not None:
            self.currLevelMap.items.remove(self.ammo)
            self.ammo.hide()
        if self.medkit is not None:
            self.currLevelMap.items.remove(self.medkit)
            self.medkit.hide()

class FoundMarcusScript(OneTimeScript):
    def on_load(self):
        print("Loaded FoundMarcusScript!")
        self.event_manager.set_handler('dialog_finished', self.dialog_finished)
        self.player.changeDirection(Direction.UP)
        self.player.clearMoveStack()
        # Clean up existing enemy NPCs and get a handle on Marcus
        self.marcus = None
        for npc in self.currLevelMap.npcs:
            if not npc.friendly:
                npc.alive = False
            else:
                # This should only happen once
                self.marcus = npc
        SoundManager.play_music("zombies")
        self.open_dialog('found_marcus.dlg')

    def spawn_relict(self):
        actualX = self.currLevelMap.waypointMap['g1a'].getTileX()
        actualY = self.currLevelMap.waypointMap['g1a'].getTileY()+1
        waypoints = [self.currLevelMap.waypointMap['g1a']]
        self.boss = Relict(actualX, actualY, waypoints, self.currLevelMap.batches, self.currLevelMap.batchGroups)
        self.boss.initialize()
        self.currLevelMap.npcs.add(self.boss)
        SoundManager.play_sfx("monster_growl")

    def reroute_marcus(self):
        wpm = self.currLevelMap.waypointMap
        path = Path([wpm['m1a'], wpm['m1b'], wpm['m1c'], wpm['m1d'], wpm['m1e']])
        self.marcus.path = path
        self.marcus.jump(path.waypoints[0].getTileX(), path.waypoints[0].getTileY()+1)
        self.marcus.alive = False
        self.marcus = self.marcus.copy()
        self.marcus.initialize()
        self.marcus.changeDirection(path.waypoints[0].direction)
        self.marcus.moveSpeed = 120
        self.currLevelMap.npcs.add(self.marcus)

    def dialog_finished(self, dialog_name):
        if dialog_name == "found_marcus.dlg":
            # We don't want the Player to get permanently stuck here, since the Relict is faster than the Player
            self.player.setHealth(self.player.maxHealth)
            self.player.jump(9, 18)
            self.reroute_marcus()
            self.spawn_relict()
            SoundManager.play_music("dark")

    def destroy(self):
        self.event_manager.remove_handler('dialog_finished', self.dialog_finished)

class EnterLabScript(OneTimeScript):
    def on_load(self):
        print("Loaded EnterLabScript!")
        self.event_manager.set_handler('dialog_finished', self.dialog_finished)
        # Create Wolfgang
        actualX = self.currLevelMap.waypointMap['wolfgang_start'].getTileX()
        actualY = self.currLevelMap.waypointMap['wolfgang_start'].getTileY()+1
        waypoints = [
            self.currLevelMap.waypointMap['wolfgang_start'],
        ]
        self.wolfgang = Wolfgang_1973(actualX, actualY, waypoints, self.currLevelMap.batches, self.currLevelMap.batchGroups)
        self.wolfgang.initialize()
        self.wolfgang.changeDirection(self.wolfgang.path.waypoints[0].direction)
        self.currLevelMap.npcs.add(self.wolfgang)
        # Create Dr. Rees
        actualX = self.currLevelMap.waypointMap['rees_start'].getTileX()
        actualY = self.currLevelMap.waypointMap['rees_start'].getTileY()+1
        rees_waypoints = [
            self.currLevelMap.waypointMap['rees_start'],
        ]
        self.rees = FemaleScientist(actualX, actualY, rees_waypoints, self.currLevelMap.batches, self.currLevelMap.batchGroups)
        self.rees.initialize()
        self.rees.changeDirection(self.rees.path.waypoints[0].direction)
        self.currLevelMap.npcs.add(self.rees)
        self.open_dialog('wolfgang_rees.dlg')

    def reroute_rees(self):
        wpm = self.currLevelMap.waypointMap
        path = Path([wpm['rees_a'], wpm['rees_b'], wpm['rees_c'], wpm['rees_d'], wpm['rees_e'], wpm['rees_f']])
        self.rees.path = path
        self.rees.jump(path.waypoints[0].getTileX(), path.waypoints[0].getTileY()+1)
        self.rees.alive = False
        self.rees = self.rees.copy()
        self.rees.initialize()
        self.rees.changeDirection(path.waypoints[0].direction)
        self.currLevelMap.npcs.add(self.rees)

    def reroute_wolfgang(self):
        wpm = self.currLevelMap.waypointMap
        # Wolfgang shares many waypoints with Dr. Rees in this scene
        path = Path([wpm['wolfgang_a'], wpm['rees_b'], wpm['rees_c'], wpm['rees_d'], wpm['rees_e'], wpm['rees_f']])
        self.wolfgang.path = path
        self.wolfgang.jump(path.waypoints[0].getTileX(), path.waypoints[0].getTileY()+1)
        self.wolfgang.alive = False
        self.wolfgang = self.wolfgang.copy()
        self.wolfgang.initialize()
        self.wolfgang.changeDirection(path.waypoints[0].direction)
        self.currLevelMap.npcs.add(self.wolfgang)

    def dialog_finished(self, dialog_name):
        if dialog_name == "wolfgang_rees.dlg":
            # We don't want the Player to get permanently stuck here, since the Relict is faster than the Player
            self.reroute_rees()
            self.reroute_wolfgang()

    def destroy(self):
        self.event_manager.remove_handler('dialog_finished', self.dialog_finished)

class Boss3Script(PersistentScript):
    def on_load(self):
        print("Loaded Boss3Script!")
        #SoundManager.stop_music()
        self.event_manager.set_handler("dialog_finished", self.dialog_finished)
        # Add the boss
        actualX = self.currLevelMap.waypointMap["intro"].getTileX()
        actualY = self.currLevelMap.waypointMap["intro"].getTileY()+1
        self.boss = Owl(actualX, actualY, [self.currLevelMap.waypointMap["intro"]], self.currLevelMap.batches, self.currLevelMap.batchGroups, self.currLevelMap.waypointMap)
        self.boss.initialize()
        self.currLevelMap.npcs.add(self.boss)
        self.doors = self.currLevelMap.doors
        for door in self.currLevelMap.doors:
            door.locked = True
        self.ammo = None
        self.medkit = None
        self.spawn_medkit(16, 9)
        self.open_dialog("boss3.dlg")

    def update(self, dt):
        if self.ammo is not None:
            if self.ammo.shouldRemove():
                self.ammo = None
        if self.medkit is not None:
            if self.medkit.shouldRemove():
                self.medkit = None
        if self.boss is not None:
            if self.boss.health <= 0:
                self.boss = None
                for door in self.currLevelMap.doors:
                    door.locked = False
                #SoundManager.play_music("zombies")
            else:
                if self.player.weapon is not None:
                    if self.player.weapon.type == WeaponType.GRENADE:
                        if self.player.weapon.ammo == 0:
                            self.spawn_ammo(17, 17, self.player.weapon.type)

    def spawn_ammo(self, x, y, weapon_type, count=1):
        if self.ammo is None:
            if isinstance(weapon_type, WeaponType):
                count = weapon_type.mag_size
                # This is a little hacky, but the weapon type is expected to be a string in the properties dict
                weapon_type = weapon_type.name.lower()
            image = common_tileset[common_spritemap["weapon"]]
            batch = batches["main"]
            batch_group = batchGroups["items"]
            self.ammo = Item(x, y, image, batch, batch_group, "ammo", {"count":count, "type":weapon_type}, temporary=True)
            self.ammo.show()
            self.currLevelMap.items.add(self.ammo)

    def spawn_medkit(self, x, y):
        if self.medkit is None:
            image = common_tileset[common_spritemap["medkit"]]
            batch = batches["main"]
            batch_group = batchGroups["items"]
            self.medkit = Item(x, y, image, batch, batch_group, "medkit", {}, temporary=True)
            self.medkit.show()
            self.currLevelMap.items.add(self.medkit)

    def dialog_finished(self, dialog_name):
        print("Dialog %s has finished" % dialog_name)

    def destroy(self):
        self.event_manager.remove_handler("dialog_finished", self.dialog_finished)
        if self.ammo is not None:
            self.currLevelMap.items.remove(self.ammo)
            self.ammo.hide()
        if self.medkit is not None:
            self.currLevelMap.items.remove(self.medkit)
            self.medkit.hide()

class Boss4Script(PersistentScript):
    def on_load(self):
        print("Loaded Boss4Script!")
        SoundManager.play_music("danger")
        self.event_manager.set_handler("dialog_finished", self.dialog_finished)
        # Add the boss
        actualX = self.currLevelMap.waypointMap["wg0"].getTileX()
        actualY = self.currLevelMap.waypointMap["wg0"].getTileY()+1
        self.boss = WolfgangBoss(actualX, actualY, [self.currLevelMap.waypointMap["wg0"]], self.currLevelMap.batches, self.currLevelMap.batchGroups)
        self.boss.initialize()
        self.currLevelMap.npcs.add(self.boss)

        # Add Dr. Rees
        actualX = self.currLevelMap.waypointMap["r0"].getTileX()
        actualY = self.currLevelMap.waypointMap["r0"].getTileY()+1
        r_waypoints = [self.currLevelMap.waypointMap["r0"], self.currLevelMap.waypointMap["r1"]]
        self.rees = FemaleScientist(actualX, actualY, r_waypoints, self.currLevelMap.batches, self.currLevelMap.batchGroups)
        self.rees.initialize()
        self.rees.changeDirection(Direction.UP)
        self.currLevelMap.npcs.add(self.rees)
        self.rees_fled = False

        # Lock the doors
        self.doors = self.currLevelMap.doors
        for door in self.currLevelMap.doors:
            if door.id == "tunnel_door":
                self.tunnel_door = door
            else:
                door.locked = True
        self.ammo = None
        self.medkit = None
        self.spawn_medkit(9, 15)
        self.spawned_mines = False
        self.spawned_grenades = False
        self.transition_script_name = "DimmingTransitionScript"
        self.open_dialog("boss4.dlg")

    def spawn_wolfgang_post_fight(self):
        actualX = self.currLevelMap.waypointMap["wg1"].getTileX()
        actualY = self.currLevelMap.waypointMap["wg1"].getTileY()+1
        waypoints = [self.currLevelMap.waypointMap["wg1"], self.currLevelMap.waypointMap["wg2"], self.currLevelMap.waypointMap["wg3"]]
        wolfgang = Wolfgang_1973(actualX, actualY, waypoints, self.currLevelMap.batches, self.currLevelMap.batchGroups)
        wolfgang.initialize()
        self.currLevelMap.npcs.add(wolfgang)

    def update(self, dt):
        if not self.rees_fled:
            # Lock the tunnel door after Dr. Rees flees
            if not self.rees.alive:
                self.tunnel_door.locked = True
                self.rees_fled = True
                self.rees = None
        if self.ammo is not None:
            if self.ammo.shouldRemove():
                self.ammo = None
        if self.medkit is not None:
            if self.medkit.shouldRemove():
                self.medkit = None
        if self.boss is not None:
            if self.boss.health <= 0:
                self.boss = None
                for door in self.currLevelMap.doors:
                    door.locked = False
                SoundManager.play_music("creepy")
                from it5.script import ScriptManager
                ScriptManager.load(self.transition_script_name)
                self.spawn_wolfgang_post_fight()
                player_waypoint = self.currLevelMap.waypointMap["p0"]
                self.player.changeDirection(Direction.UP)
                self.player.clearMoveStack()
                self.player.jump(player_waypoint.getTileX(), player_waypoint.getTileY())
                EventManager.reset_camera()
                self.open_dialog("post_boss4.dlg")
            else:
                if self.player.weapon is not None:
                    if self.player.weapon.ammo == 0:
                        if self.player.weapon.type == WeaponType.MINE:
                            if not self.spawned_mines:
                                self.spawn_ammo(13, 10, self.player.weapon.type, _count=4)
                                self.spawned_mines = True
                        elif self.player.weapon.type == WeaponType.GRENADE:
                            if not self.spawned_grenades:
                                self.spawn_ammo(13, 10, self.player.weapon.type)
                                self.spawned_grenades = True
                        elif self.player.weapon.type == WeaponType.PISTOL:
                            self.spawn_ammo(5, 10, self.player.weapon.type)
                        elif self.player.weapon.type == WeaponType.CARBINE:
                            self.spawn_ammo(5, 10, self.player.weapon.type)
                        elif self.player.weapon.type == WeaponType.SMG:
                            self.spawn_ammo(5, 10, self.player.weapon.type)

    def spawn_ammo(self, x, y, weapon_type, _count=None):
        if self.ammo is None:
            if isinstance(weapon_type, WeaponType):
                count = weapon_type.mag_size
                # This is a little hacky, but the weapon type is expected to be a string in the properties dict
                weapon_type = weapon_type.name.lower()
            else:
                count = 1
            if _count is not None:
                count = _count
            image = common_tileset[common_spritemap["weapon"]]
            batch = batches["main"]
            batch_group = batchGroups["items"]
            self.ammo = Item(x, y, image, batch, batch_group, "ammo", {"count":count, "type":weapon_type}, temporary=True)
            self.ammo.show()
            self.currLevelMap.items.add(self.ammo)

    def spawn_medkit(self, x, y):
        if self.medkit is None:
            image = common_tileset[common_spritemap["medkit"]]
            batch = batches["main"]
            batch_group = batchGroups["items"]
            self.medkit = Item(x, y, image, batch, batch_group, "medkit", {}, temporary=True)
            self.medkit.show()
            self.currLevelMap.items.add(self.medkit)

    def dialog_finished(self, dialog_name):
        print("Dialog %s has finished" % dialog_name)

    def destroy(self):
        self.event_manager.remove_handler("dialog_finished", self.dialog_finished)
        if self.ammo is not None:
            self.currLevelMap.items.remove(self.ammo)
            self.ammo.hide()
        if self.medkit is not None:
            self.currLevelMap.items.remove(self.medkit)
            self.medkit.hide()

class Boss5Script(PersistentScript):
    def on_load(self):
        print("Loaded Boss5Script!")
        self.event_manager.set_handler("dialog_finished", self.dialog_finished)
        # Add the boss
        start_waypoint_name = "asp_a"
        actualX = self.currLevelMap.waypointMap[start_waypoint_name].getTileX()
        actualY = self.currLevelMap.waypointMap[start_waypoint_name].getTileY()+1
        waypoints = [wp for name, wp in self.currLevelMap.waypointMap.items() if name.startswith("asp")]
        self.boss = Asp(actualX, actualY, waypoints, self.currLevelMap.batches, self.currLevelMap.batchGroups)
        self.boss.initialize()
        self.currLevelMap.npcs.add(self.boss)
        self.boss.jump_to_waypoint(self.currLevelMap.waypointMap[start_waypoint_name])
        self.doors = self.currLevelMap.doors
        for door in self.currLevelMap.doors:
            door.locked = True
        self.open_dialog("boss5.dlg")

    def update(self, dt):
        if self.boss is not None:
            if self.boss.health <= 0:
                self.boss = None
                for door in self.currLevelMap.doors:
                    door.locked = False

    def dialog_finished(self, dialog_name):
        print("Dialog %s has finished" % dialog_name)

    def destroy(self):
        self.event_manager.remove_handler("dialog_finished", self.dialog_finished)

class IsabelHousePostColoniaScript(OneTimeScript):
    def on_load(self):
        print("Loaded IsabelHousePostColoniaScript!")
        self.player.changeDirection(Direction.UP)
        self.player.clearMoveStack()
        # Give player level 3 key
        item = Item(0, 0, None, None, None, "item", {"type": "key", "level": 3})
        self.player.inventory.addTo(item)
        # Give player a flashlight
        item2 = Item(0, 0, None, None, None, "item", {"type": "flashlight"})
        self.player.inventory.addTo(item2)
        self.open_dialog("isabel_post_colonia.dlg")
        self.event_manager.set_handler("dialog_finished", self.dialog_finished)

    def dialog_finished(self, dialog_name):
        print("Dialog %s has finished" % dialog_name)

    def destroy(self):
        self.event_manager.remove_handler("dialog_finished", self.dialog_finished)

class CaughtScientistScript(PersistentScript):
    def on_load(self):
        print("Loaded CaughtScientistScript!")
        actualX = self.currLevelMap.waypointMap["rees1"].getTileX()
        actualY = self.currLevelMap.waypointMap["rees1"].getTileY()+1
        r_waypoints = [self.currLevelMap.waypointMap["rees1"]]
        self.rees = FemaleScientist(actualX, actualY, r_waypoints, self.currLevelMap.batches, self.currLevelMap.batchGroups)
        self.rees.initialize()
        actualX = self.currLevelMap.waypointMap["isabel1"].getTileX()
        actualY = self.currLevelMap.waypointMap["isabel1"].getTileY()+1
        i_waypoints = [self.currLevelMap.waypointMap["isabel1"]]
        self.isabel = Isabel(actualX, actualY, i_waypoints, self.currLevelMap.batches, self.currLevelMap.batchGroups)
        self.isabel.initialize()
        self.isabel.changeDirection(Direction.RIGHT)
        self.currLevelMap.npcs.add(self.rees)
        self.currLevelMap.npcs.add(self.isabel)
        self.player.changeDirection(Direction.DOWN)
        self.player.clearMoveStack()
        # Cache the warps so the player can't get stuck
        self.warp_cache = [warp for warp in self.currLevelMap.warps]
        self.currLevelMap.warps.clear()
        self.wait_to_warp = False
        self.done_waiting = False
        self.post_dlg_elapsed_time = 0
        self.wait_time = 3
        self.open_dialog("caught_rees.dlg")
        self.event_manager.set_handler("dialog_finished", self.dialog_finished)

    def update(self, dt):
        if self.wait_to_warp:
            if not self.done_waiting:
                self.post_dlg_elapsed_time += dt
                if self.post_dlg_elapsed_time >= self.wait_time:
                    self.done_waiting = True
                    self.warp_to_shed()

    def warp_to_shed(self):
        self.place_warp(self.player.getTileX(), self.player.getTileY(), 3, 5, "warehouse_shed")

    def load_script(self, script_name, **script_args):
        from it5.script import ScriptManager
        ScriptManager.load(script_name, **script_args)

    def dialog_finished(self, dialog_name):
        print("Dialog %s has finished" % dialog_name)
        self.wait_to_warp = True
        self.load_script("DarkeningScript", curr_rgb=self.currLevelMap.ambience)

    def destroy(self):
        self.event_manager.remove_handler("dialog_finished", self.dialog_finished)
        # Rebuild this map's warps from the warp cache
        for warp in self.warp_cache:
            self.currLevelMap.warps.append(warp)
        self.warp_cache.clear()

class LockShedDoorScript(OneTimeScript):
    def on_load(self):
        print("Loaded LockShedDoorScript!")
        self.player.changeDirection(Direction.UP)
        self.player.clearMoveStack()
        # Player cannot shoot the Scientist
        self.player.cant_shoot = True
        self.door = self.currLevelMap.doors[0]
        self.door.locked = True

    def destroy(self):
        # Player can shoot after exiting the map
        self.player.cant_shoot = False

class ReesInterrogationScript(PersistentScript):
    def on_load(self):
        print("Loaded ReesInterrogationScript!")
        self.prisoner_dialog2_spawned = False
        self.player.changeDirection(Direction.UP)
        self.player.clearMoveStack()
        self.isabel = None
        self.rees = None
        for npc in self.currLevelMap.npcs:
            if type(npc) == Isabel:
                self.isabel = npc
                self.isabel.changeDirection(Direction.UP)
            elif type(npc) == Rees_Interrogation:
                self.rees = npc
        self.door = self.currLevelMap.doors[0]
        self.door.locked = True
        self.open_dialog("rees_interrogation.dlg")
        self.event_manager.set_handler("dialog_finished", self.dialog_finished)

    def update(self, dt):
        if self.rees.stamina == 20:
            if not self.prisoner_dialog2_spawned:
                self.prisoner_dialog2_spawned = True
                self.open_dialog("rees_interrogation2.dlg")

    def isabel_change_direction(self):
        # Without this, Isabel will revert to facing down instead of up
        wpm = self.currLevelMap.waypointMap
        path = Path([wpm['i0b']])
        self.isabel.path = path
        self.isabel.jump(path.waypoints[0].getTileX(), path.waypoints[0].getTileY()+1)
        self.isabel.alive = False
        self.isabel = self.isabel.copy()
        self.isabel.initialize()
        self.isabel.changeDirection(path.waypoints[0].direction)
        self.currLevelMap.npcs.add(self.isabel)

    def reroute_isabel(self):
        wpm = self.currLevelMap.waypointMap
        path = Path([wpm['i1'], wpm['i2'], wpm['i3'], wpm['i4']])
        self.isabel.path = path
        self.isabel.jump(path.waypoints[0].getTileX(), path.waypoints[0].getTileY()+1)
        self.isabel.alive = False
        self.isabel = self.isabel.copy()
        self.isabel.initialize()
        self.isabel.changeDirection(path.waypoints[0].direction)
        self.currLevelMap.npcs.add(self.isabel)

    def dialog_finished(self, dialog_name):
        print("Dialog %s has finished" % dialog_name)
        if dialog_name == "rees_interrogation.dlg":
            self.isabel_change_direction()
        if dialog_name == "rees_interrogation2.dlg":
            self.complete_objective("main", "finish_interrogation")
            self.door.locked = False
            self.reroute_isabel()

    def destroy(self):
        self.event_manager.remove_handler("dialog_finished", self.dialog_finished)

class ExitShedScript(OneTimeScript):
    def on_load(self):
        print("Loaded ExitShedScript!")
        self.prisoner_dialog2_spawned = False
        self.player.changeDirection(Direction.DOWN)
        self.player.clearMoveStack()
        wpm = self.currLevelMap.waypointMap
        actualX = wpm["i0"].getTileX()
        actualY = wpm["i0"].getTileY()+1
        i_waypoints = [wpm["i0"], wpm["i1"], wpm["i2"], wpm["i3"], wpm["i4"]]
        self.isabel = Isabel(actualX, actualY, i_waypoints, self.currLevelMap.batches, self.currLevelMap.batchGroups)
        self.isabel.initialize()
        self.isabel.changeDirection(Direction.UP)
        self.currLevelMap.npcs.add(self.isabel)
        actualX = wpm["m0"].getTileX()
        actualY = wpm["m0"].getTileY()+1
        m_waypoints = [wpm["m0"], wpm["m1"], wpm["m2"]]
        marcus = Man(actualX, actualY, m_waypoints, self.currLevelMap.batches, self.currLevelMap.batchGroups, hair_color=(28,23,11), shirt_color=(255,94,118), pants_color=(49,52,64))
        marcus.initialize()
        marcus.changeDirection(Direction.RIGHT)
        self.currLevelMap.npcs.add(marcus)
        self.open_dialog("post_rees_interrogation.dlg")
        self.event_manager.set_handler("dialog_finished", self.dialog_finished)

    def dialog_finished(self, dialog_name):
        print("Dialog %s has finished" % dialog_name)
        if dialog_name == "post_rees_interrogation.dlg":
            self.isabel.changeDirection(Direction.DOWN)

    def destroy(self):
        self.event_manager.remove_handler("dialog_finished", self.dialog_finished)

class FindTailorScript(PersistentScript):
    def on_load(self):
        print("Loaded FindTailorScript!")
        self.player.changeDirection(Direction.UP)
        self.player.clearMoveStack()
        wpm = self.currLevelMap.waypointMap
        actualX = wpm["i0"].getTileX()
        actualY = wpm["i0"].getTileY()+1
        i_waypoints = [wpm["i0"], wpm["i1"], wpm["i2"], wpm["i3"]]
        self.isabel = Isabel(actualX, actualY, i_waypoints, self.currLevelMap.batches, self.currLevelMap.batchGroups)
        self.isabel.initialize()
        self.isabel.changeDirection(Direction.RIGHT)
        self.currLevelMap.npcs.add(self.isabel)
        actualX = wpm["t0"].getTileX()
        actualY = wpm["t0"].getTileY()+1
        t_waypoints = [wpm["t0"]]
        tailor = Man(actualX, actualY, t_waypoints, self.currLevelMap.batches, self.currLevelMap.batchGroups, hair_color=(237,221,95), shirt_color=(16,16,16), pants_color=(16,16,16))
        tailor.initialize()
        tailor.changeDirection(Direction.DOWN)
        self.currLevelMap.npcs.add(tailor)
        self.isabel_changed = False
        self.isabel_target_x = wpm["i3"].getTileX()
        self.isabel_target_y = wpm["i3"].getTileY()
        self.door = self.currLevelMap.doors[0]
        self.door.locked = True
        self.open_dialog("tailor_entrance.dlg")
        self.event_manager.set_handler("dialog_finished", self.dialog_finished)

    def update(self, dt):
        if not self.isabel_changed:
            if not self.isabel.moving:
                if self.isabel.getTileX() == self.isabel_target_x and self.isabel.getTileY() == self.isabel_target_y:
                    self.isabel_changed = True
                    self.isabel_change_clothes()

    def isabel_change_clothes(self):
        # Delete old Isabel and create a new one
        self.isabel.alive = False
        wpm = self.currLevelMap.waypointMap
        actualX = wpm["i3"].getTileX()
        actualY = wpm["i3"].getTileY()+1
        i_waypoints = [wpm["i3"]]
        self.isabel = Isabel_Dress(actualX, actualY, i_waypoints, self.currLevelMap.batches, self.currLevelMap.batchGroups, dialog="isabel_changing.dlg")
        self.isabel.initialize()
        self.isabel.changeDirection(Direction.UP)
        self.currLevelMap.npcs.add(self.isabel)

    def dialog_finished(self, dialog_name):
        print("Dialog %s has finished" % dialog_name)

    def destroy(self):
        self.event_manager.remove_handler("dialog_finished", self.dialog_finished)
        self.door.locked = False

class ChangeIntoFormalAttireScript(PersistentScript):
    def on_load(self):
        print("Loaded ChangeIntoFormalAttireScript!")
        self.player.changeDirection(Direction.UP)
        self.player.clearMoveStack()
        self.done_waiting = False
        self.elapsed_time = 0
        self.wait_time = 3
        self.player_changed = False
        self.load_script("DarkeningScript", curr_rgb=self.currLevelMap.ambience)

    def load_script(self, script_name, **script_args):
        from it5.script import ScriptManager
        ScriptManager.load(script_name, **script_args)

    def update(self, dt):
        if not self.done_waiting:
            if self.elapsed_time > 0 and not self.player_changed:
                # Change clothes on second frame
                # Putting this in on_load() was preventing the self.player reference from being updated
                self.event_manager.player_change_clothes(True)
                self.player_changed = False
            self.elapsed_time += dt
            if self.elapsed_time >= self.wait_time:
                self.done_waiting = True
                self.warp_to_lake()

    def on_player_change(self):
        print("Player ref has changed")
        self.player.changeDirection(Direction.UP)
        self.player.clearMoveStack()

    def warp_to_lake(self):
        self.place_warp(self.player.getTileX(), self.player.getTileY(), 15, 15, "lake")

class GoToInalcoHouseScript(OneTimeScript):
    def on_load(self):
        print("Loaded GoToInalcoHouseScript!")
        self.player.changeDirection(Direction.UP)
        self.player.clearMoveStack()
        wpm = self.currLevelMap.waypointMap
        actualX = wpm["i0"].getTileX()
        actualY = wpm["i0"].getTileY()+1
        i_waypoints = [wpm["i0"], wpm["i1"]]
        self.isabel = Isabel_Dress(actualX, actualY, i_waypoints, self.currLevelMap.batches, self.currLevelMap.batchGroups)
        self.isabel.initialize()
        self.isabel.changeDirection(Direction.DOWN)
        self.currLevelMap.npcs.add(self.isabel)
        self.open_dialog("outside_inalco_house.dlg")
        self.event_manager.set_handler("dialog_finished", self.dialog_finished)

    def dialog_finished(self, dialog_name):
        print("Dialog %s has finished" % dialog_name)

    def destroy(self):
        self.event_manager.remove_handler("dialog_finished", self.dialog_finished)

class EnterInalcoHouseScript(PersistentScript):
    def on_load(self):
        print("Loaded GoToInalcoHouseScript!")
        self.player.changeDirection(Direction.UP)
        self.player.clearMoveStack()
        wpm = self.currLevelMap.waypointMap
        actualX = wpm["i0"].getTileX()
        actualY = wpm["i0"].getTileY()+1
        i_waypoints = [wpm["i0"], wpm["i1"], wpm["i2"], wpm["i3"], wpm["i4"], wpm["i5"]]
        self.isabel = Isabel_Dress(actualX, actualY, i_waypoints, self.currLevelMap.batches, self.currLevelMap.batchGroups)
        self.isabel.initialize()
        self.isabel.changeDirection(Direction.DOWN)
        self.currLevelMap.npcs.add(self.isabel)
        self.isabel_reached_destination = False
        self.isabel_target_x = wpm["i5"].getTileX()
        self.isabel_target_y = wpm["i5"].getTileY()

    def update(self, dt):
        if not self.isabel_reached_destination:
            if not self.isabel.moving:
                if self.isabel.getTileX() == self.isabel_target_x and self.isabel.getTileY() == self.isabel_target_y:
                    self.isabel_reached_destination = True
                    self.isabel_gains_dialog()

    def isabel_gains_dialog(self):
        # Delete old Isabel and create a new one
        self.isabel.alive = False
        wpm = self.currLevelMap.waypointMap
        actualX = wpm["i5"].getTileX()
        actualY = wpm["i5"].getTileY()+1
        i_waypoints = [wpm["i5"]]
        self.isabel = Isabel_Dress(actualX, actualY, i_waypoints, self.currLevelMap.batches, self.currLevelMap.batchGroups, dialog="isabel_party.dlg")
        self.isabel.initialize()
        self.isabel.changeDirection(Direction.RIGHT)
        self.currLevelMap.npcs.add(self.isabel)

    def dialog_finished(self, dialog_name):
        print("Dialog %s has finished" % dialog_name)

    def destroy(self):
        self.event_manager.remove_handler("dialog_finished", self.dialog_finished)

class RejoinPartyScript(OneTimeScript):
    def on_load(self):
        print("Loaded RejoinPartyScript!")
        self.player.changeDirection(Direction.DOWN)
        self.player.clearMoveStack()
        wpm = self.currLevelMap.waypointMap
        actualX = wpm["ib0"].getTileX()
        actualY = wpm["ib0"].getTileY()+1
        i_waypoints = [wpm["ib0"], wpm["ib1"], wpm["ib2"], wpm["ib3"], wpm["ib4"], wpm["ib5"], wpm["ib6"]]
        self.isabel = Isabel_Dress(actualX, actualY, i_waypoints, self.currLevelMap.batches, self.currLevelMap.batchGroups)
        self.isabel.initialize()
        self.isabel.changeDirection(Direction.UP)
        self.currLevelMap.npcs.add(self.isabel)
        self.open_dialog("rejoined_party.dlg")
        self.event_manager.set_handler("dialog_finished", self.dialog_finished)

    def dialog_finished(self, dialog_name):
        print("Dialog %s has finished" % dialog_name)

    def destroy(self):
        self.event_manager.remove_handler("dialog_finished", self.dialog_finished)

class LeavePartyScript(OneTimeScript):
    def on_load(self):
        print("Loaded LeavePartyScript!")
        self.player.changeDirection(Direction.DOWN)
        self.player.clearMoveStack()
        wpm = self.currLevelMap.waypointMap
        actualX = wpm["ib0"].getTileX()
        actualY = wpm["ib0"].getTileY()+1
        i_waypoints = [wpm["ib0"], wpm["ib1"], wpm["ib2"]]
        self.isabel = Isabel_Dress(actualX, actualY, i_waypoints, self.currLevelMap.batches, self.currLevelMap.batchGroups)
        self.isabel.initialize()
        self.isabel.changeDirection(Direction.UP)
        self.currLevelMap.npcs.add(self.isabel)
        self.open_dialog("left_party.dlg")
        self.event_manager.set_handler("dialog_finished", self.dialog_finished)

    def dialog_finished(self, dialog_name):
        print("Dialog %s has finished" % dialog_name)

    def destroy(self):
        self.event_manager.remove_handler("dialog_finished", self.dialog_finished)

class RetireToGuestHouseScript(PersistentScript):
    def on_load(self):
        print("Loaded RetireToGuestHouseScript!")
        self.done_waiting = False
        self.elapsed_time = 0
        self.wait_time = 3
        self.opened_dialog = False
        wpm = self.currLevelMap.waypointMap
        actualX = wpm["i0"].getTileX()
        actualY = wpm["i0"].getTileY()+1
        i_waypoints = [wpm["i0"]]
        self.isabel = Isabel_Dress(actualX, actualY, i_waypoints, self.currLevelMap.batches, self.currLevelMap.batchGroups)
        self.isabel.initialize()
        self.isabel.changeDirection(Direction.DOWN)
        self.currLevelMap.npcs.add(self.isabel)
        # Jump the player into position
        px = wpm["p0"].getTileX()
        py = wpm["p0"].getTileY()
        self.player.changeDirection(Direction.UP)
        self.player.clearMoveStack()
        self.player.jump(px, py)
        self.door = self.currLevelMap.doors[0]
        self.door.locked = True
        self.event_manager.set_handler("dialog_finished", self.dialog_finished)

    def load_script(self, script_name, **script_args):
        from it5.script import ScriptManager
        ScriptManager.load(script_name, **script_args)

    def update(self, dt):
        if self.opened_dialog:
            if not self.done_waiting:
                self.elapsed_time += dt
                if self.elapsed_time >= self.wait_time:
                    self.done_waiting = True
                    self.warp_to_town()
        else:
            self.opened_dialog = True
            self.open_dialog("guest_house.dlg")

    def warp_to_town(self):
        self.place_warp(self.player.getTileX(), self.player.getTileY(), 10, 59, 0)

    def dialog_finished(self, dialog_name):
        print("Dialog %s has finished" % dialog_name)
        if dialog_name == "guest_house.dlg":
            self.load_script("DarkeningScript", curr_rgb=self.currLevelMap.ambience)

    def destroy(self):
        self.event_manager.remove_handler("dialog_finished", self.dialog_finished)
        self.door.locked = False

class ReturnToTownPostPartyScript(OneTimeScript):
    def on_load(self):
        print("Loaded ReturnToTownPostPartyScript!")
        self.player.changeDirection(Direction.RIGHT)
        self.player.clearMoveStack()
        wpm = self.currLevelMap.waypointMap
        actualX = wpm["i0"].getTileX()
        actualY = wpm["i0"].getTileY()+1
        i_waypoints = [wpm["i0"], wpm["i1"]]
        self.isabel = Isabel_Dress(actualX, actualY, i_waypoints, self.currLevelMap.batches, self.currLevelMap.batchGroups)
        self.isabel.initialize()
        self.isabel.changeDirection(Direction.LEFT)
        self.currLevelMap.npcs.add(self.isabel)
        self.open_dialog("return_to_town_post_party.dlg")
        self.event_manager.set_handler("dialog_finished", self.dialog_finished)

    def dialog_finished(self, dialog_name):
        print("Dialog %s has finished" % dialog_name)
        # Player gets his gear back
        self.event_manager.player_change_clothes(False)

    def destroy(self):
        self.event_manager.remove_handler("dialog_finished", self.dialog_finished)

class BaseWest3FScript(OneTimeScript):
    def on_load(self):
        print("Loaded BaseWest3FScript!")
        self.event_manager.set_handler('dialog_finished', self.dialog_finished)
        self.player.changeDirection(Direction.UP)
        self.player.clearMoveStack()
        # Lock doors
        for door in self.currLevelMap.doors:
            door.locked = True
        SoundManager.play_sfx("monster_growl")
        self.spawn_relict()
        self.open_dialog("base_west_3f.dlg")

    def spawn_relict(self):
        SoundManager.play_music("danger")
        actualX = self.currLevelMap.waypointMap['relict'].getTileX()
        actualY = self.currLevelMap.waypointMap['relict'].getTileY()+1
        waypoints = [self.currLevelMap.waypointMap['relict']]
        self.boss = Relict(actualX, actualY, waypoints, self.currLevelMap.batches, self.currLevelMap.batchGroups)
        self.boss.initialize()
        # This thing is too easy to cheese with less stamina
        self.boss.max_stamina = 200
        self.boss.stamina = self.boss.max_stamina
        self.currLevelMap.npcs.add(self.boss)

    def dialog_finished(self, dialog_name):
        print("Dialog %s has finished" % dialog_name)
        if dialog_name == "base_west_3f.dlg":
            pass

    def destroy(self):
        self.event_manager.remove_handler("dialog_finished", self.dialog_finished)
        # Unlock doors
        for door in self.currLevelMap.doors:
            door.locked = False

class RelictSnowScript(PersistentScript):
    def on_load(self):
        print("Loaded RelictSnowScript!")
        self.event_manager.set_handler('dialog_finished', self.dialog_finished)
        self.player.changeDirection(Direction.UP)
        self.player.clearMoveStack()
        # Lock doors
        for door in self.currLevelMap.doors:
            door.locked = True
        self.waiting_to_spawn = False
        self.roared = False
        self.elapsed_time = 0
        self.time_to_spawn_seconds = 6
        self.relict = None
        self.open_dialog("relict_snow.dlg")

    def update(self, dt):
        if self.waiting_to_spawn:
            self.elapsed_time += dt
            if self.elapsed_time >= self.time_to_spawn_seconds:
                self.waiting_to_spawn = False
                self.spawn_relict()
            elif not self.roared and self.elapsed_time >= self.time_to_spawn_seconds - 1:
                self.roared = True
                SoundManager.play_sfx("monster_growl")
        if self.relict is not None:
            if self.relict.health <= 0:
                SoundManager.play_sfx("monster_growl")
                self.relict = None
                self.complete_objective("main", "kill_relict")
                self.unlock_doors()
                SoundManager.play_music("tension")

    def spawn_relict(self):
        SoundManager.play_music("danger")
        actualX = self.currLevelMap.waypointMap['relict'].getTileX()
        actualY = self.currLevelMap.waypointMap['relict'].getTileY()+1
        waypoints = [self.currLevelMap.waypointMap['relict']]
        # Setting health_regen_delta=0 is super-important; the game is unbeatable otherwise
        self.relict = Relict(actualX, actualY, waypoints, self.currLevelMap.batches, self.currLevelMap.batchGroups, health=2000, move_speed=125, animation_cycles=36, health_regen_delta=0, stun_time_wait=1.0, growl_after_stun=False)
        self.relict.initialize()
        # This thing is too easy to cheese with less stamina
        self.relict.max_stamina = 200
        self.relict.stamina = self.relict.max_stamina
        self.currLevelMap.npcs.add(self.relict)
        # Be nice to less-skilled players
        self.player.setHealth(self.player.maxHealth)
        EventManager.set_notification("Adrenaline Rush")

    def unlock_doors(self):
        for door in self.currLevelMap.doors:
            door.locked = False

    def dialog_finished(self, dialog_name):
        print("Dialog %s has finished" % dialog_name)
        if dialog_name == "relict_snow.dlg":
            self.waiting_to_spawn = True

    def destroy(self):
        self.event_manager.remove_handler("dialog_finished", self.dialog_finished)
        self.unlock_doors()

class Ch2FinaleScript(PersistentScript):
    def on_load(self):
        print("Loaded Ch2FinaleScript!")
        self.event_manager.set_handler('dialog_finished', self.dialog_finished)
        self.player.cant_shoot = True
        self.player.changeDirection(Direction.UP)
        self.player.clearMoveStack()
        self.spawned_ch2_finale_dlg = False
        self.waiting_for_inalco_walk = False
        self.waiting_for_fade_out = False
        self.fade_out_time = 0
        self.fade_out_time_to_wait_seconds = 4
        self.inalco = None
        for npc in self.currLevelMap.npcs:
            if isinstance(npc, Inalco):
                self.inalco = npc
                break
        self.open_dialog("ch2_finale_pre.dlg")

    def update(self, dt):
        if self.waiting_for_inalco_walk:
            if not self.inalco.moving:
                if not self.spawned_ch2_finale_dlg:
                    self.spawned_ch2_finale_dlg = True
                    self.open_dialog("ch2_finale.dlg")
        if self.waiting_for_fade_out:
            self.fade_out_time += dt
            if self.fade_out_time >= self.fade_out_time_to_wait_seconds:
                self.waiting_for_fade_out = False
                self.game_complete()

    def dialog_finished(self, dialog_name):
        print("Dialog %s has finished" % dialog_name)
        if dialog_name == "ch2_finale_pre.dlg":
            self.waiting_for_inalco_walk = True
            self.reroute_inalco()
        elif dialog_name == "ch2_finale.dlg":
            self.waiting_for_fade_out = True
            self.load_script("DarkeningScript", curr_rgb=self.currLevelMap.ambience)

    def load_script(self, script_name, **script_args):
        from it5.script import ScriptManager
        ScriptManager.load(script_name, **script_args)

    def reroute_inalco(self):
        wpm = self.currLevelMap.waypointMap
        path = Path([wpm['i0'], wpm['i1'], wpm['i2'], wpm['i3']])
        self.inalco.path = path
        self.inalco.jump(path.waypoints[0].getTileX(), path.waypoints[0].getTileY()+1)
        self.inalco.alive = False
        self.inalco = self.inalco.copy()
        self.inalco.initialize()
        self.inalco.changeDirection(path.waypoints[0].direction)
        self.currLevelMap.npcs.add(self.inalco)

    def game_complete(self):
        self.place_warp(self.player.getTileX(), self.player.getTileY(), 0, 0, 0, level_index=4)

    def destroy(self):
        self.event_manager.remove_handler("dialog_finished", self.dialog_finished)
        self.player.cant_shoot = False
