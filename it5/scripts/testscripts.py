from it5.script import OneTimeScript
from it5.script import PersistentScript
from it5.item import Item
from it5.bosses.relict import Relict
import math

class TestScript(OneTimeScript):
    def on_load(self):
        print("Loaded OneTimeScript!")
        smg = Item(0, 0, None, None, None, 'weapon', {'rank':0, 'type':'smg'})
        self.player.inventory.addTo(smg)
        ammo = Item(0, 0, None, None, None, 'ammo', {'count':34, 'type':'smg'})
        self.player.inventory.addTo(ammo)
        self.player.inventory.weapons['smg'].attachSuppressor()
        self.player.changeWeapon(1)
        carbine = Item(0, 0, None, None, None, 'weapon', {'rank':1, 'type':'carbine'})
        self.player.inventory.addTo(carbine)
        ammo = Item(0, 0, None, None, None, 'ammo', {'count':90, 'type':'carbine'})
        self.player.inventory.addTo(ammo)
        grenade = Item(0, 0, None, None, None, 'weapon', {'rank': 0, 'type': 'grenade'})
        self.player.inventory.addTo(grenade)
        ammo = Item(0, 0, None, None, None, 'ammo', {'count':4, 'type':'grenade'})
        self.player.inventory.addTo(ammo)
        mine = Item(0, 0, None, None, None, 'weapon', {'rank': 0, 'type': 'mine'})
        self.player.inventory.addTo(mine)
        ammo = Item(0, 0, None, None, None, 'ammo', {'count':3, 'type':'mine'})
        self.player.inventory.addTo(ammo)
        nvgs = Item(0, 0, None, None, None, 'item', {'rank':0, 'type':'nvg'})
        self.player.inventory.addTo(nvgs)
        flashlight = Item(0, 0, None, None, None, 'item', {'rank':0, 'type':'flashlight'})
        self.player.inventory.addTo(flashlight)

    def update(self, dt):
        # This will run exactly one time
        print("HELLO")

class TestPersistentScript(PersistentScript):
    def on_load(self):
        print("Loaded TestPersistentScript!")
        self.counter = 0
        self.elapsed_time = 0

    def update(self, dt):
        self.elapsed_time += dt
        if self.elapsed_time >= 1:
            self.elapsed_time = 0
            self.counter += 1
            print("COUNTER: " + str(self.counter))
            if self.player.health > 3:
                self.player.receiveDamage(2)
        if self.counter >= 10:
            print("FINISHED")
            self.finish()

class TestObjectiveScript(PersistentScript):
    def on_load(self):
        print("Loaded TestObjectiveScript!")
        pass

    def update(self, dt):
        pass

    def destroy(self):
        pass

class SpawnRelictBossTestScript(PersistentScript):
    def on_load(self):
        print("Loaded SpawnRelictBossTestScript!")
        actualX = self.currLevelMap.waypointMap['g1a'].getTileX()
        actualY = self.currLevelMap.waypointMap['g1a'].getTileY()+1
        waypoints = [self.currLevelMap.waypointMap['g1a']]
        self.boss = Relict(actualX, actualY, waypoints, self.currLevelMap.batches, self.currLevelMap.batchGroups)
        self.boss.initialize()
        self.currLevelMap.npcs.add(self.boss)

    def update(self, dt):
        pass

    def destroy(self):
        pass