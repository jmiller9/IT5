from it5.script import OneTimeScript
from it5.common import EventManager

class HospitalScript(OneTimeScript):
    def on_load(self):
        print("Loaded HospitalScript!")
        self.doctor = None
        for npc in self.currLevelMap.npcs:
            if npc.script is not None:
                self.doctor = npc
                break
        self.event_manager.set_handler("dialog_finished", self.dialog_finished)
        if self.player.health < self.player.maxHealth:
            self.open_dialog("hospital.dlg")
        else:
            self.open_dialog("hospital_healthy.dlg")

    def dialog_finished(self, dialog_name):
        print("Dialog " + dialog_name + " has finished")
        if dialog_name == "hospital.dlg":
            self.player.setHealth(self.player.maxHealth)
            EventManager.set_notification("Health has been restored")
        if self.doctor is not None:
            self.doctor.script_triggered = False
            self.doctor.createDialogMarker()

    def destroy(self):
        self.event_manager.remove_handler("dialog_finished", self.dialog_finished)
