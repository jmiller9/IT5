from it5.script import PersistentScript
from it5.script import CancelScriptLoadException
from it5.staticobject import StaticObject
from it5.common import EventManager, common_tileset, common_spritemap, batches, batchGroups
from random import randrange, randint

class LaserTrapController:
    in_use = False

    @classmethod
    def completed(cls):
        cls.in_use = False

    @classmethod
    def started(cls):
        cls.in_use = True

    @classmethod
    def busy(cls):
        return cls.in_use

    @classmethod
    def exit_map(cls):
        cls.completed()

class Laser(StaticObject):
    def __init__(self, x, y, image, batch, group=None):
        super().__init__(x, y, image, batch, group)
        self.set_active()

    def destroy(self):
        self.hide()

    def set_active(self):
        self.active = True
        self.show()
        self.sprite.opacity = 0

    def set_inactive(self):
        self.active = False
        self.hide()

    def check_collision(self, px, py):
        # Takes in player's tileX and tileY
        if self.active and self.originalX == px and self.originalY == py:
            return True
        return False

class LaserTrapScript(PersistentScript):
    def __init__(self):
        super().__init__()

    def on_load(self):
        if LaserTrapController.busy():
            raise CancelScriptLoadException("Laser Trap has already been triggered")
        print("Loaded LaserTrapScript!")
        LaserTrapController.started()
        self.last_disabled = set()
        self.tripped = False
        self.create_lasers()
        self.timer = 0
        self.seconds_to_switch = 3

    def update(self, dt):
        px = self.player.getTileX()
        py = self.player.getTileY() + 1
        collision = False
        for laser in self.lasers:
            collision |= laser.check_collision(px, py)
        if collision and not self.tripped:
            print("Player has tripped a laser trap!")
            EventManager.explosion(self.player.x, self.player.y, damage=500)
            self.tripped = True
        self.timer += dt
        if self.timer >= self.seconds_to_switch:
            self.timer = 0
            self.reset_lasers()

    def create_lasers(self):
        self.lasers = []
        horizontal_image = common_tileset[common_spritemap["laser_horizontal"]]
        vertical_image = common_tileset[common_spritemap["laser_vertical"]]
        for laser_dict in self.currLevelMap.lasers:
            image = None
            if laser_dict["orientation"] == "horizontal":
                image = horizontal_image
            elif laser_dict["orientation"] == "vertical":
                image = vertical_image
            laser = Laser(laser_dict["x"], laser_dict["y"], image, batches["main"], batchGroups["items"])
            self.lasers.append(laser)
        self.reset_lasers()

    def reset_lasers(self):
        for laser in self.lasers:
            laser.set_active()
        disabled = set()
        num_disabled = randint(3,5)
        while len(disabled) < num_disabled:
            index = randrange(len(self.lasers))
            if index not in self.last_disabled:
                disabled.add(index)
        self.last_disabled.clear()
        for index in disabled:
            self.lasers[index].set_inactive()
            self.last_disabled.add(index)

    def destroy_lasers(self):
        for laser in self.lasers:
            laser.destroy()
        self.lasers.clear()

    def destroy(self):
        self.destroy_lasers()
        LaserTrapController.exit_map()