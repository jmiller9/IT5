from it5.script import PersistentScript
from it5.script import OneTimeScript
from it5.npc import Man

class FirstTransitionScript(OneTimeScript):
    def __init__(self):
        super().__init__()

    def on_load(self):
        print("Loaded FirstTransitionScript!")
        self.event_manager.set_handler('dialog_finished', self.dialog_finished)
        self.player.clear_inventory()
        self.player.setHealth(self.player.maxHealth)
        self.open_dialog('transition_opening.dlg')

    def dialog_finished(self, dialog_name):
        print("Dialog " + dialog_name + " has finished")
        if dialog_name == 'transition_opening.dlg':
            self.place_warp(self.player.getTileX(), self.player.getTileY(), 4, 5, "player_house")

    def destroy(self):
        self.event_manager.remove_handler('dialog_finished', self.dialog_finished)

class SecondTransitionScript(PersistentScript):
    def __init__(self):
        super().__init__()

    def on_load(self):
        print("Loaded SecondTransitionScript!")
        self.event_manager.set_handler('dialog_finished', self.dialog_finished)
        self.wait_time = 3
        self.elapsed_time = 0
        self.wait_time_qg = 2
        self.qg_spawned = False
        self.dlg_triggered = False
        self.quest_giver = None
        self.wait_to_warp = False
        self.done_waiting = False
        self.post_dlg_elapsed_time = 0
        self.load_script("BrighteningScript")

    def load_script(self, script_name):
        from it5.script import ScriptManager
        ScriptManager.load(script_name)

    def update(self, dt):
        if not self.dlg_triggered:
            if self.elapsed_time < self.wait_time:
                self.elapsed_time += dt
                if not self.qg_spawned:
                    if self.elapsed_time >= self.wait_time_qg:
                        self.qg_spawned = True
                        self.spawn_quest_giver()
            else:
                self.dlg_triggered = True
                self.open_dialog("offer.dlg")
        if self.wait_to_warp:
            if not self.done_waiting:
                self.post_dlg_elapsed_time += dt
                if self.post_dlg_elapsed_time >= self.wait_time:
                    self.done_waiting = True
                    self.warp_to_ch2()

    def spawn_quest_giver(self):
        actualX = self.currLevelMap.waypointMap['m1'].getTileX()
        actualY = self.currLevelMap.waypointMap['m1'].getTileY()+1
        waypoints = [self.currLevelMap.waypointMap['m1']]
        shirt_color = (1, 15, 56)
        pants_color = (15, 18, 28)
        hair_color = (156, 153, 140)
        self.quest_giver = Man(actualX, actualY, waypoints, self.currLevelMap.batches, self.currLevelMap.batchGroups, shirt_color=shirt_color, pants_color=pants_color, hair_color=hair_color)
        self.quest_giver.initialize()
        self.currLevelMap.npcs.add(self.quest_giver)

    def warp_to_ch2(self):
        self.place_warp(self.player.getTileX(), self.player.getTileY(), 36, 44, 0, level_index=3)

    def dialog_finished(self, dialog_name):
        print("Dialog " + dialog_name + " has finished")
        if dialog_name == 'offer.dlg':
            self.wait_to_warp = True
            self.load_script("DarkeningScript")

    def destroy(self):
        self.event_manager.remove_handler('dialog_finished', self.dialog_finished)