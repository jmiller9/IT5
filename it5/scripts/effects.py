from it5.script import PersistentScript
from it5.common import lighting


class DimmingTransitionScript(PersistentScript):
    def on_load_init(self):
        print("Loaded DimmingTransitionScript!")
        self.time_to_relight = 4
        r, g, b = self.currLevelMap.ambience
        self.original_r = r
        self.original_g = g
        self.original_b = b
        self.curr_r = 0
        self.curr_g = 0
        self.curr_b = 0

    def on_load(self):
        self.on_load_init()
        self.elapsed_time = 0
        self.lights = []
        self.delta_r = (self.original_r - self.curr_r) / self.time_to_relight
        self.delta_g = (self.original_g - self.curr_g) / self.time_to_relight
        self.delta_b = (self.original_b - self.curr_b) / self.time_to_relight
        # Make sure the player doesn't have a flashlight or nvgs equipped
        if self.player.getItem() == "flashlight":
            self.player.changeItem(1)
            if self.player.getItem() == "nvg":
                self.player.changeItem(1)
        elif self.player.getItem() == "nvg":
            self.player.changeItem(1)
            if self.player.getItem() == "flashlight":
                self.player.changeItem(1)
        lighting.destroy_lights()
        for light in self.currLevelMap.lights:
            lighting.create_light(**light)
        for light in lighting.lights:
            r, g, b = light.color
            curr_r = 0
            curr_g = 0
            curr_b = 0
            delta_r = (r - curr_r) / self.time_to_relight
            delta_g = (g - curr_g) / self.time_to_relight
            delta_b = (b - curr_b) / self.time_to_relight
            light_dict = {
                "light": light,
                "curr_r": curr_r,
                "curr_g": curr_g,
                "curr_b": curr_b,
                "delta_r": delta_r,
                "delta_g": delta_g,
                "delta_b": delta_b,
                "target_r": r,
                "target_g": g,
                "target_b": b,
            }
            light.set_color_f((curr_r, curr_g, curr_b))
            self.lights.append(light_dict)
        lighting.set_ambience((self.curr_r, self.curr_g, self.curr_b))
        self.relit = False

    def update(self, dt):
        if not self.relit:
            if self.elapsed_time <= self.time_to_relight:
                self.curr_r += self.delta_r * dt
                self.curr_g += self.delta_g * dt
                self.curr_b += self.delta_b * dt
                lighting.set_ambience((self.curr_r, self.curr_g, self.curr_b))
                for light_dict in self.lights:
                    light = light_dict["light"]
                    light_dict["curr_r"] += light_dict["delta_r"] * dt
                    light_dict["curr_g"] += light_dict["delta_g"] * dt
                    light_dict["curr_b"] += light_dict["delta_b"] * dt
                    light.set_color_f((light_dict["curr_r"], light_dict["curr_g"], light_dict["curr_b"]))
            else:
                self.relit = True
                lighting.set_ambience((self.original_r, self.original_g, self.original_b))
                for light_dict in self.lights:
                    light = light_dict["light"]
                    light.set_color_f((light_dict["target_r"], light_dict["target_g"], light_dict["target_b"]))
                self.lights.clear()
            self.elapsed_time += dt

class BrighteningScript(DimmingTransitionScript):
    # Assumes the "original" (target) rgb is (255, 255, 255)
    def on_load_init(self):
        print("Loaded BrighteningScript!")
        r,g,b = self._options.get("original_rgb", (1.0, 1.0, 1.0))
        cr, cg, cb = self._options.get("curr_rgb", (0, 0, 0))
        self.time_to_relight = self._options.get("time_to_relight", 3)
        self.original_r = r
        self.original_g = g
        self.original_b = b
        self.curr_r = cr
        self.curr_g = cg
        self.curr_b = cb

class DarkeningScript(DimmingTransitionScript):
    # Assumes the current rgb is (255, 255, 255)
    def on_load_init(self):
        print("Loaded DarkeningScript!")
        r,g,b = self._options.get("original_rgb", (0, 0, 0))
        cr, cg, cb = self._options.get("curr_rgb", (1.0, 1.0, 1.0))
        self.time_to_relight = self._options.get("time_to_relight", 3)
        self.original_r = r
        self.original_g = g
        self.original_b = b
        self.curr_r = cr
        self.curr_g = cg
        self.curr_b = cb
