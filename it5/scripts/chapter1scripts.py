from it5.script import OneTimeScript
from it5.script import PersistentScript
from it5.npc import Brother_1945, FemaleScientist, Guard_Blind, Wolfgang_1945, guard_sprites, brother_1945_sprites
from it5.item import Item
from it5.common import EventManager
from it5.direction import Direction
from it5.waypoint import Path
from it5.audio import SoundManager
from it5.quest import QuestManager
from it5.animation import Animation, AnimationGroup, AnimationGroupManager
from it5.dynamicobject import DynamicObject

class AlpineHillScript(OneTimeScript):
    def on_load(self):
        print("Loaded AlpineHillScript!")
        actualX = self.currLevelMap.waypointMap['w1'].getTileX()
        actualY = self.currLevelMap.waypointMap['w1'].getTileY()+1
        waypoints = [self.currLevelMap.waypointMap['w1'], self.currLevelMap.waypointMap['w2']]
        brother = Brother_1945(actualX, actualY, waypoints, self.currLevelMap.batches, self.currLevelMap.batchGroups)
        brother.initialize()
        self.currLevelMap.npcs.add(brother)
        pistol = Item(0, 0, None, None, None, 'weapon', {'rank':0, 'type':'pistol'})
        self.player.inventory.addTo(pistol)
        ammo = Item(0, 0, None, None, None, 'ammo', {'count':16, 'type':'pistol'})
        self.player.inventory.addTo(ammo)
        medkit = Item(0, 0, None, None, None, 'medkit', {})
        self.player.inventory.addTo(medkit)
        self.player.changeWeapon(1)
        self.player.changeItem(1)
        flashlight = Item(0, 0, None, None, None, 'item', {'rank':0, 'type':'flashlight'})
        self.player.inventory.addTo(flashlight)
        EventManager.set_notification('Follow Lt. Edwards')
        self.open_dialog('brother_alpinehill.dlg')

class CaveScript(OneTimeScript):
    def on_load(self):
        print("Loaded CaveScript!")
        actualX = self.currLevelMap.waypointMap['w1'].getTileX()
        actualY = self.currLevelMap.waypointMap['w1'].getTileY()+1
        waypoints = [self.currLevelMap.waypointMap['w1'], self.currLevelMap.waypointMap['w2']]
        brother = Brother_1945(actualX, actualY, waypoints, self.currLevelMap.batches, self.currLevelMap.batchGroups)
        brother.initialize()
        self.currLevelMap.npcs.add(brother)

class LobbyScript(OneTimeScript):
    def on_load(self):
        print("Loaded LobbyScript!")
        actualX = self.currLevelMap.waypointMap['w1'].getTileX()
        actualY = self.currLevelMap.waypointMap['w1'].getTileY()+1
        waypoints = [self.currLevelMap.waypointMap['w1'], self.currLevelMap.waypointMap['w2'], self.currLevelMap.waypointMap['w3'], self.currLevelMap.waypointMap['w4']]
        brother = Brother_1945(actualX, actualY, waypoints, self.currLevelMap.batches, self.currLevelMap.batchGroups)
        brother.initialize()
        self.currLevelMap.npcs.add(brother)
        self.open_dialog('brother_lobby.dlg')

class ScientistScript(PersistentScript):
    def on_load(self):
        print("Loaded ScientistScript!")
        actualX = self.currLevelMap.waypointMap['scientist1'].getTileX()
        actualY = self.currLevelMap.waypointMap['scientist1'].getTileY()+1
        waypoints = [self.currLevelMap.waypointMap['scientist1']]
        self.scientist = FemaleScientist(actualX, actualY, waypoints, self.currLevelMap.batches, self.currLevelMap.batchGroups)
        self.scientist.initialize()
        self.currLevelMap.npcs.add(self.scientist)
        self.brother = None
        self.guard = None
        self.event_manager.set_handler('dialog_finished', self.dialog_finished)
        self.last_player_x = None
        self.last_player_y = None
        self.guard_attacked = False
        self.special_door = None
        self.special_door_security_level = 0
        for door in self.currLevelMap.doors:
            if door.securityLevel > 0:
                self.special_door = door
                if self.special_door.inactive:
                    self.special_door.show()
                self.special_door_security_level = self.special_door.securityLevel
                self.special_door.securityLevel = 0
                break
    
    def update(self, dt):
        if self.guard is not None:
            if not self.guard.alive:
                self.create_guard_death_animation(self.guard.getTileX(), self.guard.getTileY()+1)
                self.guard = None
                self.open_dialog('scientist_escaped.dlg')
            else:
                if self.last_player_x != self.player.x or self.last_player_y != self.player.y or self.player.direction != Direction.UP:
                    if not self.guard_attacked:
                        self.guard.attack(self.player, self.bullet_manager)
                        self.guard_attacked = True
                else:
                    if self.brother is None and self.guard.moving == False and self.guard.getTileY() == (self.currLevelMap.waypointMap['g1b'].getTileY()):
                        actualX = self.currLevelMap.waypointMap['b1'].getTileX()
                        actualY = self.currLevelMap.waypointMap['b1'].getTileY()+1
                        waypoints = [self.currLevelMap.waypointMap['b1'], self.currLevelMap.waypointMap['b2']]
                        self.brother = Brother_1945(actualX, actualY, waypoints, self.currLevelMap.batches, self.currLevelMap.batchGroups)
                        self.brother.initialize()
                        self.brother.moveSpeed = 90
                        self.currLevelMap.npcs.add(self.brother)
                        self.brother.attack(self.guard, self.bullet_manager)

    def dialog_finished(self, dialog_name):
        print("Dialog " + dialog_name + " has finished")
        if dialog_name == 'scientist.dlg':
            actualX = self.currLevelMap.waypointMap['g1a'].getTileX()
            actualY = self.currLevelMap.waypointMap['g1a'].getTileY()+1
            waypoints = [self.currLevelMap.waypointMap['g1a'], self.currLevelMap.waypointMap['g1b']]
            self.guard = Guard_Blind(actualX, actualY, waypoints, self.currLevelMap.batches, self.currLevelMap.batchGroups)
            self.guard.initialize()
            self.guard.changeDirection(Direction.UP)
            self.currLevelMap.npcs.add(self.guard)
            self.scientist.path = Path([self.currLevelMap.waypointMap['scientist2'], self.currLevelMap.waypointMap['scientist3']])
            SoundManager.play_sfx('alarm')
            SoundManager.play_music('alert')
            self.last_player_x = self.player.x
            self.last_player_y = self.player.y
            self.open_dialog('scientist_ambushed.dlg')

    def create_guard_death_animation(self, x, y):
        batch = self.currLevelMap.batches["main"]
        batchGroups = self.currLevelMap.batchGroups
        bottom = DynamicObject(x, y, guard_sprites, 4, 2, batch, batchGroups['items'])
        top = DynamicObject(x, y+1, guard_sprites, 5, 2, batch, batchGroups['items'])
        top_frames = [(7,6), (7,7)]
        bottom_frames = [(6,6), (6,7)]
        top_anim = Animation(top, top_frames, 0.2, lifetime_seconds=2, looping=False)
        bottom_anim = Animation(bottom, bottom_frames, 0.2, lifetime_seconds=2, looping=False)
        guard_anim_group = AnimationGroup(bottom_anim, top_anim)
        AnimationGroupManager.add_animation_group(guard_anim_group)

    def destroy(self):
        self.event_manager.remove_handler('dialog_finished', self.dialog_finished)
        if self.special_door is not None:
            self.special_door.securityLevel = self.special_door_security_level
            self.special_door.hide()
            self.special_door = None

class CatwalkScript(PersistentScript):
    def on_load(self):
        print("Loaded CatwalkScript!")
        actualX = self.currLevelMap.waypointMap['b1'].getTileX()
        actualY = self.currLevelMap.waypointMap['b1'].getTileY()+1
        waypoints = [self.currLevelMap.waypointMap['b1'], self.currLevelMap.waypointMap['b2'], self.currLevelMap.waypointMap['b3']]
        self.brother = Brother_1945(actualX, actualY, waypoints, self.currLevelMap.batches, self.currLevelMap.batchGroups)
        self.brother.initialize()
        self.brother.moveSpeed = 90
        self.currLevelMap.npcs.add(self.brother)
        self.wolfgang = None
        self.event_manager.set_handler('dialog_finished', self.dialog_finished)
        self.brother_attacked = False
        self.wolfgang_attack_timer = 0
        self.wolfgang_shot_count = 0
    
    def update(self, dt):
        if self.wolfgang is not None and self.brother is not None:
            if not self.brother.alive:
                EventManager.set_notification('Lt. Edwards has died')
                self.create_brother_death_animation(self.brother.getTileX(), self.brother.getTileY()+1)
                self.open_dialog('brother_dies.dlg')
                self.brother = None
            elif self.wolfgang.direction == Direction.LEFT and self.wolfgang.getTileX() <= 7:
                if not self.brother_attacked:
                    self.brother.attack(self.wolfgang, self.bullet_manager)
                    self.brother_attacked = True
                if self.wolfgang_attack_timer >= 0.25 and self.wolfgang_shot_count < 3:
                    self.wolfgang_attack_timer = 0
                    self.wolfgang.attack(self.brother, self.bullet_manager)
                self.wolfgang_attack_timer += dt

    def dialog_finished(self, dialog_name):
        print("Dialog " + dialog_name + " has finished")
        if dialog_name == 'catwalks_nothere.dlg':
            actualX = self.currLevelMap.waypointMap['b1'].getTileX()
            actualY = self.currLevelMap.waypointMap['b1'].getTileY()+1
            waypoints = [self.currLevelMap.waypointMap['b1'], self.currLevelMap.waypointMap['b2'], self.currLevelMap.waypointMap['w3'],
                        self.currLevelMap.waypointMap['w4'], self.currLevelMap.waypointMap['w5'], self.currLevelMap.waypointMap['w6']]
            self.wolfgang = Wolfgang_1945(actualX, actualY, waypoints, self.currLevelMap.batches, self.currLevelMap.batchGroups)
            self.wolfgang.initialize()
            self.currLevelMap.npcs.add(self.wolfgang)
            SoundManager.play_music('danger')
            self.open_dialog('brother_holdhimoff.dlg')
            QuestManager.active_quest = 'escape'

    def create_brother_death_animation(self, x, y):
        batch = self.currLevelMap.batches["main"]
        batchGroups = self.currLevelMap.batchGroups
        bottom = DynamicObject(x, y, brother_1945_sprites, 4, 2, batch, batchGroups['items'])
        top = DynamicObject(x, y+1, brother_1945_sprites, 5, 2, batch, batchGroups['items'])
        top_frames = [(7,6), (7,7)]
        bottom_frames = [(6,6), (6,7)]
        top_anim = Animation(top, top_frames, 0.2)
        bottom_anim = Animation(bottom, bottom_frames, 0.2)
        brother_anim_group = AnimationGroup(bottom_anim, top_anim)
        AnimationGroupManager.add_animation_group(brother_anim_group)

    def destroy(self):
        self.event_manager.remove_handler('dialog_finished', self.dialog_finished)

class Ch1FinaleScript(PersistentScript):
    def on_load(self):
        print("Loaded Ch1FinaleScript!")
        self.event_manager.set_handler('dialog_finished', self.dialog_finished)
        SoundManager.stop_music()
        self.start_explosions = False
        self.num_explosions = 0
        self.timer = 0
    
    def update(self, dt):
        if self.start_explosions:
            if self.num_explosions < 5:
                if self.timer >= 0.125:
                    SoundManager.play_sfx('explosion')
                    self.timer = 0
                    self.num_explosions += 1
                self.timer += dt
            else:
                self.open_dialog('ch1_finale2.dlg')
                self.start_explosions = False

    def dialog_finished(self, dialog_name):
        print("Dialog " + dialog_name + " has finished")
        if dialog_name == 'ch1_finale.dlg':
            SoundManager.play_sfx('empty_gunshot')
            SoundManager.play_sfx('explosion')
            self.num_explosions = 1
            self.start_explosions = True
            EventManager.set_notification('Explosives Detonated')
        elif dialog_name == 'ch1_finale2.dlg':
            if self.player.getItem() == "flashlight":
                self.player.changeItem(1)
            self.place_warp(self.player.getTileX(), self.player.getTileY(), 1, 1, 0, level_index=2)

    def destroy(self):
        self.event_manager.remove_handler('dialog_finished', self.dialog_finished)

class PrologueScript(PersistentScript):
    def on_load(self):
        print("Loaded PrologueScript!")
        self.event_manager.set_handler('dialog_finished', self.dialog_finished)
        self.player.changeDirection(Direction.UP)
        self.player.clearMoveStack()
        for npc in self.currLevelMap.npcs:
            if npc.friendly:
                self.brother = npc
            else:
                self.prisoner = npc
        self.open_dialog('brother_intro.dlg')
        self.prisoner_dialog1_spawned = False
        self.prisoner_dialog2_spawned = False
        self.prisoner_dialog3_spawned = False
        self.prisoner_dialog4_spawned = False
        self.killed_prisoner = False
    
    def update(self, dt):
        if self.prisoner.stamina == 90:
            if not self.prisoner_dialog1_spawned:
                self.prisoner_dialog1_spawned = True
                self.open_dialog('prologue_prisoner1.dlg')
        elif self.prisoner.stamina == 70:
            if not self.prisoner_dialog2_spawned:
                self.prisoner_dialog2_spawned = True
                self.open_dialog('prologue_prisoner2.dlg')
        elif self.prisoner.stamina == 50:
            if not self.prisoner_dialog3_spawned:
                self.prisoner_dialog3_spawned = True
                self.open_dialog('prologue_prisoner3.dlg')
        elif self.prisoner.stamina == 20:
            if not self.prisoner_dialog4_spawned:
                self.prisoner_dialog4_spawned = True
                self.open_dialog('prologue_prisoner4.dlg')
                self.complete_objective("main", "complete_interrogation")
        elif self.prisoner.stamina == 0:
            if not self.killed_prisoner:
                self.killed_prisoner = True
                self.open_dialog('prologue_prisoner_died.dlg')

    def dialog_finished(self, dialog_name):
        print("Dialog " + dialog_name + " has finished")
        if dialog_name == "brother_intro.dlg":
            self.brother.path = Path([self.currLevelMap.waypointMap['w1'], self.currLevelMap.waypointMap['w2'], self.currLevelMap.waypointMap['w3']])
        elif dialog_name == "prologue_prisoner4.dlg":
            self.start_quest("exit_the_base")

    def destroy(self):
        self.event_manager.remove_handler('dialog_finished', self.dialog_finished)
        # Needed so we don't overshoot the first ch.1 script trigger on the next level
        self.player.clearMoveStack()
        self.player.stopMoving(Direction.DOWN)