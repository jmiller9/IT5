from it5.script import OneTimeScript
from it5.script import PersistentScript
from it5.npc import Guard, Tortured_BadMan, Woman, Man
from it5.bosses.relict import Relict
from it5.item import Item, WeaponType
from it5.common import EventManager, common_tileset, common_spritemap, batches, batchGroups
from it5.direction import Direction
from it5.waypoint import Path
from it5.audio import SoundManager

class SidequestDVStartScript(OneTimeScript):
    def on_load(self):
        print("Loaded SidequestDVStartScript!")
        self.event_manager.set_handler("dialog_finished", self.dialog_finished)
        self.player.changeDirection(Direction.DOWN)
        self.player.clearMoveStack()
        self.open_dialog("sidequest_dv_start.dlg")

    def spawn_barkeep_with_hint(self):
        for npc in self.currLevelMap.npcs:
            # This only works if there is one and only one woman at the inn
            if isinstance(npc, Woman):
                hair_color = npc.hair.color
                dress_color = npc.clothes_top.color
                dialog = "sidequest_dv_hint.dlg"
                waypoints = [self.currLevelMap.waypointMap["bartender"]]
                woman = Woman(npc.getTileX(), npc.getTileY()+1, waypoints, batches, batchGroups, hair_color=hair_color, dress_color=dress_color, dialog=dialog)
                woman.initialize()
                self.currLevelMap.npcs.add(woman)
                npc.alive = False
                break

    def dialog_finished(self, dialog_name):
        print("Dialog " + dialog_name + " has finished")
        if dialog_name == "sidequest_dv_start.dlg":
            self.start_quest("domestic_violence")
            qd = self.get_quest_data("domestic_violence")
            # Initialize some quest data, will come in to play later
            qd["killed_abuser"] = False
            self.spawn_barkeep_with_hint()

    def destroy(self):
        self.event_manager.remove_handler("dialog_finished", self.dialog_finished)

class SidequestDVConfrontationScript(PersistentScript):
    def on_load(self):
        print("Loaded SidequestDVConfrontationScript!")
        self.event_manager.set_handler("dialog_finished", self.dialog_finished)
        self.player.changeDirection(Direction.UP)
        self.player.clearMoveStack()
        self.lock_doors()
        self.fading_out = False
        self.fading_in = False
        self.fading_out2 = False
        self.fading_in2 = False
        self.fade_timer = 0
        self.fade_time_seconds = 3
        self.torture_sequence_active = False
        self.bad_man = None
        self.open_dialog("sidequest_dv_confrontation.dlg")
    
    def dialog_finished(self, dialog_name):
        print("Dialog " + dialog_name + " has finished")
        if dialog_name == "sidequest_dv_confrontation.dlg":
            self.load_script("DarkeningScript")
            self.fading_out = True

    def load_script(self, script_name):
        from it5.script import ScriptManager
        ScriptManager.load(script_name)

    def reposition_player(self):
        # Clean up existing NPC(s)
        for npc in self.currLevelMap.npcs:
            npc.alive = False
        self.player.changeDirection(Direction.UP)
        self.player.clearMoveStack()
        player_waypoint = self.currLevelMap.waypointMap["player"]
        self.player.jump(player_waypoint.getTileX(), player_waypoint.getTileY())
        actualX = self.currLevelMap.waypointMap["man2"].getTileX()
        actualY = self.currLevelMap.waypointMap["man2"].getTileY()+1
        self.bad_man = Tortured_BadMan(actualX, actualY, self.currLevelMap.batches, self.currLevelMap.batchGroups)
        self.bad_man.initialize()
        self.currLevelMap.npcs.add(self.bad_man)
        self.torture_sequence_active = True

    def reposition_player_final(self):
        self.player.changeDirection(Direction.DOWN)
        self.player.clearMoveStack()
        player_waypoint = self.currLevelMap.waypointMap["man1"]
        self.player.jump(player_waypoint.getTileX(), player_waypoint.getTileY())
        self.torture_sequence_active = False

    def update(self, dt):
        if self.fading_out:
            self.fade_timer += dt
            if self.fade_timer >= self.fade_time_seconds:
                self.fading_out = False
                self.fade_timer = 0
                self.reposition_player()
                self.load_script("BrighteningScript")
                self.fading_in = True
                SoundManager.play_sfx("punch")
                self.open_dialog("sidequest_dv_restrained.dlg")
        elif self.fading_in:
            self.fade_timer += dt
            if self.fade_timer >= self.fade_time_seconds:
                self.fading_in = False
                self.fade_timer = 0
                self.torture_sequence_active = True
        elif self.fading_out2:
            self.fade_timer += dt
            if self.fade_timer >= self.fade_time_seconds:
                self.fading_out2 = False
                self.fade_timer = 0
                self.reposition_player_final()
                self.load_script("BrighteningScript")
                self.fading_in2 = True
        elif self.fading_in2:
            self.fade_timer += dt
            if self.fade_timer >= self.fade_time_seconds:
                self.fading_in2 = False
                self.unlock_doors()
        if self.torture_sequence_active:
            if self.bad_man is not None:
                if self.bad_man.stamina <= 10 or self.bad_man.health <= 0:
                    if self.bad_man.health <= 0:
                        qd = self.get_quest_data("domestic_violence")
                        qd["killed_abuser"] = True
                    if not self.fading_out2:
                        self.fading_out2 = True
                        self.complete_objective("domestic_violence", "rough_up_barkeeps_bf")
                        self.load_script("DarkeningScript")
                        self.open_dialog("sidequest_dv_intimidated.dlg")

    def destroy(self):
        self.event_manager.remove_handler("dialog_finished", self.dialog_finished)
        self.unlock_doors()

class SidequestDVRewardScript(OneTimeScript):
    def on_load(self):
        print("Loaded SidequestDVRewardScript!")
        self.event_manager.set_handler("dialog_finished", self.dialog_finished)
        self.player.changeDirection(Direction.DOWN)
        self.player.clearMoveStack()
        qd = self.get_quest_data("domestic_violence")
        if qd["killed_abuser"] == True:
            self.open_dialog("sidequest_dv_end_killed_abuser.dlg")
        else:
            self.open_dialog("sidequest_dv_end.dlg")
    
    def dialog_finished(self, dialog_name):
        print("Dialog " + dialog_name + " has finished")
        if dialog_name == "sidequest_dv_end.dlg":
            # Only receive reward if abuser was not killed
            self.give_treasure(2000)
        self.start_quest("main")

    def destroy(self):
        self.event_manager.remove_handler("dialog_finished", self.dialog_finished)

class SidequestOldFlameStartScript(OneTimeScript):
    def on_load(self):
        print("Loaded SidequestOldFlameStartScript!")
        self.event_manager.set_handler("dialog_finished", self.dialog_finished)
        self.open_dialog("sidequest_of_intro.dlg")

    def dialog_finished(self, dialog_name):
        print("Dialog " + dialog_name + " has finished")
        if dialog_name == "sidequest_of_intro.dlg":
            self.start_quest("old_flame", False)

    def destroy(self):
        self.event_manager.remove_handler("dialog_finished", self.dialog_finished)

class SidequestOldFlameMatildaScript(OneTimeScript):
    def on_load(self):
        print("Loaded SidequestOldFlameMatildaScript!")
        self.event_manager.set_handler("dialog_finished", self.dialog_finished)
        quest = self.get_quest("old_flame")
        if quest is not None and quest.active:
            self.open_dialog("sidequest_of_matilda.dlg")
        else:
            self.open_dialog("sidequest_of_matilda_alt.dlg")

    def dialog_finished(self, dialog_name):
        print("Dialog " + dialog_name + " has finished")
        if dialog_name == "sidequest_of_matilda.dlg":
            self.complete_objective("old_flame", "deliver_message")

    def destroy(self):
        self.event_manager.remove_handler("dialog_finished", self.dialog_finished)

class SidequestOldFlameEndScript(OneTimeScript):
    def on_load(self):
        print("Loaded SidequestOldFlameEndScript!")
        self.event_manager.set_handler("dialog_finished", self.dialog_finished)
        self.open_dialog("sidequest_of_end.dlg")

    def dialog_finished(self, dialog_name):
        print("Dialog " + dialog_name + " has finished")
        if dialog_name == "sidequest_of_end.dlg":
            self.complete_objective("old_flame", "return_to_vicente")
            self.give_treasure(10000)
            self.start_quest("main")

    def destroy(self):
        self.event_manager.remove_handler("dialog_finished", self.dialog_finished)

class SidequestOldFlame2StartScript(OneTimeScript):
    def on_load(self):
        print("Loaded SidequestOldFlame2StartScript!")
        self.event_manager.set_handler("dialog_finished", self.dialog_finished)
        self.open_dialog("sidequest_of2_intro.dlg")

    def dialog_finished(self, dialog_name):
        print("Dialog " + dialog_name + " has finished")
        if dialog_name == "sidequest_of2_intro.dlg":
            self.start_quest("old_flame_2", False)

    def destroy(self):
        self.event_manager.remove_handler("dialog_finished", self.dialog_finished)

class SidequestOldFlame2PetraScript(OneTimeScript):
    def on_load(self):
        print("Loaded SidequestOldFlame2PetraScript!")
        self.event_manager.set_handler("dialog_finished", self.dialog_finished)
        self.petra = None
        for npc in self.currLevelMap.npcs:
            # If another woman is added to this map, this will break
            if isinstance(npc, Woman):
                self.petra = npc
                break
        if self.petra is not None:
            if not self.petra.alert:
                quest = self.get_quest("old_flame_2")
                if quest is not None and quest.active:
                    self.open_dialog("sidequest_of2_petra.dlg")
            else:
                self.open_dialog("sidequest_of2_petra_alt.dlg")

    def dialog_finished(self, dialog_name):
        print("Dialog " + dialog_name + " has finished")
        if dialog_name == "sidequest_of2_petra.dlg":
            self.complete_objective("old_flame_2", "deliver_message")
        elif dialog_name == "sidequest_of2_petra_alt.dlg":
            if self.petra is not None:
                self.petra.script_triggered = False
                self.petra.createDialogMarker()

    def destroy(self):
        self.event_manager.remove_handler("dialog_finished", self.dialog_finished)

class SidequestOldFlame2EndScript(OneTimeScript):
    def on_load(self):
        print("Loaded SidequestOldFlame2EndScript!")
        self.event_manager.set_handler("dialog_finished", self.dialog_finished)
        self.open_dialog("sidequest_of2_end.dlg")

    def dialog_finished(self, dialog_name):
        print("Dialog " + dialog_name + " has finished")
        if dialog_name == "sidequest_of2_end.dlg":
            self.complete_objective("old_flame_2", "return_to_lady_inalco")
            self.give_treasure(15000)
            self.start_quest("main")

    def destroy(self):
        self.event_manager.remove_handler("dialog_finished", self.dialog_finished)

class SidequestConcernedParentsStartScript(OneTimeScript):
    def on_load(self):
        print("Loaded SidequestConcernedParentsStartScript!")
        self.event_manager.set_handler("dialog_finished", self.dialog_finished)
        self.open_dialog("sidequest_p_intro.dlg")

    def dialog_finished(self, dialog_name):
        print("Dialog " + dialog_name + " has finished")
        if dialog_name == "sidequest_p_intro.dlg":
            self.start_quest("concerned_parents", False)

    def destroy(self):
        self.event_manager.remove_handler("dialog_finished", self.dialog_finished)

class SidequestConcernedParentsFindVioletaScript(OneTimeScript):
    def on_load(self):
        print("Loaded SidequestConcernedParentsFindVioletaScript!")
        self.event_manager.set_handler("dialog_finished", self.dialog_finished)
        self.open_dialog("sidequest_p_violeta.dlg")

    def dialog_finished(self, dialog_name):
        print("Dialog " + dialog_name + " has finished")
        if dialog_name == "sidequest_p_violeta.dlg":
            self.complete_objective("concerned_parents", "find_violeta")

    def destroy(self):
        self.event_manager.remove_handler("dialog_finished", self.dialog_finished)

class SidequestConcernedParentsFindCristianScript(PersistentScript):
    def on_load(self):
        print("Loaded SidequestConcernedParentsFindCristianScript!")
        self.event_manager.set_handler("dialog_finished", self.dialog_finished)
        self.guards = []
        self.guards_spawned = False
        self.fading_out = False
        self.fade_timer = 0
        self.fade_time_seconds = 2
        self.door = None
        for door in self.currLevelMap.doors:
            if door.id == "one":
                self.door = door
                break
        if self.door is not None:
            self.door.locked = True
        self.open_dialog("sidequest_p_cristian.dlg")

    def spawn_guards(self):
        wpm = self.currLevelMap.waypointMap
        actualX = wpm['g1a'].getTileX()
        actualY = wpm['g1a'].getTileY()+1
        waypoints = [
            wpm['g1a'],
            wpm['g1b'],
            wpm['g1c'],
        ]
        guard = Guard(actualX, actualY, waypoints, 20, self.currLevelMap.batches, self.currLevelMap.batchGroups, color=(128,128,64))
        guard.initialize()
        self.currLevelMap.npcs.add(guard)
        self.guards.append(guard)
        actualX = wpm['g2a'].getTileX()
        actualY = wpm['g2a'].getTileY()+1
        waypoints = [
            wpm['g2a'],
            wpm['g2b'],
            wpm['g2c'],
            wpm['g1a'],
            wpm['g1b'],
            wpm['g2d'],
        ]
        guard = Guard(actualX, actualY, waypoints, 20, self.currLevelMap.batches, self.currLevelMap.batchGroups, color=(128,128,64))
        guard.initialize()
        self.currLevelMap.npcs.add(guard)
        self.guards.append(guard)
        actualX = wpm['g3a'].getTileX()
        actualY = wpm['g3a'].getTileY()+1
        waypoints = [
            wpm['g3a'],
            wpm['g3b'],
            wpm['g3c'],
            wpm['g2b'],
            wpm['g2c'],
            wpm['g1a'],
            wpm['g1b'],
            wpm['g3d'],
        ]
        guard = Guard(actualX, actualY, waypoints, 20, self.currLevelMap.batches, self.currLevelMap.batchGroups, color=(128,128,64))
        guard.initialize()
        self.currLevelMap.npcs.add(guard)
        self.guards.append(guard)
        self.guards_spawned = True

    def load_script(self, script_name, **script_args):
        from it5.script import ScriptManager
        ScriptManager.load(script_name, **script_args)

    def guards_killed(self):
        self.complete_objective("concerned_parents", "kill_guards")
        self.guards_spawned = False
        self.load_script("DarkeningScript", curr_rgb=self.currLevelMap.ambience, time_to_relight=self.fade_time_seconds)
        self.fading_out = True

    def warp_to_forest(self):
        self.place_warp(self.player.getTileX(), self.player.getTileY(), 55, 24, "forest")

    def update(self, dt):
        if self.guards_spawned:
            guards_alive_count = 0
            for guard in self.guards:
                if guard.alive:
                    guards_alive_count += 1
            if guards_alive_count == 0:
                self.guards_killed()
        elif self.fading_out:
            self.fade_timer += dt
            if self.fade_timer >= self.fade_time_seconds:
                self.fading_out = False
                self.warp_to_forest()

    def dialog_finished(self, dialog_name):
        print("Dialog " + dialog_name + " has finished")
        if dialog_name == "sidequest_p_cristian.dlg":
            self.complete_objective("concerned_parents", "find_cristian")
            self.spawn_guards()

    def destroy(self):
        self.event_manager.remove_handler("dialog_finished", self.dialog_finished)
        if self.door is not None:
            self.door.locked = False

class SidequestConcernedParentsExitCaveScript(PersistentScript):
    def on_load(self):
        print("Loaded SidequestConcernedParentsExitCaveScript!")
        self.event_manager.set_handler("dialog_finished", self.dialog_finished)
        self.fading_out = False
        self.fade_timer = 0
        self.fade_time_seconds = 2
        self.ambience = self.currLevelMap.ambience
        self.player.changeDirection(Direction.RIGHT)
        self.player.clearMoveStack()
        self.spawn_cristian()
        self.open_dialog("sidequest_p_cristian2.dlg")

    def spawn_cristian(self):
        wpm = self.currLevelMap.waypointMap
        actualX = wpm['cristian'].getTileX()
        actualY = wpm['cristian'].getTileY()+1
        waypoints = [
            wpm['cristian'],
        ]
        hair_color = (43, 27, 2)
        pants_color = (0, 130, 181)
        shirt_color = (92, 114, 122)
        self.cristian = Man(actualX, actualY, waypoints, self.currLevelMap.batches, self.currLevelMap.batchGroups, hair_color=hair_color, shirt_color=shirt_color, pants_color=pants_color)
        self.cristian.initialize()
        self.currLevelMap.npcs.add(self.cristian)
        self.cristian.changeDirection(Direction.LEFT)

    def load_script(self, script_name, **script_args):
        from it5.script import ScriptManager
        ScriptManager.load(script_name, **script_args)

    def update(self, dt):
        if self.fading_out:
            self.fade_timer += dt
            if self.fade_timer >= self.fade_time_seconds:
                self.fading_out = False
                self.cristian.alive = False
                self.cristian = None
                self.load_script("BrighteningScript", original_rgb=self.ambience, time_to_relight=self.fade_time_seconds)

    def dialog_finished(self, dialog_name):
        print("Dialog " + dialog_name + " has finished")
        if dialog_name == "sidequest_p_cristian2.dlg":
            self.fading_out = True
            self.load_script("DarkeningScript", curr_rgb=self.ambience, time_to_relight=self.fade_time_seconds)

    def destroy(self):
        self.event_manager.remove_handler("dialog_finished", self.dialog_finished)

class SidequestConcernedParentsEndScript(OneTimeScript):
    def on_load(self):
        print("Loaded SidequestConcernedParentsEndScript!")
        self.event_manager.set_handler("dialog_finished", self.dialog_finished)
        self.open_dialog("sidequest_p_end.dlg")

    def dialog_finished(self, dialog_name):
        print("Dialog " + dialog_name + " has finished")
        if dialog_name == "sidequest_p_end.dlg":
            self.complete_objective("concerned_parents", "return_to_mr_nunez")
            self.give_treasure(3000)
            self.start_quest("main")

    def destroy(self):
        self.event_manager.remove_handler("dialog_finished", self.dialog_finished)

class SidequestCaveMonsterStartScript(OneTimeScript):
    def on_load(self):
        print("Loaded SidequestCaveMonsterStartScript!")
        self.event_manager.set_handler("dialog_finished", self.dialog_finished)
        self.open_dialog("sidequest_cm_intro.dlg")

    def dialog_finished(self, dialog_name):
        print("Dialog " + dialog_name + " has finished")
        if dialog_name == "sidequest_cm_intro.dlg":
            self.start_quest("cave_monster", False)

    def destroy(self):
        self.event_manager.remove_handler("dialog_finished", self.dialog_finished)

class SidequestCaveMonsterBattleScript(OneTimeScript):
    def on_load(self):
        print("Loaded SidequestCaveMonsterBattleScript!")
        self.event_manager.set_handler("dialog_finished", self.dialog_finished)
        self.open_dialog("sidequest_cm_battle.dlg")

    def spawn_relict(self):
        actualX = self.currLevelMap.waypointMap["relict"].getTileX()
        actualY = self.currLevelMap.waypointMap["relict"].getTileY()+1
        waypoints = [self.currLevelMap.waypointMap["relict"]]
        boss = Relict(actualX, actualY, waypoints, self.currLevelMap.batches, self.currLevelMap.batchGroups, stun_time_wait=1.5)
        boss.initialize()
        self.currLevelMap.npcs.add(boss)
        SoundManager.play_sfx("monster_growl")

    def dialog_finished(self, dialog_name):
        print("Dialog " + dialog_name + " has finished")
        if dialog_name == "sidequest_cm_battle.dlg":
            self.spawn_relict()

    def destroy(self):
        self.event_manager.remove_handler("dialog_finished", self.dialog_finished)

class SidequestCaveMonsterEndScript(OneTimeScript):
    def on_load(self):
        print("Loaded SidequestCaveMonsterStartScript!")
        self.event_manager.set_handler("dialog_finished", self.dialog_finished)
        self.open_dialog("sidequest_cm_end.dlg")

    def dialog_finished(self, dialog_name):
        print("Dialog " + dialog_name + " has finished")
        if dialog_name == "sidequest_cm_end.dlg":
            self.complete_objective("cave_monster", "return_to_cristian")
            # Remove Colonia Evidence from player's inventory
            self.player.inventory.items.pop("Colonia Evidence", None)
            self.give_treasure(5000)
            self.start_quest("main")

    def destroy(self):
        self.event_manager.remove_handler("dialog_finished", self.dialog_finished)

class SidequestTreasureHunterStartScript(OneTimeScript):
    def on_load(self):
        print("Loaded SidequestTreasureHunterStartScript!")
        self.event_manager.set_handler("dialog_finished", self.dialog_finished)
        self.open_dialog("sidequest_th_intro.dlg")

    def dialog_finished(self, dialog_name):
        print("Dialog " + dialog_name + " has finished")
        if dialog_name == "sidequest_th_intro.dlg":
            self.start_quest("treasure_hunter", False)

    def destroy(self):
        self.event_manager.remove_handler("dialog_finished", self.dialog_finished)

class SidequestTreasureHunterEggScript(OneTimeScript):
    def on_load(self):
        print("Loaded SidequestTreasureHunterEggScript!")
        self.event_manager.set_handler("dialog_finished", self.dialog_finished)
        self.karl = None
        for npc in self.currLevelMap.npcs:
            if npc.dialog == "sidequest_th_hint.dlg":
                self.karl = npc
                break
        self.mapuche_eggs = set(["bronze egg", "silver egg", "gold egg"])
        player_items = set(self.player.inventory.items.keys())
        result = self.mapuche_eggs.intersection(player_items)
        if len(result) == len(self.mapuche_eggs):
            # Player has found all Mapuche Eggs
            self.complete_objective("treasure_hunter", "find_eggs")
            self.open_dialog("sidequest_th_end.dlg")
        else:
            self.open_dialog("sidequest_th_hint.dlg")

    def dialog_finished(self, dialog_name):
        print("Dialog " + dialog_name + " has finished")
        if dialog_name == "sidequest_th_end.dlg":
            # Remove Mapuche Eggs from player's inventory
            for item in self.mapuche_eggs:
                self.player.inventory.items.pop(item, None)
            self.give_treasure(20000)
            self.start_quest("main")
            self.karl.dialog = None
            self.karl.hideDialogMarker()

    def destroy(self):
        self.event_manager.remove_handler("dialog_finished", self.dialog_finished)
