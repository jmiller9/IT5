from it5.script import OneTimeScript
from it5.item import Item
from it5.direction import Direction
from it5.gui.manager import UIManager

class StoreController:
    first_time = False

class StoreScript(OneTimeScript):
    def on_load(self):
        print("Loaded StoreScript!")
        self.event_manager.set_handler('dialog_finished', self.dialog_finished)
        self.gunsmith = None
        for npc in self.currLevelMap.npcs:
            if npc.script is not None:
                self.gunsmith = npc
                break
        self.player.changeDirection(Direction.UP)
        self.player.clearMoveStack()
        if StoreController.first_time:
            # First time going in to the store is special
            return
        self.dialog_color = (63,63,63,255)
        handgun = Item(0, 0, None, None, None, "weapon", {"rank": 3, "type": "pistol"})
        carbine = Item(0, 0, None, None, None, "weapon", {"rank": 3, "type": "carbine"})
        smg = Item(0, 0, None, None, None, "weapon", {"rank": 3, "type": "smg"})
        rocket_launcher = Item(0, 0, None, None, None, "weapon", {"type": "rocket_launcher"})
        suppressor = Item(0, 0, None, None, None, "item", {"type": "suppressor", "weapon_type": "pistol"})
        smg_suppressor = Item(0, 0, None, None, None, "item", {"type": "suppressor", "weapon_type": "smg"})
        grenade = Item(0, 0, None, None, None, "weapon", {"rank": 0, "type": "grenade"})
        mine = Item(0, 0, None, None, None, "weapon", {"rank": 0, "type": "mine"})
        nvgs = Item(0, 0, None, None, None, "item", {"rank": 0, "type": "nvg"})
        gasmask = Item(0, 0, None, None, None, "item", {"rank": 0, "type": "gasmask"})
        handgun_ammo = Item(0, 0, None, None, None, "ammo", {"count": 8, "type": "pistol"})
        smg_ammo = Item(0, 0, None, None, None, "ammo", {"count": 34, "type": "smg"})
        carbine_ammo = Item(0, 0, None, None, None, "ammo", {"count": 30, "type": "carbine"})
        rl_ammo = Item(0, 0, None, None, None, "ammo", {"count": 4, "type": "rocket_launcher"})
        self._inventory = {
            "Pistol R3": (handgun, 10000),
            "SMG R3": (smg, 20000),
            "Carbine R3": (carbine, 30000),
            "Rocket Launcher": (rocket_launcher, 40000),
            "Pistol Suppressor": (suppressor, 5000),
            "SMG Suppressor": (smg_suppressor, 10000),
            "Night Vision Goggles": (nvgs, 7500),
            "Gas Mask": (gasmask, 1500),
            "Pistol Ammo x8": (handgun_ammo, 100),
            "SMG Ammo x34": (smg_ammo, 250),
            "Carbine Ammo x30": (carbine_ammo, 400),
            "Grenade x2": (grenade, 350),
            "Rockets x4": (rl_ammo, 1000),
            "Smart Landmine": (mine, 500),
        }
        self.set_item_to_treasure()
        self.open_dialog('shopkeeper.dlg')

    def set_item_to_treasure(self):
        if "treasure" not in self.player.inventory.items:
            self.player.inventory.items["treasure"] = 0
        while self.player.getItem() != "treasure":
            self.player.changeItem(1)
        self.player.changeItem(0)

    def prompt_purchase(self, item_name):
        print("Attempting to purchase %s" % (str(item_name)))
        item, price = self._inventory.get(item_name, (None, 0))
        def attempt_purchase(selection):
            UIManager.hide_active()
            width = int(self.anchor_x / 2)
            height = int(self.window.height / 4)
            x = self.anchor_x / 2
            y = self.window.height / 2
            treasure = self.player.inventory.items.get("treasure", 0)
            if treasure >= price:
                if self.player.inventory.addTo(item):
                    print("Purchasing %s for %d treasure, player has %d treasure to spend" % (str(item_name), price, treasure))
                    self.player.inventory.items["treasure"] = treasure - price
                    self.player.changeWeapon(0)
                    self.player.changeItem(0)
                    title = "Purchased %s" % (str(item_name))
                    text = "Thank you for your purchase. Is there anything else you need?"
                    UIManager.show_popup(title, text, width=width, height=height, x=x, y=y,
                    anchor_x="center", anchor_y="center", background_color=self.dialog_color, title_font_size=12,
                    exit_option="Continue Browsing")
                else:
                    print("Cannot carry any more %s" % (item_name))
                    text = "Cannot carry additional %s; item not purchased." % (str(item_name))
                    UIManager.show_popup("Capacity Full", text, width=width, height=height, x=x, y=y,
                    anchor_x="center", anchor_y="center", background_color=self.dialog_color)
            else:
                print("Player does not have enough treasure")
                text = "You do not have enough treasure to purchase %s." % (str(item_name))
                UIManager.show_popup("Insufficeint Funds", text, width=width, height=height, x=x, y=y,
                anchor_x="center", anchor_y="center", background_color=self.dialog_color)
        options = [
            ("Purchase", attempt_purchase)
        ]
        width = int(self.anchor_x / 3)
        height = int(self.window.height / 4)
        x = self.anchor_x
        y = self.window.height
        title = "Price: %d treasure" % (price)
        UIManager.show_menu(title, options, x=x, y=y, width=width, height=height, anchor_x="right", anchor_y="top", title_font_size=12)

    def get_inventory(self):
        menu_options = []
        for item in self._inventory:
            menu_options.append((item, self.prompt_purchase))
        return menu_options

    def dialog_finished(self, dialog_name):
        print("Dialog %s has finished" % (dialog_name))
        if dialog_name == 'shopkeeper.dlg':
            width = int(self.anchor_x/2)
            height = int(self.window.height)
            x = 0
            y = self.window.height
            '''I know the term "anchor_x" is overloaded, but self.anchor_x is really the x value of where the HUD begins...
            anchor_x in the widget context is either "left", "right", or "center".'''
            UIManager.show_menu("Select an Item to Purchase", self.get_inventory(), width=width, height=height, x=x, y=y, anchor_y="top")
        if self.gunsmith is not None:
            self.gunsmith.script_triggered = False
            self.gunsmith.createDialogMarker()

    def destroy(self):
        self.event_manager.remove_handler('dialog_finished', self.dialog_finished)

class FirstTimeInStoreScript(OneTimeScript):
    def on_load(self):
        print("Loaded FirstTimeInStoreScript!")
        StoreController.first_time = True
        self.event_manager.set_handler('dialog_finished', self.dialog_finished)
        self.player.changeDirection(Direction.UP)
        self.player.clearMoveStack()
        gunsmith = list(self.currLevelMap.npcs)[0]
        wx = self.currLevelMap.waypointMap['w1'].getTileX()
        wy = self.currLevelMap.waypointMap['w1'].getTileY()
        gunsmith.jump(wx, wy)
        gunsmith.script_triggered = True
        gunsmith.hideDialogMarker()
        self.open_dialog('shopkeeper_intro.dlg')

    def dialog_finished(self, dialog_name):
        print("Dialog %s has finished" % (dialog_name))
        if dialog_name == 'shopkeeper_intro.dlg':
            pistol = Item(0, 0, None, None, None, 'weapon', {'rank':0, 'type':'pistol'})
            self.player.inventory.addTo(pistol)
            ammo = Item(0, 0, None, None, None, 'ammo', {'count':8, 'type':'pistol'})
            self.player.inventory.addTo(ammo)
            self.player.changeWeapon(1)

    def destroy(self):
        StoreController.first_time = False
        self.event_manager.remove_handler('dialog_finished', self.dialog_finished)