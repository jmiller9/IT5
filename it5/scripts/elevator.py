from it5.script import OneTimeScript
from it5.script import CancelScriptLoadException
from it5.direction import Direction
from it5.audio import SoundManager
from it5.gui.manager import UIManager
from it5.animation import AnimationGroupManager, Animation, AnimationGroup
from it5.dynamicobject import DynamicObject

# These are chosen when warping to an elevator
# Take care to ALWAYS warp to (12,10) in the elevator map, or wherever the ScriptTrigger that loads the ElevatorScript is located
warp_contexts = {
    "ch1_lobby" : {
        "floors": [
            {
                "name": "1",
                "map": "elevator_exterior1",
                "coords": [(7, 11), (8, 11)]
            },
            {
                "name": "2",
                "map": "elevator_exterior2",
                "coords": [(7, 11), (8, 11)]
            }
        ],
        "start": 0,
    },
    "ch1_lab": {
        "floors": [
            {
                "name": "1",
                "map": "elevator_exterior1",
                "coords": [(7, 11), (8, 11)]
            },
            {
                "name": "2",
                "map": "elevator_exterior2",
                "coords": [(7, 11), (8, 11)]
            }
        ],
        "start": 1,
    },
    "lab_floor1north": {
        "floors": [
            {
                "name": "1",
                "map": "lab_floor1north",
                "coords": [(8, 13), (9, 13)]
            },
            {
                "name": "2",
                "map": "lab_floor2north",
                "coords": [(8, 13), (9, 13)]
            }
        ],
        "start": 0,
    },
    "lab_floor2north": {
        "floors": [
            {
                "name": "1",
                "map": "lab_floor1north",
                "coords": [(8, 13), (9, 13)]
            },
            {
                "name": "2",
                "map": "lab_floor2north",
                "coords": [(8, 13), (9, 13)]
            }
        ],
        "start": 1,
    },
    "lab_floor2west": {
        "floors": [
            {
                "name": "2",
                "map": "lab_floor2west",
                "coords": [(6, 28), (7, 28)]
            },
            {
                "name": "3",
                "map": "lab_floor3west",
                "coords": [(6, 28), (7, 28)]
            },
            {
                "name": "4",
                "map": "lab_floor4west",
                "coords": [(6, 28), (7, 28)]
            }
        ],
        "start": 0,
    },
    "lab_floor3west": {
        "floors": [
            {
                "name": "2",
                "map": "lab_floor2west",
                "coords": [(6, 28), (7, 28)]
            },
            {
                "name": "3",
                "map": "lab_floor3west",
                "coords": [(6, 28), (7, 28)]
            },
            {
                "name": "4",
                "map": "lab_floor4west",
                "coords": [(6, 28), (7, 28)]
            }
        ],
        "start": 1,
    },
    "lab_floor4west": {
        "floors": [
            {
                "name": "2",
                "map": "lab_floor2west",
                "coords": [(6, 28), (7, 28)]
            },
            {
                "name": "3",
                "map": "lab_floor3west",
                "coords": [(6, 28), (7, 28)]
            },
            {
                "name": "4",
                "map": "lab_floor4west",
                "coords": [(6, 28), (7, 28)]
            }
        ],
        "start": 2,
    },
    "lab_floor3north": {
        "floors": [
            {
                "name": "3",
                "map": "lab_floor3north",
                "coords": [(8, 13), (9, 13)]
            },
            {
                "name": "4",
                "map": "lab_floor4north",
                "coords": [(8, 13), (9, 13)]
            }
        ],
        "start": 0,
    },
    "lab_floor4north": {
        "floors": [
            {
                "name": "3",
                "map": "lab_floor3north",
                "coords": [(8, 13), (9, 13)]
            },
            {
                "name": "4",
                "map": "lab_floor4north",
                "coords": [(8, 13), (9, 13)]
            }
        ],
        "start": 1,
    },
    "base_west_c": {
        "floors": [
            {
                "name": "1",
                "map": "base_west_c",
                "coords": [(5, 45), (5, 45)]
            },
            {
                "name": "2",
                "map": "base_west_d",
                "coords": [(5, 45), (5, 45)]
            }
        ],
        "start": 0,
    },
    "base_west_d": {
        "floors": [
            {
                "name": "1",
                "map": "base_west_c",
                "coords": [(5, 45), (5, 45)]
            },
            {
                "name": "2",
                "map": "base_west_d",
                "coords": [(5, 45), (5, 45)]
            }
        ],
        "start": 1,
    },
    "base_east_1f": {
        "floors": [
            {
                "name": "1",
                "map": "base_east_1f",
                "coords": [(13, 14), (14, 14)]
            },
            {
                "name": "2",
                "map": "base_east_2f",
                "coords": [(13, 14), (14, 14)]
            },
            {
                "name": "3",
                "map": "base_east_3f",
                "coords": [(13, 14), (14, 14)]
            }
        ],
        "start": 0,
    },
    "base_east_2f": {
        "floors": [
            {
                "name": "1",
                "map": "base_east_1f",
                "coords": [(13, 14), (14, 14)]
            },
            {
                "name": "2",
                "map": "base_east_2f",
                "coords": [(13, 14), (14, 14)]
            },
            {
                "name": "3",
                "map": "base_east_3f",
                "coords": [(13, 14), (14, 14)]
            }
        ],
        "start": 1,
    },
    "base_east_3f": {
        "floors": [
            {
                "name": "1",
                "map": "base_east_1f",
                "coords": [(13, 14), (14, 14)]
            },
            {
                "name": "2",
                "map": "base_east_2f",
                "coords": [(13, 14), (14, 14)]
            },
            {
                "name": "3",
                "map": "base_east_3f",
                "coords": [(13, 14), (14, 14)]
            }
        ],
        "start": 2,
    },
    "base_east_1f_ne": {
        "floors": [
            {
                "name": "1",
                "map": "base_east_1f_ne",
                "coords": [(11, 14), (12, 14)]
            },
            {
                "name": "2",
                "map": "base_east_2f_ne",
                "coords": [(11, 14), (12, 14)]
            }
        ],
        "start": 0,
    },
    "base_east_2f_ne": {
        "floors": [
            {
                "name": "1",
                "map": "base_east_1f_ne",
                "coords": [(11, 14), (12, 14)]
            },
            {
                "name": "2",
                "map": "base_east_2f_ne",
                "coords": [(11, 14), (12, 14)]
            }
        ],
        "start": 1,
    },
}

class ElevatorController:
    in_use = False
    floors = []
    curr_floor = 0
    trigger_sem = False

    @classmethod
    def completed(cls):
        cls.in_use = False

    @classmethod
    def started(cls):
        cls.in_use = True

    @classmethod
    def busy(cls):
        return cls.in_use

    @classmethod
    def exit_elevator(cls):
        cls.trigger_sem = False
        cls.completed()

    @classmethod
    def load_warp_context(cls, warp_context):
        if cls.trigger_sem == False:
            cls.floors = warp_context.get("floors", [])
            cls.curr_floor = warp_context.get("start", 0)
        cls.trigger_sem = True

class ElevatorScript(OneTimeScript):
    def __init__(self):
        super().__init__()
        self.warp_context = {}

    def get_warp_context(self):
        elevator_context_ref = self.currLevelMap.metadata.get("elevator")
        self.warp_context = warp_contexts.get(elevator_context_ref, {})

    def on_load(self):
        if ElevatorController.busy():
            raise CancelScriptLoadException("Elevator is Busy")
        print("Loaded ElevatorScript!")
        self.get_warp_context()
        ElevatorController.load_warp_context(self.warp_context)
        AnimationGroupManager.clear()
        self.show_idle_background()
        self.player.changeDirection(Direction.UP)
        self.player.clearMoveStack()
        self.moving = False
        self.prompt_floor_change()

    def show_idle_background(self):
        for anim_object in self.currLevelMap.animation_objects:
            anim_object.updateSprite(anim_object.imagerow, anim_object.imagecol)
            anim_object.setVisible(True)

    def hide_idle_background(self):
        for anim_object in self.currLevelMap.animation_objects:
            anim_object.setVisible(False)

    def lock_doors(self):
        for door in self.currLevelMap.doors:
            door.locked = True

    def unlock_doors(self):
        for door in self.currLevelMap.doors:
            door.locked = False

    def done_moving(self):
        self.moving = False
        self.show_idle_background()
        ElevatorController.completed()
        SoundManager.stop_music()
        self.unlock_doors()

    def get_start_y(self, i, original_y):
        if i == 0:
            return original_y + 9.5
        elif i == 1:
            return original_y
        else:
            return original_y - 9.5

    def start_moving(self, dy):
        SoundManager.play_music("elevator")
        self.lock_doors()
        # Create the animation
        batch = self.currLevelMap.batches["main"]
        batchGroups = self.currLevelMap.batchGroups
        animations = []
        for i in range(0, 3):
            for original_animation_object in self.currLevelMap.animation_objects:
                x = original_animation_object.x / 32
                y = self.get_start_y(i, original_animation_object.y / 32)
                imagegrid = original_animation_object.imagegrid
                imagerow = original_animation_object.imagerow
                imagecol = original_animation_object.imagecol
                _batch = original_animation_object.batch
                _group = original_animation_object.group
                anim_obj = DynamicObject(x, y, imagegrid, imagerow, imagecol, _batch, _group)
                frames = [(imagerow, imagecol)]
                velocity = 60 * dy
                anim = Animation(anim_obj, frames, 1, lifetime_seconds=5, dx=0, dy=velocity)
                animations.append(anim)
        anim_group = AnimationGroup(*animations)
        anim_group.set_on_destroy(self.done_moving)
        AnimationGroupManager.add_animation_group(anim_group)
        self.hide_idle_background()
        self.moving = True
        ElevatorController.started()

    def set_warps(self, floor_index):
        name = None
        if floor_index < len(ElevatorController.floors):
            floor_dict = ElevatorController.floors[floor_index]
            name = floor_dict.get("name", "")
            warp_map = floor_dict.get("map")
            coords = floor_dict.get("coords")
            x0, y0 = coords[0]
            x1, y1 = coords[1]
            self.currLevelMap.warps[0].update_destination(x0, y0, warp_map)
            self.currLevelMap.warps[1].update_destination(x1, y1, warp_map)
        return name

    def update_warps(self, delta):
        ElevatorController.curr_floor += delta
        name = self.set_warps(ElevatorController.curr_floor)
        print("Moving to floor %s" % name)

    def get_options(self):
        def go_up(selection):
            UIManager.hide_active()
            self.start_moving(-1)
            self.update_warps(1)
        def go_down(selection):
            UIManager.hide_active()
            self.start_moving(1)
            self.update_warps(-1)
        options = []
        if len(ElevatorController.floors) > 1:
            if ElevatorController.curr_floor < len(ElevatorController.floors) - 1:
                options.append(("Up", go_up))
            if ElevatorController.curr_floor > 0:
                options.append(("Down", go_down))
        return options

    def prompt_floor_change(self):
        def exit_menu(*args):
            UIManager.hide_active()
            # Make sure we can leave the elevator
            self.set_warps(ElevatorController.curr_floor)
        options = self.get_options()
        width = int(self.anchor_x / 3)
        height = int(self.window.height / 4)
        x = 0
        y = self.window.height
        title = "Change Floor"
        if ElevatorController.curr_floor < len(ElevatorController.floors):
            floor_dict = ElevatorController.floors[ElevatorController.curr_floor]
            title += " [ %s ]" % floor_dict.get("name", "?")
        UIManager.show_menu(title, options, x=x, y=y, width=width, height=height, anchor_x="left", anchor_y="top", title_font_size=12, exit_callback=exit_menu)

    def destroy(self):
        self.hide_idle_background()
        ElevatorController.exit_elevator()
        self.unlock_doors()
