from it5.script import OneTimeScript

class InToSuitScript(OneTimeScript):
    def __init__(self):
        super().__init__()

    def on_load(self):
        print("Loaded InToSuitScript!")
        self.event_manager.player_change_clothes(True)

    def destroy(self):
        pass

class OutOfSuitScript(OneTimeScript):
    def __init__(self):
        super().__init__()

    def on_load(self):
        print("Loaded OutOfSuitScript!")
        self.event_manager.player_change_clothes(False)

    def destroy(self):
        pass
