from it5.quest import QuestManager
from it5.common import CommonManager
import json
import os
import pyglet

class PersistenceManager(object):
    filename = "it5savedata.json"
    controls_filename = "it5controls.json"

    @classmethod
    def to_json(cls, currLevelSet, currLevel, currLevelMap, player, recently_acquired_items):
        save_dict = {
            'levelset': currLevelSet.to_dict(),
            'level': currLevel.to_dict(recently_acquired_items, currLevelMap.index),
            'quests': QuestManager.to_dict(),
            'active_quest': QuestManager.last_active_quest,
            'player': player.to_dict(),
            'map_index': currLevelMap.index,
            'name': CommonManager.get_player_name()
        }
        return json.dumps(save_dict)

    @classmethod
    def save(cls, currLevelSet, currLevel, currLevelMap, player, recently_acquired_items):
        blob = cls.to_json(currLevelSet, currLevel, currLevelMap, player, recently_acquired_items)

        _dir = pyglet.resource.get_settings_path('IT5')
        if not os.path.exists(_dir):
            os.makedirs(_dir)

        with open(os.path.join(_dir, cls.filename), 'w') as f:
            f.write(blob)

    @classmethod
    def from_json(cls, blob, init_function, change_map_function):
        load_dict = json.loads(blob)
        levelset_dict = load_dict['levelset']
        x = load_dict['player']['x']
        y = load_dict['player']['y']
        level_index = load_dict['level']['index']
        CommonManager.set_player_name(load_dict['name'])
        currLevel, player = init_function(levelset_dict['location'])
        # We have to differ loading our quests, since LevelSet.loadLevel() clears them
        # This is needed for making sure NPCs with spawn_conditionals actually spawn when loading the game
        # on a map that contains said NPCs
        currLevel, currLevelMap = change_map_function(load_dict['map_index'], x, y, levelIndex=level_index, respawning=True, load_dict=load_dict)
        currLevel.from_dict(load_dict['level'])
        player.from_dict(load_dict['player'])

    @classmethod
    def load(cls, init_function, change_map_function):
        _dir = pyglet.resource.get_settings_path('IT5')
        if not os.path.exists(_dir):
            os.makedirs(_dir)
        blob = None
        savefile = os.path.join(_dir, cls.filename)
        try:
            with open(savefile, 'r') as f:
                blob = f.read()
        except FileNotFoundError as err:
            print(str(err))
        if blob is not None:
            cls.from_json(blob, init_function, change_map_function)
            return True
        else:
            print('Could not open file "' + savefile + '"')
        return False

    @classmethod
    def save_controls(cls, controls):
        blob = json.dumps(controls, default=lambda x: x.name)
        _dir = pyglet.resource.get_settings_path("IT5")
        if not os.path.exists(_dir):
            os.makedirs(_dir)
        with open(os.path.join(_dir, cls.controls_filename), "w") as f:
            f.write(blob)

    @classmethod
    def load_controls(cls):
        _dir = pyglet.resource.get_settings_path("IT5")
        if not os.path.exists(_dir):
            os.makedirs(_dir)
        blob = None
        controls_file = os.path.join(_dir, cls.controls_filename)
        try:
            with open(controls_file, "r") as f:
                blob = f.read()
        except FileNotFoundError as err:
            print(str(err))
        if blob is None:
            return None
        return json.loads(blob)
