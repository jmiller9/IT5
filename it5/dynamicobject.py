import pyglet

class DynamicObject(object):
    def __init__(self, x, y, imagegrid, imagerow, imagecol, batch, group, color=(255,255,255), opacity=255):
        self.x = x * 32
        self.y = y * 32
        self.imagegrid = imagegrid
        self.batch = batch
        self.group = group
        # [row, col] for starting grid /frame
        if imagegrid is not None:
            imagerow = min(imagerow, imagegrid.rows - 1)
            imagecol = min(imagecol, imagegrid.columns - 1)
        self.sprite = pyglet.sprite.Sprite(
            img=imagegrid[imagerow, imagecol],
            subpixel=True,
        )
        self.opacity = opacity
        self.color = color
        self.sprite.color = self.color
        self.imagerow = imagerow
        self.imagecol = imagecol
        self.move(0,0)

    def updateSprite(self, imagerow, imagecol):
        if self.sprite is not None:
            self.sprite.delete()
        self.sprite = pyglet.sprite.Sprite(
            img=self.imagegrid[imagerow, imagecol],
            batch=self.batch,
            group=self.group,
            subpixel=True,
        )
        self.sprite.color = self.color
        self.sprite.opacity = self.opacity
        self.imagerow = imagerow
        self.imagecol = imagecol
        self.move(0,0)

    def setColor(self, color):
        if self.sprite != None:
            self.sprite.color = color
        self.color = color

    def setAlpha(self, alpha):
        if self.sprite != None:
            if self.opacity > 0:
                self.sprite.opacity = alpha

    def setVisible(self, visible):
        if self.sprite != None:
            self.sprite.visible = visible

    def isVisible(self):
        if self.sprite is not None:
            return self.sprite.visible
        return False

    def move(self, dx, dy):
        self.x += dx
        self.y += dy
        if self.sprite is not None:
            self.sprite.position = (self.x, self.y)

    def jump(self, x, y):
        self.x = x*32
        self.y = (y*32)+31
        if self.sprite is not None:
            self.sprite.position = (self.x, self.y)

    def getTileX(self):
        return int((self.x+15) / 32)

    def getTileY(self):
        return int((self.y-31) / 32)

    def destroy(self):
        if self.sprite != None:
            self.sprite.delete()
            self.sprite = None