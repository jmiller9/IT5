from it5.bosses.boss import Boss
from it5 import spriteloader

ram_sprites = spriteloader.getSprites('ram.png', 8, 8)

class Boss1(Boss):
    def __init__(self, x, y, waypoints, batches, batchGroups):
        super(Boss1, self).__init__(x, y, waypoints, batches, batchGroups, ram_sprites,
                                    name="Ram", damage=15, health=320, move_speed=80, view_distance=768, health_bar_scale_constant=64.0)

    def copy(self):
        return Boss1(self.getTileX(), self.getTileY(), self.path.waypoints, self.batches, self.batchGroups)