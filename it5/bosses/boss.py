from pyglet.graphics import OrderedGroup
from pyglet.text import Label
from it5.waypoint import Path, WaypointBehavior
from it5.util import searchForPlayer, getHalfVisionConeHeight
from it5.direction import Direction
from it5.common import EventManager, common_tileset, common_spritemap, CommonManager
from it5.npc import NPC
from it5.staticobject import StaticObject

_bg_batch_group = OrderedGroup(2)
_fg_batch_group = OrderedGroup(3)

class Boss(NPC):
    def __init__(self, x, y, waypoints, batches, batchGroups, sprites,
                 name="Boss", damage=15, health=100, move_speed=80, view_distance=768, health_bar_scale_constant=64.0):
        super(Boss, self).__init__(x, y, sprites, False, waypoints, None, damage, batches, batchGroups)
        self.protected = True
        self.health = health
        self.maxHealth = health
        self.moveSpeed = move_speed
        self.viewDistance = view_distance
        self.name = name
        self.health_bar_scale_constant = health_bar_scale_constant
        self.radarViewDistance = self.viewDistance/4
        self.halfVisionConeHeight = getHalfVisionConeHeight(self.radarViewDistance)
        self.wait_for_new_path = True
        self.create_health_bar()

    def copy(self):
        raise NotImplementedError("copy() not implemented for Boss %s" % self.name)

    def getPunched(self, x1, x2, y1, y2, player=None):
        tx = self.getTileX()
        ty = self.getTileY()
        if tx >= x1 and tx <= x2 and ty >= y1 and ty <= y2:
            print("%s has been punched! It had no effect..." % self.name)
            self.receiveDamage(0)

    def on_alert(self):
        pass

    def cancel_alert(self):
        pass

    def reachedWaypoint(self, currLevelMap, player):
        super(Boss, self).reachedWaypoint(currLevelMap, player)
        if self.path.index >= len(self.path.waypoints) - 2:
            if self.awaiting_path is None:
                self.wait_for_new_path = True

    def update(self, dt, currLevelMap, player, bulletManager):
        # Important to call NPC's superclass update() method
        super(NPC, self).update(dt, currLevelMap.obstacleMatrix, currLevelMap.doors)
        if self.alive:
            if not self.tempStunned:
                if self.shotTimer > 0:
                    self.shotTimer -= dt
            if self.waitTimer > 0:
                self.waitTimer -= dt
                if self.waitTimer <= 0:
                    self.startMoving(self.nextDirection)
            self.check_reached_waypoint(currLevelMap, player)
            self.radarUnit.setPosition((self.x/4)+2, (self.y/4)+4)

            # Search for player
            found = searchForPlayer(self.x, self.y, self.direction, self.viewDistance, currLevelMap, player)
            if found:
                if self.shotTimer <= 0:
                    self.attack(player, bulletManager)
                    self.shotTimer = self.shotCooldown
                    if self.awaiting_path is None:
                        if (len(self.path.waypoints) - self.path.index) > 3:
                            self.wait_for_new_path = True
                self.canSeePlayer = True
            else:
                self.canSeePlayer = False
            self.lastX = self.x
            self.lastY = self.y

    def receiveDamage(self, damage, player=None):
        if damage > 0:
            super(Boss, self).receiveDamage(damage)
            self.update_health_bar()
            if not self.canSeePlayer:
                if self.awaiting_path is None:
                    if (len(self.path.waypoints) - self.path.index) > 1:
                        self.wait_for_new_path = True

    def cleanUp(self):
        super(Boss, self).cleanUp()
        self.hide_health_bar()
        # Clear all bullets from the map
        # We don't want the player to get killed after defeating a boss
        CommonManager.get_bullet_manager().stop()

    def create_health_bar(self):
        # Get the window height from the HUD
        window_height = CommonManager.get_hud().window.height
        self.health_bar_bg = StaticObject(0, (window_height/32)-1, common_tileset[common_spritemap['bar']], self.batches['hud'], _bg_batch_group)
        self.health_bar_bg.show()
        self.health_bar_bg.setColor((0,0,0))
        self.health_bar_bg.sprite.scale_x = self.maxHealth / self.health_bar_scale_constant
        self.health_bar_bg.sprite.opacity = 127
    
        self.health_bar_fg = StaticObject(0, (window_height/32)-1, common_tileset[common_spritemap['bar']], self.batches['hud'], _fg_batch_group)
        self.health_bar_fg.show()
        self.health_bar_fg.setColor((255, 135, 48))
        self.health_bar_fg.sprite.scale_x = self.health / self.health_bar_scale_constant

        self.name_label = Label(self.name,
                          font_size=12,
                          x=0, y=window_height - 48,
                          batch=self.batches["hud"], group=_fg_batch_group)

    def update_health_bar(self):
        if self.health_bar_fg is not None:
            self.health_bar_fg.sprite.scale_x = self.health / self.health_bar_scale_constant

    def hide_health_bar(self):
        if self.health_bar_bg is not None:
            self.health_bar_bg.hide()
        if self.health_bar_fg is not None:
            self.health_bar_fg.hide()
        if self.name_label is not None:
            self.name_label.delete()