from pyglet.sprite import Sprite
from it5.bosses.boss import Boss
from it5.npc import NPC
from it5 import spriteloader
from it5.dynamicobject import DynamicObject
from it5.common import common_spritemap, common_tileset, EventManager
from it5.direction import Direction
from it5.audio import SoundManager
from it5.waypoint import Path
from random import randint
import math

asp_sprites = spriteloader.getSprites('asp.png', 4, 4)
crosshair_sprites = spriteloader.getSprites('asp.png', 2, 2)

class Asp(Boss):
    def __init__(self, x, y, waypoints, batches, batchGroups):
        super(Asp, self).__init__(x, y, waypoints, batches, batchGroups, asp_sprites,
                                    name="Asp", damage=34, health=350, move_speed=0, view_distance=0, health_bar_scale_constant=64.0)
        self.half_vision_width = 12
        self.vision_length = 50
        self.blindspot_length = 5
        self.crosshair = None
        self.image = crosshair_sprites[(0,1)]
        self.counter = 0
        self.shotCooldown = 2.5
        self.shotTimer = self.shotCooldown
        self.start_crosshair_color = [255,255,255]
        self.danger_crosshair_color = [255,32,32]
        self.recently_visited_waypoints = set()
        self.last_waypoint = None

    def copy(self):
        return Asp(self.getTileX(), self.getTileY(), self.path.waypoints, self.batches, self.batchGroups)

    def initialize_path_from_waypoints(self, waypoints):
        self.path = Path(waypoints, interpolate=False)

    def hide_crosshair(self):
        if self.crosshair is not None:
            self.crosshair.delete()
            self.crosshair = None
        self.shotTimer = self.shotCooldown

    def show_crosshair(self, dt, player):
        if self.crosshair is None:
            self.crosshair = Sprite(
                img=self.image,
                batch=self.batches["main"],
                group=self.batchGroups["markers"]
            )
            self.crosshair.color = self.start_crosshair_color
        self.counter += dt * 2
        offset_x = math.sin(self.counter) * 3
        offset_y = math.cos(self.counter) * 3
        self.crosshair.position = (player.x-16+offset_x, player.y+offset_y)

    def set_crosshair_danger_color(self):
        if self.crosshair is not None:
            self.crosshair.color = self.danger_crosshair_color

    def cleanUp(self):
        super(Asp, self).cleanUp()
        self.hide_crosshair()

    def set_dynamic_objects(self, x, y, sprites, batch, batchGroups, color=(255,255,255), no_movement=False):
        feet = self.get_feet_group()
        heads = self.get_heads_group()
        self.bottom = DynamicObject(x, y, sprites, 2, 1, batch, batchGroups[feet], color)
        self.top = DynamicObject(x, y+1, sprites, 3, 1, batch, batchGroups[heads], color)
        self.cover_shadow = DynamicObject(x, y, common_tileset, common_spritemap["shadow"][0], common_spritemap["shadow"][1], batch, batchGroups[heads])
        self.bottom_left = DynamicObject(x-1, y, sprites, 3, 3, batch, batchGroups[feet], color)
        self.bottom_right = DynamicObject(x+1, y, sprites, 3, 3, batch, batchGroups[feet], color)
        self.top_left = DynamicObject(x-1, y+1, sprites, 3, 3, batch, batchGroups[heads], color)
        self.top_right = DynamicObject(x+1, y+1, sprites, 3, 3, batch, batchGroups[heads], color)
        self.prone_top = DynamicObject(x, y+1, sprites, 3, 3, batch, batchGroups[feet], color)
        self.prone_bottom = DynamicObject(x, y-1, sprites, 3, 3, batch, batchGroups[feet], color)
        self.all_dynamic_objects.append(self.bottom)
        self.all_dynamic_objects.append(self.top)
        self.all_dynamic_objects.append(self.cover_shadow)
        self.all_dynamic_objects.append(self.bottom_left)
        self.all_dynamic_objects.append(self.top_left)
        self.all_dynamic_objects.append(self.bottom_right)
        self.all_dynamic_objects.append(self.top_right)
        self.all_dynamic_objects.append(self.prone_top)
        self.all_dynamic_objects.append(self.prone_bottom)

    def set_idle_sprites(self, moveDirection):
        self.top.destroy()
        if moveDirection == Direction.UP:
            self.prone_bottom.updateSprite(2,0)
            self.bottom.updateSprite(3,0)
            if self.bottom_left is not None:
                self.bottom_left.destroy()
            if self.bottom_right is not None:
                self.bottom_right.destroy()
            if self.prone_top is not None:
                self.prone_top.destroy()
        elif moveDirection == Direction.DOWN:
            self.bottom.updateSprite(2,1)
            self.prone_top.updateSprite(3,1)
            if self.bottom_left is not None:
                self.bottom_left.destroy()
            if self.bottom_right is not None:
                self.bottom_right.destroy()
            if self.prone_bottom is not None:
                self.prone_bottom.destroy()
        elif moveDirection == Direction.LEFT:
            self.bottom.updateSprite(1,0)
            self.bottom_right.updateSprite(1,1)
            if self.bottom_left is not None:
                self.bottom_left.destroy()
            if self.prone_top is not None:
                self.prone_top.destroy()
            if self.prone_bottom is not None:
                self.prone_bottom.destroy()
        elif moveDirection == Direction.RIGHT:
            self.bottom.updateSprite(0,1)
            self.bottom_left.updateSprite(0,0)
            if self.bottom_right is not None:
                self.bottom_right.destroy()
            if self.prone_top is not None:
                self.prone_top.destroy()
            if self.prone_bottom is not None:
                self.prone_bottom.destroy()

    def reachedWaypoint(self, currLevelMap, player):
        pass

    def createVisionCone(self):
        # Asp shouldn't show up on the radar
        self.visionCone = None

    def updateVisionCone(self):
        # Asp shouldn't show up on the radar
        pass

    def jump_to_waypoint(self, waypoint):
        if waypoint is not None:
            if self.counter > 0:
                # Only create a smoke bomb if we are past the initial jump
                EventManager.smoke_bomb(self.x, self.y)
            x = waypoint.getTileX()
            y = waypoint.getTileY()
            self.jump(x, y)
            self.changeDirection(waypoint.direction)
            self.recently_visited_waypoints.add(str(waypoint))
            self.last_waypoint = waypoint

    def check_wall_adjacency(self, px, py, dx, dy, obstacle_matrix):
        check_x = px + dx
        check_y = py + dy
        if check_y >= 0 and check_y < len(obstacle_matrix):
            if check_x >= 0 and check_x < len(obstacle_matrix[check_y]):
                return obstacle_matrix[check_y][check_x] != 0
        return False

    def search_for_player(self, currLevelMap, player):
        if player.concealed:
            return False
        visible = False
        px = player.getTileX()
        py = player.getTileY()
        tx = self.getTileX()
        ty = self.getTileY()
        if self.direction == Direction.DOWN:
            if px >= tx - self.half_vision_width and px <= tx + self.half_vision_width:
                if py >= ty - self.vision_length and py <= ty - self.blindspot_length:
                    visible = self.check_wall_adjacency(px, py, 0, 1, currLevelMap.obstacleMatrix)
        elif self.direction == Direction.UP:
            if px >= tx - self.half_vision_width and px <= tx + self.half_vision_width:
                if py <= ty + self.vision_length and py >= ty + self.blindspot_length:
                    visible = self.check_wall_adjacency(px, py, 0, -1, currLevelMap.obstacleMatrix)
        elif self.direction == Direction.LEFT:
            if py >= ty - self.half_vision_width and py <= ty + self.half_vision_width:
                if px >= tx - self.vision_length and px <= tx - self.blindspot_length:
                    visible = self.check_wall_adjacency(px, py, 1, 0, currLevelMap.obstacleMatrix)
        elif self.direction == Direction.RIGHT:
            if py >= ty - self.half_vision_width and py <= ty + self.half_vision_width:
                if px <= tx + self.vision_length and px >= tx + self.blindspot_length:
                    visible = self.check_wall_adjacency(px, py, -1, 0, currLevelMap.obstacleMatrix)
        return visible

    def attack(self, player, bullet_manager):
        SoundManager.play_sfx("sniper_rifle_gunshot")
        miss_probability = 0
        if player.moving:
            miss_probability = 20
        r = randint(0, 100)
        if r >= miss_probability:
            # Shot connects
            player.receiveDamage(self.damage)
        if self.crosshair is not None:
            self.crosshair.color = self.start_crosshair_color

    def getPunched(self, x1, x2, y1, y2, player=None):
        tx = self.getTileX()
        ty = self.getTileY()
        if tx >= x1 and tx <= x2 and ty >= y1 and ty <= y2:
            # This boss can be punched
            self.receiveDamage(25)

    def receiveDamage(self, damage, player=None):
        if damage >= 100:
            # Asp has a partial resistance to explosives
            damage = 50
        super(Asp, self).receiveDamage(damage)
        available_wps = [wp for wp in self.path.waypoints if str(wp) not in self.recently_visited_waypoints]
        if len(available_wps) == 0:
            self.recently_visited_waypoints.clear()
            available_wps = [wp for wp in self.path.waypoints]
        next_waypoint = available_wps[randint(0, len(available_wps)-1)]
        while next_waypoint == self.last_waypoint:
            next_waypoint = available_wps[randint(0, len(available_wps)-1)]
        self.jump_to_waypoint(next_waypoint)

    def update(self, dt, currLevelMap, player, bulletManager):
        # Important to call NPC's superclass update() method
        super(NPC, self).update(dt, currLevelMap.obstacleMatrix, currLevelMap.doors)
        if self.alive:
            # Search for player
            found = self.search_for_player(currLevelMap, player)
            if found:
                if self.shotTimer <= 0:
                    self.attack(player, bulletManager)
                    self.shotTimer = self.shotCooldown
                self.canSeePlayer = True
                self.show_crosshair(dt, player)
            else:
                self.canSeePlayer = False
                self.hide_crosshair()
            if self.shotTimer > 0:
                if self.shotTimer < 0.5:
                    self.set_crosshair_danger_color()
                self.shotTimer -= dt
            self.lastX = self.x
            self.lastY = self.y