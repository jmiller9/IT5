from it5.bosses.boss import Boss
from it5.npc import relict_sprites, NPC
from it5.direction import Direction
from it5.audio import SoundManager
from it5.animation import Animation, AnimationGroup, AnimationGroupManager
from it5.dynamicobject import DynamicObject
from it5.util import searchForPlayer, getHalfVisionConeHeight

class Relict(Boss):
    def __init__(self, x, y, waypoints, batches, batchGroups, health=2500, move_speed=150, animation_cycles=24, health_regen_delta=20, stun_time_wait=2.0, growl_after_stun=True):
        super(Relict, self).__init__(x, y, waypoints, batches, batchGroups, relict_sprites,
                                    name="Relict", damage=35, health=health, move_speed=move_speed, view_distance=32, health_bar_scale_constant=128.0)
        self.attacking_timer = 0
        self.attack_time = 0.5
        self.half_attack_time = float(self.attack_time) / 2.0
        self.attacking = False
        self.attack_frame = 0
        self.attack_animation_index = {
            Direction.UP: [5, 6],
            Direction.DOWN: [5, 6],
            Direction.RIGHT: [8, 9],
            Direction.LEFT: [8, 9],
        }
        self.batches = batches
        self.batch_groups = batchGroups
        self.relict_spritemap = {
            "blood": (7, 7),
            "blood2": (7, 8),
            "blood_edge": (6, 7),
        }
        self.target = None
        # Normal animationCycles is 48. Reducing this makes for creepy looking running animations
        self.animationCycles = animation_cycles
        self.animationStep = (self.animationCycles)/self.maxFrameX
        self.halfAnimationCycles = self.animationCycles / 2
        self.lastIdleFrame = self.halfAnimationCycles
        self.actual_view_distance = 768
        self.radarViewDistance = self.actual_view_distance/4
        self.halfVisionConeHeight = getHalfVisionConeHeight(self.radarViewDistance)
        # Health regen delta of 0 turns this effect off
        self.health_regen_delta = health_regen_delta
        self.stun_time_wait = stun_time_wait
        self.growl_after_stun = growl_after_stun
        self.max_stamina = 100
        

    def copy(self):
        return Relict(self.getTileX(), self.getTileY(), self.path.waypoints, self.batches, self.batchGroups)

    def stopMoving(self):
        if not self.attacking:
            super(Relict, self).stopMoving()

    def attack(self, player, bulletManager):
        if self.target is not None:
            self.target.receiveDamage(self.damage)
        self.attacking = True
        self.attacking_timer = 0
        self.attack_frame = 0
        self.animate(0)

    def update(self, dt, currLevelMap, player, bulletManager):
        if self.target is None:
            self.target = player
        # Important to call NPC's superclass update() method
        super(NPC, self).update(dt, currLevelMap.obstacleMatrix, currLevelMap.doors)
        if self.alive:
            self.health = min(self.maxHealth, self.health + (self.health_regen_delta * dt))
            self.update_health_bar()
            if not self.stunned:
                if self.attacking:
                    self.animate(dt)
                    if self.attacking_timer >= self.attack_time:
                        self.attacking_timer = 0
                        self.attacking = False
                        self.attack_frame = 0
                if self.shotTimer > 0:
                    self.shotTimer -= dt
                if self.waitTimer > 0:
                    self.waitTimer -= dt
                    if self.waitTimer <= 0:
                        self.startMoving(self.nextDirection)
                self.check_reached_waypoint(currLevelMap, player)
            self.radarUnit.setPosition(self.x / 4, (self.y / 4)+4)
            if not self.stunned:
                # Search for player
                in_attack_range = searchForPlayer(self.x, self.y, self.direction, self.viewDistance, currLevelMap, player)
                found = searchForPlayer(self.x, self.y, self.direction, self.actual_view_distance, currLevelMap, player)
                if found:
                    if self.shotTimer <= 0:
                        if self.awaiting_path is None:
                            if (len(self.path.waypoints) - self.path.index) > 3:
                                self.wait_for_new_path = True
                    self.canSeePlayer = True
                else:
                    self.canSeePlayer = False
                if in_attack_range:
                    if self.shotTimer <= 0:
                        self.attack(player, bulletManager)
                        self.shotTimer = self.shotCooldown
            else:
                if self.stunned:
                    self.updateStunState()
                    self.stunTimer += dt
            self.lastX = self.x
            self.lastY = self.y

    def _animate_attack(self):
        frameColumn = self.attack_animation_index[self.direction][self.attack_frame]
        self.bottom.updateSprite(self.bottom.imagerow, frameColumn)
        self.top.updateSprite(self.top.imagerow, frameColumn)

    def animate(self, dt):
        if not self.attacking:
            super(Relict, self).animate(dt)
        else:
            if self.attacking_timer == 0:
                self.attack_frame = 0
                self._animate_attack()
                self.blood_spatter("blood")
            elif self.attacking_timer >= self.half_attack_time and self.attack_frame == 0:
                self.attack_frame = 1
                self._animate_attack()
                self.blood_spatter("blood2")
            self.attacking_timer += dt

    def blood_spatter(self, blood_target):
        tileX = int((self.target.x+15)/32)
        tileY = int((self.target.y-31)/32)+1
        radius = 48
        # Create the animation
        batch = self.batches["main"]
        batchGroups = self.batch_groups
        center_obj = DynamicObject(tileX, tileY, relict_sprites, self.relict_spritemap[blood_target][0], self.relict_spritemap[blood_target][1], batch, batchGroups['explosions'])
        xdelta = self.target.x - center_obj.x
        ydelta = self.target.y - center_obj.y
        center_obj.move(xdelta, ydelta)
        n_obj = DynamicObject(tileX, tileY, relict_sprites, self.relict_spritemap['blood_edge'][0], self.relict_spritemap['blood_edge'][1], batch, batchGroups['explosions'])
        n_obj.move(xdelta, ydelta)
        s_obj = DynamicObject(tileX, tileY, relict_sprites, self.relict_spritemap['blood_edge'][0], self.relict_spritemap['blood_edge'][1], batch, batchGroups['explosions'])
        s_obj.move(xdelta, ydelta)
        e_obj = DynamicObject(tileX, tileY, relict_sprites, self.relict_spritemap['blood_edge'][0], self.relict_spritemap['blood_edge'][1], batch, batchGroups['explosions'])
        e_obj.move(xdelta, ydelta)
        w_obj = DynamicObject(tileX, tileY, relict_sprites, self.relict_spritemap['blood_edge'][0], self.relict_spritemap['blood_edge'][1], batch, batchGroups['explosions'])
        w_obj.move(xdelta, ydelta)
        ne_obj = DynamicObject(tileX, tileY, relict_sprites, self.relict_spritemap['blood_edge'][0], self.relict_spritemap['blood_edge'][1], batch, batchGroups['explosions'])
        ne_obj.move(xdelta, ydelta)
        nw_obj = DynamicObject(tileX, tileY, relict_sprites, self.relict_spritemap['blood_edge'][0], self.relict_spritemap['blood_edge'][1], batch, batchGroups['explosions'])
        nw_obj.move(xdelta, ydelta)
        se_obj = DynamicObject(tileX, tileY, relict_sprites, self.relict_spritemap['blood_edge'][0], self.relict_spritemap['blood_edge'][1], batch, batchGroups['explosions'])
        se_obj.move(xdelta, ydelta)
        sw_obj = DynamicObject(tileX, tileY, relict_sprites, self.relict_spritemap['blood_edge'][0], self.relict_spritemap['blood_edge'][1], batch, batchGroups['explosions'])
        sw_obj.move(xdelta, ydelta)
        center_frames = [self.relict_spritemap[blood_target]]
        center_anim = Animation(center_obj, center_frames, 1, lifetime_seconds=0.1)
        frames = [self.relict_spritemap['blood_edge']]
        velocity = 225
        edge_velocity = 175
        n_anim = Animation(n_obj, frames, 1, lifetime_seconds=0.1, dx=0, dy=velocity)
        s_anim = Animation(s_obj, frames, 1, lifetime_seconds=0.1, dx=0, dy=-velocity)
        e_anim = Animation(e_obj, frames, 1, lifetime_seconds=0.1, dx=velocity, dy=0)
        w_anim = Animation(w_obj, frames, 1, lifetime_seconds=0.1, dx=-velocity, dy=0)
        ne_anim = Animation(ne_obj, frames, 1, lifetime_seconds=0.1, dx=edge_velocity, dy=edge_velocity)
        nw_anim = Animation(nw_obj, frames, 1, lifetime_seconds=0.1, dx=-edge_velocity, dy=edge_velocity)
        se_anim = Animation(se_obj, frames, 1, lifetime_seconds=0.1, dx=edge_velocity, dy=-edge_velocity)
        sw_anim = Animation(sw_obj, frames, 1, lifetime_seconds=0.1, dx=-edge_velocity, dy=-edge_velocity)
        blood_anim_group = AnimationGroup(center_anim, n_anim, s_anim, e_anim, w_anim, ne_anim, nw_anim, se_anim, sw_anim)
        AnimationGroupManager.add_animation_group(blood_anim_group)
        SoundManager.play_sfx('slash')

    def takeStaminaDamage(self, damage):
        if self.stamina > 0:
            self.stamina -= damage
            if self.stamina <= 0:
                self.stunned = True
                self.hideVisionCone()
                self.stunTimer = 0
                self.stopMoving()
                self.cacheLastDirection()
                return True
        return False

    def receiveDamage(self, damage, player=None):
        super(Relict, self).receiveDamage(damage)
        self.takeStaminaDamage(damage)

    def getPunched(self, x1, x2, y1, y2, player=None):
        tx = self.getTileX()
        ty = self.getTileY()
        if tx >= x1 and tx <= x2 and ty >= y1 and ty <= y2:
            if not self.takeStaminaDamage(25):
                self.receiveDamage(0, player)
            SoundManager.play_sfx("punch")

    def updateStunState(self):
        if self.stunTimer >= self.stun_time_wait:
            self.stunned = False
            self.stamina = self.max_stamina
            self.updateVisionCone()
            self.stopMoving()
            self.resumePostEvent()
            self.moving = True
            self.attacking_timer = 0
            self.attacking = False
            self.attack_frame = 0
            if self.growl_after_stun:
                SoundManager.play_sfx("monster_growl")