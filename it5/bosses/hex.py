from it5.bosses.boss import Boss
from it5 import spriteloader
from it5.audio import SoundManager
from it5.item import WeaponType
from it5.direction import Direction
from it5.util import searchForPlayer
from it5.dynamicobject import DynamicObject
import math
from random import randint
from it5.common import lighting, EventManager
from it5.projectile import Bullet

hex_sprites = spriteloader.getSprites('hex.png', 8, 8)
horror_sprites = spriteloader.getSprites('hex.png', 2, 2)

class ShadowBomb:
    def __init__(self, owner, x, y, dx, dy, damage=20):
        self.owner = owner
        self.damage = damage
        self.active = True
        self.x = x
        self.y = y
        self.dx = dx
        self.dy = dy
        self.originX = self.x
        self.originY = self.y
        self.range = 480
        self.health = 50
        self.distance = 0
        self.light = lighting.create_light(x=x, y=y, radius=64, steps=8, color=(255,0,255), exponent=1.5)

    def __del__(self):
        self.destroy()

    def destroy(self):
        lighting.destroy_light(self.light)

    def getTileX(self):
        return int((self.x+15) / 32)
    
    def getTileY(self):
        return int((self.y-31) / 32)

    def update(self, dt, player):
        self.checkCollision(player)
        self.move(dt)

    def move(self, dt):
        if self.distance < self.range:
            delta_x = math.copysign(min(abs(self.dx * dt), 32.0), self.dx)
            delta_y = math.copysign(min(abs(self.dy * dt), 32.0), self.dy)
            self.x += delta_x
            self.y += delta_y
            self.light.set_position(self.x, self.y)
            self.distance = math.sqrt(((self.x - self.originX) ** 2) + ((self.y - self.originY) ** 2))
        else:
            self.active = False

    def checkCollision(self, player):
        if self.active:
            tileX = int((self.x+15) / 32)
            tileY = int((self.y-31) / 32)
            _y = tileY - 1
            while self.active and _y <= tileY + 1:
                _x = tileX - 1
                while self.active and _x <= tileX + 1:
                    if _x == player.getTileX() and (_y == player.getTileY() or _y == player.getTileY()+1):
                        if player.health - self.damage > 0:
                            SoundManager.play_sfx('horror_scream')
                            EventManager.show_image_popout(horror_sprites[randint(0, 1), 1], 128, 128)
                        player.receiveDamage(self.damage)
                        # Shadow Bombs recover half the damage done to the player
                        recovery = self.damage / 2
                        self.owner.health += recovery
                        if self.owner.health >= self.owner.maxHealth:
                            self.owner.health = self.owner.maxHealth
                        self.owner.update_health_bar()
                        self.active = False
                    _x += 1
                _y += 1

    def receive_damage(self, damage):
        self.health -= damage
        if self.health < 0:
            self.active = False

class Hex(Boss):
    def __init__(self, x, y, waypoints, batches, batchGroups):
        super(Hex, self).__init__(x, y, waypoints, batches, batchGroups, hex_sprites,
                                    name="Hex", damage=15, health=400, move_speed=0, view_distance=768, health_bar_scale_constant=64.0)
        self._animationIndex = {
            "vertical": [1,2,1,0],
            "horizontal": [1,2,1,0],
        }
        self.animationIndex = {False: {}, True: {}}
        self.animationIndex[False][Direction.UP] = self._animationIndex["vertical"]
        self.animationIndex[False][Direction.DOWN] = self._animationIndex["vertical"]
        self.animationIndex[False][Direction.LEFT] = self._animationIndex["horizontal"]
        self.animationIndex[False][Direction.RIGHT] = self._animationIndex["horizontal"]
        self.currFrameX = 0
        self.maxFrameX = len(self.animationIndex[False][self.direction])
        self.animationCycles = 48
        self.animationStep = (self.animationCycles)/self.maxFrameX
        self.shadow_bomb = None
        self.warp_timer = 1
        self.time_to_charge_warp_seconds = 5

    def destroy(self):
        super(Hex, self).destroy()
        if self.shadow_bomb is not None:
            self.shadow_bomb.destroy()

    def copy(self):
        return Hex(self.getTileX(), self.getTileY(), self.path.waypoints, self.batches, self.batchGroups)

    def reachedWaypoint(self, currLevelMap, player):
        pass

    def attempt_warp(self, player):
        if self.warp_timer == 0:
            furthest_point = None
            furthest_distance = None
            for waypoint in self.path.waypoints:
                if furthest_point is None:
                    furthest_point = waypoint
                    furthest_distance = math.sqrt(((waypoint.x - player.x) ** 2) + ((waypoint.y - player.y) ** 2))
                else:
                    w_distance = math.sqrt(((waypoint.x - player.x) ** 2) + ((waypoint.y - player.y) ** 2))
                    if w_distance >= furthest_distance:
                        furthest_distance = w_distance
                        furthest_point = waypoint
            travel_distance = math.sqrt(((furthest_point.x - self.x) ** 2) + ((furthest_point.y - self.y) ** 2))
            if travel_distance > 32:
                self.warp_timer = 1
                SoundManager.play_sfx('warp')
                self.jump(furthest_point.getTileX(), furthest_point.getTileY())

    def update(self, dt, currLevelMap, player, bulletManager):
        # Important to call NPC's superclass update() method
        super(Hex, self).update(dt, currLevelMap, player, bulletManager)
        self.animate(dt)
        if self.alive:
            self.lookAtPlayer(player)
            if self.shadow_bomb is not None:
                self.shadow_bomb.update(dt, player)
                if self.shadow_bomb.active == False:
                    self.shadow_bomb.destroy()
                    self.shadow_bomb = None
            self.radarUnit.setPosition(self.x / 4, (self.y / 4)+4)
            if self.warp_timer > 0:
                self.warp_timer += dt
                if self.warp_timer >= self.time_to_charge_warp_seconds + 1:
                    self.warp_timer = 0
            # Search for player
            found = searchForPlayer(self.x, self.y, self.direction, self.viewDistance, currLevelMap, player)
            if found:
                self.attack(player, bulletManager)
                self.canSeePlayer = True
            else:
                self.canSeePlayer = False
            self.lastX = self.x
            self.lastY = self.y
            self.attempt_warp(player)

    def attack(self, player, bulletManager):
        if self.shadow_bomb is None:
            dx = 0
            dy = 0
            sx = self.x
            sy = self.y
            velocity = 160
            if self.direction == Direction.UP:
                sy += 27
            elif self.direction == Direction.DOWN:
                sy += 16
            elif self.direction == Direction.LEFT:
                sy += 16
            elif self.direction == Direction.RIGHT:
                sy += 16
            x = player.x - self.x
            y = player.y - self.y

            normal = math.sqrt(float(x * x) + float(y * y))
            if normal > 0:
                nX = x/normal
                nY = y/normal
                dx = velocity * nX
                dy = velocity * nY
                SoundManager.play_sfx('electric_current')
                self.shadow_bomb = ShadowBomb(self, sx, sy, dx, dy)