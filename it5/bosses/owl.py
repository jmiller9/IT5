from it5.bosses.boss import Boss
from it5 import spriteloader
from it5.audio import SoundManager
from it5.item import WeaponType
from it5.direction import Direction
from it5.util import searchForPlayer
from it5.dynamicobject import DynamicObject
import math
from random import randint
from it5.common import lighting, EventManager, common_tileset, common_spritemap, batches, batchGroups
from it5.projectile import Bullet
from it5.waypoint import Path
from pyglet.sprite import Sprite

owl_sprites = spriteloader.getSprites("owl.png", 8, 3)


class State:
    def __init__(self, start, end, look_direction):
        self.start = start
        self.end = end
        self.look_direction = look_direction

class NSState(State):
    def check_reached_waypoint(self, owl):
        if owl.x <= self.start.x:
            owl.jump(self.start.x/32, (self.start.y-31)/32)
            owl.move_direction = Direction.RIGHT
        elif owl.x >= self.end.x:
            owl.jump(self.end.x/32, (self.end.y-31)/32)
            owl.move_direction = Direction.LEFT

class EWState(State):
    def check_reached_waypoint(self, owl):
        if owl.y <= self.start.y:
            owl.jump(self.start.x/32, (self.start.y-31)/32)
            owl.move_direction = Direction.UP
        elif owl.y >= self.end.y:
            owl.jump(self.end.x/32, (self.end.y-31)/32)
            owl.move_direction = Direction.DOWN

class IntroState(State):
    def check_reached_waypoint(self, owl):
        if owl.y <= self.end.y:
            owl.move_direction = Direction.LEFT
            owl.change_state("intro_transition")
            owl.moveSpeed = 120

class IntroTransitionState(State):
    def check_reached_waypoint(self, owl):
        if owl.x <= self.end.x:
            owl.change_state(Direction.UP)
            owl.out_of_intro = True

class MidpointToNorthState(State):
    def check_reached_waypoint(self, owl):
        if owl.y >= self.end.y:
            owl.jump(owl.x/32, (self.end.y-31)/32)
            owl.move_direction = Direction.LEFT
            owl.change_state(Direction.UP)

class MidpointToSouthState(State):
    def check_reached_waypoint(self, owl):
        if owl.y <= self.end.y:
            owl.jump(owl.x/32, (self.end.y-31)/32)
            owl.move_direction = Direction.RIGHT
            owl.change_state(Direction.DOWN)

class MidpointToEastState(State):
    def check_reached_waypoint(self, owl):
        if owl.x >= self.end.x:
            owl.jump(self.end.x/32, owl.y/32)
            owl.move_direction = Direction.DOWN
            owl.change_state(Direction.RIGHT)

class MidpointToWestState(State):
    def check_reached_waypoint(self, owl):
        if owl.x <= self.end.x:
            owl.jump(self.end.x/32, owl.y/32)
            owl.move_direction = Direction.UP
            owl.change_state(Direction.LEFT)

class NorthToSouthState(State):
    def check_reached_waypoint(self, owl):
        if owl.y <= self.end.y:
            owl.change_state("midpoint_to_south")
            owl.drop_bomb()

class NorthToEastState(State):
    def check_reached_waypoint(self, owl):
        if owl.y <= self.end.y:
            owl.move_direction = Direction.RIGHT
            owl.changeDirection(Direction.RIGHT)
            owl.change_state("midpoint_to_east")
            owl.drop_bomb()

class NorthToWestState(State):
    def check_reached_waypoint(self, owl):
        if owl.y <= self.end.y:
            owl.move_direction = Direction.LEFT
            owl.changeDirection(Direction.LEFT)
            owl.change_state("midpoint_to_west")
            owl.drop_bomb()

class SouthToNorthState(State):
    def check_reached_waypoint(self, owl):
        if owl.y >= self.end.y:
            owl.change_state("midpoint_to_north")
            owl.drop_bomb()

class SouthToEastState(State):
    def check_reached_waypoint(self, owl):
        if owl.y >= self.end.y:
            owl.move_direction = Direction.RIGHT
            owl.changeDirection(Direction.RIGHT)
            owl.change_state("midpoint_to_east")
            owl.drop_bomb()

class SouthToWestState(State):
    def check_reached_waypoint(self, owl):
        if owl.y >= self.end.y:
            owl.move_direction = Direction.LEFT
            owl.changeDirection(Direction.LEFT)
            owl.change_state("midpoint_to_west")
            owl.drop_bomb()

class EastToSouthState(State):
    def check_reached_waypoint(self, owl):
        if owl.x <= self.end.x:
            owl.move_direction = Direction.DOWN
            owl.changeDirection(Direction.DOWN)
            owl.change_state("midpoint_to_south")
            owl.drop_bomb()

class EastToNorthState(State):
    def check_reached_waypoint(self, owl):
        if owl.x <= self.end.x:
            owl.move_direction = Direction.UP
            owl.changeDirection(Direction.UP)
            owl.change_state("midpoint_to_north")
            owl.drop_bomb()

class EastToWestState(State):
    def check_reached_waypoint(self, owl):
        if owl.x <= self.end.x:
            owl.change_state("midpoint_to_west")
            owl.drop_bomb()

class WestToSouthState(State):
    def check_reached_waypoint(self, owl):
        if owl.x >= self.end.x:
            owl.move_direction = Direction.DOWN
            owl.changeDirection(Direction.DOWN)
            owl.change_state("midpoint_to_south")
            owl.drop_bomb()

class WestToNorthState(State):
    def check_reached_waypoint(self, owl):
        if owl.x >= self.end.x:
            owl.move_direction = Direction.UP
            owl.changeDirection(Direction.UP)
            owl.change_state("midpoint_to_north")
            owl.drop_bomb()

class WestToEastState(State):
    def check_reached_waypoint(self, owl):
        if owl.x >= self.end.x:
            owl.change_state("midpoint_to_east")
            owl.drop_bomb()

class TimeBomb:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        img = common_tileset[common_spritemap["c4"]]
        self.sprite = Sprite(img, x, y, batch=batches["main"], group=batchGroups["items"])
        self.timer = 0
        self.explode_time = 3.0
        self.exploded = False
        self.light_color = [0,1.0,0]
        start_light_color = [0,1.0,0]
        target_light_color = [1.0,0,0]
        self.delta_r = (target_light_color[0] - start_light_color[0]) / self.explode_time
        self.delta_g = (target_light_color[1] - start_light_color[1]) / self.explode_time
        self.delta_b = (target_light_color[2] - start_light_color[2]) / self.explode_time
        self.create_light()

    def create_light(self):
        self.light = lighting.create_light(x=self.x+16, y=self.y+16, radius=64, color=(255,0,0), light_type="glowing")

    def destroy_light(self):
        if self.light is not None:
            lighting.destroy_light(self.light)
            self.light = None

    def destroy(self):
        if self.sprite is not None:
            self.sprite.delete()
        self.destroy_light()

    def update_light_color(self, dt):
        if self.light is not None:
            self.light_color[0] += self.delta_r * dt
            self.light_color[1] += self.delta_g * dt
            self.light_color[2] += self.delta_b * dt
            self.light.set_color_f(self.light_color)

    def update(self, dt):
        if not self.exploded:
            self.update_light_color(dt)
            self.timer += dt
            if self.timer >= self.explode_time:
                # A somewhat weak explosion
                EventManager.explosion(self.x, self.y, damage=50)
                self.exploded = True

class Owl(Boss):
    def __init__(self, x, y, waypoints, batches, batchGroups, waypoint_map):
        super(Owl, self).__init__(x, y, waypoints, batches, batchGroups, owl_sprites,
                                    name="Owl", damage=12, health=800, move_speed=40, view_distance=416, health_bar_scale_constant=128.0)
        self._animationIndex = {
            "vertical": [1,2,1,0],
            "horizontal": [1,2,1,0],
        }
        self.animationIndex = {False: {}, True: {}}
        self.animationIndex[False][Direction.UP] = self._animationIndex["vertical"]
        self.animationIndex[False][Direction.DOWN] = self._animationIndex["vertical"]
        self.animationIndex[False][Direction.LEFT] = self._animationIndex["horizontal"]
        self.animationIndex[False][Direction.RIGHT] = self._animationIndex["horizontal"]
        self.currFrameX = 0
        self.maxFrameX = len(self.animationIndex[False][self.direction])
        self.animationCycles = 48
        self.animationStep = (self.animationCycles)/self.maxFrameX
        self.bombs = []
        self.active_bombs = []
        self.strafing_state_timer = 0
        self.time_to_change_state_seconds = 15
        self.move_direction = Direction.DOWN
        self.all_directions = {
            Direction.LEFT,
            Direction.RIGHT,
            Direction.UP,
            Direction.DOWN,
        }
        self.waypoint_map = waypoint_map
        self.shotCooldown = 0.25
        self.states = {
            "intro": IntroState(self.waypoint_map["intro"], self.waypoint_map["north_midpoint"], Direction.DOWN),
            "intro_transition": IntroTransitionState(self.waypoint_map["north_midpoint"], self.waypoint_map["nw"], Direction.DOWN),
            Direction.UP: NSState(self.waypoint_map["nw"], self.waypoint_map["ne"], Direction.DOWN),
            Direction.DOWN: NSState(self.waypoint_map["sw"], self.waypoint_map["se"], Direction.UP),
            Direction.LEFT: EWState(self.waypoint_map["sw"], self.waypoint_map["nw"], Direction.RIGHT),
            Direction.RIGHT: EWState(self.waypoint_map["se"], self.waypoint_map["ne"], Direction.LEFT),
            "midpoint_to_north": MidpointToNorthState(self.waypoint_map["midpoint"], self.waypoint_map["north_midpoint"], Direction.UP),
            "midpoint_to_south": MidpointToSouthState(self.waypoint_map["midpoint"], self.waypoint_map["south_midpoint"], Direction.DOWN),
            "midpoint_to_east": MidpointToEastState(self.waypoint_map["midpoint"], self.waypoint_map["east_midpoint"], Direction.RIGHT),
            "midpoint_to_west": MidpointToWestState(self.waypoint_map["midpoint"], self.waypoint_map["west_midpoint"], Direction.LEFT),
            "north_to_south": NorthToSouthState(self.waypoint_map["north_midpoint"], self.waypoint_map["midpoint"], Direction.DOWN),
            "north_to_east": NorthToEastState(self.waypoint_map["north_midpoint"], self.waypoint_map["midpoint"], Direction.DOWN),
            "north_to_west": NorthToWestState(self.waypoint_map["north_midpoint"], self.waypoint_map["midpoint"], Direction.DOWN),
            "south_to_north": SouthToNorthState(self.waypoint_map["south_midpoint"], self.waypoint_map["midpoint"], Direction.UP),
            "south_to_east": SouthToEastState(self.waypoint_map["south_midpoint"], self.waypoint_map["midpoint"], Direction.UP),
            "south_to_west": SouthToWestState(self.waypoint_map["south_midpoint"], self.waypoint_map["midpoint"], Direction.UP),
            "east_to_south": EastToSouthState(self.waypoint_map["east_midpoint"], self.waypoint_map["midpoint"], Direction.LEFT),
            "east_to_north": EastToNorthState(self.waypoint_map["east_midpoint"], self.waypoint_map["midpoint"], Direction.LEFT),
            "east_to_west": EastToWestState(self.waypoint_map["east_midpoint"], self.waypoint_map["midpoint"], Direction.LEFT),
            "west_to_south": WestToSouthState(self.waypoint_map["west_midpoint"], self.waypoint_map["midpoint"], Direction.RIGHT),
            "west_to_north": WestToNorthState(self.waypoint_map["west_midpoint"], self.waypoint_map["midpoint"], Direction.RIGHT),
            "west_to_east": WestToEastState(self.waypoint_map["west_midpoint"], self.waypoint_map["midpoint"], Direction.RIGHT),
        }
        # Set Owl's initial state
        self.out_of_intro = False
        self.transitioning = False
        self.change_state("intro")
        self.create_light()

    def get_feet_group(self):
        # Overridden because Owl is flying
        return "airborne"

    def get_heads_group(self):
        # Overridden because Owl is flying
        return "airborne"

    def create_light(self):
        self.light = lighting.create_light(x=self.x+16, y=self.y+32, radius=64, color=(255,255,255))

    def destroy_light(self):
        if self.light is not None:
            lighting.destroy_light(self.light)
            self.light = None

    def destroy(self):
        super(Owl, self).destroy()
        for bomb in self.bombs:
            bomb.destroy()
        self.destroy_light()

    def copy(self):
        return Owl(self.getTileX(), self.getTileY(), self.path.waypoints, self.batches, self.batchGroups, self.waypoint_map)

    def reset_path(self):
        pass

    def drop_bomb(self):
        bomb = TimeBomb(self.x, self.y)
        self.bombs.append(bomb)

    def change_state(self, new_state):
        print("STATE CHANGE %s" % str(new_state))
        self.state = new_state
        _state = self.states.get(self.state)
        self.changeDirection(_state.look_direction)
        self.startMoving(self.direction)
        # Make sure we don't count transitions as being in a strafing state
        if self.state not in self.all_directions:
            self.transitioning = True
            self.strafing_state_timer = 0
            if self.out_of_intro:
                self.moveSpeed = 240
        else:
            self.transitioning = False
            self.moveSpeed = 120

    def check_state(self, dt):
        if self.out_of_intro and not self.transitioning:
            self.strafing_state_timer += dt
            if self.strafing_state_timer >= self.time_to_change_state_seconds:
                n = randint(0, 2)
                if self.state == Direction.UP:
                    self.move_direction = Direction.DOWN
                    if n == 0:
                        self.change_state("north_to_south")
                    elif n == 1:
                        self.change_state("north_to_east")
                    elif n == 2:
                        self.change_state("north_to_west")
                elif self.state == Direction.DOWN:
                    self.move_direction = Direction.UP
                    if n == 0:
                        self.change_state("south_to_north")
                    elif n == 1:
                        self.change_state("south_to_east")
                    elif n == 2:
                        self.change_state("south_to_west")
                elif self.state == Direction.LEFT:
                    self.move_direction = Direction.RIGHT
                    if n == 0:
                        self.change_state("west_to_north")
                    elif n == 1:
                        self.change_state("west_to_south")
                    elif n == 2:
                        self.change_state("west_to_east")
                elif self.state == Direction.RIGHT:
                    self.move_direction = Direction.LEFT
                    if n == 0:
                        self.change_state("east_to_north")
                    elif n == 1:
                        self.change_state("east_to_south")
                    elif n == 2:
                        self.change_state("east_to_west")

    def update(self, dt, curr_level_map, player, bullet_manager):
        if self.alive:
            # Update bombs
            while len(self.bombs) > 0:
                bomb = self.bombs.pop()
                bomb.update(dt)
                if not bomb.exploded:
                    self.active_bombs.append(bomb)
                else:
                    bomb.destroy()
            while len(self.active_bombs) > 0:
                bomb = self.active_bombs.pop()
                self.bombs.append(bomb)
            # Update Owl
            if self.shotTimer > 0:
                self.shotTimer -= dt
            if self.moving == True:
                self.check_state(dt)
                _state = self.states.get(self.state)
                _state.check_reached_waypoint(self)
                if self.move_direction == Direction.UP:
                    self.move(0, self.moveSpeed, dt, curr_level_map.obstacleMatrix, curr_level_map.doors)
                elif self.move_direction == Direction.DOWN:
                    self.move(0, -1 * self.moveSpeed, dt, curr_level_map.obstacleMatrix, curr_level_map.doors)
                elif self.move_direction == Direction.LEFT:
                    self.move(-1 * self.moveSpeed, 0, dt, curr_level_map.obstacleMatrix, curr_level_map.doors)
                elif self.move_direction == Direction.RIGHT:
                    self.move(self.moveSpeed, 0, dt, curr_level_map.obstacleMatrix, curr_level_map.doors)
            if self.damageTimer > 0:
                self.damageTimer -= 1
                if self.damageTimer == 0:
                    self.top.setAlpha(255)
                    self.bottom.setAlpha(255)
                else:
                    self.top.setAlpha(128)
                    self.bottom.setAlpha(128)
            if self.protected_timer > 0:
                self.protected_timer -= dt
            # Floating animation
            self.animate(dt)
            self.radarUnit.setPosition(self.x / 4, (self.y / 4)+4)
            # Search for player
            if self.out_of_intro and not self.transitioning:
                if searchForPlayer(self.x, self.y, self.direction, self.viewDistance, curr_level_map, player, check_walls=False):
                    if self.shotTimer <= 0:
                        self.attack(player, bullet_manager)
                        self.shotTimer = self.shotCooldown
            self.lastX = self.x
            self.lastY = self.y
            if self.light is not None:
                self.light.set_position(self.x+16, self.y+32)

    def attack(self, player, bullet_manager):
        dx = 0
        dy = 0
        sx = self.x
        sy = self.y
        velocity = 1200
        if self.direction == Direction.UP:
            sy += 59
        elif self.direction == Direction.DOWN:
            sy -= 48
        elif self.direction == Direction.LEFT:
            sy += 16
            sx -= 32
        elif self.direction == Direction.RIGHT:
            sy += 16
            sx += 48
        x = player.x - self.x
        y = player.y - self.y
        normal = math.sqrt(float(x * x) + float(y * y))
        if normal > 0:
            nx = x/normal
            ny = y/normal
            dx = velocity * nx
            dy = velocity * ny
            SoundManager.play_sfx("alt_pistol_gunshot")
            bullet_manager.add(sx, sy, dx, dy, 1024, self.damage, WeaponType.PISTOL, not self.friendly)

    def move(self, dx, dy, dt, obstacle_matrix, doors):
        delta_x = dx * dt
        delta_y = dy * dt
        self.x += delta_x
        self.y += delta_y
        for dynamic_obj in self.all_dynamic_objects:
            dynamic_obj.move(delta_x, delta_y)
        self.updateVisionCone()

    def receiveDamage(self, damage, player=None):
        # This boss is invulnerable when flying to a different side to strafe on
        # This also prevents the player from being able to damage this boss with landmines
        if not self.transitioning:
            last_health = self.health
            super(Owl, self).receiveDamage(damage)
            if last_health > self.health:
                # Don't linger more than 5 more seconds after taking damage
                self.strafing_state_timer += (self.time_to_change_state_seconds/2)
