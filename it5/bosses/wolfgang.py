from it5.bosses.boss import Boss
from it5.npc import wolfgang73_sprites


class WolfgangBoss(Boss):
    def __init__(self, x, y, waypoints, batches, batchGroups):
        super(WolfgangBoss, self).__init__(x, y, waypoints, batches, batchGroups, wolfgang73_sprites,
                                    name="Wolfgang", damage=25, health=2000, move_speed=90, view_distance=768, health_bar_scale_constant=128.0)
        self.shotCooldown = 0.1

    def copy(self):
        return WolfgangBoss(self.getTileX(), self.getTileY(), self.path.waypoints, self.batches, self.batchGroups)
