from it5.shader import Shader
import pyglet
import math
from random import random, randint

# For lights:
vertex_shader_source = b"""
    #version 110
    varying vec2 pos; // position of the fragment in screen space
    varying vec2 uv;
    uniform float aspect_width;
    uniform float aspect_height;
    void main(void)
    {
        vec4 a = gl_Vertex;
        gl_Position = gl_ModelViewProjectionMatrix * a;
        pos = gl_Vertex.xy;

        //uv = gl_Position.xy * 0.5 + vec2(0.5, 0.5);
        uv = vec2((gl_Position.x * 0.5 * aspect_width) + 0.5 * aspect_width, (gl_Position.y * 0.5 * aspect_height) + 0.5 * aspect_height);
    }
"""

fragment_shader_source = b"""
    #version 110
    varying vec2 pos;
    varying vec2 uv;
    uniform vec2 light_pos;
    uniform vec3 light_color;
    uniform float attenuation;
    uniform float exponent;
    uniform sampler2D diffuse_tex;
    void main() {
        vec4 diffuse = texture2D(diffuse_tex, uv);
        float dist = max(1.0 - distance(pos, light_pos) / attenuation, 0.0);
        float lum = pow(dist, exponent);

        // This gives lights color
        gl_FragColor = lum * (diffuse * vec4(light_color, 1.0));
    }
"""

lighting_shader = Shader(vert=[vertex_shader_source], frag=[fragment_shader_source])

all_shaded_groups = set(
    [
        "floor",
        "doors",
        "walls",
        "decals",
        "items",
        "feet",
        "pants",
        "grass",
        "heads",
        "shirts",
        "hair",
        "explosions",
        "airborne",
        "weather",
        "markers"
    ]
)

class Light:
    def __init__(self, x=0, y=0, radius=64, steps=8, color=(255,255,255), exponent=2):
        steps = int(steps)
        steps = max(steps, 4)
        step = 0
        r,g,b = color
        self.x = x
        self.y = y
        self.radius = radius
        self.vertices = []
        self.color = (r/255,g/255,b/255)
        self.exponent = float(exponent)
        self.domain = pyglet.graphics.vertexdomain.create_domain("v2f/stream")
        while step < steps:
            angle = (float(step) / float(steps)) * 360
            angle2 = (float(step+1) / float(steps)) * 360
            # Convert angles to radians
            theta = angle * (math.pi / 180)
            theta2 = angle2 * (math.pi / 180)
            x1 = (self.radius * math.sin(theta)) + self.x
            y1 = (self.radius * math.cos(theta)) + self.y
            x2 = (self.radius * math.sin(theta2)) + self.x
            y2 = (self.radius * math.cos(theta2)) + self.y
            # Create the vertex list using our vertex domain
            vertex_list = self.domain.create(3)
            vertex_list.vertices[0] = x
            vertex_list.vertices[1] = y
            vertex_list.vertices[2] = x1
            vertex_list.vertices[3] = y1
            vertex_list.vertices[4] = x2
            vertex_list.vertices[5] = y2
            self.vertices.append(vertex_list)
            step += 1
        self.extra_init()

    def extra_init(self):
        pass

    def draw(self, time):
        lighting_shader.uniformf(b'light_pos', self.x, self.y)
        lighting_shader.uniformf(b'light_color', *self.color)
        lighting_shader.uniformf(b'attenuation', self.radius)
        lighting_shader.uniformf(b'exponent', self.exponent)
        self.domain.draw(pyglet.gl.GL_TRIANGLES)

    def set_position(self, x, y):
        dx = x - self.x
        dy = y - self.y
        self.x = x
        self.y = y
        for vertex_list in self.vertices:
            vertex_list.vertices[0] += dx
            vertex_list.vertices[1] += dy
            vertex_list.vertices[2] += dx
            vertex_list.vertices[3] += dy
            vertex_list.vertices[4] += dx
            vertex_list.vertices[5] += dy

    def set_color(self, color):
        r,g,b = color
        self.color = (r/255,g/255,b/255)

    def set_color_f(self, color):
        self.color = color

class GlowLight(Light):
    def extra_init(self):
        self.offset = random() * 10

    def draw(self, time):
        lighting_shader.uniformf(b'light_pos', self.x, self.y)
        lighting_shader.uniformf(b'light_color', *self.color)
        lighting_shader.uniformf(b'attenuation', self.radius)
        lighting_shader.uniformf(b'exponent', self.exponent + (math.sin((self.offset + time) * 2)/4))
        self.domain.draw(pyglet.gl.GL_TRIANGLES)

class FlickeringLight(Light):
    def extra_init(self):
        self.max_flicker = 6
        self.flicker_duration = 0.25
        self.last = 0
        self.set_next_flicker()

    def set_next_flicker(self):
        self.next_flicker = random() * self.max_flicker
        self.frame = 0

    def get_dt(self, time):
        last = self.last
        self.last = time
        return time - last

    def draw(self, time):
        visibility_scalar = 1
        self.frame += max(0, self.get_dt(time))
        if self.frame >= self.next_flicker:
            visibility_scalar = (random() * 4) + 1
            if self.frame >= self.next_flicker + self.flicker_duration:
                self.set_next_flicker()
        lighting_shader.uniformf(b'light_pos', self.x, self.y)
        lighting_shader.uniformf(b'light_color', *self.color)
        lighting_shader.uniformf(b'attenuation', self.radius)
        lighting_shader.uniformf(b'exponent', self.exponent * visibility_scalar)
        self.domain.draw(pyglet.gl.GL_TRIANGLES)

class BrokenFlickeringLight(Light):
    def draw(self, time):
        visibility_scalar = random() + 1
        lighting_shader.uniformf(b'light_pos', self.x, self.y)
        lighting_shader.uniformf(b'light_color', *self.color)
        lighting_shader.uniformf(b'attenuation', self.radius)
        lighting_shader.uniformf(b'exponent', self.exponent * visibility_scalar)
        self.domain.draw(pyglet.gl.GL_TRIANGLES)

class ShimmeringLight(Light):
    def draw(self, time):
        visibility_scalar = (random() * 0.05) + 1
        lighting_shader.uniformf(b'light_pos', self.x, self.y)
        lighting_shader.uniformf(b'light_color', *self.color)
        lighting_shader.uniformf(b'attenuation', self.radius)
        lighting_shader.uniformf(b'exponent', self.exponent * visibility_scalar)
        self.domain.draw(pyglet.gl.GL_TRIANGLES)

class LightingManager:
    def __init__(self):
        self.lights = set()
        self.time = 0
        self.ambience = (1.0, 1.0, 1.0)
        self.last_ambience = (1.0, 1.0, 1.0)

        ambient_fragment_shader_source = b"""
            uniform float red;
            uniform float green;
            uniform float blue;
            uniform float ambient_red;
            uniform float ambient_green;
            uniform float ambient_blue;
            uniform sampler2D image;
            void main() {
                vec4 texel = texture2D(image, gl_TexCoord[0].xy).rgba;
                gl_FragColor = vec4(texel.x,texel.y,texel.z, texel.a);
                gl_FragColor.r = dot(texel, vec4(red * ambient_red, 0, 0, 0));
                gl_FragColor.g = dot(texel, vec4(0, green * ambient_green, 0, 0));
                gl_FragColor.b = dot(texel, vec4(0, 0, blue * ambient_blue, 0));
            }
        """

        white_fragment_shader_source = b"""
            uniform sampler2D image;
            void main() {
                vec4 texel = texture2D(image, gl_TexCoord[0].xy).rgba;
                gl_FragColor = vec4(1.0, 1.0, 1.0, texel.a);
            }
        """

        alarm_shader_source = b"""
            uniform float time;
            uniform sampler2D image;

            void main() {
                // Have something to vary the radius (can also just be a linear counter (time))
                float wave = abs(sin(time));

                vec4 texel = texture2D(image, gl_TexCoord[0].xy).rgba;
                gl_FragColor = vec4(texel.x,texel.y,texel.z, texel.a);
                gl_FragColor.r = dot(texel, vec4(1.0, 0, 0, 0));
                gl_FragColor.g = dot(texel, vec4(0, wave, 0, 0));
                gl_FragColor.b = dot(texel, vec4(0, 0, wave, 0));
            }
        """

        alarm_secondary_fragment_shader_source = b"""
            uniform sampler2D image;
            uniform float time;
            uniform float red;
            uniform float green;
            uniform float blue;

            void main() {
                // Have something to vary the radius (can also just be a linear counter (time))
                float wave = abs(sin(time));

                vec4 texel = texture2D(image, gl_TexCoord[0].xy).rgba;
                gl_FragColor = vec4(texel.x * red, texel.y * green, texel.z * blue, texel.a);
                gl_FragColor.r = dot(texel, vec4(1.0 * red, 0, 0, 0));
                gl_FragColor.g = dot(texel, vec4(0, wave * green, 0, 0));
                gl_FragColor.b = dot(texel, vec4(0, 0, wave * blue, 0));
            }
        """

        point_light_vertex_shader_source = b"""
            void main() {
                gl_Position = ftransform();
            }
        """

        point_light_fragment_shader_source = b"""
            uniform float time;

            void main() {
                // Have something to vary the radius (can also just be a linear counter (time))
                float wave = abs(sin(time));
                float brightness = 0.5 - (wave / 5.0);

                gl_FragColor = vec4(1.0, 1.0, 0, brightness);
            }
        """

        lighting_vertex_shader_source = b"""
            void main() {
                gl_TexCoord[0] = gl_MultiTexCoord0;
                gl_Position = ftransform();
            }
        """

        lighting_fragment_shader_source = b"""
            uniform float red;
            uniform float green;
            uniform float blue;
            uniform float ambient_red;
            uniform float ambient_green;
            uniform float ambient_blue;
            uniform sampler2DRect image;
            void main() {
                vec4 texel = texture2DRect(image, gl_TexCoord[0].xy).rgba;
                gl_FragColor = vec4(texel.x,texel.y,texel.z, texel.a);
                gl_FragColor.r = dot(texel, vec4(red * ambient_red, 0, 0, 0));
                gl_FragColor.g = dot(texel, vec4(0, green * ambient_green, 0, 0));
                gl_FragColor.b = dot(texel, vec4(0, 0, blue * ambient_blue, 0));
            }
        """

        gas_fragment_shader_source = b"""
            uniform float time;
            uniform sampler2DRect image;

            void main() {
                // Have something to vary the radius (can also just be a linear counter (time))
                float wave = abs(sin(time)/2.0f);
                float partial_wave = wave / 4.0f;

                vec4 texel = texture2DRect(image, gl_TexCoord[0].xy).rgba;
                gl_FragColor = vec4(texel.x,texel.y,texel.z, texel.a);
                gl_FragColor.r = dot(texel, vec4(1.0, partial_wave, 0, 0));
                gl_FragColor.g = dot(texel, vec4(partial_wave, 1.0, 0, 0));
                gl_FragColor.b = dot(texel, vec4(0.0, 0.0, wave, 0));
            }
        """

        self.shaders = {
            "night_vision": {
                "primary": {
                    "shader": Shader(vert=[lighting_vertex_shader_source], frag=[lighting_fragment_shader_source]),
                    "ambience": (0.4, 1.0, 0.5)
                },
                "secondary": [
                    {
                        "shader": Shader(frag=[white_fragment_shader_source]),
                        "groups": set(["items", "feet", "pants", "heads", "shirts", "hair", "explosions", "heads_player", "feet_player"])
                    }
                ]
            },
            "alarm": {
                "primary": {
                    "shader": Shader(frag=[alarm_shader_source])
                }
            },
            "ambient": {
                "primary": {
                    "shader": Shader(vert=[lighting_vertex_shader_source], frag=[lighting_fragment_shader_source])
                }
            },
            "gas": {
                "primary": {
                    "shader": Shader(vert=[lighting_vertex_shader_source], frag=[gas_fragment_shader_source])
                }
            }
        }
        # This is the key to the current shader
        self.selected_shader = None
        # This is the primary shader
        self.primary_shader = None
        # These are drawn on certain batch groups with matching tags
        self.secondary_shaders = []
        self.set_shader("ambient")

    def set_time(self, time):
        self.time = time

    def get_lights_array(self):
        raw_lights = []
        for light, raw_light in self.lights:
            raw_lights.append(float(raw_light[0]))
            raw_lights.append(float(raw_light[1]))
            raw_lights.append(float(raw_light[2]))
        return raw_lights

    def bind(self):
        if self.primary_shader is not None:
            self.primary_shader.bind()
            self.primary_shader.uniformf(b"time", self.time)
            self.primary_shader.uniformf(b"red", 1)
            self.primary_shader.uniformf(b"green", 1)
            self.primary_shader.uniformf(b"blue", 1)
            self.primary_shader.uniformf(b"ambient_red", self.ambience[0])
            self.primary_shader.uniformf(b"ambient_green", self.ambience[1])
            self.primary_shader.uniformf(b"ambient_blue", self.ambience[2])
            #lights = self.get_lights_array()
            #self.primary_shader.uniform_arrayf(b"lights", *lights)
            #self.primary_shader.uniformi(b"lights_size", len(self.lights))

    def unbind(self):
        if self.primary_shader is not None:
            self.primary_shader.unbind()

    def bind_secondary(self, group_name, color):
        for shader_dict in self.secondary_shaders:
            if group_name in shader_dict["groups"]:
                shader = shader_dict["shader"]
                shader.bind()
                shader.uniformf(b"time", self.time)
                r, g, b = color
                shader.uniformf(b"red", r/255)
                shader.uniformf(b"green", g/255)
                shader.uniformf(b"blue", b/255)
                shader.uniformf(b"ambient_red", self.ambience[0])
                shader.uniformf(b"ambient_green", self.ambience[1])
                shader.uniformf(b"ambient_blue", self.ambience[2])

    def unbind_secondary(self, group_name):
        for shader_dict in self.secondary_shaders:
            if group_name in shader_dict["groups"]:
                shader_dict["shader"].unbind()

    def set_shader(self, name):
        shaders = self.shaders.get(name, None)
        if shaders is not None:
            self.selected_shader = name
            self.primary_shader = shaders.get("primary", {}).get("shader", None)
            self.secondary_shaders = shaders.get("secondary", [])
            self.set_ambience(use_last_ambience=True)
        else:
            self.selected_shader = None
            self.primary_shader = None
            self.secondary_shaders = []

    def set_ambience(self, ambience=(1.0, 1.0, 1.0), use_last_ambience=False):
        if not use_last_ambience:
            self.last_ambience = ambience
        _ambience = ambience
        shaders = self.shaders.get(self.selected_shader, None)
        if shaders is not None:
            for name, shader in shaders.items():
                if "ambience" in shader:
                    _ambience = shader.get("ambience", _ambience)
                    use_last_ambience = False
                    break
        self.ambience = _ambience
        if use_last_ambience:
            self.ambience = self.last_ambience

    def create_light(self, x=0, y=0, radius=32, steps=8, color=(255,255,255), exponent=2, light_type=None):
        if light_type == "glow":
            light = GlowLight(x=x, y=y, radius=radius, steps=steps, color=color, exponent=exponent)
        elif light_type == "flickering":
            light = FlickeringLight(x=x, y=y, radius=radius, steps=steps, color=color, exponent=exponent)
        elif light_type == "broken":
            light = BrokenFlickeringLight(x=x, y=y, radius=radius, steps=steps, color=color, exponent=exponent)
        elif light_type == "shimmering":
            light = ShimmeringLight(x=x, y=y, radius=radius, steps=steps, color=color, exponent=exponent)
        else:
            light = Light(x=x, y=y, radius=radius, steps=steps, color=color, exponent=exponent)
        self.lights.add(light)
        return light

    def draw_lights(self, aspect_width, aspect_height):
        lighting_shader.bind()
        lighting_shader.uniformi(b"diffuse_tex", 0)
        lighting_shader.uniformf(b"aspect_width", aspect_width)
        lighting_shader.uniformf(b"aspect_height", aspect_height)
        pyglet.gl.glEnable(pyglet.gl.GL_BLEND)
        pyglet.gl.glBlendFunc(pyglet.gl.GL_ONE, pyglet.gl.GL_ONE)
        for light in self.lights:
            light.draw(self.time)
        lighting_shader.unbind()

    def destroy_lights(self):
        self.lights.clear()

    def destroy_light(self, light):
        if light in self.lights:
            self.lights.remove(light)
