import pyglet
import os
import re
from collections import deque
from it5.common import CommonManager, common_tileset, common_spritemap
from it5.controls import Controls, ControlAction

speaker_pattern = re.compile("\\[(.*)\\]")

class DialogAction:
    NEXT_PAGE = 0
    CLOSE = 1

class DialogPage(object):
    def __init__(self, speaker, lines, dynamic_token_replacer_map):
        self.speaker = speaker
        self.lines = self.populate_dynamic_tokens(lines, dynamic_token_replacer_map)

    @staticmethod
    def populate_dynamic_tokens(lines, dynamic_token_replacer_map):
        for token, replacement in dynamic_token_replacer_map.items():
            lines = lines.replace(token, replacement)
        return lines

class Dialog(object):
    def __init__(self, location, dynamic_token_replacer_map):
        self.pages = []
        f = open(location, 'r')
        raw_dialog = f.read()
        f.close()
        self.finished = False
        self.remote = False
        self.parse(raw_dialog, dynamic_token_replacer_map)

    def next(self):
        if self.hasNext():
            return self.queue.popleft()
        else:
            self.finished = True

    def rewind(self):
        self.queue = deque(self.pages)
        self.finished = False

    def isFinished(self):
        return self.finished

    def hasNext(self):
        return len(self.queue) > 0

    def parse(self, raw_dialog, dynamic_token_replacer_map):
        lines = raw_dialog.split('\n')
        curr_lines = []
        speaker = None
        index = 0
        for line in lines:
            if len(line.strip()) > 0:
                if line.lstrip().startswith('#') == False:
                    speaker_check = speaker_pattern.match(line)
                    if speaker_check != None:
                        if speaker != None:
                            speaker_text = '\n'.join(curr_lines)
                            self.pages.append(DialogPage(speaker, speaker_text, dynamic_token_replacer_map))
                        speaker = speaker_check.groups()[0]
                        curr_lines = []
                    else:
                        curr_lines.append(line)
                elif index == 0:
                    if "remote" in line.strip().lower():
                        self.remote = True
            index += 1
        if speaker != None:
            speaker_text = '\n'.join(curr_lines)
            self.pages.append(DialogPage(speaker, speaker_text, dynamic_token_replacer_map))
        self.queue = deque(self.pages)

    def is_remote(self):
        return self.remote

    def to_dict(self):
        dialog_dict = {
            "finished": self.finished
        }
        return dialog_dict

class DialogManager(object):
    def __init__(self, batch, groups, window, anchor_x):
        self.batch = batch
        self.batchGroups = groups
        self.window = window
        self.dialogs = {}
        self.curr_dialog = None
        self.anchor_x = int(anchor_x)

        self.backgroundColor = (16,16,16,255)
        self.buttonColor = (0,255,127,255)
        self.backgroundPattern = pyglet.image.SolidColorImagePattern(self.backgroundColor)
        self.backgroundImage = self.backgroundPattern.create_image(self.anchor_x, window.height//4)
        self.backgroundSprite = None
        self.speakerLabel = None
        self.speakerDialog = None
        self.buttonLabel = None
        self.radio_indicator = None
        self.buffer = []
        self.elapsed_time = 0
        self.time_to_next = 1/120.0
        self.longer_time_to_next = 0.5
        self.original_time_to_next = self.time_to_next
        self.next_status = "[CONTINUE]"
        # Will replace things like {KEY::UP} with actual key names for UP
        self.dynamic_token_replacer_map = {"{KEY::%s}" % action_name : Controls.get_key_names_str(action) for action_name, action in ControlAction.__members__.items()}
        # Will replace {Player} with the player's name
        self.dynamic_token_replacer_map["{Player}"] = CommonManager.get_player_name()

    def resize(self, anchor_x):
        self.anchor_x = int(anchor_x)
        self.backgroundImage = self.backgroundPattern.create_image(self.anchor_x, self.window.height//4)

    # Only loads dialog relevant to the level
    def loadDialogs(self, dialog_directory):
        working_dir = os.path.realpath(pyglet.resource.location("dialog_index.txt").path)
        dialog_path = os.path.join(working_dir, dialog_directory)
        dlist = os.listdir(dialog_path)
        for dname in dlist:
            dialog_name = str(dname)
            if dialog_name.endswith(".dlg"):
                self.dialogs[dialog_name] = Dialog(os.path.join(dialog_path, dialog_name), self.dynamic_token_replacer_map)

    def rewind(self, dname):
        if dname in self.dialogs:
            self.dialogs[dname].rewind()

    def _parse_speaker(self, speaker):
        if speaker == 'Player':
            return CommonManager.get_player_name()
        return speaker

    def _buffer_dialog(self, lines):
        self.buffer.clear()
        for line in lines:
            self.buffer.append(line)
        self.buffer.reverse()
        self.elapsed_time = 0
        self.time_to_next = self.original_time_to_next

    def openDialog(self, dname):
        if dname in self.dialogs:
            self.curr_dialog = self.dialogs[dname]
            dialogPage = self.curr_dialog.next()
            if self.curr_dialog.isFinished() == False:
                self.backgroundSprite = pyglet.sprite.Sprite(
                    img=self.backgroundImage,
                    batch=self.batch,
                    group=self.batchGroups['dialogBackground']
                )
                speaker_label_xoffset = 0
                speaker_label_yoffset = 0
                if self.curr_dialog.is_remote():
                    self.radio_indicator = pyglet.sprite.Sprite(
                        img=common_tileset[common_spritemap["radio_icon"]],
                        batch=self.batch,
                        group=self.batchGroups["dialogForeground"],
                        x=12,
                        y=self.backgroundSprite.height-32
                    )
                    speaker_label_xoffset = 40
                    speaker_label_yoffset = 8
                self.speakerLabel = pyglet.text.Label(self._parse_speaker(dialogPage.speaker),
                    font_size=12,
                    x=12+speaker_label_xoffset, y=(self.window.height//4)-speaker_label_yoffset,
                    anchor_x='left', anchor_y='top',
                    batch=self.batch,
                    group=self.batchGroups['dialogForeground'],
                    width=self.anchor_x-24, multiline=True
                )
                self.speakerLabel.color = (0,255,63,255)
                self._buffer_dialog(dialogPage.lines)
                self.speakerDialog = pyglet.text.Label('',
                    font_size=12,
                    x=12, y=self.window.height//4 - (self.speakerLabel.content_height * 2) - speaker_label_yoffset,
                    anchor_x='left', anchor_y='top',
                    batch=self.batch,
                    group=self.batchGroups['dialogForeground'],
                    width=self.anchor_x-24, multiline=True
                )
                blText = '[CONTINUE]'
                if self.curr_dialog.hasNext() == False:
                    blText = '[END]'
                self.next_status = blText
                self.buttonLabel = pyglet.text.Label('',
                    font_size=12,
                    x=self.anchor_x-12, y=0,
                    anchor_x='right', anchor_y='bottom',
                    batch=self.batch,
                    group=self.batchGroups['dialogForeground'],
                    color=self.buttonColor
                )
                self.buttonLabel.y = self.buttonLabel.y + (self.buttonLabel.content_height//2)
            else:
                self.curr_dialog = None
        else:
            print("Cannot find dialog " + str(dname))
            self.curr_dialog = None
        return self.curr_dialog != None

    def closeDialog(self):
        if self.curr_dialog != None:
            self.curr_dialog = None
            self.speakerDialog.delete()
            self.speakerLabel.delete()
            self.buttonLabel.delete()
            if self.radio_indicator is not None:
                self.radio_indicator.delete()
                self.radio_indicator = None
            self.backgroundSprite.delete()

    def isOpen(self):
        return self.curr_dialog != None

    def nextPage(self):
        if self.curr_dialog.hasNext():
            dialogPage = self.curr_dialog.next()
            self._buffer_dialog(dialogPage.lines)
            self.speakerDialog.text = ''
            self.speakerLabel.text = self._parse_speaker(dialogPage.speaker)
            self.buttonLabel.text = ''
            if self.curr_dialog.hasNext() == False:
                self.next_status = '[END]'
            return True
        else:
            self.closeDialog()
        return False

    def update(self, dt):
        if self.curr_dialog is not None:
            if len(self.buffer) > 0:
                if self.elapsed_time >= self.time_to_next:
                    element = self.buffer.pop()
                    self.speakerDialog.text += element
                    if element == '.' or element == '!' or element == '?':
                        self.time_to_next = self.longer_time_to_next
                    else:
                        self.time_to_next = self.original_time_to_next
                    self.elapsed_time = 0
                else:
                    self.elapsed_time += dt
                if len(self.buffer) == 0:
                    self.buttonLabel.text = self.next_status

    def on_key_press(self, action, modifiers):
        if action == ControlAction.FIRE or action == ControlAction.MELEE or action == ControlAction.TOGGLE_PAUSE:
            fast_forwarded = False
            if self.curr_dialog is not None:
                if len(self.buffer) > 0:
                    fast_forwarded = True
                    self.buffer.reverse()
                    self.speakerDialog.text += ''.join(self.buffer)
                    self.buffer.clear()
                    self.buttonLabel.text = self.next_status
            if not fast_forwarded:
                if self.nextPage():
                    return DialogAction.NEXT_PAGE
                else:
                    return DialogAction.CLOSE
        elif action == ControlAction.EXIT:
            self.closeDialog()
            return DialogAction.CLOSE

    def to_dict(self):
        dialogs_dict = {}
        for dialog in self.dialogs:
            dialogs_dict[dialog] = self.dialogs[dialog].to_dict()
        return dialogs_dict

    def from_dict(self, dialogs_dict):
        for dialog in dialogs_dict:
            self.dialogs[dialog].finished = dialogs_dict[dialog]["finished"]
