import copy
import pyglet
from it5.controls import Controls, ControlAction
from it5.common import common_spritemap, common_tileset, EventManager
from heapq import heappush, heappop

class Stage(object):
    def __init__(self, name, title, order, completed=False):
        self.name = name
        self.title = title
        '''
        As a convention, ordering should be initially incremented by 10
        in case we want to insert additional stages later without having
        to change the order on every subsequent stage
        '''
        self.order = order
        self.completed = completed

    def __lt__(self, other):
        # For sorting with a heapq
        return self.order < other.order

class Quest(object):
    def __init__(self, name, title):
        self.name = name
        self.title = title
        self.stages = {}
        self.active = False
        self.data = {}

    def addStage(self, stage):
        self.stages[stage.name] = stage

    def isStageComplete(self, stage_name):
        if stage_name in self.stages:
            return self.stages[stage_name].completed
        print('Warning: cannot find stage "' + str(stage_name) + '" in quest "' + str(self.name) + '"')
        return False

    def completeStage(self, stage_name):
        curr_stage = self.getNextStage()
        if stage_name in self.stages:
            stage = self.stages[stage_name]
            if stage == curr_stage:
                stage.completed = True
                title = stage_name
                if stage.title is not None:
                    title = stage.title
                EventManager.set_notification('Objective Completed: ' + title)
                if self.getProgress() == 100:
                    EventManager.set_notification('Completed Mission: ' + self.title)
                return True
            else:
                if curr_stage is not None:
                    print("Complete objective %s first" % curr_stage.name)
                else:
                    print("Quest is complete")
        else:
            print('Warning: cannot find stage "' + str(stage_name) + '" in quest "' + str(self.name) + '"')
        return False

    def forceCompleteStage(self, stage_name):
        if stage_name in self.stages:
            ordered_stages = self.getStagesOrdered()
            for stage in ordered_stages:
                if not stage.completed:
                    self.completeStage(stage.name)
                if stage.name == stage_name:
                    break
        else:
            print("Stage %s in quest %s does not exist" % (str(stage_name), str(self.name)))

    def getProgress(self):
        if len(self.stages) == 0:
            return 100
        completed_count = 0
        for key in self.stages:
            if self.stages[key].completed:
                completed_count += 1
        if completed_count == len(self.stages):
            return 100
        return int((float(completed_count)/len(self.stages)*100))

    def getNextStage(self):
        min_order = float("inf")
        next_stage = None
        for key in self.stages:
            stage = self.stages[key]
            if not stage.completed:
                if stage.order < min_order:
                    min_order = stage.order
                    next_stage = stage
        return next_stage

    def getStagesOrdered(self):
        unordered = []
        for key in self.stages:
            unordered.append(self.stages[key])
        return sorted(unordered, key = lambda s: s.order)

    def setActive(self, active):
        self.active = active
        if self.active:
            EventManager.set_notification('New Mission: ' + self.title)

class QuestManager:
    quests = {}
    quests_copy = {}
    main_quest_name = "main"
    active_quest = None

    @classmethod
    def initialize(cls, quest_names=None):
        cls.quests = {}
        cls.quests_copy = {}
        '''
        Every level must have a main quest
        The main quest is always active
        '''
        if quest_names != None:
            for key in quest_names:
                quest = Quest(key, quest_names[key])
                cls.addQuest(quest)
            if cls.main_quest_name not in quest_names:
                cls.addQuest(Quest(cls.main_quest_name, 'Unknown Mission'))
        else:
            cls.addQuest(Quest(cls.main_quest_name, 'Unknown Mission'))
        cls.active_quest = cls.main_quest_name
    
    @classmethod
    def addQuest(cls, quest):
        cls.quests[quest.name] = quest
        if quest.name == cls.main_quest_name:
            quest.active = True
    
    @classmethod
    def getQuest(cls, quest_name):
        if quest_name in cls.quests:
            return cls.quests[quest_name]
        print('Could not find quest "' + str(quest_name) + '"')
        return None
    
    @classmethod
    def getMainQuest(cls):
        return cls.quests[cls.main_quest_name]
    
    @classmethod
    def getCompletedQuests(cls):
        completed = []
        for key in cls.quests:
            if cls.quests[key].getProgress >= 100:
                completed.append(key)
        return completed
    
    @classmethod
    def getCompletionSummary(cls):
        summary = {}
        for key in cls.quests:
            quest = cls.quests[key]
            if quest.active:
                summary[key] = quest.getProgress()
        return summary
    
    @classmethod
    def getActiveQuests(cls):
        active_quests = {}
        for key in cls.quests:
            quest = cls.quests[key]
            if quest.active:
                active_quests[key] = quest
        return active_quests

    @classmethod
    def _to_dict(cls, quests):
        active_quests = {}
        for key in quests:
            quest = quests[key]
            if quest.active:
                quest_dict = vars(quest)
                simplified_quest = copy.deepcopy(quest_dict)
                simplified_quest['stages'] = {}
                stages = quest_dict['stages']
                sorted_stages = []
                for stage_key in stages:
                    heappush(sorted_stages, stages[stage_key])
                while sorted_stages:
                    stage = heappop(sorted_stages)
                    simplified_quest['stages'][stage.name] = vars(stage)
                active_quests[key] = simplified_quest
        return active_quests

    @classmethod
    def getActiveQuestsJSON(cls):
        return cls._to_dict(cls.quests)

    @classmethod
    def to_dict(cls):
        return cls._to_dict(cls.quests_copy)

    @classmethod
    def from_dict(cls, quests_dict, active_quest):
        for quest_name in quests_dict:
            _quest = quests_dict[quest_name]
            quest = cls.quests[quest_name]
            quest.setActive(True)
            quest.data = _quest.get('data', {})
            stages = _quest['stages']
            for stage_key in stages:
                quest.stages[stage_key].completed = stages[stage_key]['completed']
        cls.active_quest = active_quest
        cls.saveCopy()

    @classmethod
    def saveCopy(cls):
        cls.quests_copy = copy.deepcopy(cls.quests)
        cls.last_active_quest = str(cls.active_quest)

    @classmethod
    def loadCopy(cls):
        cls.quests = copy.deepcopy(cls.quests_copy)
        cls.active_quest = str(cls.last_active_quest)

    @classmethod
    def checkObjectiveSatisfaction(cls, quests):
        for quest_name in quests:
            quest = cls.getQuest(quest_name)
            if quest == None:
                return False
            stages_to_check = quests[quest_name]
            for stage_name in stages_to_check:
                if quest.isStageComplete(stage_name) == False:
                    return False
        return True

class QuestUIAction:
    SELECT = 0
    PREV_PAGE = 1
    NEXT_PAGE = 2
    CLOSE = 3

class QuestUI:
    batch = None
    batchGroups = None
    window = None
    is_open = False
    backgroundColor = (0,0,0,255)
    buttonColor = (63,0,0,255)
    initialized = False

    @classmethod
    def initialize(cls, batch, groups, window, anchor_x):
        cls.batch = batch
        cls.batchGroups = groups
        cls.window = window
        cls.is_open = False
        cls.anchor_x = int(anchor_x)
        cls.backgroundPattern = pyglet.image.SolidColorImagePattern(cls.backgroundColor)
        cls.backgroundImage = cls.backgroundPattern.create_image(cls.anchor_x, window.height)
        cls.backgroundSprite = None
        cls.current_quest_ui = []
        cls.initialized = True

    @classmethod
    def resize(cls, anchor_x):
        if cls.initialized:
            cls.anchor_x = int(anchor_x)
            cls.backgroundPattern = pyglet.image.SolidColorImagePattern(cls.backgroundColor)
            cls.backgroundImage = cls.backgroundPattern.create_image(cls.anchor_x, cls.window.height)

    @classmethod
    def isOpen(cls):
        return cls.is_open

    @classmethod
    def clearWidgets(cls):
        for widget in cls.current_quest_ui:
            widget.delete()

    @classmethod
    def close(cls):
        cls.clearWidgets()
        cls.backgroundSprite.delete()
        cls.is_open = False

    @classmethod
    def getQuestUI(cls, quest_name):
        quest = cls.all_quests[quest_name]
        widgets = []
        titleText = quest.title
        titleLabel = pyglet.text.Label(titleText,
            font_size=18,
            x=cls.anchor_x//2, y=cls.window.height,
            anchor_x='center', anchor_y='top',
            batch=cls.batch,
            group=cls.batchGroups['dialogForeground']
        )
        titleLabel.color = (255,255,255,255)
        widgets.append(titleLabel)

        if len(cls.sorted_quest_names) > 1:
            left = pyglet.sprite.Sprite(
                img=common_tileset[common_spritemap['left']],
                batch=cls.batch,
                group=cls.batchGroups['dialogForeground']
            )
            right = pyglet.sprite.Sprite(
                img=common_tileset[common_spritemap['right']],
                batch=cls.batch,
                group=cls.batchGroups['dialogForeground']
            )
            left.position = (0, cls.window.height - 22)
            right.position = (cls.anchor_x-16, cls.window.height - 22)
            left.scale_x = 0.5
            left.scale_y = 0.5
            right.scale_x = 0.5
            right.scale_y = 0.5
            widgets.append(left)
            widgets.append(right)

        isActiveText = "Active"
        if quest.name != QuestManager.active_quest:
            isActiveText = "Press %s to make this the active mission" % Controls.get_key_names_str(ControlAction.FIRE)
        activeLabel = pyglet.text.Label(isActiveText,
            font_size=10,
            x=cls.anchor_x//2, y=cls.window.height - 30,
            anchor_x='center', anchor_y='top',
            batch=cls.batch,
            group=cls.batchGroups['dialogForeground']
        )
        if isActiveText == "Active":
            activeLabel.color = (0,255,63,255)
        else:
            activeLabel.color = (255,255,63,255)
        widgets.append(activeLabel)

        objectivesText = "Objectives: (" + str(quest.getProgress()) + "% complete)"
        objectivesLabel = pyglet.text.Label(objectivesText,
            font_size=14,
            x=12, y=cls.window.height - 60,
            anchor_x='left', anchor_y='top',
            batch=cls.batch,
            group=cls.batchGroups['dialogForeground'],
            width=cls.anchor_x-24, multiline=True
        )
        objectivesLabel.color = (0,255,255,255)
        widgets.append(objectivesLabel)

        stages = quest.getStagesOrdered()
        completed_stages = []
        for stage in stages:
            if stage.completed:
                completed_stages.append(stage.title)

        offset = 90
        current_stage = quest.getNextStage()
        currentLabel = None
        if current_stage is not None:
            currentLabel = pyglet.text.Label(current_stage.title,
                font_size=12,
                x=24, y=cls.window.height - offset,
                anchor_x='left', anchor_y='top',
                batch=cls.batch,
                group=cls.batchGroups['dialogForeground'],
                width=cls.anchor_x-24, multiline=True
            )
            currentLabel.color = (0,255,63,255)
            widgets.append(currentLabel)
            offset += 30

        recently_completed = completed_stages[-9:]
        recently_completed.reverse()

        for recent in recently_completed:
            completedLabel = pyglet.text.Label("[COMPLETE] " + recent,
                font_size=12,
                x=24, y=cls.window.height - offset,
                anchor_x='left', anchor_y='top',
                batch=cls.batch,
                group=cls.batchGroups['dialogForeground'],
                width=cls.anchor_x-24, multiline=True
            )
            completedLabel.color = (255,255,255,255)
            widgets.append(completedLabel)
            offset += 30

        instructions = "Press %s to return to the game" % Controls.get_key_names_str(ControlAction.TOGGLE_MISSION_VIEWER)
        instructionsLabel = pyglet.text.Label(instructions,
            font_size=12,
            x=12, y=0,
            anchor_x='left', anchor_y='bottom',
            batch=cls.batch,
            group=cls.batchGroups['dialogForeground'],
            width=cls.anchor_x-24, multiline=True
        )
        instructionsLabel.color = (255,255,255,255)
        widgets.append(instructionsLabel)

        if len(cls.sorted_quest_names) > 1:
            additional_instructions = "Press %s to view previous mission, %s to view next mission" % (Controls.get_key_names_str(ControlAction.LEFT), Controls.get_key_names_str(ControlAction.RIGHT))
            additionalInstructionsLabel = pyglet.text.Label(additional_instructions,
                font_size=12,
                x=12, y=instructionsLabel.content_height,
                anchor_x='left', anchor_y='bottom',
                batch=cls.batch,
                group=cls.batchGroups['dialogForeground'],
                width=cls.anchor_x-24, multiline=True
            )
            additionalInstructionsLabel.color = (255,255,255,255)
            widgets.append(additionalInstructionsLabel)

        cls.current_quest_ui = widgets

    @classmethod
    def open(cls):
        cls.backgroundSprite = pyglet.sprite.Sprite(
            img=cls.backgroundImage,
            batch=cls.batch,
            group=cls.batchGroups['dialogBackground']
        )
        cls.all_quests = QuestManager.getActiveQuests()
        cls.sorted_quest_names = []
        cls.sorted_quest_index = 0
        idx = 0
        for key, value in sorted(cls.all_quests.items()):
            cls.sorted_quest_names.append(key)
            if key == QuestManager.active_quest:
                cls.sorted_quest_index = idx
            idx += 1
        cls.getQuestUI(QuestManager.active_quest)
        cls.is_open = True

    @classmethod
    def next(cls):
        if cls.sorted_quest_index + 1 >= len(cls.sorted_quest_names):
            cls.sorted_quest_index = 0
        else:
            cls.sorted_quest_index += 1
        cls.clearWidgets()
        to_show = cls.sorted_quest_names[cls.sorted_quest_index]
        cls.getQuestUI(to_show)

    @classmethod
    def prev(cls):
        if cls.sorted_quest_index - 1 < 0:
            cls.sorted_quest_index = len(cls.sorted_quest_names) - 1
        else:
            cls.sorted_quest_index -= 1
        cls.clearWidgets()
        to_show = cls.sorted_quest_names[cls.sorted_quest_index]
        cls.getQuestUI(to_show)

    @classmethod
    def select(cls):
        cls.clearWidgets()
        to_show = cls.sorted_quest_names[cls.sorted_quest_index]
        QuestManager.active_quest = to_show
        cls.getQuestUI(to_show)

    @classmethod
    def on_key_press(cls, action, modifiers):
        if action == ControlAction.TOGGLE_MISSION_VIEWER:
            # Close the objectives viewer
            cls.close()
            return QuestUIAction.CLOSE
        elif action == ControlAction.LEFT:
            # View previous quest
            cls.prev()
            return QuestUIAction.PREV_PAGE
        elif action == ControlAction.RIGHT:
            # View next quest
            cls.next()
            return QuestUIAction.NEXT_PAGE
        elif action == ControlAction.FIRE:
            # Select active quest
            cls.select()
            return QuestUIAction.SELECT
