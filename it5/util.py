from it5.direction import Direction
import math
import heapq
from multiprocessing import Process, Queue
from threading import Thread
from it5.waypoint import pathCreator

FOV_ANGLE_DEGREES = 60
HALF_FOV_ANGLE_RADIANS = (FOV_ANGLE_DEGREES / 2.0) * math.pi / 180.0


def _is_player_concealed(x, y, vx, vy, distance, curr_level_map):
    concealed = False
    check_x = x
    check_y = y
    traveled = 0
    last_zx = -1
    last_zy = -1
    while traveled <= distance and concealed == False:
        check_x += vx
        check_y += vy
        zx = int(check_x / 32)
        zy = int(check_y / 32)
        if zx != last_zx or zy != last_zy:
            if zy >= 0 and zy < len(curr_level_map.obstacleMatrix) and zx >= 0 and zx < len(curr_level_map.obstacleMatrix[0]):
                if curr_level_map.obstacleMatrix[zy][zx] == 0:
                    if ((zx ^ x) | (zy ^ y)) != 0:
                        concealed = True
                else:
                    # Check doors
                    for door in curr_level_map.doors:
                        if door.isClosed():
                            if (zx == door.tileX or zx == door.tileX + (door.width-1)) and zy == door.tileY:
                                concealed = True
        last_zx = zx
        last_zy = zy
        dx = check_x - x
        dy = check_y - y
        traveled = math.sqrt(float(dx * dx) + float(dy * dy))
    return concealed

def is_player_concealed(x, y, vx, vy, distance, curr_level_map):
    return _is_player_concealed(x, y, vx, vy, distance, curr_level_map) and _is_player_concealed(x, y+15, vx, vy, distance, curr_level_map)

def getHalfVisionConeHeight(viewDistance):
    return viewDistance * math.tan(HALF_FOV_ANGLE_RADIANS)

def searchForPlayer(npc_x, npc_y, direction, view_distance, curr_level_map, player, is_camera=False, check_walls=True):
    tx = int((npc_x + 15)/32)
    ty = int((npc_y - 31)/32)
    nx = npc_x + 15
    ny = npc_y - 15
    # If the player and npc share the same tile, they can be spotted
    if player.getTileX() == tx and player.getTileY() == ty and is_camera == False:
        return True
    # IF the player is under tall grass or underwater, they are invisible
    if player.concealed:
        return False
    dir_x = 0
    dir_y = 0
    if direction == Direction.UP:
        dir_y = 1.0
    elif direction == Direction.DOWN:
        dir_y = -1.0
    elif direction == Direction.LEFT:
        dir_x = -1.0
    elif direction == Direction.RIGHT:
        dir_x = 1.0
    x = (player.x + 15) - nx
    y = (player.y - 15) - ny
    distance = math.sqrt(float(x * x) + float(y * y))
    vx = x / distance
    vy = y / distance
    dot_product = (dir_x * vx) + (dir_y * vy)
    angle = math.acos(dot_product)
    # Player is in FOV
    if angle <= HALF_FOV_ANGLE_RADIANS and distance <= view_distance:
        # Player is theoretically visible
        if not check_walls:
            return True
        if not is_player_concealed(int(nx), int(ny), vx, vy, distance, curr_level_map):
            # Player is absolutely visible
            return True
    return False

class Cell(object):
    def __init__(self, x, y, reachable):
        """Initialize new cell.
        @param reachable is cell reachable? not a wall?
        @param x cell x coordinate
        @param y cell y coordinate
        @param g cost to move from the starting cell to this cell.
        @param h estimation of the cost to move from this cell
                 to the ending cell.
        @param f f = g + h
        """
        self.reachable = reachable
        self.x = x
        self.y = y
        self.parent = None
        self.g = 0
        self.h = 0
        self.f = 0

    def __lt__(self, other):
        return self.f < other.f

class AStar(object):
    def __init__(self, obstacleMatrix):
        # open list
        self.opened = []
        heapq.heapify(self.opened)
        # visited cells list
        self.closed = set()
        # grid cells
        self.cells = []
        self.grid_height = None
        self.grid_width = None
        self.init_grid(obstacleMatrix)

    def init_grid(self, obstacleMatrix):
        """Prepare grid cells, walls.
        @param obstacleMatrix obstacle matrix from game
        @param start grid starting point x,y tuple.
        @param end grid ending point x,y tuple.
        """
        self.grid_height = len(obstacleMatrix)
        self.grid_width = len(obstacleMatrix[0])

        for y in range(self.grid_height):
            for x in range(self.grid_width):
                if obstacleMatrix[y][x] == 0:
                    reachable = False
                else:
                    reachable = True
                self.cells.append(Cell(x, y, reachable))

    def get_heuristic(self, cell):
        """Compute the heuristic value H for a cell.
        Distance between this cell and the ending cell multiply by 10.
        @returns heuristic value H
        """
        return 10 * (abs(cell.x - self.end.x) + abs(cell.y - self.end.y))

    def get_cell(self, x, y):
        """Returns a cell from the cells list.
        @param x cell x coordinate
        @param y cell y coordinate
        @returns cell
        """
        return self.cells[y * self.grid_width + x]

    def get_adjacent_cells(self, cell):
        """Returns adjacent cells to a cell.
        Clockwise starting from the one on the right.
        @param cell get adjacent cells for this cell
        @returns adjacent cells list.
        """
        cells = []
        if cell.x < self.grid_width-1:
            cells.append(self.get_cell(cell.x+1, cell.y))
        if cell.y > 0:
            cells.append(self.get_cell(cell.x, cell.y-1))
        if cell.x > 0:
            cells.append(self.get_cell(cell.x-1, cell.y))
        if cell.y < self.grid_height-1:
            cells.append(self.get_cell(cell.x, cell.y+1))
        return cells

    def get_path(self):
        cell = self.end
        path = [(cell.x, cell.y)]
        try:
            while cell.parent is not self.start:
                if cell != None:
                    cell = cell.parent
                    if cell != None:
                        path.append((cell.x, cell.y))
        except:
            pass

        path.append((self.start.x, self.start.y))
        return path

    def update_cell(self, adj, cell):
        """Update adjacent cell.
        @param adj adjacent cell to current cell
        @param cell current cell being processed
        """
        adj.g = cell.g + 10
        adj.h = self.get_heuristic(adj)
        adj.parent = cell
        adj.f = adj.h + adj.g

    def solve(self, start, end):
        """Solve maze, find path to ending cell.
        @param start grid starting point x,y tuple.
        @param end grid ending point x,y tuple.
        @returns path or None if not found.
        """

        # Reset the vertices
        while len(self.opened) > 0:
            f, cell = heapq.heappop(self.opened)
            if cell != None:
                cell.parent = None
                cell.g = 0
                cell.h = 0
                cell.f = 0
        for cell in self.closed:
            cell.parent = None
            cell.g = 0
            cell.h = 0
            cell.f = 0
        self.closed.clear()
        self.start = self.get_cell(*end)
        self.end = self.get_cell(*start)

        if self.start == self.end:
            return [(self.start.x, self.start.y)]

        # add starting cell to open heap queue
        heapq.heappush(self.opened, (self.start.f, self.start))
        while len(self.opened):
            # pop cell from heap queue
            f, cell = heapq.heappop(self.opened)
            # add cell to closed list so we don't process it twice
            self.closed.add(cell)
            # if ending cell, return found path
            if cell is self.end:
                return self.get_path()
            # get adjacent cells for cell
            adj_cells = self.get_adjacent_cells(cell)
            for adj_cell in adj_cells:
                if adj_cell.reachable and adj_cell not in self.closed:
                    if (adj_cell.f, adj_cell) in self.opened:
                        # if adj cell in open list, check if current path is
                        # better than the one previously found
                        # for this adj cell.
                        if adj_cell.g > cell.g + 10:
                            self.update_cell(adj_cell, cell)
                    else:
                        self.update_cell(adj_cell, cell)
                        # add adj cell to open list
                        heapq.heappush(self.opened, (adj_cell.f, adj_cell))

def pathfinder_processor(outgoing, incoming):
    running = True
    pathfinder = AStar([[1]])
    print("Pathfinder process has started")
    while running:
        message = incoming.get()
        if message is None:
            running = False
            outgoing.put(None)
            break
        msg_type, payload = message
        #print("pathfinder_processor: %s" % msg_type)
        if msg_type == "change_map":
            pathfinder = AStar(payload)
        elif msg_type == "find_path":
            future_id, source, destination = payload
            nodes = pathfinder.solve(source, destination)
            if nodes != None:
                path = pathCreator(nodes)
            else:
                path = pathCreator([source])
            outgoing.put((future_id, path))
    print("Pathfinder process has terminated")

class PathfindingFuture:
    def __init__(self):
        self._done = False
        self._result = None

    def done(self):
        return self._done

    def result(self):
        return self._result

class PathfindingProcessManager:
    pathfinder = None
    rolling_id = 0
    running = True
    futures = {}
    incoming = Queue()
    outgoing = Queue()

    @classmethod
    def initialize(cls):
        print("initialize() called")
        if cls.pathfinder is None:
            cls.pathfinder = Process(target=pathfinder_processor, args=(cls.incoming, cls.outgoing))
            cls.pathfinder.start()

    @classmethod
    def monitor_nowait(cls):
        while not cls.incoming.empty():
            message = cls.incoming.get_nowait()
            if message is not None:
                future_id, nodes = message
                future = cls.futures.pop(future_id, None)
                if future is not None:
                    future._result = nodes
                    future._done = True

    @classmethod
    def change_map(cls, obstacle_matrix):
        cls.outgoing.put(("change_map", obstacle_matrix))

    @classmethod
    def find_path(cls, source, destination):
        my_id = cls.rolling_id
        cls.rolling_id += 1
        future = PathfindingFuture()
        cls.futures[my_id] = future
        cls.outgoing.put(("find_path", (my_id, source, destination)))
        return future

    @classmethod
    def shutdown(cls):
        cls.outgoing.put(None)
        cls.pathfinder.join()