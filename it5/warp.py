class Warp(object):
    def __init__(self, x, y, warpX, warpY, mapIndex, levelIndex=None, elevatorRef=None):
        self.x = x*32
        self.y = y*32
        self.tileX = x
        self.tileY = y
        self.warpX = warpX
        self.warpY = warpY
        self.mapIndex = mapIndex
        self.levelIndex = levelIndex
        self.alreadyIn = False
        self.elevatorRef = elevatorRef

    def check(self, playerTileX, playerTileY):
        if self.tileX == playerTileX and self.tileY == playerTileY:
            if self.alreadyIn == False:
                self.alreadyIn = True
                return True
        else:
            self.alreadyIn = False
        return False

    def update_destination(self, warp_x, warp_y, map_index):
        self.warpX = warp_x
        self.warpY = warp_y
        self.mapIndex = map_index