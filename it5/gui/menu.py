import pyglet
from it5.controls import ControlAction
from pyglet.window import key
from it5.common import batches, batchGroups
from it5.gui.widget import Widget


class Menu(Widget):
    # options will be a list of tuples in the form (text, callback)
    def __init__(self, title, options, x=0, y=0, width=768, height=768,
                title_font_size=16, option_font_size=12,
                background_color=(64,0,0,255), button_color=(0,0,0,255),
                title_background_color=(32,0,0,255),
                option_font_color=(255,255,255,255), title_font_color=(255,255,255,255),
                exit_option="Quit", exit_callback=None, anchor_x="left", anchor_y="bottom",
                background_batch_group_name="menuBackground",
                foreground_batch_group_name="menuForeground",
                buttons_batch_group_name="menuButtons"):
        super(Menu, self).__init__(x, y, width, height, anchor_x=anchor_x, anchor_y=anchor_y)
        menu_background_pattern = pyglet.image.SolidColorImagePattern(background_color)
        title_background_pattern = pyglet.image.SolidColorImagePattern(title_background_color)
        menu_button_pattern = pyglet.image.SolidColorImagePattern(button_color)
        menu_background_image = menu_background_pattern.create_image(width, height)
        batch = batches["menu"]
        batch_groups = batchGroups
        self.options = options
        if exit_option is not None:
            self.options.append((exit_option, exit_callback))
        self.option_labels = []
        start_y = self.y + height
        self.menu_button_sprites = []
        self.selected_index = 0
        self.menu_background_sprite = pyglet.sprite.Sprite(
            img=menu_background_image,
            batch=batch,
            group=batch_groups[background_batch_group_name]
        )
        self.menu_background_sprite.position = (self.x, self.y)
        self.title_label = pyglet.text.Label(title,
            font_size=title_font_size,
            x=self.x + width//2, y=start_y,
            anchor_x='center', anchor_y='top', batch=batch,
            bold=True, color=title_font_color,
            group=batch_groups[foreground_batch_group_name]
        )
        title_background_image = title_background_pattern.create_image(width, int(self.title_label.content_height * 1.5))
        self.title_background_sprite = pyglet.sprite.Sprite(
            img=title_background_image,
            batch=batch,
            group=batch_groups[buttons_batch_group_name]
        )
        self.title_background_sprite.position = (self.x, self.title_label.y - self.title_label.content_height)
        curr_step = self.title_label.content_height * 2
        for option, callback in self.options:
            option_label = pyglet.text.Label(option,
                font_size=option_font_size,
                x=self.x + width//2, y=start_y - curr_step,
                anchor_x='center', anchor_y='center', batch=batch,
                color=option_font_color,
                group=batch_groups[foreground_batch_group_name]
            )
            self.option_labels.append(option_label)
            curr_step += (option_label.content_height * 2)
            menu_button_image = menu_button_pattern.create_image(width, int(option_label.content_height * 1.5))
            menu_button_sprite = pyglet.sprite.Sprite(
                img=menu_button_image,
                batch=batch,
                group=batch_groups[buttons_batch_group_name]
            )
            menu_button_sprite.position = (self.x, option_label.y - (option_label.content_height//2 + option_label.content_height//4))
            self.menu_button_sprites.append(menu_button_sprite)
            menu_button_sprite.visible = False
        self.menu_button_sprites[self.selected_index].visible = True

    def change_selection(self, direction):
        next_index = self.selected_index + direction
        if next_index < 0:
            next_index = len(self.options) - 1
        if next_index >= len(self.options):
            next_index = 0
        self.selected_index = next_index
        for mbs in self.menu_button_sprites:
            mbs.visible = False
        self.menu_button_sprites[self.selected_index].visible = True

    def on_key_press(self, action, symbol, modifiers):
        # UP and DOWN keys will ALWAYS work for menus
        if symbol == key.UP:
            self.change_selection(-1)
        elif symbol == key.DOWN:
            self.change_selection(1)
        elif action == ControlAction.UP:
            self.change_selection(-1)
        elif action == ControlAction.DOWN:
            self.change_selection(1)
        elif action == ControlAction.FIRE or action == ControlAction.MELEE or action == ControlAction.TOGGLE_PAUSE:
            option, callback = self.options[self.selected_index]
            if callback is not None:
                callback(option)
        elif action == ControlAction.EXIT:
            option, callback = self.options[-1]
            if callback is not None:
                callback(option)

    def delete(self):
        for ol in self.option_labels:
            ol.delete()
        self.option_labels = []
        for mbs in self.menu_button_sprites:
            mbs.delete()
        self.menu_button_sprites = []
        self.title_background_sprite.delete()
        self.title_label.delete()
        self.menu_background_sprite.delete()