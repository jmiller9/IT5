import pyglet
from it5.controls import ControlAction
from it5.common import batches, batchGroups, common_tileset, common_spritemap
from it5.gui.widget import Widget


class Popup(Widget):
    def __init__(self, title, text, x=0, y=768, width=768, height=768,
                title_font_size=16, font_size=12,
                background_color=(64,0,0,255), button_color=(0,0,0,255),
                title_background_color=(32,0,0,255),
                font_color=(255,255,255,255), title_font_color=(255,255,255,255),
                exit_option="OK", exit_callback=None, anchor_x="left", anchor_y="bottom"):
        super(Popup, self).__init__(x, y, width, height, anchor_x=anchor_x, anchor_y=anchor_y)
        background_pattern = pyglet.image.SolidColorImagePattern(background_color)
        title_background_pattern = pyglet.image.SolidColorImagePattern(title_background_color)
        button_pattern = pyglet.image.SolidColorImagePattern(button_color)
        scrollbar_pattern = pyglet.image.SolidColorImagePattern((127,127,127,255))
        background_image = background_pattern.create_image(width, height)
        batch = batches["menu"]
        batch_groups = batchGroups
        ok_label_padding = 4
        text_layout_padding = 2
        self.exit_callback = exit_callback
        self.background_sprite = pyglet.sprite.Sprite(
            img=background_image,
            batch=batch,
            group=batch_groups['dialogBackground']
        )
        self.background_sprite.position = (self.x, self.y)
        self.title_label = pyglet.text.Label(title,
            font_size=title_font_size,
            x=self.x + width//2, y=self.y + height,
            anchor_x='center', anchor_y='top', batch=batch,
            bold=True, color=title_font_color,
            group=batch_groups['dialogForeground']
        )
        title_background_image = title_background_pattern.create_image(width, int(self.title_label.content_height * 1.5))
        self.title_background_sprite = pyglet.sprite.Sprite(
            img=title_background_image,
            batch=batch,
            group=batch_groups['dialogButtons']
        )
        self.title_background_sprite.position = (self.x, self.title_label.y - self.title_label.content_height)

        self.ok_label = pyglet.text.Label(exit_option,
            font_size=12,
            x=self.x + width//2, y=self.y + ok_label_padding,
            anchor_x='center', anchor_y='bottom',
            batch=batch,
            group=batch_groups['dialogForeground']
        )
        ok_button_padding = 16
        ok_button_image = button_pattern.create_image(self.ok_label.content_width + ok_button_padding, int(self.ok_label.content_height * 1.5))
        self.ok_button_sprite = pyglet.sprite.Sprite(
            img=ok_button_image,
            batch=batch,
            group=batch_groups['dialogButtons']
        )
        self.ok_button_sprite.position = (self.ok_label.x - (self.ok_label.content_width//2) - ok_button_padding/2, self.ok_label.y - ok_label_padding)

        tl_height = int(height - (self.title_label.content_height * 1.5) - (self.ok_label.content_height * 1.5) - text_layout_padding)

        scrollbar_image = scrollbar_pattern.create_image(16, int(tl_height))
        self.scrollbar = pyglet.sprite.Sprite(
            img=scrollbar_image,
            batch=batch,
            group=batch_groups['dialogButtons']
        )
        self.scrollbar.position = (self.x + width - 16, self.y + self.ok_label.content_height * 1.5)
        self.scrollbar.opacity = 127

        self.max_y = self.y + (height - self.title_label.content_height * 1.5) - 32 - text_layout_padding
        self.min_y = self.scrollbar.y + 16

        self.up_arrow = pyglet.sprite.Sprite(
            img=common_tileset[common_spritemap["up"]],
            batch=batch,
            group=batch_groups['dialogForeground']
        )
        self.up_arrow.scale_x = 0.5
        self.up_arrow.scale_y = 0.5
        self.up_arrow.position = (self.x + width - 16, self.max_y + 16)

        self.down_arrow = pyglet.sprite.Sprite(
            img=common_tileset[common_spritemap["down"]],
            batch=batch,
            group=batch_groups['dialogForeground']
        )
        self.down_arrow.scale_x = 0.5
        self.down_arrow.scale_y = 0.5
        self.down_arrow.position = (self.x + width - 16, self.scrollbar.y)
    
        self.scrollbar_indicator = pyglet.sprite.Sprite(
            img=common_tileset[common_spritemap["bar"]],
            batch=batch,
            group=batch_groups['dialogForeground']
        )
        self.scrollbar_indicator.scale_x = 0.5
        self.scrollbar_indicator.scale_y = 0.5
        self.scrollbar_indicator.position = (self.x + width - 16, self.max_y)

        document = pyglet.text.document.FormattedDocument(text)
        self.text_layout = pyglet.text.layout.ScrollableTextLayout(
            document, width - (16 + text_layout_padding + 1), tl_height, multiline=True, batch=batch, group=batch_groups['dialogForeground']
        )
        self.text_layout.x = int(self.x + text_layout_padding)
        self.text_layout.y = int(self.y + self.title_label.content_height)
        document.set_style(0, len(text), dict(font_size=font_size, color=font_color, background_color=background_color))
        if self.text_layout.content_height <= tl_height:
            self.up_arrow.visible = False
            self.down_arrow.visible = False
            self.scrollbar.visible = False
            self.scrollbar_indicator.visible = False

    def update_scrollbar_indicator_position(self):
        denominator = self.text_layout.height - self.text_layout.content_height
        pos = 0
        if denominator != 0:
            pos = self.text_layout.view_y / denominator
        actual_y = self.max_y - (pos * (self.max_y - self.min_y))
        self.scrollbar_indicator.position = (self.scrollbar_indicator.x, actual_y)

    def on_key_press(self, action, symbol, modifiers):
        if action == ControlAction.UP:
            self.text_layout.view_y += self.text_layout.height
            self.update_scrollbar_indicator_position()
        elif action == ControlAction.DOWN:
            self.text_layout.view_y -= self.text_layout.height
            self.update_scrollbar_indicator_position()
        elif action == ControlAction.FIRE or action == ControlAction.MELEE or action == ControlAction.TOGGLE_PAUSE or action == ControlAction.EXIT:
            if self.exit_callback is not None:
                self.exit_callback()

    def delete(self):
        self.scrollbar_indicator.delete()
        self.up_arrow.delete()
        self.down_arrow.delete()
        self.scrollbar.delete()
        self.text_layout.delete()
        self.ok_label.delete()
        self.ok_button_sprite.delete()
        self.title_background_sprite.delete()
        self.title_label.delete()
        self.background_sprite.delete()