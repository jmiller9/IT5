import pyglet
from it5.controls import ControlAction
from it5.common import batches, batchGroups
from it5.gui.widget import Widget

class ImagePopout(Widget):
    def __init__(self, image, x=0, y=0, width=128, height=128,
                 window_width=768, window_height=768,
                 exit_callback=None, anchor_x="left", anchor_y="bottom"):
        super(ImagePopout, self).__init__(x, y, width, height, anchor_x=anchor_x, anchor_y=anchor_y)
        self.exit_callback = exit_callback
        self.sprite = pyglet.sprite.Sprite(
                img=image,
                batch=batches["menu"],
                group=batchGroups["dialogForeground"]
            )
        self.sprite.scale_x = float(window_width) / float(width)
        self.sprite.scale_y = float(window_height) / float(height)
        self.opacity = 255
        self.opacity_drain_fps = 255.0 / 2.0

    def delete(self):
        self.sprite.delete()

    def update(self, dt):
        self.opacity = max(0, self.opacity - self.opacity_drain_fps * dt)
        self.sprite.opacity = int(self.opacity)
        return self.opacity > 0