import pyglet
from it5.controls import ControlAction, Controls
from pyglet.window import key
from it5.common import batches, batchGroups
from it5.gui.widget import Widget


class TextInput(Widget):
    def __init__(self, title, exit_callback, window, x=0, y=768, width=768, height=768,
                title_font_size=16, font_size=12,
                background_color=(64,0,0,255), button_color=(0,0,0,255),
                title_background_color=(32,0,0,255),
                font_color=(255,255,255,255), title_font_color=(255,255,255,255),
                exit_option=None, anchor_x="left", anchor_y="bottom"):
        super(TextInput, self).__init__(x, y, width, height, anchor_x=anchor_x, anchor_y=anchor_y)
        background_pattern = pyglet.image.SolidColorImagePattern(background_color)
        title_background_pattern = pyglet.image.SolidColorImagePattern(title_background_color)
        button_pattern = pyglet.image.SolidColorImagePattern(button_color)
        background_image = background_pattern.create_image(width, height)
        batch = batches["menu"]
        batch_groups = batchGroups
        ok_label_padding = 4
        text_layout_padding = 2
        self.exit_callback = exit_callback
        self.background_sprite = pyglet.sprite.Sprite(
            img=background_image,
            batch=batch,
            group=batch_groups['dialogBackground']
        )
        self.background_sprite.position = (self.x, self.y)
        self.title_label = pyglet.text.Label(title,
            font_size=title_font_size,
            x=self.x + width//2, y=self.y + height,
            anchor_x='center', anchor_y='top', batch=batch,
            bold=True, color=title_font_color,
            group=batch_groups['dialogForeground']
        )
        title_background_image = title_background_pattern.create_image(width, int(self.title_label.content_height * 1.5))
        self.title_background_sprite = pyglet.sprite.Sprite(
            img=title_background_image,
            batch=batch,
            group=batch_groups['dialogButtons']
        )
        self.title_background_sprite.position = (self.x, self.title_label.y - self.title_label.content_height)

        if exit_option is None:
            exit_option = "Press %s to Confrim, %s to Cancel" % (Controls.get_key_names_str(ControlAction.TOGGLE_PAUSE), Controls.get_key_names_str(ControlAction.EXIT))

        self.ok_label = pyglet.text.Label(exit_option,
            font_size=12,
            x=self.x + width//2, y=self.y + ok_label_padding,
            anchor_x='center', anchor_y='bottom',
            batch=batch,
            group=batch_groups['dialogForeground']
        )
        ok_button_padding = 16
        ok_button_image = button_pattern.create_image(self.ok_label.content_width + ok_button_padding, int(self.ok_label.content_height * 1.5))
        self.ok_button_sprite = pyglet.sprite.Sprite(
            img=ok_button_image,
            batch=batch,
            group=batch_groups['dialogButtons']
        )
        self.ok_button_sprite.position = (self.ok_label.x - (self.ok_label.content_width//2) - ok_button_padding/2, self.ok_label.y - ok_label_padding)

        tl_height = int(height - (self.title_label.content_height * 1.5) - (self.ok_label.content_height * 1.5) - text_layout_padding)

        self.document = pyglet.text.document.FormattedDocument()

        self.text_layout = pyglet.text.layout.IncrementalTextLayout(
            self.document, width - (16 + text_layout_padding + 1), tl_height, multiline=True, batch=batch, group=batch_groups['dialogForeground']
        )
        self.text_layout.x = int(self.x + text_layout_padding)
        self.text_layout.y = int(self.y + self.title_label.content_height)

        self.window = window
        self.caret = pyglet.text.caret.Caret(self.text_layout, batch=batch, color=(255,255,255))
        self.caret.set_style(dict(font_size=font_size, color=font_color))
        self.ready = False
    
    def get_text(self):
        return self.document.text.strip()

    def on_key_press(self, action, symbol, modifiers):
        if action == ControlAction.TOGGLE_PAUSE:
            if self.exit_callback is not None:
                self.exit_callback(self.get_text())
        elif action == ControlAction.EXIT:
            if self.exit_callback is not None:
                self.exit_callback(None)

    def on_key_release(self, action, symbol, modifiers):
        if not self.ready:
            # After the first key has been released, let the caret receive key input
            # This is to prevent unintended characters from showing up in the text layout
            self.window.push_handlers(self.caret)
            self.ready = True

    def delete(self):
        self.window.pop_handlers()
        self.caret.delete()
        self.text_layout.delete()
        self.ok_label.delete()
        self.ok_button_sprite.delete()
        self.title_background_sprite.delete()
        self.title_label.delete()
        self.background_sprite.delete()