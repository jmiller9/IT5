class Widget:
    def __init__(self, x, y, width, height, anchor_x="left", anchor_y="bottom"):
        self.x = x
        self.y = y
        anchor_x = anchor_x.strip().lower()
        anchor_y = anchor_y.strip().lower()
        if anchor_x == "center":
            self.x -= (width / 2)
        elif anchor_x == "right":
            self.x = x - width
        if anchor_y == "center":
            self.y -= (height / 2)
        elif anchor_y == "top":
            self.y = y - height

    def delete(self):
        # Release all resources here
        pass

    def on_key_press(self, action, symbol, modifiers):
        # Handle key press events
        pass

    def on_key_release(self, action, symbol, modifiers):
        # Handle key release events
        pass

    def update(self, dt):
        # Update method
        return True
