import pyglet
from it5.controls import ControlAction
from pyglet.window import key
from it5.common import batches, batchGroups
from it5.gui.widget import Widget
from it5.controls import Controls, ControlAction


class KeyMapper(Widget):
    def __init__(self, control_action_name, x=0, y=768, width=768, height=768,
                title_font_size=16, font_size=12,
                background_color=(64,0,0,255), button_color=(0,0,0,255),
                title_background_color=(32,0,0,255),
                font_color=(255,255,255,255), title_font_color=(255,255,255,255),
                exit_callback=None, anchor_x="left", anchor_y="bottom"):
        super(KeyMapper, self).__init__(x, y, width, height, anchor_x=anchor_x, anchor_y=anchor_y)
        self.control_action = ControlAction[control_action_name]
        background_pattern = pyglet.image.SolidColorImagePattern(background_color)
        title_background_pattern = pyglet.image.SolidColorImagePattern(title_background_color)
        button_pattern = pyglet.image.SolidColorImagePattern(button_color)
        background_image = background_pattern.create_image(width, height)
        batch = batches["menu"]
        batch_groups = batchGroups
        ok_label_padding = 4
        text_layout_padding = 2
        self.exit_callback = exit_callback
        self.background_sprite = pyglet.sprite.Sprite(
            img=background_image,
            batch=batch,
            group=batch_groups['dialogBackground']
        )
        self.background_sprite.position = (self.x, self.y)
        self.title_label = pyglet.text.Label(control_action_name,
            font_size=title_font_size,
            x=self.x + width//2, y=self.y + height,
            anchor_x='center', anchor_y='top', batch=batch,
            bold=True, color=title_font_color,
            group=batch_groups['dialogForeground']
        )
        title_background_image = title_background_pattern.create_image(width, int(self.title_label.content_height * 1.5))
        self.title_background_sprite = pyglet.sprite.Sprite(
            img=title_background_image,
            batch=batch,
            group=batch_groups['dialogButtons']
        )
        self.title_background_sprite.position = (self.x, self.title_label.y - self.title_label.content_height)

        bottom_message = "Waiting for new key. Press Escape to cancel or Backspace to clear."
        self.ok_label = pyglet.text.Label(bottom_message,
            font_size=12,
            x=self.x + width//2, y=self.y + ok_label_padding,
            anchor_x='center', anchor_y='bottom',
            batch=batch,
            group=batch_groups['dialogForeground']
        )
        ok_button_padding = 16
        ok_button_image = button_pattern.create_image(self.ok_label.content_width + ok_button_padding, int(self.ok_label.content_height * 1.5))
        self.ok_button_sprite = pyglet.sprite.Sprite(
            img=ok_button_image,
            batch=batch,
            group=batch_groups['dialogButtons']
        )
        self.ok_button_sprite.position = (self.ok_label.x - (self.ok_label.content_width//2) - ok_button_padding/2, self.ok_label.y - ok_label_padding)

        tl_height = int(height - (self.title_label.content_height * 1.5) - (self.ok_label.content_height * 1.5) - text_layout_padding)

        text = Controls.get_key_names_str(self.control_action)
        self.document = pyglet.text.document.FormattedDocument(text)
        self.text_layout = pyglet.text.layout.TextLayout(
            self.document, width - (16 + text_layout_padding + 1), tl_height, multiline=True, batch=batch, group=batch_groups['dialogForeground']
        )
        self.text_layout.x = int(self.x + text_layout_padding)
        self.text_layout.y = int(self.y + self.title_label.content_height)
        self.document.set_style(0, len(text), dict(font_size=font_size, color=font_color, background_color=background_color))

    def refresh_document(self):
        text = Controls.get_key_names_str(self.control_action)
        self.document.delete_text(0, len(self.document.text))
        self.document.insert_text(0, text)

    def clear_binding(self):
        Controls.unbind_action(self.control_action)
        self.refresh_document()

    def bind_key(self, symbol):
        Controls.bind_key(symbol, self.control_action)
        self.refresh_document()

    def on_key_press(self, action, symbol, modifiers):
        if symbol == key.ESCAPE or symbol == key.ENTER:
            if self.exit_callback is not None:
                self.exit_callback()
        elif symbol == key.BACKSPACE:
            self.clear_binding()
        else:
            self.bind_key(symbol)

    def delete(self):
        self.text_layout.delete()
        self.ok_label.delete()
        self.ok_button_sprite.delete()
        self.title_background_sprite.delete()
        self.title_label.delete()
        self.background_sprite.delete()