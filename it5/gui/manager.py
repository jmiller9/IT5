from it5.gui.menu import Menu
from it5.gui.popup import Popup
from it5.gui.text_input import TextInput
from it5.gui.image_popout import ImagePopout
from it5.gui.key_mapper import KeyMapper


class UIManager:
    widgets = []
    # Only one ephemeral widget can exist at a time
    ephemeral_widget = None

    @classmethod
    def show_menu(cls, title, options, x=0, y=0, width=768, height=768,
                    title_font_size=16, option_font_size=12,
                    background_color=(64,0,0,255), button_color=(0,0,0,255),
                    title_background_color=(32,0,0,255),
                    option_font_color=(255,255,255,255), title_font_color=(255,255,255,255),
                    exit_option="Cancel", exit_callback=None,
                    anchor_x="left", anchor_y="bottom"):
        menu = Menu(title, options, x=x, y=y, width=width, height=height,
                        title_font_size=title_font_size, option_font_size=option_font_size,
                        background_color=background_color, button_color=button_color,
                        title_background_color=title_background_color,
                        option_font_color=option_font_color, title_font_color=title_font_color,
                        exit_option=exit_option, exit_callback=exit_callback or cls.hide_active,
                        anchor_x=anchor_x, anchor_y=anchor_y)
        cls.widgets.append(menu)

    @classmethod
    def show_foreground_menu(cls, title, options, x=0, y=0, width=768, height=768,
                    title_font_size=16, option_font_size=12,
                    background_color=(64,0,0,255), button_color=(0,0,0,255),
                    title_background_color=(32,0,0,255),
                    option_font_color=(255,255,255,255), title_font_color=(255,255,255,255),
                    exit_option="Cancel", exit_callback=None,
                    anchor_x="left", anchor_y="bottom"):
        menu = Menu(title, options, x=x, y=y, width=width, height=height,
                        title_font_size=title_font_size, option_font_size=option_font_size,
                        background_color=background_color, button_color=button_color,
                        title_background_color=title_background_color,
                        option_font_color=option_font_color, title_font_color=title_font_color,
                        exit_option=exit_option, exit_callback=exit_callback or cls.hide_active,
                        anchor_x=anchor_x, anchor_y=anchor_y,
                        background_batch_group_name="dialogBackground",
                        foreground_batch_group_name="dialogForeground",
                        buttons_batch_group_name="dialogButtons")
        cls.widgets.append(menu)

    @classmethod
    def show_popup(cls, title, text, x=0, y=0, width=768, height=768,
                title_font_size=16, font_size=12,
                background_color=(64,0,0,255), button_color=(0,0,0,255),
                title_background_color=(32,0,0,255),
                font_color=(255,255,255,255), title_font_color=(255,255,255,255),
                exit_option="OK", anchor_x="left", anchor_y="bottom"):
        popup = Popup(title, text, x=x, y=y, width=width, height=height,
                title_font_size=title_font_size, font_size=font_size,
                background_color=background_color, button_color=button_color,
                title_background_color=title_background_color,
                font_color=font_color, title_font_color=title_font_color,
                exit_option=exit_option, exit_callback=cls.hide_active,
                anchor_x=anchor_x, anchor_y=anchor_y)
        cls.widgets.append(popup)

    @classmethod
    def show_image_popout(cls, image, x=0, y=0, width=128, height=128,
                 window_width=768, window_height=768,
                 exit_callback=None, anchor_x="left", anchor_y="bottom"):
        popup = ImagePopout(image, x=x, y=y, width=width, height=height,
                window_width=window_width, window_height=window_height,
                exit_callback=cls.hide_active,
                anchor_x=anchor_x, anchor_y=anchor_y)
        if cls.ephemeral_widget is not None:
            cls.ephemeral_widget.delete()
        cls.ephemeral_widget = popup

    @classmethod
    def show_text_input(cls, title, exit_callback, window,
                x=0, y=768, width=768, height=768,
                title_font_size=16, font_size=12,
                background_color=(64,0,0,255), button_color=(0,0,0,255),
                title_background_color=(32,0,0,255),
                font_color=(255,255,255,255), title_font_color=(255,255,255,255),
                exit_option=None, anchor_x="left", anchor_y="bottom"):
        def modified_exit_callback(the_text):
            cls.hide_active()
            return exit_callback(the_text)
        text_input = TextInput(title, modified_exit_callback, window,
                x=x, y=y, width=width, height=height,
                title_font_size=title_font_size, font_size=font_size,
                background_color=background_color, button_color=button_color,
                title_background_color=title_background_color,
                font_color=font_color, title_font_color=title_font_color,
                exit_option=exit_option, anchor_x=anchor_x, anchor_y=anchor_y)
        cls.widgets.append(text_input)

    @classmethod
    def show_key_mapper(cls, control_action_name, x=0, y=768, width=768, height=768,
                title_font_size=16, font_size=12,
                background_color=(64,0,0,255), button_color=(0,0,0,255),
                title_background_color=(32,0,0,255),
                font_color=(255,255,255,255), title_font_color=(255,255,255,255),
                anchor_x="left", anchor_y="bottom"):
        key_mapper = KeyMapper(control_action_name, x=x, y=y, width=width, height=height,
                title_font_size=title_font_size, font_size=font_size,
                background_color=background_color, button_color=button_color,
                title_background_color=title_background_color,
                font_color=font_color, title_font_color=title_font_color,
                exit_callback=cls.hide_active, anchor_x=anchor_x, anchor_y=anchor_y)
        cls.widgets.append(key_mapper)

    @classmethod
    def hide(cls, *args):
        count = len(cls.widgets)
        for i in range(0, count):
            cls.hide_active()

    @classmethod
    def hide_active(cls, *args):
        if len(cls.widgets) > 0:
            widget = cls.widgets.pop()
            widget.delete()

    @classmethod
    def is_open(cls):
        return len(cls.widgets) != 0

    @classmethod
    def attach(cls, widget):
        cls.widgets.append(widget)

    @classmethod
    def on_key_press(cls, action, symbol, modifiers):
        if len(cls.widgets) != 0:
            widget = cls.widgets[-1]
            widget.on_key_press(action, symbol, modifiers)
        return cls.is_open()

    @classmethod
    def on_key_release(cls, action, symbol, modifiers):
        if len(cls.widgets) != 0:
            widget = cls.widgets[-1]
            widget.on_key_release(action, symbol, modifiers)

    @classmethod
    def update(cls, dt):
        if cls.ephemeral_widget is not None:
            ew_active = cls.ephemeral_widget.update(dt)
            if not ew_active:
                cls.ephemeral_widget.delete()
                cls.ephemeral_widget = None
