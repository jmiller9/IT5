import pyglet
from it5 import spriteloader
from it5.staticobject import StaticObject
from it5.controls import Controls, ControlAction

common_sprites = spriteloader.getSprites('common.png', 8, 8)
hud_spritemap = {
    'bar': (5, 7),
    'up': (4, 6),
    'down': (3, 6),
    'left': (4, 7),
    'right': (3, 7)
}

class HUD(object):
    def __init__(self, window, batch, anchor_x):
        self.anchor_x = anchor_x
        x_coord = anchor_x + 32
        self.batch = batch
        self.window = window
        self.controls_str = "Controls:\n\n" + Controls.get_controls_str()
        self.background = pyglet.graphics.OrderedGroup(0)
        self.foreground = pyglet.graphics.OrderedGroup(1)
        self.healthLabel = pyglet.text.Label('Health:',
                          font_size=12,
                          x=x_coord, y=window.height-282,
                          batch=batch)
        self.healthBarBackground = StaticObject((window.width / 32) - 7, (window.height / 32) - 10, common_sprites[hud_spritemap['bar']], batch, self.background)
        self.healthBarBackground.show()
        self.healthBarBackground.setPosition(x_coord, self.healthBarBackground.y)
        self.healthBarBackground.sprite.color = (24,24,24)
        self.setMaxPlayerHealth(100)
        self.healthBar = StaticObject((window.width / 32) - 7, (window.height / 32) - 10, common_sprites[hud_spritemap['bar']], batch, self.foreground)
        self.healthBar.show()
        self.healthBar.setPosition(x_coord, self.healthBar.y)
        self.healthBar.sprite.color = (0,255,127)
        self.lastPlayerHealth = 0

        self.o2Label = pyglet.text.Label('Oxygen:',
                          font_size=12,
                          x=x_coord, y=window.height-345,
                          batch=batch)
        self.o2BarBackground = StaticObject((window.width / 32) - 7, (window.height / 32) - 12, common_sprites[hud_spritemap['bar']], batch, self.background)
        self.o2BarBackground.show()
        self.o2BarBackground.setPosition(x_coord, self.o2BarBackground.y)
        self.o2BarBackground.sprite.color = (24,24,24)
        self.setMaxPlayerOxygen(100)
        self.o2Bar = StaticObject((window.width / 32) - 7, (window.height / 32) - 12, common_sprites[hud_spritemap['bar']], batch, self.foreground)
        self.o2Bar.show()
        self.o2Bar.setPosition(x_coord, self.o2Bar.y)
        self.o2Bar.sprite.color = (0,127,255)
        self.lastPlayerOxygen = 0

        self.playerWeapon = 'None'
        self.playerMaxAmmoStr = None
        self.setPlayerHealth(100)
        self.setPlayerOxygen(100)

        self.weaponLabel = pyglet.text.Label('Weapon:',
                          font_size=12,
                          x=x_coord, y=window.height-416,
                          batch=batch)
        self.weaponDescription = pyglet.text.Label('None',
                          font_size=12,
                          x=x_coord, y=window.height-436,
                          batch=batch)
        self.itemLabel = pyglet.text.Label('Item:',
                          font_size=12,
                          x=x_coord, y=window.height-544,
                          batch=batch)
        self.itemDescription = pyglet.text.Label('None',
                          font_size=12,
                          x=x_coord, y=window.height-564,
                          batch=batch)

        self.messageLabel = pyglet.text.Label('',
                          font_size=12,
                          x=x_coord, y=128,
                          batch=batch, width=224, multiline=True)

        self.levelDescription = pyglet.text.Label('',
                          font_size=10,
                          x=x_coord, y=16,
                          batch=batch)

        dead_msg = "You have died. Press %s to respawn." % Controls.get_key_names_str(ControlAction.TOGGLE_PAUSE)
        self.paused_document = pyglet.text.decode_text('PAUSED')
        self.paused_document.set_style(0, len('PAUSED'), dict(font_size=36, background_color=(0,0,0,127), color=(255,255,255,255)))
        self.dead_document = pyglet.text.decode_text(dead_msg)
        self.dead_document.set_style(0, len(dead_msg), dict(font_size=24, background_color=(0,0,0,127), color=(255,255,255,255)))
        self.paused_layout = None
        self.controls_document = pyglet.text.decode_text(self.controls_str)
        self.controls_document.set_style(0, len(self.controls_str), dict(font_size=10, background_color=(0,0,0,127), color=(255,255,255,255)))
        self.controls_layout = None
        self.left = StaticObject((window.width / 32) - 8, (window.height / 32) - 14, common_sprites[hud_spritemap['left']], self.batch)
        self.right = StaticObject((window.width / 32) - 1, (window.height / 32) - 14, common_sprites[hud_spritemap['right']], self.batch)
        self.up = StaticObject((window.width / 32) - 5, (window.height / 32) - 16, common_sprites[hud_spritemap['up']], self.batch)
        self.down = StaticObject((window.width / 32) - 5, (window.height / 32) - 19, common_sprites[hud_spritemap['down']], self.batch)
        self.left.show()
        self.right.show()
        self.up.show()
        self.down.show()
        self.left.setPosition(anchor_x, self.left.y)
        self.right.setPosition(self.window.width - 32, self.right.y)
        self.up.setPosition(anchor_x + 32, self.up.y)
        self.down.setPosition(anchor_x + 32, self.down.y)
        self.left.hide()
        self.right.hide()
        self.up.hide()
        self.down.hide()

        pmx = (anchor_x + (window.width - 256)) / 2
        self.playerMarker = StaticObject((window.width / 32) - 4, (window.height / 32) - 4, common_sprites[hud_spritemap['bar']], self.batch)
        self.playerMarker.show()
        self.playerMarker.setPosition(pmx + 128, self.playerMarker.y)
        self.playerMarker.sprite.scale_x = 0.125
        self.playerMarker.sprite.scale_y = 0.125
        self.notification_time_elapsed = 0
        self.notification_time = 3

    def setLevel(self, levelDesc):
        self.levelDescription.text = levelDesc

    def setMaxPlayerHealth(self, maxHealth):
        self.healthBarBackground.sprite.scale_x = maxHealth / float(25)

    def setPlayerHealth(self, health):
        if health != self.lastPlayerHealth:
            practical_health = health
            if health < 0:
                practical_health = 0
            self.healthBar.sprite.scale_x = practical_health / float(25)
            self.lastPlayerHealth = health

    def setMaxPlayerOxygen(self, max_o2):
        self.o2BarBackground.sprite.scale_x = max_o2 / float(25)

    def setPlayerOxygen(self, o2):
        if o2 != self.lastPlayerOxygen:
            practical_o2 = o2
            if o2 < 0:
                practical_o2 = 0
            self.o2Bar.sprite.scale_x = practical_o2 / float(25)
            self.lastPlayerOxygen = o2

    def setItem(self, item, itemVal, title=True):
        itemStr = item.title()
        if not title:
            itemStr = item
        if itemVal != None:
            itemStr += ' ' + str(itemVal)
        self.itemDescription.text = itemStr
    
    def setWeapon(self, weapon, ammo, maxAmmo):
        weaponStr = weapon
        self.playerWeapon = weapon
        if ammo != None and maxAmmo != None:
            self.playerMaxAmmoStr = '/' + str(maxAmmo)
            weaponStr += ' ' + str(ammo) + self.playerMaxAmmoStr
        self.weaponDescription.text = weaponStr

    def updateAmmoDisplay(self, ammo):
        if self.playerMaxAmmoStr != None:
            self.weaponDescription.text = self.playerWeapon + ' ' + str(ammo) + self.playerMaxAmmoStr

    def updateNotificationMessage(self, notification_text, elapsed=1):
        self.messageLabel.text = notification_text
        self.notification_time_elapsed = elapsed

    def pause(self):
        self.paused_layout = pyglet.text.layout.TextLayout(self.paused_document, self.anchor_x//2, self.window.height//4, batch=self.batch)
        self.paused_layout.anchor_x = 'center'
        self.paused_layout.anchor_y = 'top'
        self.paused_layout.x = self.anchor_x//2
        self.paused_layout.y = self.window.height//2
        self.controls_layout = pyglet.text.layout.TextLayout(self.controls_document, self.anchor_x//2, self.window.height//2, multiline=True, batch=self.batch)
        self.controls_layout.anchor_x = 'left'
        self.controls_layout.anchor_y = 'top'
        self.controls_layout.x = 0
        self.controls_layout.y = self.window.height
        self.left.show()
        self.right.show()
        self.up.show()
        self.down.show()
        self.left.sprite.scale_x = 0.5
        self.right.sprite.scale_x = 0.5
        self.up.sprite.scale_y = 0.5
        self.down.sprite.scale_y = 0.5
        if self.itemDescription.text.startswith('Medkit') or self.itemDescription.text.startswith('Booster'):
            self.updateNotificationMessage("Press %s to Consume Item" % Controls.get_key_names_str(ControlAction.FIRE))
    
    def gameOver(self):
        self.paused_layout = pyglet.text.layout.TextLayout(self.dead_document, self.anchor_x//2, self.window.height//4, batch=self.batch)
        self.paused_layout.anchor_x = 'center'
        self.paused_layout.anchor_y = 'top'
        self.paused_layout.x = self.anchor_x//2
        self.paused_layout.y = self.window.height//2
    
    def resume(self):
        if self.paused_layout != None:
            self.paused_layout.delete()
            self.paused_layout = None
        if self.controls_layout != None:
            self.controls_layout.delete()
            self.controls_layout = None
        if self.left != None:
            self.left.hide()
        if self.right != None:
            self.right.hide()
        if self.up != None:
            self.up.hide()
        if self.down != None:
            self.down.hide()
        self.updateNotificationMessage('', 0)

    def update(self, dt):
        if self.notification_time_elapsed > 0:
            self.notification_time_elapsed += dt
            if self.notification_time_elapsed > self.notification_time:
                # Reset the notification message
                self.updateNotificationMessage('', 0)

    def resize(self, anchor_x):
        self.anchor_x = anchor_x
        x_coord = anchor_x + 32
        horiz_y = self.window.height - (14 * 32)
        up_y = self.window.height - (16 * 32)
        down_y = self.window.height - (19 * 32)
        self.left.show()
        self.right.show()
        self.up.show()
        self.down.show()
        self.left.setPosition(anchor_x, horiz_y)
        self.right.setPosition(self.window.width - 32, horiz_y)
        self.up.setPosition(anchor_x + 32, up_y)
        self.down.setPosition(anchor_x + 32, down_y)
        self.left.hide()
        self.right.hide()
        self.up.hide()
        self.down.hide()

        self.healthBarBackground.setPosition(x_coord, self.window.height - 320)
        self.healthBar.setPosition(x_coord, self.window.height - 320)
        self.o2BarBackground.setPosition(x_coord, self.window.height - 384)
        self.o2Bar.setPosition(x_coord, self.window.height - 384)

        self.healthLabel.delete()
        self.o2Label.delete()
        self.weaponLabel.delete()
        self.weaponDescription.delete()
        self.itemLabel.delete()
        self.itemDescription.delete()
        self.messageLabel.delete()
        self.levelDescription.delete()

        self.healthLabel = pyglet.text.Label('Health:',
                          font_size=12,
                          x=x_coord, y=self.window.height-282,
                          batch=self.batch)

        self.o2Label = pyglet.text.Label('Oxygen:',
                          font_size=12,
                          x=x_coord, y=self.window.height-345,
                          batch=self.batch)

        self.weaponLabel = pyglet.text.Label('Weapon:',
                          font_size=12,
                          x=x_coord, y=self.window.height-416,
                          batch=self.batch)
        self.weaponDescription = pyglet.text.Label('None',
                          font_size=12,
                          x=x_coord, y=self.window.height-436,
                          batch=self.batch)
        self.itemLabel = pyglet.text.Label('Item:',
                          font_size=12,
                          x=x_coord, y=self.window.height-544,
                          batch=self.batch)
        self.itemDescription = pyglet.text.Label('None',
                          font_size=12,
                          x=x_coord, y=self.window.height-564,
                          batch=self.batch)

        self.messageLabel = pyglet.text.Label('',
                          font_size=12,
                          x=x_coord, y=128,
                          batch=self.batch, width=224, multiline=True)

        self.levelDescription = pyglet.text.Label('',
                          font_size=10,
                          x=x_coord, y=16,
                          batch=self.batch)

        pmx = (anchor_x + (self.window.width - 256)) / 2
        self.playerMarker.setPosition(pmx + 128, self.window.height - 128)