from it5.direction import Direction
from copy import copy

class WaypointBehavior(object):
    CONTINUE = 'CONTINUE'
    STOP = 'STOP'
    SLEEP = 'SLEEP'
    DIE = 'DIE'
    EXPLODE = 'EXPLODE'
    WAIT = 'WAIT'
    WAIT_LONGER = 'WAIT_LONGER'
    WAIT_LONGEST = 'WAIT_LONGEST'
    WARP = 'WARP'

class Waypoint(object):
    def __init__(self, x, y, direction, behavior):
        self.x = x*32
        self.y = y*32
        self.direction = direction
        self.behavior = behavior
    
    def getTileX(self):
        return int((self.x+15) / 32)
    
    def getTileY(self):
        return int((self.y-31) / 32)

    def copy(self):
        return Waypoint(self.x/32, self.y/32, self.direction, self.behavior)
    
    def __str__(self):
        return str((self.getTileX(), self.getTileY(), self.direction, self.behavior))

class Path(object):
    def __init__(self, waypoints, interpolate=True):
        self.waypoints = []
        self.index = 0
        if interpolate:
            self.interpolate(waypoints)
        else:
            for waypoint in waypoints:
                self.waypoints.append(waypoint.copy())

    def __interpolate(self, waypoint, target_waypoint):
        actual_direction = waypoint.direction
        dx = 0
        dy = 0
        if target_waypoint.x - waypoint.x > 0:
            dx = 1
            actual_direction = Direction.RIGHT
        elif target_waypoint.x - waypoint.x < 0:
            dx = -1
            actual_direction = Direction.LEFT
        if target_waypoint.y - waypoint.y > 0:
            dy = 1
            actual_direction = Direction.UP
        elif target_waypoint.y - waypoint.y < 0:
            dy = -1
            actual_direction = Direction.DOWN
        if dx !=0 or dy != 0:
            if abs(dx) ^ abs(dy) == 0:
                raise RuntimeError("Cannot interpolate between waypoints %s and %s" % (str(waypoint), str(target_waypoint)))
        if waypoint.direction != actual_direction:
            # Looks like I messed up the direction in Tiled...
            self.waypoints[-1].direction = actual_direction
        manhattan_distance = abs(waypoint.getTileX() - target_waypoint.getTileX()) + abs(waypoint.getTileY() - target_waypoint.getTileY())
        tx = waypoint.getTileX()
        ty = waypoint.getTileY() + 1
        for j in range(0, max(0, manhattan_distance - 1)):
            tx += dx
            ty += dy
            _waypoint = Waypoint(tx, ty, actual_direction, WaypointBehavior.CONTINUE)
            #print("Interpolating waypoint " + str(waypoint) + " -> " + str(_waypoint) + " j=" + str(j))
            self.waypoints.append(_waypoint.copy())

    def interpolate(self, waypoints):
        num_waypoints = len(waypoints)
        for i in range(0, num_waypoints):
            waypoint = waypoints[i]
            self.waypoints.append(waypoint.copy())
            if i + 1 < num_waypoints:
                self.__interpolate(waypoint, waypoints[i+1])
            else:
                if (waypoint.behavior == WaypointBehavior.CONTINUE or
                    waypoint.behavior == WaypointBehavior.WAIT or
                    waypoint.behavior == WaypointBehavior.WAIT_LONGER or
                    waypoint.behavior == WaypointBehavior.WAIT_LONGEST):
                    self.__interpolate(waypoint, waypoints[0])

    def getNextWaypoint(self):
        if len(self.waypoints) == 0:
            return None
        waypoint = self.waypoints[self.index]
        self.index+=1
        if self.index >= len(self.waypoints):
            self.index = 0
        return waypoint
    
    def getStartingWaypoint(self):
        self.index = 0
        return self.getNextWaypoint()
    
    def copy(self, reverse=False):
        waypoints = copy(self.waypoints)
        if reverse:
            i = len(waypoints) - 1
            _waypoints = []
            while i >= 0:
                waypoint = waypoints[i].copy()
                if waypoint.direction == Direction.UP:
                    waypoint.direction = Direction.DOWN
                elif waypoint.direction == Direction.DOWN:
                    waypoint.direction = Direction.UP
                elif waypoint.direction == Direction.LEFT:
                    waypoint.direction = Direction.RIGHT
                elif waypoint.direction == Direction.RIGHT:
                    waypoint.direction = Direction.LEFT
                _waypoints.append(waypoint)
                i -= 1
            waypoints = _waypoints
        return Path(waypoints, interpolate=False)

    def __str__(self):
        path_str = "[Path]\n"
        for waypoint in self.waypoints:
            path_str += str(waypoint) + "\n"
        return path_str

def pathCreator(nodes):
    waypoints = []
    last_direction = None
    last_index = len(nodes) - 1
    for i in range(0, len(nodes)):
        x, y = nodes[i]
        direction = Direction.DOWN
        if i+1 < len(nodes):
            x2, y2 = nodes[i+1]
            if x2 - x > 0:
                direction = Direction.RIGHT
            elif x2 - x < 0:
                direction = Direction.LEFT
            elif y2 - y > 0:
                direction = Direction.UP
            elif y2 - y < 0:
                direction = Direction.DOWN
        waypoint = Waypoint(x, y+1, direction, WaypointBehavior.CONTINUE)
        waypoints.append(waypoint)
    if len(waypoints) > 0:
        waypoints[-1].behavior = WaypointBehavior.STOP
    if len(waypoints) > 1:
        waypoints[-1].direction = waypoints[-2].direction
    """
    for waypoint in waypoints:
        print(str(waypoint))
    """
    return Path(waypoints, interpolate=False)