import pyglet
import pyglet_ffmpeg
from concurrent.futures import ThreadPoolExecutor

pyglet_ffmpeg.load_ffmpeg()

class SoundManager:
    current_soundtrack = None
    player = None
    executor = ThreadPoolExecutor(max_workers=1)
    music = {
        "title": "opening_theme.ogg",
        "alert": "tactical_pursuit.ogg",
        "sneaking": "sneaking.ogg",
        "dark": "dark_ambience.ogg",
        "creepy": "cant_stop_waiting.ogg",
        "lab": "continuum_alternative.ogg",
        "danger": "alert_outsider.ogg",
        "infiltration": "enemy_infiltration.ogg",
        "evasion": "evasion.ogg",
        "tension": "perpetual_tension.ogg",
        "zombies": "zombies_are_coming.ogg",
        "town": "delusion.ogg",
        "elevator": "elevator.ogg",
        "rain": "rain.ogg",
        "i_am_your_product": "i_am_your_product.ogg",
        "mystical_theme": "mystical_theme.ogg",
    }
    sfx = {
        "pistol_gunshot": "pistol_gunshot.wav",
        "alt_pistol_gunshot": "alt_pistol_gunshot.wav",
        "rifle_gunshot": "rifle_gunshot.wav",
        "sniper_rifle_gunshot":"sniper_rifle_gunshot.wav",
        "electric_current":"electriccurrent.wav",
        "warp": "laser1.wav",
        "silenced_gunshot": "silenced_gunshot.wav",
        "empty_gunshot": "empty_gunshot.wav",
        "punch": "punch.wav",
        "explosion": "explosion.wav",
        "alarm": "ambient_alarm1.wav",
        "horror_scream": "scream_horror1.wav",
        "slash": "slash.wav",
        "monster_growl": "monster_growl.wav",
        "door": "door_sound.wav",
    }

    @classmethod
    def initialize(cls):
        cls.player = pyglet.media.Player()

    @classmethod    
    def _play_sfx(cls, effect):
        if effect in cls.sfx:
            sound = pyglet.resource.media(cls.sfx[effect])
            sound.play()

    @classmethod
    def play_sfx(cls, effect):
        cls.executor.submit(cls._play_sfx, effect)

    @classmethod
    def _play_music(cls, track, loop):
        if track != cls.current_soundtrack:
            cls.stop_music()
            cls.current_soundtrack = track
            if track in cls.music:
                source = pyglet.resource.media(cls.music[track], streaming=True)
                cls.player.queue(source)
                cls.player.loop = loop

    @classmethod
    def play_music(cls, track, loop=True):
        cls.executor.submit(cls._play_music, track, loop)

    @classmethod
    def stop_music(cls):
        cls.player.pause()
        cls.player = pyglet.media.Player()
        cls.player.play()
        cls.current_soundtrack = None