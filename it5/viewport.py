#!/usr/bin/env python

from pyglet.gl import *
import pyglet
from it5.fbo import FrameBuffer

class FixedResolutionViewport(object):
    def __init__(self, window, width, height, filtered=False):
        self.window = window
        self.width = width
        self.height = height
        self.fbo = FrameBuffer(1024, 768, num_bufs=1)
        self.lights_fbo = FrameBuffer(1024, 768, num_bufs=1)
        #print("Window is resolution is %dx%d" % (window.width, window.height))
        self.vl = pyglet.graphics.vertex_list(
                4,
                ('v2f/static', (0, 0, 1, 0, 1, 1, 0, 1)),
                ('t2f/static', (0, 0, 1, 0, 1, 1, 0, 1))
            )

    def draw_quad(self):
        """Draw a full-screen quad."""
        glPushMatrix()
        glLoadIdentity()
        glScalef(self.window.width, self.window.height, 1.0)
        self.vl.draw(GL_QUADS)
        glPopMatrix()
    
    def begin(self):
        self.fbo.begin()
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        glPushAttrib(GL_ALL_ATTRIB_BITS)

    def end(self, lighting_manager):
        glPopAttrib()
        self.fbo.end()

        glEnable(GL_TEXTURE_2D)
        glActiveTexture(GL_TEXTURE0)
        glBindTexture(GL_TEXTURE_2D, self.fbo.textures[0])

        # Draw the lights to a separate framebuffer
        with self.lights_fbo:
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
            glPushAttrib(GL_ALL_ATTRIB_BITS)
            lighting_manager.draw_lights(self.window.width / self.width, self.window.height / self.height)
            glPopAttrib()

        # Render the lights using a full-screen quad
        #glEnable(GL_TEXTURE_2D)
        #glActiveTexture(GL_TEXTURE0)
        glBindTexture(GL_TEXTURE_2D, self.lights_fbo.textures[0])
        glEnable(GL_BLEND)
        glBlendFunc(GL_ONE, gl.GL_ONE)
        self.draw_quad()

        #glEnable(GL_TEXTURE_2D)
        #glActiveTexture(GL_TEXTURE0)
        glBindTexture(GL_TEXTURE_2D, self.fbo.textures[0])

        # Draw ambient using a full-screen quad
        glColor3f(*lighting_manager.ambience)
        self.draw_quad()
        glColor4f(1, 1, 1, 1)
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA) 
