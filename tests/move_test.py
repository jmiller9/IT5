from unittest.mock import Mock, MagicMock
from it5.level import LevelSet, Level
from it5.player import Player
from it5 import player as _player
from it5.item import Item
from it5.persistence import PersistenceManager
from it5.common import CommonManager

import sys
sys.modules['pyglet'] = __import__('tests')
sys.modules['pyglet.sprite'] = __import__('tests')
sys.modules['pyglet.text'] = __import__('tests')
from it5 import fbo
fbo.FrameBuffer = MagicMock()
from it5 import game

def print_coords(player):
    print("x=" + str(player.x) + " y=" + str(player.y))

def test_move(monkeypatch):
    monkeypatch.setattr(_player, "EventManager", Mock())
    player = Player(1, 2, MagicMock(), MagicMock(), MagicMock())

    matrix = [
        [0,0,0,0,0],
        [0,1,1,1,0],
        [0,1,0,1,0],
        [0,1,1,1,0],
        [0,0,0,0,0]
    ]
    print("Obstacle Matrix:")
    for row in matrix:
        print(str(row))
    player.jump(1, 2)

    dt = 0.6
    
    print_coords(player)
    player.move(120, 0, dt, matrix, [])
    print_coords(player)
    assert(player.getTileX() == 1)

    last_x = player.getTileX()
    last_y = player.getTileY()
    player.move(0, 120, dt, matrix, [])
    print_coords(player)
    assert(player.getTileX() == 1)
    assert(player.getTileY() - last_y == 1)

    last_x = player.getTileX()
    last_y = player.getTileY()
    player.move(0, 120, dt, matrix, [])
    print_coords(player)
    assert(player.getTileX() == 1)
    assert(player.getTileY() - last_y == 1)
    
    last_x = player.getTileX()
    last_y = player.getTileY()
    player.move(120, 0, dt, matrix, [])
    print_coords(player)
    assert(player.getTileX() - last_x == 1)
    assert(player.getTileY() == last_y)