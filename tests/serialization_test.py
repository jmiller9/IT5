from unittest.mock import Mock, MagicMock, patch
from it5.level import LevelSet, Level
from it5.player import Player
from it5 import player as _player
from it5.item import Item
from it5.persistence import PersistenceManager
from it5.common import CommonManager

import sys
sys.modules['pyglet'] = __import__('tests')
sys.modules['pyglet.sprite'] = __import__('tests')
sys.modules['pyglet.text'] = __import__('tests')
sys.modules['pyglet.resource'] = __import__('tests')
sys.modules['pyglet.media'] = __import__('tests')
from it5 import game

import os

def mock_get_file_obj(filename):
    for dirpath, dirnames, files in os.walk("it5"):
        for name in files:
            if name == filename:
                return open(os.path.join(dirpath, name), "r")

@patch("it5.audio.SoundManager.play_music", MagicMock())
@patch("it5.resources.ResourceManager._get_file_obj", mock_get_file_obj)
def test_serialize_map(monkeypatch):
    monkeypatch.setattr(_player, "EventManager", Mock())
    monkeypatch.setattr(game, "lighting", Mock())

    currLevelSet = LevelSet("test_levelset.json", MagicMock(), MagicMock(), MagicMock(), MagicMock())
    currLevel = currLevelSet.loadLevel(0)
    currLevelMap = currLevel.getCurrentMap()
    player = Player(9, 15, MagicMock(), MagicMock(), MagicMock())
    pistol = Item(0, 0, None, None, None, 'weapon', {'rank':0, 'type':'pistol'})
    player.inventory.addTo(pistol)
    ammo = Item(0, 0, None, None, None, 'ammo', {'count':16, 'type':'pistol'})
    player.inventory.addTo(ammo)
    medkit = Item(0, 0, None, None, None, 'medkit', {})
    player.inventory.addTo(medkit)
    player.changeWeapon(1)
    player.changeItem(1)
    cardkey = Item(0, 0, None, None, None, 'item', {'level':5, 'type':'key'})
    player.inventory.addTo(cardkey)
    player.saveStatus()

    CommonManager.set_player_name("Unknown Soldier")

    game.initialize('test_levelset.json')
    blob = PersistenceManager.to_json(currLevelSet, currLevel, currLevelMap, player, set())
    print(blob)
    PersistenceManager.from_json(blob, game.initialize, game.changeMap)